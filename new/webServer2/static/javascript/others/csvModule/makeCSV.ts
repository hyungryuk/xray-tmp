export var csvModule = {

    makeCSVRowsList : function(jsonInput:string){
        try{
            var inputData = JSON.parse(jsonInput);
        }catch(err){
            throw new Error("Json form incorrect");
        }

        var keyList = Object.keys(inputData[0]);

        var inputLength = inputData.length;

        var csvContent = [];

        var rowData = "";

        for(var i=0;i<keyList.length;i++){

            rowData += "'";
            rowData += keyList[i];
            rowData += "'";
            if(i!=keyList.length-1){
                rowData += ',';
            }

        }

        csvContent.push(rowData);



        for(var i=0;i<inputLength;i++){

            rowData = "";

            for(var j=0;j<keyList.length;j++){

                rowData += "'";
                rowData += inputData[i][keyList[j]];
                rowData += "'";
                if(j!=keyList.length-1){
                    rowData += ',';
                }

            }
            csvContent.push(rowData);
        }

        return csvContent;

    }

};
