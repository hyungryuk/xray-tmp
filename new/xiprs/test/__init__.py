# -*- coding: utf-8 -*-
"""
    Pytest  Package.

    :copyright: (c) 2018 by SKInovation.
"""

__version__ = '0.1.0'

# utilities we import from flask and flask_restful that are unused
# in the module but are exported as public interface.

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Api
from flask_cors import CORS