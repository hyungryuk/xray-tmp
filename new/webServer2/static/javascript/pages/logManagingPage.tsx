import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { HeaderComponent } from "../modulesForPages/header";
import {StaticLanguageInterface} from '../others/staticLanguageData/staticLanguageData'
import {myContainer} from '../DIContainerConf/inversify.config'
import {TYPES} from '../DIContainerConf/types'

import { LogDataAjaxCall } from '../others/restCall/dbRestcall'
import {encoder} from "../others/encoder/encoder";
import {properties} from '../properties/properties'

var selectedList:string[] = [];
ReactDOM.render(
  <HeaderComponent />,
  document.getElementById("header")
);
Date.prototype.yyyymmddhhmmss = function() {
	var mm = this.getMonth() + 1;
	var dd = this.getDate();
	var hh = this.getHours();
	var minutes = this.getMinutes();
	var ss = this.getSeconds();

	return [
		this.getFullYear(),
		(mm > 9 ? "" : "0") + mm,
		(dd > 9 ? "" : "0") + dd,
		(hh > 9 ? "" : "0") + hh,
		(minutes > 9 ? "" : "0") + minutes,
		(ss > 9 ? "" : "0") + ss
	].join("");
};
var csvModule = {
	onclickCsvButton: function(kind:string,title:string,startdate:string, enddate:string) {
		var csvData = csvModule.makeCsvData(title, startdate, enddate);
		csvModule.downloadCsvFile(csvData, startdate, enddate);
	},
	downloadCsvFile: function(data:any[][], startdate:string, enddate:string) {
		var encodedStartdate = encoder.dateToYYMMDDHHMMSS(startdate);
		var encodedEnddate = encoder.dateToYYMMDDHHMMSS(enddate);

		let csvContent = "data:text/csv;charset=euc-kr,";
		data.forEach(function(rowArray) {
			let row = rowArray.join(",");
			csvContent += row + "\r\n";
		});

		var encodedUri = encodeURI(csvContent);
		var link = document.createElement("a");
		link.setAttribute("href", encodedUri);
		link.setAttribute(
			"download",
			encodedStartdate + "_" + encodedEnddate + ".csv"
		);
		document.body.appendChild(link); // Required for FF

		link.click();
	},
	makeCsvData: function(title:string, startdate_orin:string, enddate_orin:string) {
		var startdate = new Date(startdate_orin + " 00:00:00");
		var enddate = new Date(enddate_orin + " 23:59:59");

		var option1:{[key:string]:string} = {};
		if (title != "") {
			option1["title"] = title;
		}

		var option2:{[key:string]:string} = {};
		option2["startdate"] = startdate.yyyymmddhhmmss();
		option2["enddate"] = enddate.yyyymmddhhmmss();

        let logDataAjaxCallObj = new LogDataAjaxCall();
		var logDataAjaxResult;
		if(startdate_orin==""||enddate_orin==""){
			logDataAjaxResult  = logDataAjaxCallObj.retrieve([],[option1]);
		}else{
			logDataAjaxResult  = logDataAjaxCallObj.retrieve([],[option1, option2]);
		}

		var csvData = [];
		var firstRow = [];
		firstRow.push(myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("logtable_kind_name"));
		firstRow.push(myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("logtable_title_name"));
		firstRow.push(myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("logtable_contents_name"));
		firstRow.push(myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_regdate_name"));
		csvData.push(firstRow);

		var logsDataList = logDataAjaxResult["items"];
		for (var i = 0; i < logsDataList.length; i++) {
			var row = [];
            if (logsDataList[i].kind==0)
            {
                row.push("error");
            }else if(logsDataList[i].kind==1){
                row.push("warn");
            }else if(logsDataList[i].kind==2){
                row.push("info");
            }else{
                row.push("");
            }
			row.push(logsDataList[i].title);
			row.push(logsDataList[i].contents);
			row.push(logsDataList[i].logdate);
			csvData.push(row);
		}
		return csvData;
	}
};

class CheckBoxInTable extends React.Component<{logdate:string},{}> {
    constructor(props:any){
        super(props);
        this.checkBoxOnChange = this.checkBoxOnChange.bind(this);
    }
    checkBoxOnChange(e:any){
        if(e.target.checked==true){
            selectedList.push(this.props.logdate);
        }else{
            var index = selectedList.indexOf(this.props.logdate);
            selectedList.splice(index,1);
        }
    }
    render(){
        return(
            <input type="checkbox" onChange={this.checkBoxOnChange} className="checkbox"/>
        );
    }

}
class KindInTable extends React.Component<{kind:string},{}> {

    render(){
        if(this.props.kind=="1"){
            return(
                <div>error</div>
            );
        }
        if(this.props.kind=="2"){
            return(
                <div>warn</div>
            );
        }
        if(this.props.kind=="3"){
            return(
                <div>info</div>
            );
        }
        return(
            <div>unknown</div>
        );
    }

}
class TableBody extends React.Component<{data:any[]},{}> {
    render() {
        const data = this.props.data.map(item => (
            <tr>
                <td><CheckBoxInTable logdate={item.logdate}/></td>
                <td><KindInTable kind={item.kind} /></td>
                <td>{item.title}</td>
                <td>{item.contents}</td>
                <td>{item.logdate}</td>
            </tr>
        ));
        return data;
    }
}
class TableHeaderAndBox extends React.Component<{data:any[]},{isSelectallClicked:boolean}> {
    constructor(props:any){
        super(props);
        this.state = {
            isSelectallClicked:false
        }
        this.selectAllOnClick = this.selectAllOnClick.bind(this);

    }
    selectAllOnClick(){

        var checkboxs = document.getElementsByClassName("checkbox");
        if(this.state.isSelectallClicked==false){
            for(var i=0;i<this.props.data.length;i++){
                selectedList.push(this.props.data[i].logdate);
            }
            for(var i=0;i<checkboxs.length;i++){
                checkboxs[i].checked=true;
            }
        }else{
            selectedList=[];
            for(var i=0;i<checkboxs.length;i++){
                checkboxs[i].checked=false;
            }
        }

        this.setState(prevState => ({
            isSelectallClicked: !prevState.isSelectallClicked
        }));

    }
    render() {
        return (
            <div className="table_box">
                <table className="table">
                    <thead className="thead">
                        <tr>
                            <th onClick={this.selectAllOnClick}>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("table_select_name")}
                            </th>
                            <th>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("logtable_kind_name")}
                            </th>
                            <th>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("logtable_title_name")}
                            </th>
                            <th>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("logtable_contents_name")}
                            </th>
                            <th>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("usertable_regdate_name")}
                            </th>
                        </tr>
                    </thead>
                    <tbody className="tbody">
                        <TableBody
                            data={this.props.data}
                        />
                    </tbody>
                </table>
            </div>
        );
    }
}


class SearchBoxAndTable extends React.Component<{},{logdate:string,kind:string,title:string,contents:string,startDate:string,endDate:string,resultData:any[],currentPage:number,maxPage:number,perpage:number}> {

    constructor(props:any) {
        super(props);
        this.state = {
            startDate: "",
            endDate: "",
            logdate: "",
            kind: "",
            title: "",
            contents: "",
            currentPage: 1,
            maxPage: -1,
            perpage: 10,
            resultData:[]
        };

        this.getDataUsingRestCallFunc = this.getDataUsingRestCallFunc.bind(this);

        this.startDateOnChange = this.startDateOnChange.bind(this);
        this.endDateOnChange = this.endDateOnChange.bind(this);
        this.kindOnChange = this.kindOnChange.bind(this);
        this.titleOnChange = this.titleOnChange.bind(this);

        this.searchOnclick = this.searchOnclick.bind(this);
        this.csvOnclick = this.csvOnclick.bind(this);

        this.firstPageOnclick = this.firstPageOnclick.bind(this);
        this.lastPageOnclick = this.lastPageOnclick.bind(this);
        this.prePageOnclick = this.prePageOnclick.bind(this);
        this.nextPageOnclick = this.nextPageOnclick.bind(this);
        this.currentPageOnKeyPress = this.currentPageOnKeyPress.bind(this);
    }
    getDataUsingRestCallFunc(page:number) {
        var logDataAjaxResult;
        var option1:{[key:string]:any} = {};
        var option2:{[key:string]:any} = {};

        if (this.state.kind != "") {
            option1["kind"] = this.state.kind;
        }
        if (this.state.title != "") {
            option1["title"] = this.state.title;
        }
        if (this.state.startDate != "" && this.state.endDate != "") {
            option2["startdate"] = encoder.dateToYYMMDDHHMMSS(
                this.state.startDate + "000000"
            );
            option2["enddate"] = encoder.dateToYYMMDDHHMMSS(
                this.state.endDate + "235959"
            );
        }
        var logDataAjaxCallObj = new LogDataAjaxCall();
        logDataAjaxCallObj.create([properties["Log_view_log"]],[]);
        logDataAjaxResult = logDataAjaxCallObj.retrieve(
            [],
            [option1, option2, {orderby:"logdate"}, { page: page, perpage: this.state.perpage }]
        );
        return logDataAjaxResult;
    }

    firstPageOnclick() {
        var logDataResult;
        if (this.state.currentPage != 1) {
            logDataResult = this.getDataUsingRestCallFunc(1);
            this.setState({
                resultData: logDataResult["items"],
                currentPage: 1
            });

            $("#current_page").val(1);
        }
    }

    lastPageOnclick() {
        var logDataResult;
        if (
            this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage
        ) {
            logDataResult = this.getDataUsingRestCallFunc(this.state.maxPage);
            this.setState({
                resultData: logDataResult["items"],
                currentPage: this.state.maxPage
            });

            $("#current_page").val(this.state.maxPage);
        }
    }
    prePageOnclick() {
        var logDataResult;
        if (this.state.maxPage != -1 && this.state.currentPage > 1) {
            logDataResult = this.getDataUsingRestCallFunc(
                this.state.currentPage - 1
            );
            this.setState({ resultData: logDataResult["items"] });
            $("#current_page").val(this.state.currentPage - 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage - 1
            }));
        }
    }
    nextPageOnclick() {
        var logDataResult;
        if (
            this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage
        ) {
            logDataResult = this.getDataUsingRestCallFunc(
                this.state.currentPage + 1
            );
            this.setState({ resultData: logDataResult["items"] });
            $("#current_page").val(this.state.currentPage + 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage + 1
            }));
        }
    }
    currentPageOnKeyPress(e:any) {
        if (e.which == 13) {
            var pageVal = $("#current_page").val() as string;
            var page = parseInt(pageVal);
            if (isNaN(page)) {
                alert(myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_is_not_number_alert"));
                $("#current_page").val(this.state.currentPage);
            } else {
                if (page > this.state.maxPage) {
                    alert(
                        myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_greater_than_max_alert")
                    );
                    $("#current_page").val(this.state.currentPage);
                } else {
                    if (
                        this.state.maxPage != -1 &&
                        this.state.currentPage != page
                    ) {
                        var logDataResult = this.getDataUsingRestCallFunc(
                            page
                        );
                        this.setState({
                            resultData: logDataResult["items"],
                            currentPage: page
                        });
                        $("#current_page").val(page);
                    }
                }
            }
        }
    }

    startDateOnChange(e:any) {
        this.setState({
            startDate: e.target.value
        });
    }
    endDateOnChange(e:any) {
        this.setState({
            endDate: e.target.value
        });
    }
    kindOnChange(e:any) {
        this.setState({
            kind: e.target.value
        });
    }
    titleOnChange(e:any) {
        this.setState({
            title: e.target.value
        });
    }
    csvOnclick() {
        csvModule.onclickCsvButton(
            this.state.kind,
            this.state.title,
            this.state.startDate,
            this.state.endDate
        );
    }
    deleteOnClick(e:any){
        var logDataAjaxCallObj = new LogDataAjaxCall();
        var optionList:any[] = [];
        for(var i=0;i<selectedList.length;i++){
            optionList.push({logdate:selectedList[i]});
        }
        logDataAjaxCallObj.delete(
            [],
            optionList
        );

        location.reload();
    }

    searchOnclick() {
        var logDataAjaxResult = this.getDataUsingRestCallFunc(1);

        this.setState({ resultData: logDataAjaxResult["items"] });

        var maxpage = Math.ceil(logDataAjaxResult["total"] / this.state.perpage);
        this.setState({ maxPage: maxpage });

        if (logDataAjaxResult["items"].length == 0) {
            $("#current_page").val(0);
            $("#max_page").val(0);
            this.setState({ maxPage: 0, currentPage: 0 });
        } else {
            $("#current_page").val(1);
            $("#max_page").val(maxpage);
            this.setState({ maxPage: maxpage, currentPage: 1 });
        }
    }

    componentDidMount(){
        this.searchOnclick();
    }
    render() {
        return (
            <div className="main_box">
                <div className="current_page_info">
                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_setting_root_name")}
                    <i className="fas fa-angle-right"></i>
                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_logmanager_name")}
                </div>
                <div className="searchBox">
                    {/* <div className="kind_input_box">
                        <label>
                            {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                "logtable_kind_name"
                            )}
                            <input
                                type="text"
                                className="kind_input"
                                id="kind"
                                onChange={this.kindOnChange}
                                defaultValue={this.state.kind}
                            />
                        </label>
                    </div> */}
                    <div className="title_input_box">
                        <label>
                            {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                "logtable_title_name"
                            )}
                            <input
                                type="text"
                                className="title_input"
                                id="title"
                                onChange={this.titleOnChange}
                                defaultValue={this.state.title}
                            />
                        </label>
                    </div>
                    <div className="date_input_box">
                        <div className="start_date_input_box">
                            <label>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "imagefiletable_startdate_label"
                                )}
                                <input
                                    type="date"
                                    className="start_date_input"
                                    onChange={this.startDateOnChange}
                                    defaultValue={this.state.startDate}
                                />
                            </label>
                        </div>
                        <div className="end_date_input_box">
                            <label>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "imagefiletable_enddate_label"
                                )}
                                <input
                                    type="date"
                                    className="end_date_input"
                                    onChange={this.endDateOnChange}
                                    defaultValue={this.state.endDate}
                                />
                            </label>
                        </div>
                    </div>
                    <div className="button_box">
                        <div className="csv_button_box">
                            <button
                                className="csv_button"
                                onClick={this.csvOnclick}
                            >
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_csv_button_name")}
                            </button>
                        </div>
                        <div className="search_button_box">
                            <button
                                className="searchButton"
                                onClick={this.searchOnclick}
                            >
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "dashtable_search_button_name"
                                )}
                            </button>
                        </div>
                        <div>
                            <button className="items_delete_button" onClick={this.deleteOnClick}>{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                "table_selected_item_delete_button"
                            )}</button>
                        </div>
                    </div>
                </div>

                <div>
                    <TableHeaderAndBox
                        data={this.state.resultData}
                    />
                </div>
                <div className="page_box">
                    <div className="pre_page_box">
                        <i
                            className="fas fa-angle-double-left"
                            onClick={this.firstPageOnclick}
                        />
                        <i
                            className="fas fa-angle-left"
                            onClick={this.prePageOnclick}
                        />
                    </div>
                    <div className="page_input_box">
                        <input
                            type="text"
                            className="current_page_input"
                            id="current_page"
                            onKeyPress={this.currentPageOnKeyPress}
                        />
                        <div className="slash">
                        </div>
                        <input
                            type="text"
                            className="max_page_input"
                            readOnly="true"
                            id="max_page"
                        />
                    </div>
                    <div className="post_page_box">
                        <i
                            className="fas fa-angle-right"
                            onClick={this.nextPageOnclick}
                        />
                        <i
                            className="fas fa-angle-double-right"
                            onClick={this.lastPageOnclick}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

ReactDOM.render(
    <div>
        <SearchBoxAndTable />
    </div>,
    document.getElementById("main")
);
