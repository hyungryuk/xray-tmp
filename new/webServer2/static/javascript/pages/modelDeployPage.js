"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const ReactDOM = require("react-dom");
const header_1 = require("../modulesForPages/header");
const inversify_config_1 = require("../DIContainerConf/inversify.config");
const types_1 = require("../DIContainerConf/types");
const dbRestcall_1 = require("../others/restCall/dbRestcall");
const othersRestCall_1 = require("../others/restCall/othersRestCall");
ReactDOM.render(React.createElement(header_1.HeaderComponent, null), document.getElementById("header"));
class ModelNameInTable extends React.Component {
    constructor(props) {
        super(props);
        this.modelOnChange = this.modelOnChange.bind(this);
    }
    modelOnChange() {
    }
    componentDidMount() {
        $("#" + this.props.linecode).val(this.props.modelcode + "");
    }
    render() {
        const selectItems = this.props.modeldata.map((model) => React.createElement("option", { value: model.modelcode }, model.modelname));
        return (React.createElement("div", null,
            React.createElement("select", { id: this.props.linecode, onChange: this.modelOnChange }, selectItems)));
    }
}
class FilterNameInTable extends React.Component {
    render() {
        for (var i = 0; i < this.props.filterdata.length; i++) {
            if (this.props.filterdata[i].filtercode == this.props.filtercode) {
                return (React.createElement("div", null, this.props.filterdata[i].filtername));
            }
        }
        return (React.createElement("div", null));
    }
}
class LineNameInTable extends React.Component {
    render() {
        for (var i = 0; i < this.props.linedata.length; i++) {
            if (parseInt(this.props.linedata[i].linecode) == parseInt(this.props.linecode)) {
                return (React.createElement("div", null, this.props.linedata[i].factory + " " + this.props.linedata[i].line));
            }
        }
        return (React.createElement("div", null));
    }
}
class DeployButton extends React.Component {
    constructor(props) {
        super(props);
        this.deployOnClick = this.deployOnClick.bind(this);
    }
    deployOnClick() {
        var selectedModel = $("#" + this.props.modelSelectorId + " option:selected").val();
        if (confirm(inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("check_if_deploy_message")) == true) {
            var deployModelRestCallObj = new othersRestCall_1.DeployModelRestCall();
            var result = deployModelRestCallObj.deployModel([], [{ modelcode: selectedModel, linecode: this.props.linecode, userid: "haha" }]);
            if (result["resturn_code"] == "0000") {
                alert("complete");
            }
            else {
                alert(result["cause"]);
            }
        }
        else {
        }
    }
    render() {
        return (React.createElement("button", { onClick: this.deployOnClick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("deploy_button_name")));
    }
}
class Historypopup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            close: true
        };
        this.closeOnClick = this.closeOnClick.bind(this);
    }
    closeOnClick() {
        this.setState(prevState => ({
            close: !prevState.close
        }));
    }
    render() {
        if (this.props.open != this.state.close) {
            return (React.createElement("div", null));
        }
        else {
            var historyList = this.props.data.map((item) => React.createElement("tr", null,
                React.createElement("td", null, item.modelname),
                React.createElement("td", null, item.accuracy),
                React.createElement("td", null, item.user),
                React.createElement("td", null, item.deploydate)));
            return (React.createElement("div", { className: "history_popup_box" },
                React.createElement("div", { className: "history_popup" },
                    React.createElement("div", { className: "history_popup_table_box" },
                        React.createElement("table", null,
                            React.createElement("thead", null,
                                React.createElement("th", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_model_name")),
                                React.createElement("th", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_accuracy_name")),
                                React.createElement("th", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_user_name")),
                                React.createElement("th", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_regdate_name"))),
                            React.createElement("tbody", null, historyList))),
                    React.createElement("div", { className: "history_popup_button_group" },
                        React.createElement("button", { onClick: this.closeOnClick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("popup_close"))))));
        }
    }
}
class HistoryButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            isOpened: false
        };
        this.historyOnClick = this.historyOnClick.bind(this);
    }
    historyOnClick() {
        if (this.state.data.length == 0) {
            var deployDataAjaxCallObj = new dbRestcall_1.DeployDataAjaxCall();
            var deployDataRestCallResult = deployDataAjaxCallObj.retrieve([], [{ linecode: this.props.linecode }]);
            this.setState({
                data: deployDataRestCallResult["items"]
            });
        }
        this.setState(prevState => ({
            isOpened: !prevState.isOpened
        }));
    }
    render() {
        return (React.createElement("div", null,
            React.createElement("button", { onClick: this.historyOnClick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("deploy_history_button_name")),
            React.createElement(Historypopup, { open: this.state.isOpened, data: this.state.data })));
    }
}
class TableBody extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentDeployDatas: []
        };
        this.getDeployColDataWithLineCode = this.getDeployColDataWithLineCode.bind(this);
    }
    componentDidMount() {
        var deployDataAjaxCallObj = new dbRestcall_1.DeployDataAjaxCall();
        var deployDataAjaxRusult = deployDataAjaxCallObj.retrieve([], [{ isrunning: 1 }]);
        this.setState({
            currentDeployDatas: deployDataAjaxRusult["items"]
        });
    }
    getDeployColDataWithLineCode(linecode, colName) {
        for (var i = 0; i < this.state.currentDeployDatas.length; i++) {
            if (this.state.currentDeployDatas[i]["linecode"] == linecode) {
                return this.state.currentDeployDatas[i][colName];
            }
        }
        return "";
    }
    render() {
        const data = this.props.data.map(item => (React.createElement("tr", null,
            React.createElement("td", null,
                React.createElement(LineNameInTable, { linecode: item.linecode, linedata: this.props.linedata })),
            React.createElement("td", null,
                React.createElement(FilterNameInTable, { filtercode: this.getDeployColDataWithLineCode(item.linecode, "filtercode"), filterdata: this.props.filterdata })),
            React.createElement("td", null,
                React.createElement(ModelNameInTable, { modelcode: this.getDeployColDataWithLineCode(item.linecode, "modelcode"), modeldata: this.props.modeldata, linecode: item.linecode })),
            React.createElement("td", null,
                React.createElement(DeployButton, { linecode: item.linecode, modelSelectorId: item.linecode })),
            React.createElement("td", null,
                React.createElement(HistoryButton, { linecode: item.linecode })))));
        return data;
    }
}
class TableHeaderAndBox extends React.Component {
    render() {
        return (React.createElement("div", { className: "table_box" },
            React.createElement("table", { className: "table" },
                React.createElement("thead", { className: "thead" },
                    React.createElement("tr", null,
                        React.createElement("th", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("linetable_linename_name")),
                        React.createElement("th", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modeltraining_table_filter_name")),
                        React.createElement("th", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_model_name")),
                        React.createElement("th", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("deploy_button_name")),
                        React.createElement("th", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("deploy_history_button_name")))),
                React.createElement("tbody", { className: "tbody" },
                    React.createElement(TableBody, { data: this.props.data, linedata: this.props.linedata, filterdata: this.props.filterdata, modeldata: this.props.modeldata })))));
    }
}
class SearchBoxAndTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            linedata: [],
            factory: "",
            linename: "",
            currentPage: 1,
            maxPage: -1,
            perpage: 10,
            resultData: [],
            filterdata: [],
            modeldata: []
        };
        this.getDataUsingRestCallFunc = this.getDataUsingRestCallFunc.bind(this);
        this.factoryOnChange = this.factoryOnChange.bind(this);
        this.linenameOnChange = this.linenameOnChange.bind(this);
        this.searchOnclick = this.searchOnclick.bind(this);
        this.firstPageOnclick = this.firstPageOnclick.bind(this);
        this.lastPageOnclick = this.lastPageOnclick.bind(this);
        this.prePageOnclick = this.prePageOnclick.bind(this);
        this.nextPageOnclick = this.nextPageOnclick.bind(this);
        this.currentPageOnKeyPress = this.currentPageOnKeyPress.bind(this);
    }
    getDataUsingRestCallFunc(page) {
        var lineDataRestCallResult;
        var deployDataRestCallResult;
        var filterDataRestCallResult;
        var modelDataRestCallResult;
        var option1 = {};
        if (this.state.factory != "") {
            option1["factory"] = this.state.factory;
        }
        if (this.state.linename != "") {
            option1["line"] = this.state.linename;
        }
        var lineAjaxCallObj = new dbRestcall_1.LineAjaxCall();
        lineDataRestCallResult = lineAjaxCallObj.retrieve([], [option1]);
        var linecode = lineDataRestCallResult["items"][0]["linecode"];
        this.setState({
            linedata: lineDataRestCallResult["items"]
        });
        lineDataRestCallResult = lineAjaxCallObj.retrieve([], [option1, {}, {}, { page: page, perpage: this.state.perpage }]);
        var filterDataAjaxCallObj = new dbRestcall_1.FilterDataAjaxCall();
        filterDataRestCallResult = filterDataAjaxCallObj.retrieve([], []);
        this.setState({
            filterdata: filterDataRestCallResult["items"]
        });
        var modelDataAjaxCallObj = new dbRestcall_1.ModelDataAjaxCall();
        modelDataRestCallResult = modelDataAjaxCallObj.retrieve([], [{ available: 1 }]);
        this.setState({
            modeldata: modelDataRestCallResult["items"]
        });
        return lineDataRestCallResult;
    }
    firstPageOnclick() {
        var deployDataResult;
        if (this.state.currentPage != 1) {
            deployDataResult = this.getDataUsingRestCallFunc(1);
            this.setState({
                resultData: deployDataResult["items"],
                currentPage: 1
            });
            $("#current_page").val(1);
        }
    }
    lastPageOnclick() {
        var deployDataResult;
        if (this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage) {
            deployDataResult = this.getDataUsingRestCallFunc(this.state.maxPage);
            this.setState({
                resultData: deployDataResult["items"],
                currentPage: this.state.maxPage
            });
            $("#current_page").val(this.state.maxPage);
        }
    }
    prePageOnclick() {
        var deployDataResult;
        if (this.state.maxPage != -1 && this.state.currentPage > 1) {
            deployDataResult = this.getDataUsingRestCallFunc(this.state.currentPage - 1);
            this.setState({ resultData: deployDataResult["items"] });
            $("#current_page").val(this.state.currentPage - 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage - 1
            }));
        }
    }
    nextPageOnclick() {
        var deployDataResult;
        if (this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage) {
            deployDataResult = this.getDataUsingRestCallFunc(this.state.currentPage + 1);
            this.setState({ resultData: deployDataResult["items"] });
            $("#current_page").val(this.state.currentPage + 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage + 1
            }));
        }
    }
    currentPageOnKeyPress(e) {
        if (e.which == 13) {
            var pageVal = $("#current_page").val();
            var page = parseInt(pageVal);
            if (isNaN(page)) {
                alert(inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_is_not_number_alert"));
                $("#current_page").val(this.state.currentPage);
            }
            else {
                if (page > this.state.maxPage) {
                    alert(inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_greater_than_max_alert"));
                    $("#current_page").val(this.state.currentPage);
                }
                else {
                    if (this.state.maxPage != -1 &&
                        this.state.currentPage != page) {
                        var deployDataResult = this.getDataUsingRestCallFunc(page);
                        this.setState({
                            resultData: deployDataResult["items"],
                            currentPage: page
                        });
                        $("#current_page").val(page);
                    }
                }
            }
        }
    }
    factoryOnChange(e) {
        this.setState({
            factory: e.target.value
        });
    }
    linenameOnChange(e) {
        this.setState({
            linename: e.target.value
        });
    }
    searchOnclick() {
        var deployDataResult = this.getDataUsingRestCallFunc(1);
        this.setState({ resultData: deployDataResult["items"] });
        var maxpage = Math.ceil(deployDataResult["total"] / this.state.perpage);
        this.setState({ maxPage: maxpage });
        if (deployDataResult["items"].length == 0) {
            $("#current_page").val(0);
            $("#max_page").val(0);
            this.setState({ maxPage: 0, currentPage: 0 });
        }
        else {
            $("#current_page").val(1);
            $("#max_page").val(maxpage);
            this.setState({ maxPage: maxpage, currentPage: 1 });
        }
    }
    componentDidMount() {
        this.searchOnclick();
    }
    render() {
        return (React.createElement("div", { className: "main_box" },
            React.createElement("div", { className: "current_page_info" },
                inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_model_root_name"),
                React.createElement("i", { className: "fas fa-angle-right" }),
                inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_deploymodel_name")),
            React.createElement("div", { className: "searchBox" },
                React.createElement("div", { className: "userid_input_box" },
                    React.createElement("label", null,
                        inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("linetable_factory_name"),
                        React.createElement("input", { type: "text", className: "userid_input", id: "factory", onChange: this.factoryOnChange, defaultValue: this.state.factory }))),
                React.createElement("div", { className: "username_input_box" },
                    React.createElement("label", null,
                        inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("linetable_linename_name"),
                        React.createElement("input", { type: "text", className: "username_input", id: "username", onChange: this.linenameOnChange, defaultValue: this.state.linename }))),
                React.createElement("div", { className: "button_box" },
                    React.createElement("div", { className: "search_button_box" },
                        React.createElement("button", { className: "searchButton", onClick: this.searchOnclick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_search_button_name"))))),
            React.createElement("div", null,
                React.createElement(TableHeaderAndBox, { data: this.state.resultData, linedata: this.state.linedata, filterdata: this.state.filterdata, modeldata: this.state.modeldata })),
            React.createElement("div", { className: "page_box" },
                React.createElement("div", { className: "pre_page_box" },
                    React.createElement("i", { className: "fas fa-angle-double-left", onClick: this.firstPageOnclick }),
                    React.createElement("i", { className: "fas fa-angle-left", onClick: this.prePageOnclick })),
                React.createElement("div", { className: "page_input_box" },
                    React.createElement("input", { type: "text", className: "current_page_input", id: "current_page", onKeyPress: this.currentPageOnKeyPress }),
                    React.createElement("div", { className: "slash" }),
                    React.createElement("input", { type: "text", className: "max_page_input", readOnly: "true", id: "max_page" })),
                React.createElement("div", { className: "post_page_box" },
                    React.createElement("i", { className: "fas fa-angle-right", onClick: this.nextPageOnclick }),
                    React.createElement("i", { className: "fas fa-angle-double-right", onClick: this.lastPageOnclick })))));
    }
}
ReactDOM.render(React.createElement("div", null,
    React.createElement(SearchBoxAndTable, null)), document.getElementById("main"));
