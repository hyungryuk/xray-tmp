﻿"""
    xiprs
    ~~~~~

    Xray Image Processing Restful Server based on Flask Restful framework.
    It's extensively documented
    and follows best practice patterns.

    :copyright: (c) 2018 by SKInovation.
"""
from .configs.config import configure_app
from flask_restful import Api
from flask_cors import CORS

__version__ = '0.1.0'

# utilities we import from flask and flask_restful that are unused
# in the module but are exported as public interface.

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

# env = sys.argv[1] if len(sys.argv) > 2 else 'dev'
# if env == 'dev':
#     app.config = config.DevelopmentConfig
# elif env == 'dev_mac':
#     app.config = config.MacDevelopmentConfig
# elif env == 'prod':
#     app.config = config.BaseConfig
# else:
#     raise ValueError('Invalid environment name')
configure_app(app)

db = SQLAlchemy(app)

api = Api(app)

CORS(app)

db.create_all()

from .controller.imgctr import ImageFileRESTApi
from .controller.algctr import AlgorithmInfoRESTApi
from .controller.dlmctr import DLModelInfoRESTApi
from .controller.ftrctr import FilterInfoRESTApi
from .controller.linectr import LineInfoRESTApi
from .controller.logctr import LogInfoRESTApi
from .controller.usrctr import UserInfoRESTApi
from .controller.dirctr import DirectoryRESTApi
from .controller.trnctr import TrainingInfoRESTApi
from .controller.dwfctr import DownloadFileRESTApi
from .controller.sttctr import StatisticRESTApi
from .controller.dpmctr import DeployModelInfoRESTApi

IMG_CTR_URL = '/rest/db/imagefile'
api.add_resource(ImageFileRESTApi, IMG_CTR_URL)
ALG_CTR_URL = '/rest/db/algorithm'
api.add_resource(AlgorithmInfoRESTApi, ALG_CTR_URL)
DLM_CTR_URL = '/rest/db/modelHandler'
api.add_resource(DLModelInfoRESTApi, DLM_CTR_URL)
FTR_CTR_URL = '/rest/db/filter'
api.add_resource(FilterInfoRESTApi, FTR_CTR_URL)
LINE_CTR_URL = '/rest/db/line'
api.add_resource(LineInfoRESTApi, LINE_CTR_URL)
LOG_CTR_URL = '/rest/db/log'
api.add_resource(LogInfoRESTApi, LOG_CTR_URL)
USER_CTR_URL = '/rest/db/user'
api.add_resource(UserInfoRESTApi, USER_CTR_URL)
DIR_CTR_URL = '/rest/dir'
api.add_resource(DirectoryRESTApi, DIR_CTR_URL)
TRN_CTR_URL = '/rest/trn'
api.add_resource(TrainingInfoRESTApi, TRN_CTR_URL)
DWL_CTR_URL = '/rest/dwn'
api.add_resource(DownloadFileRESTApi, DWL_CTR_URL)
STDAILY_CTR_URL = '/rest/db/statistics'
api.add_resource(StatisticRESTApi, STDAILY_CTR_URL)
DPM_CTR_URL = '/rest/db/deploy'
api.add_resource(DeployModelInfoRESTApi, DPM_CTR_URL)
