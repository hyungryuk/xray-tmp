import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { HeaderComponent } from "../modulesForPages/header";
import {LineSelectorInterface} from '../others/selector/selector'
import {StaticLanguageInterface} from '../others/staticLanguageData/staticLanguageData'
import {myContainer} from '../DIContainerConf/inversify.config'
import {TYPES} from '../DIContainerConf/types'

import { ImageFileAjaxCall,LineAjaxCall } from '../others/restCall/dbRestcall'
import { OpenImageRestCall } from '../others/restCall/othersRestCall'
import {encoder} from "../others/encoder/encoder";

import {properties} from '../properties/properties'

import 'owl.carousel';

ReactDOM.render(
  <HeaderComponent />,
  document.getElementById("header")
);


class OwlContents extends React.Component<{imagedatas:any[],infoDatas:any[],lines:any[]},{}> {

    constructor(props:any){
        super(props);
        this.setOwl = this.setOwl.bind(this);
        this.ngByUserOnChange = this.ngByUserOnChange.bind(this);
    }
    ngByUserOnChange(e:any){
        var changedValue = $("#ngbyuser option:selected").val() as string;
        var filename = e.target.parentNode.parentNode.getElementsByClassName("filename_div_in_item")[0].getElementsByTagName("input")[0].value
        var filedate = e.target.parentNode.parentNode.getElementsByClassName("filedate_div_in_item")[0].getElementsByTagName("input")[0].value
        filedate = encoder.dateToYYMMDDHHMMSS(filedate);
        var imageFileAjaxCallObj = new ImageFileAjaxCall();
        imageFileAjaxCallObj.update([{ng_by_user:parseInt(changedValue)}],[{filename:filename,filedate:filedate}]);
    }
    setOwl(){
        if(document.getElementById("owlcarousel")){
            document.getElementById("owlcarousel").innerHTML="";
            $('.owl-carousel').trigger('destroy.owl.carousel').removeClass('owl-carousel owl-loaded');
            document.getElementById("owlcarousel").setAttribute("class","owl-carousel owl-theme")

        }
        for(var i=0;i<this.props.imagedatas.length;i++){
            var imagediv = document.createElement("div");
            imagediv.setAttribute("class","item")
            imagediv.appendChild(this.props.imagedatas[i]);
            var datadiv = document.createElement("div");

            var filenameLabel = document.createElement("label");
            var filedateLabel = document.createElement("label");
            var linenameLabel = document.createElement("label");
            var ngLabel = document.createElement("label");
            var ngByMesLabel = document.createElement("label");
            var ngByUserLabel = document.createElement("label");





            filenameLabel.innerHTML=myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_filename_table");

            filedateLabel.innerHTML=myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_filedate_table");

            linenameLabel.innerHTML=myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_line_table");

            ngLabel.innerHTML=myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_table");

            ngByMesLabel.innerHTML=myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_by_mes_table");

            ngByUserLabel.innerHTML=myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_by_user_table");


            var filename = document.createElement("input");
            var filedate = document.createElement("input");
            var linename = document.createElement("input");
            var ng = document.createElement("select");
            var ngByMes = document.createElement("select");
            var ngByUser = document.createElement("select");



            filename.setAttribute("type", "text");
            filedate.setAttribute("type", "date-time");
            linename.setAttribute("type", "text");

            filename.setAttribute("value",this.props.infoDatas[i].filename);
            filedate.setAttribute("value",this.props.infoDatas[i].filedate);
            for(var j=0;j<this.props.lines.length;j++){
                if(this.props.infoDatas[i].linecode==this.props.lines[j].linecode){
                    linename.setAttribute("value",this.props.lines[j].line);
                    break;
                }
            }

            for(var j=0;j<3;j++){
                var optionDefault = document.createElement("option");
                var optionOk = document.createElement("option");
                var optionNg = document.createElement("option");
                var optionNr = document.createElement("option");

                 optionDefault.setAttribute("value","-1");
                 optionOk.setAttribute("value","0");
                 optionNg.setAttribute("value","1");
                 optionNr.setAttribute("value","2");

                 optionDefault.innerHTML=myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("select_box_default_option");

                 optionOk.innerHTML=myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ok_name");

                 optionNg.innerHTML=myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_name");

                 optionNr.innerHTML=myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_nr_name");

                 if(j==0){
                     ng.appendChild(optionDefault);
                     ng.appendChild(optionOk);
                     ng.appendChild(optionNg);
                     ng.appendChild(optionNr);
                 }else if(j==1){
                     ngByUser.appendChild(optionDefault);
                     ngByUser.appendChild(optionOk);
                     ngByUser.appendChild(optionNg);
                     ngByUser.appendChild(optionNr);
                 }else{
                     ngByMes.appendChild(optionDefault);
                     ngByMes.appendChild(optionOk);
                     ngByMes.appendChild(optionNg);
                     ngByMes.appendChild(optionNr);
                 }
            }








             ng.value=this.props.infoDatas[i].ng+"";
             ngByUser.value=this.props.infoDatas[i].ng_by_user+"";
             ngByMes.value=this.props.infoDatas[i].ng_by_mes+"";

            datadiv.setAttribute("class","data_box_div_in_item")

            filenameLabel.setAttribute("class","filename_div_in_item")
            filedateLabel.setAttribute("class","filedate_div_in_item")
            linenameLabel.setAttribute("class","linename_div_in_item")
            ngLabel.setAttribute("class","ng_div_in_item")
            ngByMesLabel.setAttribute("class","ngbymes_div_in_item")
            ngByUserLabel.setAttribute("class","ngbyuser_div_in_item")

            filename.setAttribute("class","filename_in_item")
            filedate.setAttribute("class","filedate_in_item")
            linename.setAttribute("class","linename_in_item")
            ng.setAttribute("class","ng_in_item")
            ngByMes.setAttribute("class","ngbymes_in_item")
            ngByUser.setAttribute("class","ngbyuser_in_item")

            ngByUser.setAttribute("id","ngbyuser")
            ngByUser.onchange=this.ngByUserOnChange;


            filename.setAttribute("readonly","true")
            filedate.setAttribute("readonly","true")
            linename.setAttribute("readonly","true")
            ng.setAttribute("disabled","true")
            ngByMes.setAttribute("disabled","true")

            filenameLabel.appendChild(filename);
            filedateLabel.appendChild(filedate);
            linenameLabel.appendChild(linename);
            ngLabel.appendChild(ng);
            ngByMesLabel.appendChild(ngByMes);
            ngByUserLabel.appendChild(ngByUser);

            datadiv.appendChild(filenameLabel);
            datadiv.appendChild(filedateLabel);
            datadiv.appendChild(linenameLabel);
            datadiv.appendChild(ngLabel);
            datadiv.appendChild(ngByMesLabel);
            datadiv.appendChild(ngByUserLabel);

            imagediv.appendChild(datadiv);
            document.getElementById("owlcarousel").appendChild(imagediv);
        }

        $('.owl-carousel').owlCarousel({
            loop:false,
            margin:10,
            nav:true,
            autoplay:true,
            autoplayTimeout:parseInt(properties["LabelingPageItemSlideSpeed"]),
            autoplayHoverPause:true,
        })

    }
    render() {

        return(
            <div>
            <div id="owlcarousel"/>
                {this.setOwl()}
            </div>
        );
    }
}



class LineNameInTable extends React.Component<{lineList:any[],linecode:number},{}> {
    constructor(props:any) {
        super(props);
        this.lineCodeToFactoryAndLineName = this.lineCodeToFactoryAndLineName.bind(
            this
        );
    }

    lineCodeToFactoryAndLineName() {
        for (var i = 0; i < this.props.lineList.length; i++) {
            if (this.props.lineList[i].linecode == this.props.linecode) {
                return (
                    this.props.lineList[i].factory +
                    " " +
                    this.props.lineList[i].line
                );
            }
        }
    }

    render() {
        return <div>{this.lineCodeToFactoryAndLineName()}</div>;
    }
}

class LotnoInPopup extends React.Component<{lotno:string,id:string},{}> {
    constructor(props:any) {
        super(props);
        this.initDate = this.initDate.bind(this);
    }

    initDate() {
        $("#" + this.props.id).val(this.props.lotno);
    }

    componentDidMount() {
        this.initDate();
    }

    render() {
        return <input type="text" className="lotno_input" id={this.props.id} />;
    }
}

class NgSelectBoxInPopup extends React.Component<{ng:string,id:string},{}> {
    render() {
        if (this.props.ng == "-1") {
            return (
                <select className="ng_input" id={this.props.id}>
                    <option value="-1">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("select_box_default_option")}
                    </option>
                    <option value="0">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ok_name")}
                    </option>
                    <option value="1">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_name")}
                    </option>
                    <option value="2">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_nr_name")}
                    </option>
                </select>
            );
        }
        if (this.props.ng == "0") {
            return (
                <select className="ng_input" id={this.props.id}>
                    <option value="-1">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("select_box_default_option")}
                    </option>
                    <option value="0" selected>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ok_name")}
                    </option>
                    <option value="1">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_name")}
                    </option>
                    <option value="2">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_nr_name")}
                    </option>
                </select>
            );
        }
        if (this.props.ng == "1") {
            return (
                <select className="ng_input" id={this.props.id}>
                    <option value="-1">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("select_box_default_option")}
                    </option>
                    <option value="0">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ok_name")}
                    </option>
                    <option value="1" selected>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_name")}
                    </option>
                    <option value="2">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_nr_name")}
                    </option>
                </select>
            );
        }
        if (this.props.ng == "2") {
            return (
                <select className="ng_input" id={this.props.id}>
                    <option value="-1">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("select_box_default_option")}
                    </option>
                    <option value="0">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ok_name")}
                    </option>
                    <option value="1">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_name")}
                    </option>
                    <option value="2" selected>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_nr_name")}
                    </option>
                </select>
            );
        }
    }
}

class NgToString extends React.Component<{ng:number},{}> {
    render() {
        if (this.props.ng == 0) {
            return <td>{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ok_name")}</td>;
        }
        if (this.props.ng == 1) {
            return <td>{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_name")}</td>;
        }
        if (this.props.ng == 2) {
            return <td>{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_nr_name")}</td>;
        }
    }
}

class SearchBoxAndTable extends React.Component<{},{fileName:string,line:string,lotno:string,ng:string,ng_by_user:string,ng_by_mes:string,startDate:string,endDate:string,resultImages:any[],resultInfos:any[],lineList:any[],isfilterOpen:boolean,currentPage:number,maxPage:number,perpage:number}> {
    constructor(props:any) {
        super(props);
        this.state = {
            startDate: "",
            endDate: "",
            fileName: "",
            line: "all",
            resultImages: [],
            resultInfos: [],
            lotno: "",
            lineList: [],
            isfilterOpen: false,
            ng: "-1",
            ng_by_user: "-1",
            ng_by_mes: "-1",
            currentPage: 1,
            maxPage: -1,
            perpage: 3
        };

        this.getDataUsingRestCallFunc = this.getDataUsingRestCallFunc.bind(
            this
        );

        this.startDateOnChange = this.startDateOnChange.bind(this);
        this.endDateOnChange = this.endDateOnChange.bind(this);
        this.fileNameOnChange = this.fileNameOnChange.bind(this);
        this.lineSelectOnChange = this.lineSelectOnChange.bind(this);
        this.lotnoOnChange = this.lotnoOnChange.bind(this);
        this.searchOnclick = this.searchOnclick.bind(this);
        this.filterOnclick = this.filterOnclick.bind(this);
        this.filterCloseOnclick = this.filterCloseOnclick.bind(this);
        this.setFilterOnclick = this.setFilterOnclick.bind(this);
        this.initFilterBox = this.initFilterBox.bind(this);
        this.perpageOnChange = this.perpageOnChange.bind(this);

        this.firstPageOnclick = this.firstPageOnclick.bind(this);
        this.lastPageOnclick = this.lastPageOnclick.bind(this);
        this.prePageOnclick = this.prePageOnclick.bind(this);
        this.nextPageOnclick = this.nextPageOnclick.bind(this);
        this.currentPageOnKeyPress = this.currentPageOnKeyPress.bind(this);

    }
    initFilterBox() {
        $("#lotno").val(this.state.lotno);
        $("#ng option:selected").val(this.state.ng);
        $("#ng_by_user option:selected").val(this.state.ng_by_user);
        $("#ng_by_mes option:selected").val(this.state.ng_by_mes);
    }
    setFilterOnclick() {
        this.setState({ lotno: $("#lotno").val() as string });
        this.setState({ ng: $("#ng option:selected").val() as string });
        this.setState({ ng_by_user: $("#ng_by_user option:selected").val()  as string});
        this.setState({ ng_by_mes: $("#ng_by_mes option:selected").val()  as string});
        this.setState(prevState => ({
            isfilterOpen: !prevState.isfilterOpen
        }));
    }
    filterCloseOnclick() {
        this.setState(prevState => ({
            isfilterOpen: !prevState.isfilterOpen
        }));
    }
    filterOnclick() {
        this.setState(prevState => ({
            isfilterOpen: !prevState.isfilterOpen
        }));
    }
    getDataUsingRestCallFunc(page:number) {
        var imageFileResult;
        var option1:{[key:string]:any} = {};
        var option2:{[key:string]:any} = {};

        if (this.state.line != "all") {
            option1["linecode"] = this.state.line;
        }
        if (this.state.fileName != "") {
            option1["filename"] = this.state.fileName;
        }
        if (this.state.lotno != "") {
            option1["lotno"] = this.state.lotno;
        }
        if (this.state.ng != "-1") {
            option1["ng"] = this.state.ng;
        }
        if (this.state.ng_by_user != "-1") {
            option1["ng_by_user"] = this.state.ng_by_user;
        }
        if (this.state.ng_by_mes != "-1") {
            option1["ng_by_mes"] = this.state.ng_by_mes;
        }
        if (this.state.startDate != "" && this.state.endDate != "") {
            option2["startdate"] = encoder.dateToYYMMDDHHMMSS(
                this.state.startDate + "000000"
            );
            option2["enddate"] = encoder.dateToYYMMDDHHMMSS(
                this.state.endDate + "235959"
            );
        }

        option1["isremoved"] = 0;
        var imageFileAjaxCall = new ImageFileAjaxCall();
        imageFileResult = imageFileAjaxCall.retrieve(
            [],
            [option1, option2,{},{page:page,perpage:this.state.perpage}]
        );

        var imageFiles = imageFileResult["items"];
        var paths=[];
        for(var i=0;i<imageFiles.length;i++){
            paths.push(imageFiles[i].filepath);
        }

        var openImageRestCallObj = new OpenImageRestCall();
        var resultData = openImageRestCallObj.openImageWithBMP(paths);
        imageFileResult["images"] = resultData;
        return imageFileResult;
    }
    firstPageOnclick() {
        var imageFileResult;
        if (this.state.currentPage != 1) {
            imageFileResult = this.getDataUsingRestCallFunc(1);
            this.setState({
                resultImages: imageFileResult["images"],
                resultInfos: imageFileResult["items"],
                currentPage: 1
            });

            $("#current_page").val(1);
        }
    }

    lastPageOnclick() {
        var imageFileResult;
        if (
            this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage
        ) {
            imageFileResult = this.getDataUsingRestCallFunc(this.state.maxPage);
            this.setState({
                resultImages: imageFileResult["images"],
                resultInfos: imageFileResult["items"],
                currentPage: this.state.maxPage
            });

            $("#current_page").val(this.state.maxPage);
        }
    }
    prePageOnclick() {
        var imageFileResult;
        if (this.state.maxPage != -1 && this.state.currentPage > 1) {
            imageFileResult = this.getDataUsingRestCallFunc(
                this.state.currentPage - 1
            );
            this.setState({
                resultImages: imageFileResult["images"],
                resultInfos: imageFileResult["items"]
             });
            $("#current_page").val(this.state.currentPage - 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage - 1
            }));
        }
    }
    nextPageOnclick() {
        var imageFileResult;
        if (
            this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage
        ) {
            imageFileResult = this.getDataUsingRestCallFunc(
                this.state.currentPage + 1
            );
            this.setState({
                resultImages: imageFileResult["images"],
                resultInfos: imageFileResult["items"]
             });
            $("#current_page").val(this.state.currentPage + 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage + 1
            }));
        }
    }
    currentPageOnKeyPress(e:any) {
        if (e.which == 13) {
            var pageVal = $("#current_page").val() as string;
            var page = parseInt(pageVal);
            if (isNaN(page)) {
                alert(myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_is_not_number_alert"));
                $("#current_page").val(this.state.currentPage);
            } else {
                if (page > this.state.maxPage) {
                    alert(
                        myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_greater_than_max_alert")
                    );
                    $("#current_page").val(this.state.currentPage);
                } else {
                    if (
                        this.state.maxPage != -1 &&
                        this.state.currentPage != page
                    ) {
                        var imageFileResult = this.getDataUsingRestCallFunc(
                            page
                        );
                        this.setState({
                            resultImages: imageFileResult["images"],
                            resultInfos: imageFileResult["items"],
                            currentPage: page
                        });
                        $("#current_page").val(page);
                    }
                }
            }
        }
    }

    perpageOnChange(e:any) {
        this.setState({
            perpage: parseInt(e.target.value),
            resultImages: [],
            resultInfos: []
        });
    }
    startDateOnChange(e:any) {
        this.setState({
            startDate: e.target.value,
            resultImages: [],
            resultInfos: []
        });
    }
    endDateOnChange(e:any) {
        this.setState({
            endDate: e.target.value,
            resultImages: [],
            resultInfos: []
        });
    }
    fileNameOnChange() {
        this.setState({
            fileName: $("#fileName").val() as string,
            resultImages: [],
            resultInfos: []
        });
    }
    lineSelectOnChange() {
        this.setState({
            line: $("#lineSelector option:selected").val() as string,
            resultImages: [],
            resultInfos: []
        });
    }
    lotnoOnChange() {
        this.setState({
            lotno: $("#lotno").val() as string,
            resultImages: [],
            resultInfos: []
        });
    }

    searchOnclick() {
        var imageFileResult = this.getDataUsingRestCallFunc(1);

        this.setState({
            resultImages: imageFileResult["images"],
            resultInfos: imageFileResult["items"]
         });

        var maxpage = Math.ceil(imageFileResult["total"] / this.state.perpage);
        this.setState({ maxPage: maxpage });

        if (imageFileResult["items"].length == 0) {
            $("#current_page").val(0);
            $("#max_page").val(0);
            this.setState({ maxPage: 0, currentPage: 0 });
        } else {
            $("#current_page").val(1);
            $("#max_page").val(maxpage);
            this.setState({ maxPage: maxpage, currentPage: 1 });
        }

    }

    componentDidMount() {
        var lineAjaxCall = new LineAjaxCall();
        var lineList = lineAjaxCall.retrieve([], [])["items"];
        let selectorObj = myContainer.get<LineSelectorInterface>(TYPES.LineSelectorInterface);
        selectorObj.setDivId("lineSelector")
        selectorObj.setDatas(lineList);
        selectorObj.makeSelector();
        this.setState({ lineList: lineList });
    }

    render() {
        const popup = (
            <div className="optional_settings_box">
                <div className="popup_close_button">
                    <i
                        className="fas fa-window-close"
                        onClick={this.filterCloseOnclick}
                    />
                </div>
                <div className="lotno_input_box">
                    <label>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_lotno_table")}
                        <LotnoInPopup id="lotno" lotno={this.state.lotno} />
                    </label>
                </div>
                <div className="ng_input_box">
                    <label>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_table")}
                        <NgSelectBoxInPopup ng={this.state.ng} id="ng" />
                    </label>
                </div>
                <div className="ng_user_input_box">
                    <label>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_by_user_table")}
                        <NgSelectBoxInPopup
                            ng={this.state.ng_by_user}
                            id="ng_by_user"
                        />
                    </label>
                </div>
                <div className="ng_mes_input_box">
                    <label>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_by_mes_table")}
                        <NgSelectBoxInPopup
                            ng={this.state.ng_by_mes}
                            id="ng_by_mes"
                        />
                    </label>
                </div>
                <div className="filter_set_button">
                    <button
                        className="set_button"
                        onClick={this.setFilterOnclick}
                    >
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("filter_setting_button")}
                    </button>
                </div>
            </div>
        );
        if (this.state.isfilterOpen) {
            return (
                <div className="main_box">
                    <div className="current_page_info">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_file_root_name")}
                        <i className="fas fa-angle-right"></i>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_filelabel_name")}
                    </div>
                    <div>{popup}</div>
                    <div className="searchBox">
                        <div className="filename_input_box">
                            <label>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "imagefiletable_filename_label"
                                )}
                                <input
                                    type="text"
                                    className="filename_input"
                                    id="fileName"
                                    onChange={this.fileNameOnChange}
                                />
                            </label>
                        </div>
                        <div className="date_input_box">
                            <div className="start_date_input_box">
                                <label>
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "imagefiletable_startdate_label"
                                    )}
                                    <input
                                        type="date"
                                        className="start_date_input"
                                        onChange={this.startDateOnChange}
                                    />
                                </label>
                            </div>
                            <div className="end_date_input_box">
                                <label>
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "imagefiletable_enddate_label"
                                    )}
                                    <input
                                        type="date"
                                        className="end_date_input"
                                        onChange={this.endDateOnChange}
                                    />
                                </label>
                            </div>
                        </div>
                        <div className="line_input_box">
                            <label>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_line_label")}
                                <select
                                    className="line_input"
                                    id="lineSelector"
                                    onChange={this.lineSelectOnChange}
                                >
                                    <option value="all">
                                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                            "dashtable_line_selector_option_all_name"
                                        )}
                                    </option>
                                </select>
                            </label>
                        </div>
                        <div className="perpage_input_box">
                            <label>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagelabeling_perpage_name")}
                                <select
                                    className="perpage"
                                    id="perpage"
                                    onChange={this.perpageOnChange}
                                >
                                    <option value="3">{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagelabeling_three_perpage_name")}</option>
                                    <option value="5">{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagelabeling_five_perpage_name")}</option>
                                    <option value="10">{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagelabeling_ten_perpage_name")}</option>
                                </select>
                            </label>
                        </div>
                        <div className="button_box">
                            <div className="set_specific_filter_button_box">
                                <button
                                    className="set_specific_filter_button"
                                    onClick={this.filterOnclick}
                                >
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "imagefiletable_set_specific_filter_button_box_name"
                                    )}
                                </button>
                            </div>
                            <div className="search_button_box">
                                <button
                                    className="searchButton"
                                    onClick={this.searchOnclick}
                                >
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "dashtable_search_button_name"
                                    )}
                                </button>
                            </div>
                        </div>
                    </div>

                    <div className="owlcarousel_box">
                        <OwlContents
                            imagedatas={this.state.resultImages} infoDatas={this.state.resultInfos}
                            lines={this.state.lineList}
                        />
                    </div>
                    <div className="page_box">
                        <div className="pre_page_box">
                            <i
                                className="fas fa-angle-double-left"
                                onClick={this.firstPageOnclick}
                            />
                            <i
                                className="fas fa-angle-left"
                                onClick={this.prePageOnclick}
                            />
                        </div>
                        <div className="page_input_box">
                            <input
                                type="text"
                                className="current_page_input"
                                id="current_page"
                                onKeyPress={this.currentPageOnKeyPress}
                            />
                            <div className="slash">
                            </div>
                            <input
                                type="text"
                                className="max_page_input"
                                readOnly="true"
                                id="max_page"
                            />
                        </div>
                        <div className="post_page_box">
                            <i
                                className="fas fa-angle-right"
                                onClick={this.nextPageOnclick}
                            />
                            <i
                                className="fas fa-angle-double-right"
                                onClick={this.lastPageOnclick}
                            />
                        </div>
                    </div>

                </div>
            );
        } else {
            return (
                <div className="main_box">
                    <div className="current_page_info">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_file_root_name")}
                        <i className="fas fa-angle-right"></i>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_filelabel_name")}
                    </div>
                    <div className="searchBox">
                        <div className="filename_input_box">
                            <label>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "imagefiletable_filename_label"
                                )}
                                <input
                                    type="text"
                                    className="filename_input"
                                    id="fileName"
                                    onChange={this.fileNameOnChange}
                                />
                            </label>
                        </div>
                        <div className="date_input_box">
                            <div className="start_date_input_box">
                                <label>
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "imagefiletable_startdate_label"
                                    )}
                                    <input
                                        type="date"
                                        className="start_date_input"
                                        onChange={this.startDateOnChange}
                                    />
                                </label>
                            </div>
                            <div className="end_date_input_box">
                                <label>
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "imagefiletable_enddate_label"
                                    )}
                                    <input
                                        type="date"
                                        className="end_date_input"
                                        onChange={this.endDateOnChange}
                                    />
                                </label>
                            </div>
                        </div>
                        <div className="line_input_box">
                            <label>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_line_label")}
                                <select
                                    className="line_input"
                                    id="lineSelector"
                                    onChange={this.lineSelectOnChange}
                                >
                                    <option value="all">
                                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                            "dashtable_line_selector_option_all_name"
                                        )}
                                    </option>
                                </select>
                            </label>
                        </div>
                        <div className="perpage_input_box">
                            <label>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagelabeling_perpage_name")}
                                <select
                                    className="perpage"
                                    id="perpage"
                                    onChange={this.perpageOnChange}
                                >
                                    <option value="3">{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagelabeling_three_perpage_name")}</option>
                                    <option value="5">{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagelabeling_five_perpage_name")}</option>
                                    <option value="10">{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagelabeling_ten_perpage_name")}</option>
                                </select>
                            </label>
                        </div>
                        <div className="button_box">
                            <div className="set_specific_filter_button_box">
                                <button
                                    className="set_specific_filter_button"
                                    onClick={this.filterOnclick}
                                >
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "imagefiletable_set_specific_filter_button_box_name"
                                    )}
                                </button>
                            </div>
                            <div className="search_button_box">
                                <button
                                    className="searchButton"
                                    onClick={this.searchOnclick}
                                >
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "dashtable_search_button_name"
                                    )}
                                </button>
                            </div>
                        </div>
                    </div>

                    <div className="owlcarousel_box">
                        <OwlContents
                            imagedatas={this.state.resultImages} infoDatas={this.state.resultInfos}
                            lines={this.state.lineList}
                        />
                    </div>
                    <div className="page_box">
                        <div className="pre_page_box">
                            <i
                                className="fas fa-angle-double-left"
                                onClick={this.firstPageOnclick}
                            />
                            <i
                                className="fas fa-angle-left"
                                onClick={this.prePageOnclick}
                            />
                        </div>
                        <div className="page_input_box">
                            <input
                                type="text"
                                className="current_page_input"
                                id="current_page"
                                onKeyPress={this.currentPageOnKeyPress}
                            />
                            <div className="slash">
                            </div>
                            <input
                                type="text"
                                className="max_page_input"
                                readOnly="true"
                                id="max_page"
                            />
                        </div>
                        <div className="post_page_box">
                            <i
                                className="fas fa-angle-right"
                                onClick={this.nextPageOnclick}
                            />
                            <i
                                className="fas fa-angle-double-right"
                                onClick={this.lastPageOnclick}
                            />
                        </div>
                    </div>

                </div>
            );
        }
    }
}

ReactDOM.render(
    <div>
        <SearchBoxAndTable />
    </div>,
    document.getElementById("main")
);
