"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const properties_1 = require("../../properties/properties");
class AjaxCall {
    constructor(url) {
        this.url = url;
    }
    create(items, options) {
        var responsejson;
        let rquestjson;
        rquestjson = JSON.stringify({ items: items, method_type: 'insert', options: options });
        $.ajax({
            async: false,
            url: this.url,
            type: 'post',
            data: rquestjson,
            accepts: {
                text: 'application/json'
            },
            contentType: 'application/json',
            success: function (data) {
                data = JSON.parse(data);
                responsejson = data;
            },
            error: function (jqXHR, exception) { }
        });
        return responsejson;
    }
    delete(items, options) {
        var responsejson;
        let rquestjson;
        rquestjson = JSON.stringify({ items: items, method_type: 'delete', options: options });
        $.ajax({
            async: false,
            url: this.url,
            type: 'post',
            data: rquestjson,
            accepts: {
                text: 'application/json'
            },
            contentType: 'application/json',
            success: function (data) {
                data = JSON.parse(data);
                responsejson = data;
            },
            error: function (jqXHR, exception) { }
        });
        return responsejson;
    }
    update(items, options) {
        var responsejson;
        let rquestjson;
        rquestjson = JSON.stringify({ items: items, method_type: 'update', options: options });
        $.ajax({
            async: false,
            url: this.url,
            type: 'post',
            data: rquestjson,
            accepts: {
                text: 'application/json'
            },
            contentType: 'application/json',
            success: function (data) {
                data = JSON.parse(data);
                responsejson = data;
            },
            error: function (jqXHR, exception) { }
        });
        return responsejson;
    }
    retrieve(items, options) {
        var responsejson;
        let rquestjson;
        rquestjson = JSON.stringify({ items: items, method_type: 'view', options: options });
        $.ajax({
            async: false,
            url: this.url,
            type: 'post',
            data: rquestjson,
            accepts: {
                text: 'application/json'
            },
            contentType: 'application/json',
            success: function (data) {
                data = JSON.parse(data);
                responsejson = data;
            },
            error: function (jqXHR, exception) { }
        });
        return responsejson;
    }
}
class LineAjaxCall extends AjaxCall {
    constructor() {
        super(properties_1.properties["WEB_PATH"] + "/rest/db/line");
    }
}
exports.LineAjaxCall = LineAjaxCall;
class ImageFileAjaxCall extends AjaxCall {
    constructor() {
        super(properties_1.properties["WEB_PATH"] + "/rest/db/imagefile");
    }
}
exports.ImageFileAjaxCall = ImageFileAjaxCall;
class StatisticDataAjaxCall extends AjaxCall {
    constructor() {
        super(properties_1.properties["WEB_PATH"] + "/rest/db/statistics");
    }
}
exports.StatisticDataAjaxCall = StatisticDataAjaxCall;
class AlgorithmDataAjaxCall extends AjaxCall {
    constructor() {
        super(properties_1.properties["WEB_PATH"] + "/rest/db/algorithm");
    }
}
exports.AlgorithmDataAjaxCall = AlgorithmDataAjaxCall;
class FilterDataAjaxCall extends AjaxCall {
    constructor() {
        super(properties_1.properties["WEB_PATH"] + "/rest/db/filter");
    }
}
exports.FilterDataAjaxCall = FilterDataAjaxCall;
class TrainingDataAjaxCall extends AjaxCall {
    constructor() {
        super(properties_1.properties["WEB_PATH"] + "/rest/db/training");
    }
}
exports.TrainingDataAjaxCall = TrainingDataAjaxCall;
class ModelDataAjaxCall extends AjaxCall {
    constructor() {
        super(properties_1.properties["WEB_PATH"] + "/rest/db/model");
    }
}
exports.ModelDataAjaxCall = ModelDataAjaxCall;
class UserDataAjaxCall extends AjaxCall {
    constructor() {
        super(properties_1.properties["WEB_PATH"] + "/rest/db/user");
    }
}
exports.UserDataAjaxCall = UserDataAjaxCall;
class LogDataAjaxCall extends AjaxCall {
    constructor() {
        super(properties_1.properties["WEB_PATH"] + "/rest/db/log");
    }
}
exports.LogDataAjaxCall = LogDataAjaxCall;
class DeployDataAjaxCall extends AjaxCall {
    constructor() {
        super(properties_1.properties["WEB_PATH"] + "/rest/db/deploy");
    }
}
exports.DeployDataAjaxCall = DeployDataAjaxCall;
