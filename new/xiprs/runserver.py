#-*- coding: utf-8 -*-
from xiprs import app
from sched_module import sched

if __name__ == "__main__":
  sched.start()

  app.run(host='0.0.0.0', port='20005')
