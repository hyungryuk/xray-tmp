import {injectable} from "inversify";
import {language} from '../../../language/languageProperties';
import {errorMessage} from '../../properties/errorMessage';
import {properties} from '../../properties/properties';

export interface StaticLanguageInterface{
    languageType : string;
    getTextUsingKey(key:string):string;
}

@injectable()
export class StaticLanguageWithCsv implements StaticLanguageInterface {
    languageType:string
    constructor(){
        try{

            let metaTags = document.getElementsByTagName("meta");
            for(var i=0;i<metaTags.length;i++){
                if(metaTags[i].hasAttribute("language")){
                    this.languageType = metaTags[i].getAttribute("language");
                    break;
                }
            }

        }catch(err){
            location.href=properties["WEB_PATH"]+""
        }
    }
    getTextUsingKey(key:string){
        return language[this.languageType][key];
    }
}
