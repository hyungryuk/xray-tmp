import * as React from 'react'
import * as ReactDOM from 'react-dom'
import {StaticLanguageInterface} from '../others/staticLanguageData/staticLanguageData'
import {myContainer} from '../DIContainerConf/inversify.config'
import {TYPES} from '../DIContainerConf/types'
import {properties} from '../properties/properties'


class Menu extends React.Component<{}, {userAuth:number,isOpened:boolean}>{
    constructor(props:any) {
        super(props);
        this.state = {
            userAuth:undefined,
            isOpened:false
        };
        this.menuOnClick = this.menuOnClick.bind(this);
    }

    menuOnClick(e:any){
        this.setState(prevState =>({
            isOpened:!prevState.isOpened
        }));
    }

    componentDidMount(){
        var metaTags = document.getElementsByTagName("meta");
        for(var i=0;i<metaTags.length;i++){
            if(metaTags[i].hasAttribute("userAuth")){
                this.setState({
                    userAuth:parseInt(metaTags[i].getAttribute("userAuth"))
                });
                break;
            }
        }
    }


    render(){
        if(this.state.userAuth==0 && this.state.isOpened==true){
            return(
                <div className="main_menu_box" onClick={this.menuOnClick}>
                    <div className="menuSymbol"><i className="fas fa-bars"></i></div>
                    <div className="main_menu">
                        <div className="root_menu_box">
                            <div className="root_menu">
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_dash_root_name")}
                            </div>
                            <div className="first_level_sub_menu_box">
                                <div className="first_level_sub_menu" onClick={function() {location.href="/web/dashGraph"}}>
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_dashgraph_name")}
                                </div>
                                <div className="first_level_sub_menu" onClick={function() {location.href="/web/dashTable"}}>
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_dashtable_name")}
                                </div>
                            </div>
                        </div>
                        <div className="root_menu_box">
                            <div className="root_menu">{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_file_root_name")}</div>
                            <div className="first_level_sub_menu_box">
                                <div className="first_level_sub_menu" onClick={function() {location.href="/web/imageFileTree"}}>
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_filetree_name")}
                                </div>
                                <div className="first_level_sub_menu" onClick={function() {location.href="/web/imageFileTable"}}>
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_filetable_name")}
                                </div>
                                <div className="first_level_sub_menu" onClick={function() {location.href="/web/imageFileLabeling"}}>
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_filelabel_name")}
                                </div>
                            </div>
                        </div>
                        <div className="root_menu_box">
                            <div className="root_menu">{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_model_root_name")}</div>
                            <div className="first_level_sub_menu_box">
                                <div className="first_level_sub_menu" onClick={function() {location.href="/web/modelTraining"}}>
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_trainmodel_name")}
                                </div>
                                <div className="first_level_sub_menu" onClick={function() {location.href="/web/modelStatus"}}>
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_traintable_name")}
                                </div>
                                <div className="first_level_sub_menu" onClick={function() {location.href="/web/modelDeploy"}}>
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_deploymodel_name")}
                                </div>
                            </div>
                        </div>
                        <div className="root_menu_box">
                            <div className="root_menu">{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_setting_root_name")}</div>
                            <div className="first_level_sub_menu_box">
                                <div className="first_level_sub_menu" onClick={function() {location.href="/web/userSetting"}}>
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_usersetting_name")}
                                </div>
                                <div className="first_level_sub_menu" onClick={function() {location.href="/web/lineSetting"}}>
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_linesetting_name")}
                                </div>
                                <div className="first_level_sub_menu" onClick={function() {location.href="/web/algorithmSetting"}}>
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_algorithmsetting_name")}
                                </div>
                                <div className="first_level_sub_menu" onClick={function() {location.href="/web/filterSetting"}}>
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_filtersetting_name")}
                                </div>
                                <div className="first_level_sub_menu" onClick={function() {location.href="/web/logManaging"}}>
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_logmanager_name")}
                                </div>
                                <div className="first_level_sub_menu">
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_enviromentsetting_name")}
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            );
        }else if(this.state.userAuth==1 && this.state.isOpened==true){
            return(
                <div className="main_menu_box" onClick={this.menuOnClick}>
                    <div className="menuSymbol"><i className="fas fa-bars"></i></div>
                    <div className="main_menu">

                        <div className="root_menu_box">
                            <div className="root_menu">{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_dash_root_name")}</div>
                            <div className="first_level_sub_menu_box">
                                <div className="first_level_sub_menu">
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_dashgraph_name")}
                                </div>
                                <div className="first_level_sub_menu">
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_dashtable_name")}
                                </div>
                            </div>
                        </div>
                        <div className="root_menu_box">
                            <div className="root_menu">{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_file_root_name")}</div>
                            <div className="first_level_sub_menu_box">
                                <div className="first_level_sub_menu">
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_filetree_name")}
                                </div>
                                <div className="first_level_sub_menu">
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_filetable_name")}
                                </div>
                                <div className="first_level_sub_menu">
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_filelabel_name")}
                                </div>
                            </div>
                        </div>
                        <div className="root_menu_box">
                            <div className="root_menu">{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_model_root_name")}</div>
                            <div className="first_level_sub_menu_box">
                                <div className="first_level_sub_menu">
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_trainmodel_name")}
                                </div>
                                <div className="first_level_sub_menu">
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_traintable_name")}
                                </div>
                                <div className="first_level_sub_menu">
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_deploymodel_name")}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            );
        }else{
            return(
                <div className="main_menu_box" onClick={this.menuOnClick}>
                    <div className="menuSymbol"><i className="fas fa-bars"></i></div>
                </div>
            );
        }

    }

}

class TitleComponent extends React.Component{
    render(){
        return(
            <div className="main_title">
                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("common_main_title")}
            </div>
        );
    }
}

class LogoutComponent extends React.Component{
    logoutOnClick(){
        $.ajax({
            url:properties["WEB_PATH"]+"/web/do_logout",
            type:'post',
        });

        location.href="/";
    }
    render(){
        return(
            <div className="logout_box">
                <div className="logout" onClick={this.logoutOnClick}>
                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("logout_sign")}
                </div>
            </div>
        );
    }
}

class CurrentUserComponent extends React.Component<{}, {username:string}>{
    constructor(props:any){
        super(props);
        this.state={
            username:''
        }
    }
    componentDidMount(){
        var metaTags = document.getElementsByTagName("meta");
        for(var i=0;i<metaTags.length;i++){
            if(metaTags[i].hasAttribute("username")){
                this.setState({
                    username:metaTags[i].getAttribute("username")
                });
                break;
            }
        }
    }
    render(){
        return(
            <div className="user_name">
                {this.state.username}
            </div>
        );
    }
}
export class HeaderComponent extends React.Component{
    render(){
        return(
            <div className="main_title_Box">
                <Menu />
                <TitleComponent />
                <CurrentUserComponent />
                <LogoutComponent />
            </div>
        );
    }
}
