###############
# how to use??
# 1. make CSV_COLUMS_INDEX list same as csv first row
# 2. run
# 3. result files will be created in LANGUAGE_JS_FILE_DIR_FROM_PROJECT_ROOT_PATH
###############

import csv
import os
from pathlib import Path
import codecs

def get_all_comma_seperated_items_in_file(path):

    comma_seperated_items=[]

    with open(path, newline='') as csv_file:
        csv_contents = csv.reader(csv_file,delimiter=',',quotechar='|')

        for row in csv_contents:
            
            for item in row:
                comma_seperated_items.append(item)

    return comma_seperated_items




PROJECT_ROOT_PATH = Path(__file__).parents[1]
CSV_FILE_PATH_FROM_PROJECT_ROOT_PATH = "./static/language/language.csv"
CSV_FILE_ABSOLUTE_PATH = os.path.join(PROJECT_ROOT_PATH, CSV_FILE_PATH_FROM_PROJECT_ROOT_PATH)

comma_seperated_items = get_all_comma_seperated_items_in_file(CSV_FILE_ABSOLUTE_PATH)

CSV_FIRST_ROW = ["key","english","korean","chinese"]



def structured_csv_items_with_each_language(csv_items):

    structured_csv_items_result = {}

    for one_language in CSV_FIRST_ROW:
        structured_csv_items_result[one_language]={}

    for i in range(0,len(csv_items),len(CSV_FIRST_ROW)):
        for j in range(1,len(CSV_FIRST_ROW)):
            try :

                structured_csv_items_result[CSV_FIRST_ROW[(i+j)%len(CSV_FIRST_ROW)]][csv_items[i]] = csv_items[i+j]
            except Exception:
                print("%d %d" %(i,j))
                print(i+j)

    return structured_csv_items_result


structured_csv_items_result = structured_csv_items_with_each_language(comma_seperated_items)

RESULT_LANGUAGE_JS_FILE_DIR_FROM_PROJECT_ROOT_PATH = "./static/language"

LAGUAGE_JS_FILE_NAME = "languageProperties.js"



def make_language_js_file_with_structured_languages(structured_languages):

    language_file_root_path = os.path.join(PROJECT_ROOT_PATH, RESULT_LANGUAGE_JS_FILE_DIR_FROM_PROJECT_ROOT_PATH)
    with codecs.open(os.path.join(language_file_root_path, LAGUAGE_JS_FILE_NAME), 'w+','utf-8') as file:

        file.write("export var language = {\n")

        for one_language in CSV_FIRST_ROW:

            if one_language!=CSV_FIRST_ROW[0]:

                file.write("\t"+structured_languages[one_language]["key"]+" : { ")
                file.write("\n\t\t")

                for key in structured_languages[one_language]:

                    file.write("\t"+key+" : \""+structured_languages[one_language][key]+"\"")
                    file.write(",")
                    file.write("\t\n\t")

                file.seek(-2, os.SEEK_END)
                file.truncate()
                file.write("\n\t},\n\n")

        file.write("\n}")




make_language_js_file_with_structured_languages(structured_csv_items_result)
