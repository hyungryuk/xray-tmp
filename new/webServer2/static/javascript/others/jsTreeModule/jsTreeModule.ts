import 'jstree';
import { LineAjaxCall,ImageFileAjaxCall } from '../restCall/dbRestcall';
import { OpenImageRestCall } from '../restCall/othersRestcall';
import {properties} from '../../properties/properties';

var selectedPath:string;

export var jsTreeModule = {

	reportMenu : function(node:any){
		return {
			deleteItem: {
				"label": "Remove",
				"action": function(obj:any) {
                        var ImageFileAjaxCallObj = new ImageFileAjaxCall();
                        ImageFileAjaxCallObj.delete([{"isremoved":1}],[{"filepath":selectedPath}]);
						$("#tree").jstree(true).delete_node(node);

				}
			}

		};
	},

	setDoubleClickFunc : function(divId:string,imageDivId:string){

		$('#'+divId).bind("dblclick.jstree", function (data) {
            var OpenImageRestCallObj = new OpenImageRestCall();
			var image = OpenImageRestCallObj.openImageWithJPG([selectedPath]);
			image[0].setAttribute("class","image");
			image[0].setAttribute("id",selectedPath);
			document.getElementById(imageDivId).innerHTML="";
			document.getElementById(imageDivId).appendChild(image[0]);
		});

	},



	setOnChangeFunction : function(divId:string){

		$('#'+divId).on("changed.jstree", function (e, data) {
			if(data.selected[0]!=undefined){
				var imagepath = data.selected[0].replace(/@/gi, properties["DIRECTORY_SEPERATOR"]);
				selectedPath = imagepath;
			}

		});

	},


	setLazyLoadingJsTree : function(divId:string, urlToGetTreeData:string){



		if(!document.getElementById(divId)){

				return false;
		}else{

				$('#'+divId).jstree({
				'core' : {
					'check_callback': true,
					'data' : {
						'url' : function (node:any) {

							if(node.id === '#'){
									return urlToGetTreeData+"?nodeId=0";
							}else{
									return urlToGetTreeData+"?nodeId="+node.id;
							}

						},
						'data' : function (node:any) {
							return { 'id' : node.id };
						},

					}
				},
				'plugins': ["contextmenu"],
				"contextmenu": {
					"items": jsTreeModule.reportMenu      //builds context menu based on selected node
				}

				});

				return true;

		}

	}

}
