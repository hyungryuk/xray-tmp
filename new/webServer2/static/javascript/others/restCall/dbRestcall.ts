import {properties} from '../../properties/properties'

class AjaxCall {
    url:string
    constructor(url:string){
        this.url = url;
    }
    create(items: {[key: string]: any}[], options: {[key: string]: any}[]):{[key:string]:any[]}{
      var responsejson: any
      let rquestjson: string

      rquestjson = JSON.stringify({items: items, method_type: 'insert', options: options})

      $.ajax({
        async: false,
        url: this.url,
        type: 'post',
        data: rquestjson,
        accepts: {
          text: 'application/json'
        },
        contentType: 'application/json',
        success: function(data) {
          data = JSON.parse(data)
          responsejson = data
        },
        error: function(jqXHR, exception) {}
      })
      return responsejson
    }
    delete(items: {[key: string]: any}[], options: {[key: string]: any}[]):{[key:string]:any[]} {
      var responsejson: any
      let rquestjson: string

      rquestjson = JSON.stringify({items: items, method_type: 'delete', options: options})

      $.ajax({
        async: false,
        url: this.url,
        type: 'post',
        data: rquestjson,
        accepts: {
          text: 'application/json'
        },
        contentType: 'application/json',
        success: function(data) {
          data = JSON.parse(data)
          responsejson = data
        },
        error: function(jqXHR, exception) {}
      })
      return responsejson
    }
    update(items: {[key: string]: any}[], options: {[key: string]: any}[]):{[key:string]:any[]} {
      var responsejson: any
      let rquestjson: string

      rquestjson = JSON.stringify({items: items, method_type: 'update', options: options})

      $.ajax({
        async: false,
        url: this.url,
        type: 'post',
        data: rquestjson,
        accepts: {
          text: 'application/json'
        },
        contentType: 'application/json',
        success: function(data) {
          data = JSON.parse(data)
          responsejson = data
        },
        error: function(jqXHR, exception) {}
      })
      return responsejson
    }
    retrieve(items: {[key: string]: any}[], options: {[key: string]: any}[]):{[key:string]:any[]} {
      var responsejson: any
      let rquestjson: string

      rquestjson = JSON.stringify({items: items, method_type: 'view', options: options})

      $.ajax({
        async: false,
        url: this.url,
        type: 'post',
        data: rquestjson,
        accepts: {
          text: 'application/json'
        },
        contentType: 'application/json',
        success: function(data) {
          data = JSON.parse(data)
          responsejson = data
        },
        error: function(jqXHR, exception) {}
      })
      return responsejson
    }
}

export class LineAjaxCall extends AjaxCall {
  constructor() {
      super(properties["WEB_PATH"]+"/rest/db/line");
  }
}

export class ImageFileAjaxCall extends AjaxCall {
    constructor() {
        super(properties["WEB_PATH"]+"/rest/db/imagefile");
    }
}

export class StatisticDataAjaxCall extends AjaxCall {
    constructor() {
        super(properties["WEB_PATH"]+"/rest/db/statistics");
    }
}

export class AlgorithmDataAjaxCall extends AjaxCall {
    constructor() {
        super(properties["WEB_PATH"]+"/rest/db/algorithm");
    }
}

export class FilterDataAjaxCall extends AjaxCall {
    constructor() {
        super(properties["WEB_PATH"]+"/rest/db/filter");
    }
}

export class TrainingDataAjaxCall extends AjaxCall {
    constructor() {
        super(properties["WEB_PATH"]+"/rest/db/training");
    }
}

export class ModelDataAjaxCall extends AjaxCall {
    constructor() {
        super(properties["WEB_PATH"]+"/rest/db/model");
    }
}
export class UserDataAjaxCall extends AjaxCall {
    constructor() {
        super(properties["WEB_PATH"]+"/rest/db/user");
    }
}
export class LogDataAjaxCall extends AjaxCall {
    constructor() {
        super(properties["WEB_PATH"]+"/rest/db/log");
    }
}

export class DeployDataAjaxCall extends AjaxCall {
    constructor() {
        super(properties["WEB_PATH"]+"/rest/db/deploy");
    }
}
