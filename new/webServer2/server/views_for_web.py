from server import app
from flask import render_template,redirect,request
from flask_login import current_user, login_required

@app.route("/web/dashGraph")
@login_required
def dash_graph_page():
    return render_template('dashGraphPage.html', user=current_user)

@app.route("/web/dashTable")
@login_required
def dash_table_page():
    return render_template('dashTablePage.html', user=current_user)

@app.route("/web/imageFileTable")
@login_required
def imageFile_table_page():
    return render_template('imageFileTablePage.html', user=current_user)

@app.route("/web/imageFileTree")
@login_required
def imageFile_tree_page():
    return render_template('imageFileTreePage.html', user=current_user)







@app.route("/web/imageFileLabeling")
@login_required
def imageFile_labeling_page():
    return render_template('imageFileLabelingPage.html', user=current_user)

@app.route("/web/modelTraining")
@login_required
def model_train_page():
    return render_template('modelTrainingPage.html', user=current_user)

@app.route("/web/modelStatus")
@login_required
def model_status_page():
    return render_template('modelStatusPage.html', user=current_user)

@app.route("/web/modelDeploy")
@login_required
def model_deploy_page():
    return render_template('modelDeployPage.html', user=current_user)

@app.route("/web/userSetting")
@login_required
def user_setting_page():
    return render_template('userSettingPage.html', user=current_user)

@app.route("/web/lineSetting")
@login_required
def line_setting_page():
    return render_template('lineSettingPage.html', user=current_user)

@app.route("/web/algorithmSetting")
@login_required
def algorithm_setting_page():
    return render_template('algorithmSettingPage.html', user=current_user)

@app.route("/web/filterSetting")
@login_required
def filter_setting_page():
    return render_template('filterSettingPage.html', user=current_user)

@app.route("/web/logManaging")
@login_required
def log_managing_page():
    return render_template('logManagingPage.html', user=current_user)

@app.route('/web/errorpage', methods = ['GET'])
@login_required
def error_page():
    get_data = request.args
    return render_template('errorPage.html', error_code=get_data.get("errorcode"), error_message=get_data.get("errormessage"), error_cause=get_data.get("errorcause"))

@app.route('/')
def home():
    return redirect("/web/login")

@app.route('/web/login')
def login():
    try:
        if current_user.userid:
            return render_template('dashGraphPage.html', user=current_user)
        else :
            return render_template('login.html')
    except:
        return render_template('login.html')
