import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as Chartjs from 'chart.js'

import { HeaderComponent } from "../modulesForPages/header";
import {LineSelectorInterface} from '../others/selector/selector'
import {StaticLanguageInterface} from '../others/staticLanguageData/staticLanguageData'
import {myContainer} from '../DIContainerConf/inversify.config'
import {TYPES} from '../DIContainerConf/types'

import { LineAjaxCall,StatisticDataAjaxCall } from '../others/restCall/dbRestcall'
import {encoder} from "../others/encoder/encoder";

Date.prototype.yyyymmddhhmmss = function() {
    var mm = this.getMonth() + 1;
    var dd = this.getDate();
    var hh = this.getHours();
    var minutes = this.getMinutes();
    var ss = this.getSeconds();

    return [
        this.getFullYear(),
        (mm > 9 ? "" : "0") + mm,
        (dd > 9 ? "" : "0") + dd,
        (hh > 9 ? "" : "0") + hh,
        (minutes > 9 ? "" : "0") + minutes,
        (ss > 9 ? "" : "0") + ss
    ].join("");
};


ReactDOM.render(
  <HeaderComponent />,
  document.getElementById("header")
);



class MainComponent extends React.Component<{}, {lines:any[]}>{
    constructor(props:any){
        super(props);
        let lineRestCall = new LineAjaxCall()
        let lineRestCallRes = lineRestCall.retrieve([],[]);
        this.state = {
            lines:lineRestCallRes["items"],

        };
    }

    componentDidMount(){

    }



    render(){
        const chartTags = this.state.lines.map((line) =>
            <div className="line_box">
                <div className="line_name">
                    {line.factory +" "+line.line}
                </div>
                <div className="charts_in_line_box">
                    <DashChart categoryType="daily" linename={line.line} linecode={line.linecode}/>
                    <DashChart categoryType="hourly" linename={line.line} linecode={line.linecode}/>
                </div>
            </div>
         );
        return(
            <div>
                <div className="current_page_info">
                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_dash_root_name")}
                    <i className="fas fa-angle-right"></i>
                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_dashgraph_name")}
                </div>
                {chartTags}
            </div>
        );
    }


}
class DashChart extends React.Component<{categoryType:string,linecode:number,linename:string}, {}> {
    constructor(props:any) {
        super(props);
    }

    getStatisticsData(type:string,linecode:number) {
        var date = new Date();
        var weekAgoDate = new Date();
        var dayAgoDate = new Date();

        weekAgoDate.setDate(date.getDate() - 6);
        dayAgoDate.setHours(date.getHours() - 24);

        let StatisticDataRestCall = new StatisticDataAjaxCall()

        if(type=="daily"){
            var staticticsResultForDailyChart = StatisticDataRestCall.retrieve(
                [],
                [
                    {linecode:linecode,kind: 1},
                    {
                        startdate: weekAgoDate.yyyymmddhhmmss(),
                        enddate: date.yyyymmddhhmmss()
                    }
                ]
            );
            return staticticsResultForDailyChart["items"];
        }else{
            var staticticsResultForHourlyChart = StatisticDataRestCall.retrieve(
                [],
                [
                    {linecode:linecode,kind: 0 },
                    {
                        startdate: dayAgoDate.yyyymmddhhmmss(),
                        enddate: date.yyyymmddhhmmss()
                    }
                ]
            );
            return staticticsResultForHourlyChart["items"];
            
        }
    }


    componentDidMount() {
        var dateLabel = [];
        var datas:{[key:string]:number[]} = {};
        if (this.props.categoryType == "daily") {
            var todayForLabel = new Date();
            dateLabel.push(todayForLabel.getDate());

            for (var i = 0; i < 6; i++) {
                todayForLabel.setDate(todayForLabel.getDate() - 1);
                dateLabel.push(todayForLabel.getDate());
            }
            dateLabel.reverse();

            datas["ok"] = [0, 0, 0, 0, 0, 0, 0];
            datas["ng"] = [0, 0, 0, 0, 0, 0, 0];
            datas["nr"] = [0, 0, 0, 0, 0, 0, 0];

            var graphData = this.getStatisticsData("daily",this.props.linecode);
            for (var i = 0; i < graphData.length; i++) {
                for (var j = 0; j < dateLabel.length; j++) {
                    if (
                        graphData[i].date.substring(8, 10) ==
                        dateLabel[j]
                    ) {
                        datas["ng"][j] = graphData[i].ngcount;
                        datas["ok"][j] = graphData[i].okcount;
                        datas["nr"][j] = graphData[i].nrcount;
                    }
                }
            }
        } else {
            var todayForLabel = new Date();
            dateLabel.push(todayForLabel.getHours());

            for (var i = 0; i < 23; i++) {
                todayForLabel.setHours(todayForLabel.getHours() - 1);
                dateLabel.push(todayForLabel.getHours());
            }
            dateLabel.reverse();

            datas["ok"] = [
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0
            ];
            datas["ng"] = [
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0
            ];
            datas["nr"] = [
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0
            ];

            var graphData = this.getStatisticsData("hourly",this.props.linecode);
            for (var i = 0; i < graphData.length; i++) {
                for (var j = 0; j < dateLabel.length; j++) {
                    if (
                        graphData[i].date.substring(11, 13) ==
                        dateLabel[j]
                    ) {
                        datas["ng"][j] = graphData[i].ngcount;
                        datas["ok"][j] = graphData[i].okcount;
                        datas["nr"][j] = graphData[i].nrcount;
                    }
                }
            }
        }

        //line chart data
        var dataObject = {
            labels: dateLabel,
            datasets: [
                {
                    label: "OK",
                    data: datas["ok"],
                    backgroundColor: "green",
                    borderColor: "lightgreen",
                    fill: false,
                    lineTension: 0,
                    radius: 5
                },
                {
                    label: "NG",
                    data: datas["ng"],
                    backgroundColor: "red",
                    borderColor: "lightred",
                    fill: false,
                    lineTension: 0,
                    radius: 5
                },
                {
                    label: "NR",
                    data: datas["nr"],
                    backgroundColor: "gray",
                    borderColor: "lightgray",
                    fill: false,
                    lineTension: 0,
                    radius: 5
                }
            ]
        };

        var chart_title = "";
        if(this.props.categoryType=="daily"){
            chart_title = myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("daily_chart_name");
        }else{
            chart_title = myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("hourly_chart_name");
        }
        var optionsObject = {
            responsive: true,
            title: {
                display: true,
                position: "top",
                text: chart_title,
                fontSize: 18,
                fontColor: "#111"
            },
            legend: {
                display: true,
                position: "bottom",
                labels: {
                    fontColor: "#333",
                    fontSize: 16
                }
            }
        };
        var ctx = $("#"+this.props.linecode+"_"+this.props.categoryType);
        new Chartjs.Chart(ctx, {
            type: "bar",
            data: dataObject,
            options: optionsObject
        });
    }

    render() {
        return (
            <div>
                <div className="chart-container">
                    <canvas id={this.props.linecode +"_"+this.props.categoryType} />
                </div>
            </div>
        );
    }
}

ReactDOM.render(
    <div>
        <MainComponent />
    </div>,
  document.getElementById("main")
);
