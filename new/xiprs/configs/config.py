﻿import os

class BaseConfig(object):
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://xray:xray1234@xray.cywt7zffil4m.us-east-2.rds.amazonaws.com/ski_xray?charset=utf8'
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True


class DevelopmentConfig(BaseConfig):
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://xray:xray@1234@192.168.0.112/ski_xray?charset=utf8'
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True


class MacDevelopmentConfig(BaseConfig):
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://ski:xray@1234@localhost/ski_xray?charset=utf8'
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True



FILE_TREE_ROOT_PATH = os.getenv('FILE_TREE_ROOT_PATH', 'C:\\xray\\fileTree')
FILE_TREE_ROOT_ID = os.getenv('FILE_TREE_ROOT_ID', 'C:@xray@fileTree')
FILE_TREE_SEPERATOR = os.getenv('FILE_TREE_SEPERATOR', '\\')
CSV_FILE_PATH_AND_NAME = os.getenv('CSV_FILE_PATH', 'C:\\xray\\tmpCSV\\temp.csv')
FILE_ID_SEPERATOR = os.getenv('FILE_ID_SEPERATOR', '@')
ALGORITHM_PATH = os.getenv('ALGORITHM_PATH', 'C:\\xray\\algorithms\\')
FILTER_PATH = os.getenv('FILTER_PATH', 'C:\\xray\\filters\\')

ML_API_PATH = os.getenv('ML_API_PATH', 'http://localhost:10002')

config = {
    "development": "xiprs.configs.config.DevelopmentConfig",
    "mac_development": "xiprs.configs.config.MacDevelopmentConfig",
    "product": "xiprs.configs.config.BaseConfig",
}

def configure_app(app):
    config_name = os.getenv('FLASK_CONFIGURATION', 'development')
    app.config.from_object(config[config_name])
