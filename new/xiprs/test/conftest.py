import json
import pytest
from .. import app


@pytest.fixture
def client(request):
    test_client = app.test_client()

    def teardown():
        pass

    request.addfinalizer(teardown)
    return test_client


def post_json(client, url, json_dict):
    return client.post(url, data=json.dumps(json_dict), content_type='application/json')

def get_json(client, url):
    return client.get(url)

def json_of_response(response):
    """
    Decode json from response
    :param response:
    :return:
    """
    return json.loads(response.data.decode('utf8'))
