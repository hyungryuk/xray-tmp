import "reflect-metadata";
import { Container } from "inversify";
import { TYPES } from "./types";
import { StaticLanguageInterface,StaticLanguageWithCsv } from "../others/staticLanguageData/staticLanguageData";
import{ LineSelectorInterface,LineSelector,AlgorithmSelectorInterface,AlgorithmSelector,FilterSelectorInterface,FilterSelector } from "../others/selector/selector";

const myContainer = new Container();
myContainer.bind<StaticLanguageInterface>(TYPES.StaticLanguageInterface).to(StaticLanguageWithCsv);
myContainer.bind<LineSelectorInterface>(TYPES.LineSelectorInterface).to(LineSelector);
myContainer.bind<AlgorithmSelectorInterface>(TYPES.AlgorithmSelectorInterface).to(AlgorithmSelector);
myContainer.bind<FilterSelectorInterface>(TYPES.FilterSelectorInterface).to(FilterSelector);

export { myContainer };
