"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.errorMessage = {};
exports.errorMessage = {
    HEADER_MUST_HAVE_LANGUAGE: {
        errorcode: 20001,
        message: 'All HTML file must have <meta language={{language}}> tag in <head> and this {{language}} must be on of first row\'s item in language.csv file'
    }
};
