# -*-coding: utf-8 -*-

from ..configs.config import *
from .messages import *

import csv
import os
from flask import json
import requests as rq
from random import randint

def make_input_csv(modelcode,input):
    try:

        result = rq.post(REST_SERVER_URL+'/rest/trn',json={'items': [{"contents":"making input csv "}],
                           'method_type': 'update',
                           'options': [{"modelcode":modelcode}]})

        csv_row_list = input.get("csvrowlist")

        os.makedirs(INPUT_CSV_DIR % (modelcode))
        with open(INPUT_CSV_DIR % (modelcode)+"inputCsv.csv",'w+')as csvfile:
            filewriter = csv.writer(csvfile, delimiter=',',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)

            tmp_list=[]
            row_index = 0
            index_filename=0
            index_filepath=0

            for row in csv_row_list:
                row_splitted = row.split(",")
                index = 0
                filename = ""
                if row_index==0:
                    for splitted in row_splitted:
                        splitted = splitted.replace("'","")
                        if splitted == "filename":
                            index_filename = index
                            tmp_list.append(splitted)
                        elif splitted == "filepath":
                            index_filepath = index
                            tmp_list.append(splitted)
                        else:
                            tmp_list.append(splitted)
                        index += 1
                    filewriter.writerow(tmp_list)

                else :
                    for splitted in row_splitted:

                        splitted = splitted.replace("'","")

                        if index == index_filepath:
                            if len(filename.split("."))>1:
                                tmp_list.append(IMG_DOWNLOAD_DIR % (modelcode,filename))
                            else:
                                filepath_comma_sep = splitted.split(".")
                                tmp_list.append(IMG_DOWNLOAD_DIR % (modelcode,filename+"."+filepath_comma_sep[1]))
                        elif index == index_filename:
                            filename = splitted
                            tmp_list.append(splitted)
                        else :
                            tmp_list.append(splitted)
                        index+=1
                    filewriter.writerow(tmp_list)
                tmp_list=[]

                row_index+=1

            csvfile.close()

        message = MAKEFILE_CSV_SUCCESS
        message.update({'modelcode': modelcode})

        result = rq.post(REST_SERVER_URL+'/rest/trn',json={'items': [{"contents":"complete to make input csv"}],
                           'method_type': 'update',
                           'options': [{"modelcode":modelcode}]})

        return message

    except Exception as e:
        result = rq.post(REST_SERVER_URL+'/rest/trn',json={'items': [{"contents":"error occured to make input csv"}],
                           'method_type': 'update',
                           'options': [{"modelcode":modelcode}]})
        message = MAKEFILE_CSV_SUCCESS_FAIL
        message.update({'cause': str(e)})
        print(message)
        return message





def make_tmp_csv(modelcode,input):
    try:

        result = rq.post(REST_SERVER_URL+'/rest/trn',json={'items': [{"contents":"make tmp csv"}],
                           'method_type': 'update',
                           'options': [{"modelcode":modelcode}]})

        csv_row_list = input.get("csvrowlist")

        os.makedirs(TMP_CSV_DIR % (modelcode))
        print("dir done")

        with open(TMP_CSV_DIR % (modelcode) +"tmpCsv.csv",'w+')as csvfile:
            filewriter = csv.writer(csvfile, delimiter=',',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)

            tmp_list=[]
            for row in csv_row_list:
                row_splitted = row.split(",")
                for splitted in row_splitted:
                    splitted = splitted.replace("'","")
                    tmp_list.append(splitted)
                filewriter.writerow(tmp_list)
                tmp_list=[]

            csvfile.close()

        message = MAKEFILE_CSV_SUCCESS
        message.update({'modelcode': modelcode})

        result = rq.post(REST_SERVER_URL+'/rest/trn',json={'items': [{"contents":"make tmp csv done"}],
                           'method_type': 'update',
                           'options': [{"modelcode":modelcode}]})

        return message

    except Exception as e:
        result = rq.post(REST_SERVER_URL+'/rest/trn',json={'items': [{"contents":"error occured in make tmp csv "}],
                           'method_type': 'update',
                           'options': [{"modelcode":modelcode}]})
        message = MAKEFILE_CSV_SUCCESS_FAIL
        message.update({'cause': str(e)})
        print(message)
        return message
