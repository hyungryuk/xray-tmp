# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 10:48:59 2018

@author: ryuk
"""
from .. import db

def create_line(item):
    db.session.add(item)

class LineInfo(db.Model):
    __tablename__ = 'iis_line'
    __table_args__ = {'mysql_collate': 'utf8_general_ci', 'mysql_engine': 'MyISAM'}

    linecode = db.Column(db.Integer, primary_key=True, nullable=False)
    factory = db.Column(db.VARCHAR(64), nullable=False)
    line = db.Column(db.VARCHAR(64), nullable=False)
    ipaddr = db.Column(db.VARCHAR(32), nullable=False)
    regdate = db.Column(db.DateTime, nullable=True)
    modelcode = db.Column(db.VARCHAR(16), nullable=False)

    def __init__(self, line_code, factory, line, ipaddr):
        print(line_code)
        self.linecode = line_code
        self.factory = factory
        self.line = line
        self.ipaddr = ipaddr
        self.modelcode = ""

    def __repr__(self):
        return repr((self.linecode, self.factory, self.line, self.ipaddr, self.regdate, self.modelcode))
