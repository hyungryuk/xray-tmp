from pytz import utc

from apscheduler.schedulers.background import BackgroundScheduler


from datetime import datetime
from datetime import timedelta
import calendar
from dateutil.relativedelta import relativedelta

from xiprs import db

from xiprs.model.imgmdl import *
from xiprs.model.sttmdl import *


sched = BackgroundScheduler()


def gather_yearly_statistics_info():

    currentDate = datetime.now()

    currentDate_with_starttime = currentDate.replace(hour=0, minute=0,second=0)

    lastyear_starttime=currentDate_with_starttime-relativedelta(years=1)
    lastyear_firstmonth_firstdate_starttime=lastyear_starttime.replace(month=1, day=1, hour=0, minute=0, second=0)

    lastyear_lastmonth_lastdate_endtime = lastyear_starttime.replace(month=12, day=31, hour=23, minute=59,second=59)

    sched.add_job(gather_yearly_statistics_info,'date', run_date=lastyear_lastmonth_lastdate_endtime+relativedelta(years=2))

    get_lastyear_image_db_list = db.session.query(ImageFileInfo).filter(
        ImageFileInfo.filedate >= lastyear_firstmonth_firstdate_starttime, ImageFileInfo.filedate <= lastyear_lastmonth_lastdate_endtime).all()


    lines_ngoptions_sum={}

    for lastyear_image_db in get_lastyear_image_db_list:
        if (lastyear_image_db.linecode) in lines_ngoptions_sum:
            lines_ngoptions_sum[lastyear_image_db.linecode][lastyear_image_db.ng] += 1

        else:
            lines_ngoptions_sum[lastyear_image_db.linecode] = [0, 0, 0]
            lines_ngoptions_sum[lastyear_image_db.linecode][lastyear_image_db.ng] += 1

    for i in lines_ngoptions_sum:
        db_insert_data = StatisticInfo(lastyear_firstmonth_firstdate_starttime,i,lines_ngoptions_sum[i][0],lines_ngoptions_sum[i][1],lines_ngoptions_sum[i][2],4)


        create_statistic(db_insert_data)

        try:
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            db.session.remove()



def gather_monthly_statistics_info():

    currentDate = datetime.now()

    currentDate_with_starttime = currentDate.replace(hour=0, minute=0,second=0)

    lastmonth_starttime=currentDate_with_starttime-relativedelta(months=1)
    lastmonth_firstdate_starttime=lastmonth_starttime.replace(day=1)

    lastmonth_lastdate_endtime = lastmonth_firstdate_starttime.replace(day=calendar.monthrange(lastmonth_firstdate_starttime.year,lastmonth_firstdate_starttime.month)[1],hour=23, minute=59,second=59)

    sched.add_job(gather_monthly_statistics_info,'date', run_date=lastmonth_firstdate_starttime+relativedelta(months=2))

    get_lastmonth_image_db_list = db.session.query(ImageFileInfo).filter(
        ImageFileInfo.filedate >= lastmonth_firstdate_starttime, ImageFileInfo.filedate <= lastmonth_lastdate_endtime).all()


    lines_ngoptions_sum={}

    for lastmonth_image_db in get_lastmonth_image_db_list:
        if (lastmonth_image_db.linecode) in lines_ngoptions_sum:
            lines_ngoptions_sum[lastmonth_image_db.linecode][lastmonth_image_db.ng] += 1

        else:
            lines_ngoptions_sum[lastmonth_image_db.linecode] = [0, 0, 0]
            lines_ngoptions_sum[lastmonth_image_db.linecode][lastmonth_image_db.ng] += 1

    for i in lines_ngoptions_sum:
        db_insert_data = StatisticInfo(lastmonth_firstdate_starttime,i,lines_ngoptions_sum[i][0],lines_ngoptions_sum[i][1],lines_ngoptions_sum[i][2],3)


        create_statistic(db_insert_data)

        try:
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            db.session.remove()


def gather_weekly_statistics_info():

    currentDate = datetime.now()

    currentDate_with_starttime = currentDate.replace(hour=0, minute=0,second=0)
    currentDate_with_endtime = currentDate.replace(hour=23, minute=59,second=59)

    lastweek_monday_startDate=currentDate_with_starttime+timedelta(days=-currentDate.weekday(), weeks=2)
    lastweek_sunday = lastweek_monday_startDate+timedelta(days=6)
    lastweek_sunday_endDate = lastweek_sunday.replace(hour=23, minute=59,second=59)

    sched.add_job(gather_weekly_statistics_info,'date', run_date=lastweek_monday_startDate+timedelta(days=14))

    get_lastweek_image_db_list = db.session.query(ImageFileInfo).filter(
        ImageFileInfo.filedate >= lastweek_monday_startDate, ImageFileInfo.filedate <= lastweek_sunday_endDate).all()


    lines_ngoptions_sum={}

    for lastweek_image_db in get_lastweek_image_db_list:
        if (lastweek_image_db.linecode) in lines_ngoptions_sum:
            lines_ngoptions_sum[lastweek_image_db.linecode][lastweek_image_db.ng] += 1

        else:
            lines_ngoptions_sum[lastweek_image_db.linecode] = [0, 0, 0]
            lines_ngoptions_sum[lastweek_image_db.linecode][lastweek_image_db.ng] += 1

    for i in lines_ngoptions_sum:
        db_insert_data = StatisticInfo(lastweek_monday_startDate,i,lines_ngoptions_sum[i][0],lines_ngoptions_sum[i][1],lines_ngoptions_sum[i][2],2)


        create_statistic(db_insert_data)

        try:
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            db.session.remove()



def gather_daily_statistics_info():

    currentDate = datetime.now()

    currentDate_with_starttime = currentDate.replace(hour=0, minute=0,second=0)
    currentDate_with_endtime = currentDate.replace(hour=23, minute=59,second=59)

    yesterday_startDate=currentDate_with_starttime-timedelta(days=1)
    yesterday_endDate= currentDate_with_endtime-timedelta(days=1)

    sched.add_job(gather_daily_statistics_info,'date', run_date=currentDate_with_starttime+timedelta(days=1))

    get_yesterday_image_db_list = db.session.query(ImageFileInfo).filter(
        ImageFileInfo.filedate >= yesterday_startDate, ImageFileInfo.filedate <= yesterday_endDate).all()


    lines_ngoptions_sum={}

    for yesterday_image_db in get_yesterday_image_db_list:
        if (yesterday_image_db.linecode) in lines_ngoptions_sum:
            lines_ngoptions_sum[yesterday_image_db.linecode][yesterday_image_db.ng] += 1

        else:
            lines_ngoptions_sum[yesterday_image_db.linecode] = [0, 0, 0]
            lines_ngoptions_sum[yesterday_image_db.linecode][yesterday_image_db.ng] += 1

    for i in lines_ngoptions_sum:
        db_insert_data = StatisticInfo(yesterday_startDate,i,lines_ngoptions_sum[i][0],lines_ngoptions_sum[i][1],lines_ngoptions_sum[i][2],1)

        print(db_insert_data)

        create_statistic(db_insert_data)

        try:
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            db.session.remove()





def gather_hourly_statistics_info():

    currentDate = datetime.now()

    currentDate_with_starttime = currentDate.replace(minute=0,second=0)
    currentDate_with_endtime = currentDate.replace(minute=59,second=59)

    pre_hour_startDate=currentDate_with_starttime-timedelta(hours=1)
    pre_hour_endDate= currentDate_with_endtime-timedelta(hours=1)

    sched.add_job(gather_hourly_statistics_info,'date', run_date=currentDate_with_starttime+timedelta(hours=1))

    get_hour_image_db_list = db.session.query(ImageFileInfo).filter(
        ImageFileInfo.filedate >= pre_hour_startDate, ImageFileInfo.filedate <= pre_hour_endDate).all()


    lines_ngoptions_sum={}

    for hour_image_db in get_hour_image_db_list:
        if (hour_image_db.linecode) in lines_ngoptions_sum:
            lines_ngoptions_sum[hour_image_db.linecode][hour_image_db.ng] += 1

        else:
            lines_ngoptions_sum[hour_image_db.linecode] = [0, 0, 0]
            lines_ngoptions_sum[hour_image_db.linecode][hour_image_db.ng] += 1

    for i in lines_ngoptions_sum:
        db_insert_data = StatisticInfo(pre_hour_startDate,i,lines_ngoptions_sum[i][0],lines_ngoptions_sum[i][1],lines_ngoptions_sum[i][2],0)

        print(db_insert_data)

        create_statistic_daily(db_insert_data)

        try:
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            db.session.remove()






currentDate = datetime.now()
scheduler_run_time = currentDate+timedelta(seconds=2)

sched.add_job(gather_yearly_statistics_info, 'date', run_date=scheduler_run_time)
sched.add_job(gather_monthly_statistics_info, 'date', run_date=scheduler_run_time)
sched.add_job(gather_weekly_statistics_info, 'date', run_date=scheduler_run_time)
sched.add_job(gather_daily_statistics_info, 'date', run_date=scheduler_run_time)
sched.add_job(gather_hourly_statistics_info, 'date', run_date=scheduler_run_time)
