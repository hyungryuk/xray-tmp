# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 10:48:59 2018

@author: ryuk
"""
from .. import db

def create_log(item):
    db.session.add(item)

class LogInfo(db.Model):
    __tablename__ = 'iis_log'
    __table_args__ = {'mysql_collate': 'utf8_general_ci', 'mysql_engine': 'MyISAM'}

    kind = db.Column(db.Integer, nullable=False)
    logdate = db.Column(db.DateTime, primary_key=True, nullable=False)
    title = db.Column(db.VARCHAR(128), nullable=False)
    contents = db.Column(db.VARCHAR(256), nullable=False)

    def __init__(self, kind, title, contents):
        self.kind = kind
        self.title = title
        self.contents = contents

    def __repr__(self):
        return repr((self.kind, self.logdate, self.title, self.content))
