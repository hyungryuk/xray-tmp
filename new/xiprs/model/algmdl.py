# -*- coding: utf-8 -*-

from .. import db

def create_algorithm(item):
    db.session.add(item)

class AlgorithmInfo(db.Model):
    __tablename__ = 'iis_algorithm'
    __table_args__ = {'mysql_collate': 'utf8_general_ci','mysql_engine':'MyISAM'}

    algorithmcode = db.Column(db.VARCHAR(16), primary_key=True, nullable=False)
    algorithmname = db.Column(db.VARCHAR(128), nullable=False)
    version = db.Column(db.VARCHAR(16), nullable=False)
    fullpath = db.Column(db.VARCHAR(256), nullable=False)
    available = db.Column(db.Boolean, nullable=False)
    regdate = db.Column(db.DateTime, nullable=True)

    def __init__(self,algorithmcode, algorithm_name, version, full_path, available):
        self.algorithmcode = algorithmcode
        self.algorithmname = algorithm_name
        self.version = version
        self.fullpath = full_path
        self.available = available


    def __repr__(self):
        return repr((self.algorithmcode, self.algorithmname, self.version, self.fullpath, self.available, self.regdate))
