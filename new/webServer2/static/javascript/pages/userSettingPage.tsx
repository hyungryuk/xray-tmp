import * as React from 'react';
import * as ReactDOM from 'react-dom';
import PopPop from 'react-poppop';

import { HeaderComponent } from "../modulesForPages/header";
import {StaticLanguageInterface} from '../others/staticLanguageData/staticLanguageData'
import {myContainer} from '../DIContainerConf/inversify.config'
import {TYPES} from '../DIContainerConf/types'

import { UserDataAjaxCall,LogDataAjaxCall } from '../others/restCall/dbRestcall'
import {encoder} from "../others/encoder/encoder";
import {properties} from '../properties/properties'

var selectedList:string[] = [];
ReactDOM.render(
  <HeaderComponent />,
  document.getElementById("header")
);

class GradeInPopup extends React.Component<{grade:string,id:string},{}> {
    constructor(props:any) {
        super(props);
        this.initDate = this.initDate.bind(this);
    }

    initDate() {
        $("#" + this.props.id).val(this.props.grade);
    }

    componentDidMount() {
        this.initDate();
    }

    render() {
        return (
            <select id="grade">
                <option value="all">
                    All
                </option>
                <option value="0">
                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("user_grade_0")}
                </option>
                <option value="1">
                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("user_grade_1")}
                </option>
                <option value="2">
                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("user_grade_2")}
                </option>
                <option value="3">
                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("user_grade_3")}
                </option>
            </select>
        );

    }
}
class EmailBoxInPopup extends React.Component<{email:string,id:string},{}> {
    constructor(props:any) {
        super(props);
        this.initDate = this.initDate.bind(this);
    }

    initDate() {
        $("#" + this.props.id).val(this.props.email);
    }

    componentDidMount() {
        this.initDate();
    }

    render() {
        return <input type="text" className="email_input" id={this.props.id} />;
    }
}
class PhoneBoxInPopup extends React.Component<{phone:string,id:string},{}> {
    constructor(props:any) {
        super(props);
        this.initDate = this.initDate.bind(this);
    }

    initDate() {
        $("#" + this.props.id).val(this.props.phone);
    }

    componentDidMount() {
        this.initDate();
    }

    render() {
        return <input type="text" className="phone_input" id={this.props.id} />;
    }
}

class DeptBoxInPopup extends React.Component<{department:string,id:string},{}> {
    constructor(props:any) {
        super(props);
        this.initDate = this.initDate.bind(this);
    }

    initDate() {
        $("#" + this.props.id).val(this.props.department);
    }

    componentDidMount() {
        this.initDate();
    }

    render() {
        return(
            <select id="department">
                <option value="all">
                    All
                </option>
                <option value="0">
                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("user_dept_0")}
                </option>
                <option value="1">
                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("user_dept_1")}
                </option>
                <option value="2">
                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("user_dept_2")}
                </option>
                <option value="3">
                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("user_dept_3")}
                </option>
            </select>);
    }
}
class GradeNameInTable extends React.Component<{grade:number, primKey:string},{}> {
    constructor(props:any){
        super(props);
        this.onChange = this.onChange.bind(this);
    }
    onChange(){
        var userDataAjaxCallObj = new UserDataAjaxCall();
        userDataAjaxCallObj.update(
            [{grade: $("#"+this.props.primKey+" option:selected").val()}],
            [{userid:this.props.primKey}]
        );
    }
    componentDidMount(){
        $("#"+this.props.primKey).val(this.props.grade);
    }
    render() {
        return(
            <select id={this.props.primKey} onChange={this.onChange}>
                <option value="0">
                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("user_grade_0")}
                </option>
                <option value="1">
                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("user_grade_1")}
                </option>
                <option value="2">
                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("user_grade_2")}
                </option>
                <option value="3">
                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("user_grade_3")}
                </option>
            </select>
        );
    }
}
class CheckBoxInTable extends React.Component<{userid:string},{}> {
    constructor(props:any){
        super(props);
        this.checkBoxOnChange = this.checkBoxOnChange.bind(this);
    }
    checkBoxOnChange(e:any){
        if(e.target.checked==true){
            selectedList.push(this.props.userid);
        }else{
            var index = selectedList.indexOf(this.props.userid);
            selectedList.splice(index,1);
        }
    }
    render(){
        return(
            <input type="checkbox" onChange={this.checkBoxOnChange} className="checkbox"/>
        );
    }

}
class TableContents extends React.Component<{primKey:string,id:string,value:string},{isEditState:boolean,value:string}> {
    constructor(props:any){
        super(props);
        this.valueOnclick = this.valueOnclick.bind(this);
        this.valueOnKeyPress = this.valueOnKeyPress.bind(this);
        this.state={
            isEditState:false,
            value:this.props.value
        };
    }
    valueOnclick(){
        if(this.state.isEditState==false){
            this.setState(prevState => ({
                isEditState: !prevState.isEditState
            }));
        }

    }
    valueOnKeyPress(e:any){
        if (e.key == 'Enter') {

            var userDataAjaxCallObj = new UserDataAjaxCall();
            var optionList:any[] = [];
            var option:{[key:string]:string}={};
            var item:{[key:string]:string}={};
            option["userid"] = this.props.primKey;
            item[this.props.id]=e.target.value;
            userDataAjaxCallObj.update(
                [item],
                [option]
            );
            this.setState({
                value:e.target.value,
                isEditState:false

            });
        }
        if(e.keyCode === 27){
            this.setState({
                isEditState:false
            });
        }
    }
    render(){
        if(this.state.isEditState){
            return(
                <div onClick={this.valueOnclick}>
                    <input type="text" defaultValue={this.state.value} onKeyDown={this.valueOnKeyPress}/>
                </div>
            );
        }else{
            return(
                <div onClick={this.valueOnclick}>
                    {this.state.value}
                </div>
            );
        }
    }

}

class TableBody extends React.Component<{data:any[]},{}> {
    render() {
        const data = this.props.data.map(item => (
            <tr>
                <td><CheckBoxInTable userid={item.userid}/></td>
                <td>{item.userid}</td>
                <td><TableContents primKey={item.userid} id="username" value={item.username}/></td>
                <td><TableContents primKey={item.userid} id="department" value={item.department}/></td>
                <td><TableContents primKey={item.userid} id="phone" value={item.phone}/></td>
                <td><TableContents primKey={item.userid} id="email" value={item.email}/></td>
                <td><GradeNameInTable primKey={item.userid} grade={item.grade} /></td>
            </tr>
        ));
        return data;
    }
}
class TableHeaderAndBox extends React.Component<{data:any[]},{isSelectallClicked:boolean}> {
    constructor(props:any){
        super(props);
        this.state = {
            isSelectallClicked:false
        }
        this.selectAllOnClick = this.selectAllOnClick.bind(this);

    }
    selectAllOnClick(){

        var checkboxs = document.getElementsByClassName("checkbox");
        if(this.state.isSelectallClicked==false){
            for(var i=0;i<this.props.data.length;i++){
                selectedList.push(this.props.data[i].userid);
            }
            for(var i=0;i<checkboxs.length;i++){
                checkboxs[i].checked=true;
            }
        }else{
            selectedList=[];
            for(var i=0;i<checkboxs.length;i++){
                checkboxs[i].checked=false;
            }
        }

        this.setState(prevState => ({
            isSelectallClicked: !prevState.isSelectallClicked
        }));

    }
    render() {
        return (
            <div className="table_box">
                <table className="table">
                    <thead className="thead">
                        <tr>
                            <th onClick={this.selectAllOnClick}>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("table_select_name")}
                            </th>
                            <th>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("usertable_userid_name")}
                            </th>
                            <th>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("usertable_username_name")}
                            </th>
                            <th>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("usertable_department_name")}
                            </th>
                            <th>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("usertable_phone_name")}
                            </th>
                            <th>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("usertable_email_name")}
                            </th>
                            <th>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("usertable_grade_name")}
                            </th>
                        </tr>
                    </thead>
                    <tbody className="tbody">
                        <TableBody
                            data={this.props.data}
                        />
                    </tbody>
                </table>
            </div>
        );
    }
}
class Addpopup extends React.Component<{open:boolean},{close:boolean}>{
    constructor(props:any){
        super(props);
        this.state={
            close:true
        }
        this.closeOnClick = this.closeOnClick.bind(this);
    }
    addOnClick(){
        var passwords = $('input[type="password"]');
        if(passwords[0].value == passwords[1].value ){
            var inputs = $(".addpopupBox :input");
            var item:{[key:string]:any} = {};
            for(var i=0;i<inputs.length;i++){
                item[inputs[i].getAttribute("name")] = inputs[i].value;
            }
            var userDateAjaxCallObj = new UserDataAjaxCall();
            var userDateAjaxResult = userDateAjaxCallObj.create(
                [item],
                []
            );
            if(userDateAjaxResult["return_code"]=="0000"){
                    alert("success");
            }else{
                alert(userDateAjaxResult["cause"]);
            }
        }else{
            alert(myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                "password_check_error"
            ));
        }
        location.reload();


    }
    closeOnClick(){
        this.setState(prevState => ({
            close: !prevState.close
        }));
    }

    render(){
        if(this.props.open!=this.state.close){
            return(
                <div>
                </div>
            );
        }
        else{
            return(
                <div className="addpopupBox">
                    <div className="addpopup_inside_Box">
                        <label>
                            {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                "usertable_userid_name"
                            )}
                            <input type="text" name="userid" placeholder="userid"></input>
                        </label>
                        <label>
                            {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                "usertable_password_name"
                            )}
                            <input type="password" name="password" placeholder="password"></input>
                        </label>
                        <label>
                            {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                "usertable_password_check_name"
                            )}
                            <input type="password" name="password_check" placeholder="check_password"></input>
                        </label>
                        <label>
                            {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                "usertable_username_name"
                            )}
                            <input type="text" name="username" placeholder="username"></input>
                        </label>
                        <label>
                            {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                "usertable_department_name"
                            )}
                            <input type="text" name="department" placeholder="department"></input>
                        </label>
                        <label>
                            {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                "usertable_phone_name"
                            )}
                            <input type="text" name="phone" placeholder="phone"></input>
                        </label>
                        <label>
                            {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                "usertable_email_name"
                            )}
                            <input type="text" name="email" placeholder="email"></input>
                        </label>
                        <label>
                            {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                "usertable_grade_name"
                            )}
                            <input type="text" name="grade" placeholder="grade"></input>
                        </label>
                        <div className="add_popup_button_group">
                            <button onClick={this.addOnClick}>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "table_selected_item_add_button"
                                )}
                            </button>
                            <button onClick={this.closeOnClick}>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "popup_close"
                                )}
                            </button>
                        </div>
                    </div>
                </div>
            );
        }

    }
}
class SearchBoxAndTable extends React.Component<{},{userid:string,username:string,department:string,phone:string,email:string,grade:string,startDate:string,endDate:string,resultData:any[],currentPage:number,maxPage:number,perpage:number,isAddClicked:boolean}> {
    constructor(props:any) {
        super(props);
        this.state = {
            startDate: "",
            endDate: "",
            userid: "",
            username: "",
            grade: "all",
            email: "",
            phone: "",
            department: "all",
            currentPage: 1,
            maxPage: -1,
            perpage: 10,
            resultData:[],
            isAddClicked:false
        };

        this.getDataUsingRestCallFunc = this.getDataUsingRestCallFunc.bind(
            this
        );


        this.useridOnChange = this.useridOnChange.bind(this);
        this.usernameOnChange = this.usernameOnChange.bind(this);
        this.addOnClick = this.addOnClick.bind(this);

        this.searchOnclick = this.searchOnclick.bind(this);

        this.firstPageOnclick = this.firstPageOnclick.bind(this);
        this.lastPageOnclick = this.lastPageOnclick.bind(this);
        this.prePageOnclick = this.prePageOnclick.bind(this);
        this.nextPageOnclick = this.nextPageOnclick.bind(this);
        this.currentPageOnKeyPress = this.currentPageOnKeyPress.bind(this);
    }

    getDataUsingRestCallFunc(page:number) {
        var userDateResult;
        var option1:{[key:string]:any} = {};

        if (this.state.department != "all") {
            option1["department"] = this.state.department;
        }
        if (this.state.userid != "") {
            option1["userid"] = this.state.userid;
        }
        if (this.state.username != "") {
            option1["username"] = this.state.username;
        }

        var userDateAjaxCallObj = new UserDataAjaxCall();
        userDateResult = userDateAjaxCallObj.retrieve(
            [],
            [option1, {}, {}, { page: page, perpage: this.state.perpage }]
        );
        var logDataAjaxCallObj = new LogDataAjaxCall();
        logDataAjaxCallObj.create([properties["User_view_log"]],[]);
        return userDateResult;
    }

    firstPageOnclick() {
        var userDateResult;
        if (this.state.currentPage != 1) {
            userDateResult = this.getDataUsingRestCallFunc(1);
            this.setState({
                resultData: userDateResult["items"],
                currentPage: 1
            });

            $("#current_page").val(1);
        }
    }

    lastPageOnclick() {
        var userDateResult;
        if (
            this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage
        ) {
            userDateResult = this.getDataUsingRestCallFunc(this.state.maxPage);
            this.setState({
                resultData: userDateResult["items"],
                currentPage: this.state.maxPage
            });

            $("#current_page").val(this.state.maxPage);
        }
    }
    prePageOnclick() {
        var userDateResult;
        if (this.state.maxPage != -1 && this.state.currentPage > 1) {
            userDateResult = this.getDataUsingRestCallFunc(
                this.state.currentPage - 1
            );
            this.setState({ resultData: userDateResult["items"] });
            $("#current_page").val(this.state.currentPage - 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage - 1
            }));
        }
    }
    nextPageOnclick() {
        var userDateResult;
        if (
            this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage
        ) {
            userDateResult = this.getDataUsingRestCallFunc(
                this.state.currentPage + 1
            );
            this.setState({ resultData: userDateResult["items"] });
            $("#current_page").val(this.state.currentPage + 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage + 1
            }));
        }
    }
    currentPageOnKeyPress(e:any) {
        if (e.which == 13) {
            var pageVal = $("#current_page").val() as string;
            var page = parseInt(pageVal);
            if (isNaN(page)) {
                alert(myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_is_not_number_alert"));
                $("#current_page").val(this.state.currentPage);
            } else {
                if (page > this.state.maxPage) {
                    alert(
                        myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_greater_than_max_alert")
                    );
                    $("#current_page").val(this.state.currentPage);
                } else {
                    if (
                        this.state.maxPage != -1 &&
                        this.state.currentPage != page
                    ) {
                        var userDateResult = this.getDataUsingRestCallFunc(
                            page
                        );
                        this.setState({
                            resultData: userDateResult["items"],
                            currentPage: page
                        });
                        $("#current_page").val(page);
                    }
                }
            }
        }
    }

    useridOnChange(e:any) {
        this.setState({
            userid: e.target.value
        });
    }
    usernameOnChange(e:any) {
        this.setState({
            username: e.target.value
        });
    }
    deleteOnClick(e:any){
        var userDataAjaxCallObj = new UserDataAjaxCall();
        var optionList:any[] = [];
        for(var i=0;i<selectedList.length;i++){
            optionList.push({userid:selectedList[i]});
        }
        userDataAjaxCallObj.delete(
            [],
            optionList
        );
        location.reload();
    }
    addOnClick(){
        this.setState(prevState => ({
            isAddClicked: !prevState.isAddClicked
        }));
    }

    searchOnclick() {
        var userDateResult = this.getDataUsingRestCallFunc(1);

        this.setState({ resultData: userDateResult["items"] });

        var maxpage = Math.ceil(userDateResult["total"] / this.state.perpage);
        this.setState({ maxPage: maxpage });

        if (userDateResult["items"].length == 0) {
            $("#current_page").val(0);
            $("#max_page").val(0);
            this.setState({ maxPage: 0, currentPage: 0 });
        } else {
            $("#current_page").val(1);
            $("#max_page").val(maxpage);
            this.setState({ maxPage: maxpage, currentPage: 1 });
        }
    }

    componentDidMount(){
        this.searchOnclick();
    }


    render() {
        return (
            <div className="main_box">
                <div className="current_page_info">
                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_setting_root_name")}
                    <i className="fas fa-angle-right"></i>
                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_usersetting_name")}
                </div>
                <div className="searchBox">
                    <div className="userid_input_box">
                        <label>
                            {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                "usertable_userid_name"
                            )}
                            <input
                                type="text"
                                className="userid_input"
                                id="userid"
                                onChange={this.useridOnChange}
                                defaultValue={this.state.userid}
                            />
                        </label>
                    </div>
                    <div className="username_input_box">
                        <label>
                            {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                "usertable_username_name"
                            )}
                            <input
                                type="text"
                                className="username_input"
                                id="username"
                                onChange={this.usernameOnChange}
                                defaultValue={this.state.username}
                            />
                        </label>
                    </div>
                    <div className="username_input_box">
                        <label>
                            {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("usertable_department_name")}
                            <DeptBoxInPopup
                                department={this.state.department}
                                id="department"
                            />
                        </label>
                    </div>
                    <div className="button_box">

                        <div className="search_button_box">
                            <button
                                className="searchButton"
                                onClick={this.searchOnclick}
                            >
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "dashtable_search_button_name"
                                )}
                            </button>
                        </div>
                        <div>
                            <button className="items_delete_button" onClick={this.deleteOnClick}>{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                "table_selected_item_delete_button"
                            )}</button>
                        </div>
                        <div>
                            <button className="items_add_button" onClick={this.addOnClick}>{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                "table_selected_item_add_button"
                            )}</button>
                            <Addpopup open={this.state.isAddClicked} />

                        </div>
                    </div>
                </div>

                <div>
                    <TableHeaderAndBox
                        data={this.state.resultData}
                    />
                </div>
                <div className="page_box">
                    <div className="pre_page_box">
                        <i
                            className="fas fa-angle-double-left"
                            onClick={this.firstPageOnclick}
                        />
                        <i
                            className="fas fa-angle-left"
                            onClick={this.prePageOnclick}
                        />
                    </div>
                    <div className="page_input_box">
                        <input
                            type="text"
                            className="current_page_input"
                            id="current_page"
                            onKeyPress={this.currentPageOnKeyPress}
                        />
                        <div className="slash">
                        </div>
                        <input
                            type="text"
                            className="max_page_input"
                            readOnly="true"
                            id="max_page"
                        />
                    </div>
                    <div className="post_page_box">
                        <i
                            className="fas fa-angle-right"
                            onClick={this.nextPageOnclick}
                        />
                        <i
                            className="fas fa-angle-double-right"
                            onClick={this.lastPageOnclick}
                        />
                    </div>
                </div>
            </div>
        );
    }

}

ReactDOM.render(
    <div>
        <SearchBoxAndTable />
    </div>,
    document.getElementById("main")
);
