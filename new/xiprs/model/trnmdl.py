# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 10:48:59 2018

@author: ryuk
"""
from .. import db


class TrainingInfo(db.Model):
    __tablename__ = 'iis_training'
    __table_args__ = {'mysql_collate': 'utf8_general_ci', 'mysql_engine': 'MyISAM'}

    modelcode = db.Column(db.VARCHAR(16), primary_key=True, nullable=False)
    modelname = db.Column(db.VARCHAR(128), nullable=False)
    regdate = db.Column(db.DateTime, nullable=False)
    status = db.Column(db.Integer, nullable=False)
    accuracy = db.Column(db.Float, nullable=True)
    filter = db.Column(db.VARCHAR(16), nullable=False)
    algorithm = db.Column(db.VARCHAR(16), nullable=False)
    user = db.Column(db.VARCHAR(32), nullable=False)
    time = db.Column(db.Float, nullable=True)
    contents = db.Column(db.Text, nullable=False)

    def __init__(self, modelcode, modelname, status, accuracy, filter, algorithm, user, contents):
        self.modelcode = modelcode
        self.modelname = modelname
        self.status = status
        self.accuracy = accuracy
        self.filter = filter
        self.algorithm = algorithm
        self.user = user
        self.contents = contents

    def __repr__(self):
        return repr((self.modelcode, self.modelname, self.regdate, self.status, self.accuracy, self.filter, self.algorithm, self.user, self.time, self.contents))
