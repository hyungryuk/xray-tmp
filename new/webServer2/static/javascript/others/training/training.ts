import {encoder} from "../encoder/encoder";
import {myContainer} from '../../DIContainerConf/inversify.config'
import {TYPES} from '../../DIContainerConf/types'
import {StaticLanguageInterface} from '../staticLanguageData/staticLanguageData'

export class ImageDataToJson{

    tableBodyId:string;

    constructor(tableBodyId:string){
        this.tableBodyId = tableBodyId;
    }

    getJson(){
        var tableBody = document.getElementById(this.tableBodyId);
        var tdElement = tableBody.getElementsByTagName("td");

        var resultList = [];


        for(var i=0;i<tdElement.length;i=i+6){
            if(tdElement[i].getElementsByTagName("input")[0].checked){
                var dict:{[key:string]:any} = {};
                dict["filename"] = tdElement[i+1].innerHTML;
                dict["filedate"] = encoder.dateToYYMMDDHHMMSS(tdElement[i+2].innerHTML);
                dict["filepath"] = tdElement[i+3].innerHTML;
                if(tdElement[i+5].innerHTML==myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                    "imagefiletable_ok_name"
                )){
                    dict["ng"] = 0;
                }
                else if(tdElement[i+5].innerHTML===myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                    "imagefiletable_ng_name"
                )){
                    dict["ng"] = 1;
                }
                else if(tdElement[i+5].innerHTML===myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                    "imagefiletable_nr_name"
                )){
                    dict["ng"] = 2;
                }
                resultList.push(dict);
            }
        }
        return JSON.stringify(resultList);
    }

}
