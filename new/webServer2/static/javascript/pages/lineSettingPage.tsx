import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { HeaderComponent } from "../modulesForPages/header";
import {StaticLanguageInterface} from '../others/staticLanguageData/staticLanguageData'
import {myContainer} from '../DIContainerConf/inversify.config'
import {TYPES} from '../DIContainerConf/types'

import { LineAjaxCall,ModelDataAjaxCall,LogDataAjaxCall } from '../others/restCall/dbRestcall'
import {encoder} from "../others/encoder/encoder";
import {properties} from '../properties/properties'


var selectedList:string[] = [];
ReactDOM.render(
  <HeaderComponent />,
  document.getElementById("header")
);


class ModelCodeBoxInPopup extends React.Component<{modelcode:string,id:string},{}> {
    constructor(props:any) {
        super(props);
        this.initDate = this.initDate.bind(this);
    }

    initDate() {
        $("#" + this.props.id).val(this.props.modelcode);
    }




    componentDidMount() {
        this.initDate();
    }

    render() {
        return <input type="text" className="modelcode_input" id={this.props.id} />;

    }
}
class FactoryBoxInPopup extends React.Component<{factory:string,id:string},{}> {
    constructor(props:any) {
        super(props);
        this.initDate = this.initDate.bind(this);
    }

    initDate() {
        $("#" + this.props.id).val(this.props.factory);
    }

    componentDidMount() {
        this.initDate();
    }

    render() {
        return <input type="text" className="factory_input" id={this.props.id} />;

    }
}
class IpAddrBoxInPopup extends React.Component<{ipaddr:string,id:string},{}> {
    constructor(props:any) {
        super(props);
        this.initDate = this.initDate.bind(this);
    }

    initDate() {
        $("#" + this.props.id).val(this.props.ipaddr);
    }

    componentDidMount() {
        this.initDate();
    }

    render() {
        return <input type="text" className="ipAddr_input" id={this.props.id} />;
    }
}
class ModelNameAndCodeInTable extends React.Component<{modelList:any[],modelcode:string},{}> {
    constructor(props:any) {
        super(props);
        this.modelcodeToModelnameAndVersion = this.modelcodeToModelnameAndVersion.bind(
            this
        );
    }

    modelcodeToModelnameAndVersion() {
        for (var i = 0; i < this.props.modelList.length; i++) {
            if (this.props.modelList[i].linecode == this.props.modelcode) {
                return (
                    this.props.modelList[i].modelname +
                    " " +
                    this.props.modelList[i].version
                );
            }
        }
    }

    render() {
        return <div>{this.modelcodeToModelnameAndVersion()}</div>;
    }
}
class CheckBoxInTable extends React.Component<{linecode:string},{}> {
    constructor(props:any){
        super(props);
        this.checkBoxOnChange = this.checkBoxOnChange.bind(this);
    }
    checkBoxOnChange(e:any){
        if(e.target.checked==true){
            selectedList.push(this.props.linecode);
        }else{
            var index = selectedList.indexOf(this.props.linecode);
            selectedList.splice(index,1);
        }
    }
    render(){
        return(
            <input type="checkbox" onChange={this.checkBoxOnChange} className="checkbox"/>
        );
    }

}
class TableContents extends React.Component<{primKey:string,id:string,value:string},{isEditState:boolean,value:string}> {
    constructor(props:any){
        super(props);
        this.valueOnclick = this.valueOnclick.bind(this);
        this.valueOnKeyPress = this.valueOnKeyPress.bind(this);
        this.state={
            isEditState:false,
            value:this.props.value
        };
    }
    valueOnclick(){
        if(this.state.isEditState==false){
            this.setState(prevState => ({
                isEditState: !prevState.isEditState
            }));
        }

    }
    valueOnKeyPress(e:any){
        if (e.key == 'Enter') {

            var lineAjaxCallObj = new LineAjaxCall();
            var optionList:any[] = [];
            var option:{[key:string]:string}={};
            var item:{[key:string]:string}={};
            option["linecode"] = this.props.primKey;
            item[this.props.id]=e.target.value;
            lineAjaxCallObj.update(
                [item],
                [option]
            );
            this.setState({
                value:e.target.value,
                isEditState:false

            });
        }
        if(e.keyCode === 27){
            this.setState({
                isEditState:false
            });
        }
    }
    render(){
        if(this.state.isEditState){
            return(
                <div onClick={this.valueOnclick}>
                    <input type="text" defaultValue={this.state.value} onKeyDown={this.valueOnKeyPress}/>
                </div>
            );
        }else{
            return(
                <div onClick={this.valueOnclick}>
                    {this.state.value}
                </div>
            );
        }
    }

}
class TableBody extends React.Component<{data:any[],modelList:any[]},{}> {
    render() {
        const data = this.props.data.map(item => (
            <tr>
                <td><CheckBoxInTable linecode={item.linecode}/></td>
                <td><TableContents primKey={item.linecode} id="factory" value={item.factory}/></td>
                <td><TableContents primKey={item.linecode} id="line" value={item.line}/></td>
                <td><TableContents primKey={item.linecode} id="ipaddr" value={item.ipaddr}/></td>
                <td><ModelNameAndCodeInTable modelList={this.props.modelList} modelcode={item.modelcode}/></td>

            </tr>
        ));
        return data;
    }
}

class TableHeaderAndBox extends React.Component<{data:any[],modelList:any[]},{isSelectallClicked:boolean}> {
        constructor(props:any){
            super(props);
            this.state = {
                isSelectallClicked:false
            }
            this.selectAllOnClick = this.selectAllOnClick.bind(this);

        }
        selectAllOnClick(){

            var checkboxs = document.getElementsByClassName("checkbox");
            if(this.state.isSelectallClicked==false){
                for(var i=0;i<this.props.data.length;i++){
                    selectedList.push(this.props.data[i].linecode);
                }
                for(var i=0;i<checkboxs.length;i++){
                    checkboxs[i].checked=true;
                }
            }else{
                selectedList=[];
                for(var i=0;i<checkboxs.length;i++){
                    checkboxs[i].checked=false;
                }
            }

            this.setState(prevState => ({
                isSelectallClicked: !prevState.isSelectallClicked
            }));

        }
    render() {
        return (
            <div className="table_box">
                <table className="table">
                    <thead className="thead">
                        <tr>
                            <th onClick={this.selectAllOnClick}>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("table_select_name")}
                            </th>

                            <th>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("linetable_factory_name")}
                            </th>
                            <th>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("linetable_linename_name")}
                            </th>
                            <th>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("linetable_ipaddr_name")}
                            </th>
                            <th>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("linetable_modelcode_name")}
                            </th>
                        </tr>
                    </thead>
                    <tbody className="tbody">
                        <TableBody
                            data={this.props.data} modelList={this.props.modelList}
                        />
                    </tbody>
                </table>
            </div>
        );
    }
}
class Addpopup extends React.Component<{open:boolean},{close:boolean}>{
    constructor(props:any){
        super(props);
        this.state={
            close:true
        }
        this.closeOnClick = this.closeOnClick.bind(this);
    }
    addOnClick(){
        var passwords = $('input[type="password"]');
        var inputs = $(".addpopupBox :input");
        var item:{[key:string]:any} = {};
        for(var i=0;i<inputs.length;i++){
            item[inputs[i].getAttribute("name")] = inputs[i].value;
        }
        item["linecode"] = parseInt(Date.now().toString().substring(5,13));
        var lineAjaxCallObj = new LineAjaxCall();
        var lineAjaxResult = lineAjaxCallObj.create(
            [item],
            []
        );
        if(lineAjaxResult["return_code"]=="0000"){
                alert("success");
        }else{
            alert(lineAjaxResult["cause"]);
        }
        location.reload();

    }
    closeOnClick(){
        this.setState(prevState => ({
            close: !prevState.close
        }));
    }

    render(){
        if(this.props.open!=this.state.close){
            return(
                <div>
                </div>
            );
        }
        else{
            return(
                <div className="addpopupBox">
                    <div className="addpopup_inside_Box">
                        <label>
                            {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                "linetable_factory_name"
                            )}
                            <input type="text" name="factory" placeholder="factory"></input>
                        </label>
                        <label>
                            {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                "linetable_linename_name"
                            )}
                            <input type="text" name="line" placeholder="linename"></input>
                        </label>
                        <label>
                            {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                "linetable_ipaddr_name"
                            )}
                            <input type="text" name="ipaddr" placeholder="ipaddr"></input>
                        </label>

                        <div className="add_popup_button_group">
                            <button onClick={this.addOnClick}>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "table_selected_item_add_button"
                                )}
                            </button>
                            <button onClick={this.closeOnClick}>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "popup_close"
                                )}
                            </button>
                        </div>
                    </div>
                </div>
            );
        }

    }
}

class SearchBoxAndTable extends React.Component<{},{linecode:string,line:string,modelList:any[],factory:string,ipaddr:string,modelcode:string,startDate:string,endDate:string,resultData:any[],currentPage:number,maxPage:number,perpage:number,isfilterOpen:boolean,isAddClicked:boolean}> {

    constructor(props:any) {
        super(props);
        var modelDataAjaxCall = new ModelDataAjaxCall();
        var modelList = modelDataAjaxCall.retrieve([], [])["items"];
        this.state = {
            startDate: "",
            endDate: "",
            linecode: "",
            line: "",
            factory: "",
            ipaddr: "",
            modelcode: "",
            modelList:modelList,
            currentPage: 1,
            maxPage: -1,
            perpage: 10,
            isfilterOpen: false,
            resultData:[],
            isAddClicked:false
        };

        this.getDataUsingRestCallFunc = this.getDataUsingRestCallFunc.bind(this);

        this.startDateOnChange = this.startDateOnChange.bind(this);
        this.endDateOnChange = this.endDateOnChange.bind(this);
        this.linecodeOnChange = this.linecodeOnChange.bind(this);

        this.searchOnclick = this.searchOnclick.bind(this);
        this.filterOnclick = this.filterOnclick.bind(this);
        this.filterCloseOnclick = this.filterCloseOnclick.bind(this);
        this.setFilterOnclick = this.setFilterOnclick.bind(this);
        this.addOnClick = this.addOnClick.bind(this);

        this.firstPageOnclick = this.firstPageOnclick.bind(this);
        this.lastPageOnclick = this.lastPageOnclick.bind(this);
        this.prePageOnclick = this.prePageOnclick.bind(this);
        this.nextPageOnclick = this.nextPageOnclick.bind(this);
        this.currentPageOnKeyPress = this.currentPageOnKeyPress.bind(this);
    }
    setFilterOnclick() {
        this.setState({ factory: $("#factory").val() as string });
        this.setState({ ipaddr: $("#ipaddr").val() as string });
        this.setState({ modelcode: $("#modelcode").val() as string });
        this.setState(prevState => ({
            isfilterOpen: !prevState.isfilterOpen
        }));
    }
    filterCloseOnclick() {
        this.setState(prevState => ({
            isfilterOpen: !prevState.isfilterOpen
        }));
    }
    filterOnclick() {
        this.setState(prevState => ({
            isfilterOpen: !prevState.isfilterOpen
        }));
    }
    getDataUsingRestCallFunc(page:number) {
        var lineDataResult;
        var option1:{[key:string]:any} = {};
        var option2:{[key:string]:any} = {};

        if (this.state.linecode != "") {
            option1["linecode"] = this.state.linecode;
        }
        if (this.state.factory != "") {
            option1["factory"] = this.state.factory;
        }
        if (this.state.line != "") {
            option1["line"] = this.state.line;
        }
        if (this.state.ipaddr != "") {
            option1["ipaddr"] = this.state.ipaddr;
        }
        if (this.state.modelcode != "") {
            option1["modelcode"] = this.state.modelcode;
        }
        if (this.state.startDate != "" && this.state.endDate != "") {
            option2["startdate"] = encoder.dateToYYMMDDHHMMSS(
                this.state.startDate + "000000"
            );
            option2["enddate"] = encoder.dateToYYMMDDHHMMSS(
                this.state.endDate + "235959"
            );
        }
        var lineAjaxCallObj = new LineAjaxCall();
        lineDataResult = lineAjaxCallObj.retrieve(
            [],
            [option1, option2, {}, { page: page, perpage: this.state.perpage }]
        );
        var logDataAjaxCallObj = new LogDataAjaxCall();
        logDataAjaxCallObj.create([properties["Line_view_log"]],[]);
        return lineDataResult;
    }

    firstPageOnclick() {
        var lineDataResult;
        if (this.state.currentPage != 1) {
            lineDataResult = this.getDataUsingRestCallFunc(1);
            this.setState({
                resultData: lineDataResult["items"],
                currentPage: 1
            });

            $("#current_page").val(1);
        }
    }

    lastPageOnclick() {
        var lineDataResult;
        if (
            this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage
        ) {
            lineDataResult = this.getDataUsingRestCallFunc(this.state.maxPage);
            this.setState({
                resultData: lineDataResult["items"],
                currentPage: this.state.maxPage
            });

            $("#current_page").val(this.state.maxPage);
        }
    }
    prePageOnclick() {
        var lineDataResult;
        if (this.state.maxPage != -1 && this.state.currentPage > 1) {
            lineDataResult = this.getDataUsingRestCallFunc(
                this.state.currentPage - 1
            );
            this.setState({ resultData: lineDataResult["items"] });
            $("#current_page").val(this.state.currentPage - 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage - 1
            }));
        }
    }
    nextPageOnclick() {
        var lineDataResult;
        if (
            this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage
        ) {
            lineDataResult = this.getDataUsingRestCallFunc(
                this.state.currentPage + 1
            );
            this.setState({ resultData: lineDataResult["items"] });
            $("#current_page").val(this.state.currentPage + 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage + 1
            }));
        }
    }
    currentPageOnKeyPress(e:any) {
        if (e.which == 13) {
            var pageVal = $("#current_page").val() as string;
            var page = parseInt(pageVal);
            if (isNaN(page)) {
                alert(myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_is_not_number_alert"));
                $("#current_page").val(this.state.currentPage);
            } else {
                if (page > this.state.maxPage) {
                    alert(
                        myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_greater_than_max_alert")
                    );
                    $("#current_page").val(this.state.currentPage);
                } else {
                    if (
                        this.state.maxPage != -1 &&
                        this.state.currentPage != page
                    ) {
                        var lineDataResult = this.getDataUsingRestCallFunc(
                            page
                        );
                        this.setState({
                            resultData: lineDataResult["items"],
                            currentPage: page
                        });
                        $("#current_page").val(page);
                    }
                }
            }
        }
    }

    startDateOnChange(e:any) {
        this.setState({
            startDate: e.target.value
        });
    }
    endDateOnChange(e:any) {
        this.setState({
            endDate: e.target.value
        });
    }
    linecodeOnChange(e:any) {
        this.setState({
            linecode: e.target.value
        });
    }
    lineOnChange(e:any) {
        this.setState({
            line: e.target.value
        });
    }
    deleteOnClick(e:any){
        var lineAjaxCallObj = new LineAjaxCall();
        var optionList:any[] = [];
        for(var i=0;i<selectedList.length;i++){
            optionList.push({linecode:selectedList[i]});
        }
        lineAjaxCallObj.delete(
            [],
            optionList
        );
        location.reload();
    }
    addOnClick(){
        this.setState(prevState => ({
            isAddClicked: !prevState.isAddClicked
        }));
    }

    searchOnclick() {
        var lineDataResult = this.getDataUsingRestCallFunc(1);

        this.setState({ resultData: lineDataResult["items"] });

        var maxpage = Math.ceil(lineDataResult["total"] / this.state.perpage);
        this.setState({ maxPage: maxpage });

        if (lineDataResult["items"].length == 0) {
            $("#current_page").val(0);
            $("#max_page").val(0);
            this.setState({ maxPage: 0, currentPage: 0 });
        } else {
            $("#current_page").val(1);
            $("#max_page").val(maxpage);
            this.setState({ maxPage: maxpage, currentPage: 1 });
        }
    }

    componentDidMount(){
        this.searchOnclick();
    }
    render() {
        const popup = (
            <div className="optional_settings_box">
                <div className="popup_close_button">
                    <i
                        className="fas fa-window-close"
                        onClick={this.filterCloseOnclick}
                    />
                </div>
                <div className="factory_input_box">
                    <label>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("linetable_factory_name")}
                        <FactoryBoxInPopup id="factory" factory={this.state.factory} />
                    </label>
                </div>
                <div className="ipaddr_input_box">
                    <label>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("linetable_ipaddr_name")}
                        <IpAddrBoxInPopup ipaddr={this.state.ipaddr} id="ipaddr" />
                    </label>
                </div>
                <div className="modelcode_input_box">
                    <label>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("linetable_modelcode_name")}
                        <ModelCodeBoxInPopup modelcode={this.state.modelcode} id="modelcode" />
                    </label>
                </div>
                <div className="filter_set_button">
                    <button
                        className="set_button"
                        onClick={this.setFilterOnclick}
                    >
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("filter_setting_button")}
                    </button>
                </div>
            </div>
        );
        if (this.state.isfilterOpen) {
            return (
                <div className="main_box">
                    <div className="current_page_info">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_setting_root_name")}
                        <i className="fas fa-angle-right"></i>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_linesetting_name")}
                    </div>
                    <div>{popup}</div>
                    <div className="searchBox">
                        {/* <div className="linecode_input_box">
                            <label>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "linetable_linecode_name"
                                )}
                                <input
                                    type="text"
                                    className="linecode_input"
                                    id="linecode"
                                    onChange={this.linecodeOnChange}
                                    defaultValue={this.state.linecode}
                                />
                            </label>
                        </div> */}
                        <div className="line_input_box">
                            <label>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "linetable_linename_name"
                                )}
                                <input
                                    type="text"
                                    className="line_input"
                                    id="line"
                                    onChange={this.lineOnChange}
                                    defaultValue={this.state.line}
                                />
                            </label>
                        </div>
                        <div className="date_input_box">
                            <div className="start_date_input_box">
                                <label>
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "imagefiletable_startdate_label"
                                    )}
                                    <input
                                        type="date"
                                        className="start_date_input"
                                        onChange={this.startDateOnChange}
                                        defaultValue={this.state.startDate}
                                    />
                                </label>
                            </div>
                            <div className="end_date_input_box">
                                <label>
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "imagefiletable_enddate_label"
                                    )}
                                    <input
                                        type="date"
                                        className="end_date_input"
                                        onChange={this.endDateOnChange}
                                        defaultValue={this.state.endDate}
                                    />
                                </label>
                            </div>
                        </div>
                        <div className="button_box">
                            <div className="set_specific_filter_button_box">
                                <button
                                    className="set_specific_filter_button"
                                    onClick={this.filterOnclick}
                                >
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "imagefiletable_set_specific_filter_button_box_name"
                                    )}
                                </button>
                            </div>
                            <div className="search_button_box">
                                <button
                                    className="searchButton"
                                    onClick={this.searchOnclick}
                                >
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "dashtable_search_button_name"
                                    )}
                                </button>
                            </div>
                            <div>
                                <button className="items_delete_button" onClick={this.deleteOnClick}>{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "table_selected_item_delete_button"
                                )}</button>
                            </div>
                            <div>
                                <button className="items_add_button" onClick={this.addOnClick}>{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "table_selected_item_add_button"
                                )}</button>
                                <Addpopup open={this.state.isAddClicked} />

                            </div>
                        </div>
                    </div>

                    <div>
                        <TableHeaderAndBox
                            data={this.state.resultData}
                            modelList={this.state.modelList}
                        />
                    </div>
                    <div className="page_box">
                        <div className="pre_page_box">
                            <i
                                className="fas fa-angle-double-left"
                                onClick={this.firstPageOnclick}
                            />
                            <i
                                className="fas fa-angle-left"
                                onClick={this.prePageOnclick}
                            />
                        </div>
                        <div className="page_input_box">
                            <input
                                type="text"
                                className="current_page_input"
                                id="current_page"
                                onKeyPress={this.currentPageOnKeyPress}
                            />
                            <div className="slash">
                            </div>
                            <input
                                type="text"
                                className="max_page_input"
                                readOnly="true"
                                id="max_page"
                            />
                        </div>
                        <div className="post_page_box">
                            <i
                                className="fas fa-angle-right"
                                onClick={this.nextPageOnclick}
                            />
                            <i
                                className="fas fa-angle-double-right"
                                onClick={this.lastPageOnclick}
                            />
                        </div>
                    </div>
                </div>
            );
        } else {
            return (
                <div className="main_box">
                    <div className="current_page_info">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_setting_root_name")}
                        <i className="fas fa-angle-right"></i>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_linesetting_name")}
                    </div>
                    <div className="searchBox">
                        {/* <div className="linecode_input_box">
                            <label>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "linetable_linecode_name"
                                )}
                                <input
                                    type="text"
                                    className="linecode_input"
                                    id="linecode"
                                    onChange={this.linecodeOnChange}
                                    defaultValue={this.state.linecode}
                                />
                            </label>
                        </div> */}
                        <div className="line_input_box">
                            <label>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "linetable_linename_name"
                                )}
                                <input
                                    type="text"
                                    className="line_input"
                                    id="line"
                                    onChange={this.lineOnChange}
                                    defaultValue={this.state.line}
                                />
                            </label>
                        </div>
                        <div className="date_input_box">
                            <div className="start_date_input_box">
                                <label>
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "imagefiletable_startdate_label"
                                    )}
                                    <input
                                        type="date"
                                        className="start_date_input"
                                        onChange={this.startDateOnChange}
                                        defaultValue={this.state.startDate}
                                    />
                                </label>
                            </div>
                            <div className="end_date_input_box">
                                <label>
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "imagefiletable_enddate_label"
                                    )}
                                    <input
                                        type="date"
                                        className="end_date_input"
                                        onChange={this.endDateOnChange}
                                        defaultValue={this.state.endDate}
                                    />
                                </label>
                            </div>
                        </div>
                        <div className="button_box">
                            <div className="set_specific_filter_button_box">
                                <button
                                    className="set_specific_filter_button"
                                    onClick={this.filterOnclick}
                                >
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "imagefiletable_set_specific_filter_button_box_name"
                                    )}
                                </button>
                            </div>
                            <div className="search_button_box">
                                <button
                                    className="searchButton"
                                    onClick={this.searchOnclick}
                                >
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "dashtable_search_button_name"
                                    )}
                                </button>
                            </div>
                            <div>
                                <button className="items_delete_button" onClick={this.deleteOnClick}>{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "table_selected_item_delete_button"
                                )}</button>
                            </div>
                            <div>
                                <button className="items_add_button" onClick={this.addOnClick}>{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "table_selected_item_add_button"
                                )}</button>
                                <Addpopup open={this.state.isAddClicked} />

                            </div>
                        </div>
                    </div>

                    <div>
                        <TableHeaderAndBox
                            data={this.state.resultData}
                            modelList={this.state.modelList}
                        />
                    </div>
                    <div className="page_box">
                        <div className="pre_page_box">
                            <i
                                className="fas fa-angle-double-left"
                                onClick={this.firstPageOnclick}
                            />
                            <i
                                className="fas fa-angle-left"
                                onClick={this.prePageOnclick}
                            />
                        </div>
                        <div className="page_input_box">
                            <input
                                type="text"
                                className="current_page_input"
                                id="current_page"
                                onKeyPress={this.currentPageOnKeyPress}
                            />
                            <div className="slash">
                            </div>
                            <input
                                type="text"
                                className="max_page_input"
                                readOnly="true"
                                id="max_page"
                            />
                        </div>
                        <div className="post_page_box">
                            <i
                                className="fas fa-angle-right"
                                onClick={this.nextPageOnclick}
                            />
                            <i
                                className="fas fa-angle-double-right"
                                onClick={this.lastPageOnclick}
                            />
                        </div>
                    </div>
                </div>
            );
        }
    }
}

ReactDOM.render(
    <div>
        <SearchBoxAndTable />
    </div>,
    document.getElementById("main")
);
