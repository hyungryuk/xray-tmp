"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const inversify_1 = require("inversify");
let LineSelector = class LineSelector {
    setDivId(divId) {
        this.divId = divId;
    }
    setDatas(datas) {
        this.datas = datas;
    }
    makeSelector() {
        for (var i = 0; i < this.datas.length; i++) {
            $("#" + this.divId).append(new Option(this.datas[i].factory + " " + this.datas[i].line, this.datas[i].linecode));
        }
    }
};
LineSelector = __decorate([
    inversify_1.injectable()
], LineSelector);
exports.LineSelector = LineSelector;
let AlgorithmSelector = class AlgorithmSelector {
    setDivId(divId) {
        this.divId = divId;
    }
    setDatas(datas) {
        this.datas = datas;
    }
    makeSelector() {
        for (var i = 0; i < this.datas.length; i++) {
            $("#" + this.divId).append(new Option(this.datas[i].algorithmname + " " + this.datas[i].version, this.datas[i].algorithmcode));
        }
    }
};
AlgorithmSelector = __decorate([
    inversify_1.injectable()
], AlgorithmSelector);
exports.AlgorithmSelector = AlgorithmSelector;
let FilterSelector = class FilterSelector {
    setDivId(divId) {
        this.divId = divId;
    }
    setDatas(datas) {
        this.datas = datas;
    }
    makeSelector() {
        for (var i = 0; i < this.datas.length; i++) {
            $("#" + this.divId).append(new Option(this.datas[i].filtername + " " + this.datas[i].version, this.datas[i].filtercode));
        }
    }
};
FilterSelector = __decorate([
    inversify_1.injectable()
], FilterSelector);
exports.FilterSelector = FilterSelector;
