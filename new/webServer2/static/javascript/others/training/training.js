"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const encoder_1 = require("../encoder/encoder");
const inversify_config_1 = require("../../DIContainerConf/inversify.config");
const types_1 = require("../../DIContainerConf/types");
class ImageDataToJson {
    constructor(tableBodyId) {
        this.tableBodyId = tableBodyId;
    }
    getJson() {
        var tableBody = document.getElementById(this.tableBodyId);
        var tdElement = tableBody.getElementsByTagName("td");
        var resultList = [];
        for (var i = 0; i < tdElement.length; i = i + 6) {
            if (tdElement[i].getElementsByTagName("input")[0].checked) {
                var dict = {};
                dict["filename"] = tdElement[i + 1].innerHTML;
                dict["filedate"] = encoder_1.encoder.dateToYYMMDDHHMMSS(tdElement[i + 2].innerHTML);
                dict["filepath"] = tdElement[i + 3].innerHTML;
                if (tdElement[i + 5].innerHTML == inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ok_name")) {
                    dict["ng"] = 0;
                }
                else if (tdElement[i + 5].innerHTML === inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_name")) {
                    dict["ng"] = 1;
                }
                else if (tdElement[i + 5].innerHTML === inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_nr_name")) {
                    dict["ng"] = 2;
                }
                resultList.push(dict);
            }
        }
        return JSON.stringify(resultList);
    }
}
exports.ImageDataToJson = ImageDataToJson;
