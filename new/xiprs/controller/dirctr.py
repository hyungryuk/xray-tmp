# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 10:56:25 2018

@author: ryuk

@des :  -DB 이미지파일정보에 대한 REST API를 설정한다.
"""

from flask import request
from flask_restful import Resource

from ..model.imgmdl import *
from ..helper.messages import *
from .. import db
from ..configs.config import *

import os

class DirectoryRESTApi(Resource):

    def post(self):

        request_data = request.get_json()
        method_type = request_data.get('method_type')
        items = request_data.get('items')

        if method_type is not None:
            if items is not None:
                if method_type == 'rename':
                    return rename_processing(request_data)
                else:
                    pass
            else:
                message = COMMON_ITEMS_NOT_FOUND
                return json.dumps(message, ensure_ascii=True)
        else:
            message = COMMON_METHOD_TYPE_NOT_FOUND
            message.update({'items': request.get_json().get('items')})
            return json.dumps(message, ensure_ascii=True)

def rename_processing(request_data):
    try:
        items = request_data.get('items')
        options = request_data.get('options')
        if items is not None and options is not None:
            if type(items) == list:
                for i in range(0, len(items)):
                    split_path = options[i].get('filepath').split(FILE_TREE_SEPERATOR)
                    split_path[-1] = items[i].get('filename')
                    new_path = FILE_TREE_SEPERATOR.join(split_path)
                    os.rename(options[i].get('filepath'),new_path)

                message = DIR_RENAME_SUCCESS
                message.update({'items': items})
                return json.dumps(message, ensure_ascii=True)
        else:
            message = COMMON_ITEMS_NOT_FOUND
            return json.dumps(message, ensure_ascii=True)
    except Exception as e:
        message = DIR_RENAME_SUCCESS_FAIL
        message.update({'cause': str(e)})
        message.update({'items': request.get_json()['items']})
        return json.dumps(message, ensure_ascii=True)
