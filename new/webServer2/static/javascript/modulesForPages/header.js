"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const inversify_config_1 = require("../DIContainerConf/inversify.config");
const types_1 = require("../DIContainerConf/types");
const properties_1 = require("../properties/properties");
class Menu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userAuth: undefined,
            isOpened: false
        };
        this.menuOnClick = this.menuOnClick.bind(this);
    }
    menuOnClick(e) {
        this.setState(prevState => ({
            isOpened: !prevState.isOpened
        }));
    }
    componentDidMount() {
        var metaTags = document.getElementsByTagName("meta");
        for (var i = 0; i < metaTags.length; i++) {
            if (metaTags[i].hasAttribute("userAuth")) {
                this.setState({
                    userAuth: parseInt(metaTags[i].getAttribute("userAuth"))
                });
                break;
            }
        }
    }
    render() {
        if (this.state.userAuth == 0 && this.state.isOpened == true) {
            return (React.createElement("div", { className: "main_menu_box", onClick: this.menuOnClick },
                React.createElement("div", { className: "menuSymbol" },
                    React.createElement("i", { className: "fas fa-bars" })),
                React.createElement("div", { className: "main_menu" },
                    React.createElement("div", { className: "root_menu_box" },
                        React.createElement("div", { className: "root_menu" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_dash_root_name")),
                        React.createElement("div", { className: "first_level_sub_menu_box" },
                            React.createElement("div", { className: "first_level_sub_menu", onClick: function () { location.href = "/web/dashGraph"; } }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_dashgraph_name")),
                            React.createElement("div", { className: "first_level_sub_menu", onClick: function () { location.href = "/web/dashTable"; } }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_dashtable_name")))),
                    React.createElement("div", { className: "root_menu_box" },
                        React.createElement("div", { className: "root_menu" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_file_root_name")),
                        React.createElement("div", { className: "first_level_sub_menu_box" },
                            React.createElement("div", { className: "first_level_sub_menu", onClick: function () { location.href = "/web/imageFileTree"; } }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_filetree_name")),
                            React.createElement("div", { className: "first_level_sub_menu", onClick: function () { location.href = "/web/imageFileTable"; } }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_filetable_name")),
                            React.createElement("div", { className: "first_level_sub_menu", onClick: function () { location.href = "/web/imageFileLabeling"; } }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_filelabel_name")))),
                    React.createElement("div", { className: "root_menu_box" },
                        React.createElement("div", { className: "root_menu" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_model_root_name")),
                        React.createElement("div", { className: "first_level_sub_menu_box" },
                            React.createElement("div", { className: "first_level_sub_menu", onClick: function () { location.href = "/web/modelTraining"; } }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_trainmodel_name")),
                            React.createElement("div", { className: "first_level_sub_menu", onClick: function () { location.href = "/web/modelStatus"; } }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_traintable_name")),
                            React.createElement("div", { className: "first_level_sub_menu", onClick: function () { location.href = "/web/modelDeploy"; } }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_deploymodel_name")))),
                    React.createElement("div", { className: "root_menu_box" },
                        React.createElement("div", { className: "root_menu" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_setting_root_name")),
                        React.createElement("div", { className: "first_level_sub_menu_box" },
                            React.createElement("div", { className: "first_level_sub_menu", onClick: function () { location.href = "/web/userSetting"; } }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_usersetting_name")),
                            React.createElement("div", { className: "first_level_sub_menu", onClick: function () { location.href = "/web/lineSetting"; } }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_linesetting_name")),
                            React.createElement("div", { className: "first_level_sub_menu", onClick: function () { location.href = "/web/algorithmSetting"; } }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_algorithmsetting_name")),
                            React.createElement("div", { className: "first_level_sub_menu", onClick: function () { location.href = "/web/filterSetting"; } }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_filtersetting_name")),
                            React.createElement("div", { className: "first_level_sub_menu", onClick: function () { location.href = "/web/logManaging"; } }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_logmanager_name")),
                            React.createElement("div", { className: "first_level_sub_menu" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_enviromentsetting_name")))))));
        }
        else if (this.state.userAuth == 1 && this.state.isOpened == true) {
            return (React.createElement("div", { className: "main_menu_box", onClick: this.menuOnClick },
                React.createElement("div", { className: "menuSymbol" },
                    React.createElement("i", { className: "fas fa-bars" })),
                React.createElement("div", { className: "main_menu" },
                    React.createElement("div", { className: "root_menu_box" },
                        React.createElement("div", { className: "root_menu" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_dash_root_name")),
                        React.createElement("div", { className: "first_level_sub_menu_box" },
                            React.createElement("div", { className: "first_level_sub_menu" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_dashgraph_name")),
                            React.createElement("div", { className: "first_level_sub_menu" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_dashtable_name")))),
                    React.createElement("div", { className: "root_menu_box" },
                        React.createElement("div", { className: "root_menu" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_file_root_name")),
                        React.createElement("div", { className: "first_level_sub_menu_box" },
                            React.createElement("div", { className: "first_level_sub_menu" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_filetree_name")),
                            React.createElement("div", { className: "first_level_sub_menu" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_filetable_name")),
                            React.createElement("div", { className: "first_level_sub_menu" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_filelabel_name")))),
                    React.createElement("div", { className: "root_menu_box" },
                        React.createElement("div", { className: "root_menu" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_model_root_name")),
                        React.createElement("div", { className: "first_level_sub_menu_box" },
                            React.createElement("div", { className: "first_level_sub_menu" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_trainmodel_name")),
                            React.createElement("div", { className: "first_level_sub_menu" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_traintable_name")),
                            React.createElement("div", { className: "first_level_sub_menu" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_deploymodel_name")))))));
        }
        else {
            return (React.createElement("div", { className: "main_menu_box", onClick: this.menuOnClick },
                React.createElement("div", { className: "menuSymbol" },
                    React.createElement("i", { className: "fas fa-bars" }))));
        }
    }
}
class TitleComponent extends React.Component {
    render() {
        return (React.createElement("div", { className: "main_title" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("common_main_title")));
    }
}
class LogoutComponent extends React.Component {
    logoutOnClick() {
        $.ajax({
            url: properties_1.properties["WEB_PATH"] + "/web/do_logout",
            type: 'post',
        });
        location.href = "/";
    }
    render() {
        return (React.createElement("div", { className: "logout_box" },
            React.createElement("div", { className: "logout", onClick: this.logoutOnClick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("logout_sign"))));
    }
}
class CurrentUserComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: ''
        };
    }
    componentDidMount() {
        var metaTags = document.getElementsByTagName("meta");
        for (var i = 0; i < metaTags.length; i++) {
            if (metaTags[i].hasAttribute("username")) {
                this.setState({
                    username: metaTags[i].getAttribute("username")
                });
                break;
            }
        }
    }
    render() {
        return (React.createElement("div", { className: "user_name" }, this.state.username));
    }
}
class HeaderComponent extends React.Component {
    render() {
        return (React.createElement("div", { className: "main_title_Box" },
            React.createElement(Menu, null),
            React.createElement(TitleComponent, null),
            React.createElement(CurrentUserComponent, null),
            React.createElement(LogoutComponent, null)));
    }
}
exports.HeaderComponent = HeaderComponent;
