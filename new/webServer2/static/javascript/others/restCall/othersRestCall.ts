import {properties} from '../../properties/properties'

export class OpenImageRestCall{
    openImageWithJPG(paths:string[]){
        var result:any[]=[];

        var object:{[key:string]:any}={};
        object["method_type"] = 'show';
        object["items"] = paths;

        var jsonData = JSON.stringify(object);

        $.ajax({
            async: false,
            url:properties["WEB_PATH"]+"/rest/db/imagefile",
            type:'post',
            data:jsonData,
            accepts: {
              text: 'application/json'
            },
            contentType : 'application/json',
            success:function(data){

                data = JSON.parse(data);
                data = data["items"];
                for(var i=0;i<data.length;i++){

                    var image = new Image();
                    var src = "data:image/jpg;base64,"+data[i];
                    image.src = src.replace(/"/g,"");
                    result.push(image);
                    console.log(src);

                }
            },
             error: function(jqXHR, exception) {

             }
        });

        return result;

    }

    openImageWithBMP(paths:string[]){

        var result:any[]=[];

        var object={items:paths,method_type:"show"};

        var jsonData = JSON.stringify(object);

        $.ajax({
            async: false,
            url:properties["WEB_PATH"]+"/rest/db/imagefile",
            type:'post',
            data:jsonData,
            accepts: {
              text: 'application/json'
            },
            contentType : 'application/json',
            success:function(data){

                data = JSON.parse(data);
                data = data["items"];
                for(var i=0;i<data.length;i++){

                    var image = new Image();
                    var src = "data:image/bmp;base64,"+data[i];
                    image.src = src.replace(/"/g,"");
                    result.push(image);
                    console.log(src);

                }
            },
             error: function(jqXHR, exception) {

             }
        });

        return result;

    }
}

export class TrainingModelRestCall{
    trainModel(items:any[],options:any[]){
        var result:any[]=[];

        var object:{[key:string]:any}={};
        object["method_type"] = 'train';
        object["items"] = items;
        object["options"] = options;

        var jsonData = JSON.stringify(object);

        $.ajax({
            async: false,
            url:properties["WEB_PATH"]+"/ml/trainModel",
            type:'post',
            data:jsonData,
            accepts:{
                text:'application/json'
            },
            contentType : 'application/json',
            success:function(data){
                data = JSON.parse(data);
                result=data;
            },
             error: function(jqXHR, exception) {

             }
        });
        return result;
    }
    getTrainModelLog(items:any[],options:any[]):{[key:string]:any[]}{
        var result:any[]=[];

        var object:{[key:string]:any}={};
        object["method_type"] = 'view';
        object["items"] = items;
        object["options"] = options;

        var jsonData = JSON.stringify(object);

        $.ajax({
            async: false,
            url:properties["WEB_PATH"]+"/ml/trainModel",
            type:'post',
            data:jsonData,
            accepts:{
                text:'application/json'
            },
            contentType : 'application/json',
            success:function(data){
                data = JSON.parse(data);
                result=data;
            },
             error: function(jqXHR, exception) {

             }
        });
        return result;
    }

}

export class DeployModelRestCall{
    deployModel(items:any[],options:any[]){
        var result:any[]=[];

        var object:{[key:string]:any}={};
        object["method_type"] = 'pub';
        object["items"] = items;
        object["options"] = options;

        var jsonData = JSON.stringify(object);

        $.ajax({
            async: false,
            url:properties["WEB_PATH"]+"/ml/publishing",
            type:'post',
            data:jsonData,
            accepts:{
                text:'application/json'
            },
            contentType : 'application/json',
            success:function(data){
                data = JSON.parse(data);
                result=data;
            },
             error: function(jqXHR, exception) {

             }
        });
        return result;
    }

}

export class LoginRestCall{

    login(id:string,pw:string):{[key:string]:string}{

        var result:any;

        var item:{[key:string]:string}={};
        var object:{[key:string]:any[]}={};
        item["userid"] = id;
        item["password"] = pw;

        object["items"] = [item];

        var jsonData = JSON.stringify(object);

        $.ajax({
            async: false,
            url:properties["WEB_PATH"]+"/web/do_login",
            type:'post',
            data:jsonData,
            accepts:{
                text:'application/json'
            },
            contentType : 'application/json',
            success:function(data){
                data = JSON.parse(data);
                result = data;
            },
            error: function(jqXHR, exception) {

            }

        });
        return result;
    }
}
