const TYPES = {
    StaticLanguageInterface: Symbol.for("StaticLanguageInterface"),
    LineSelectorInterface:Symbol.for("LineSelectorInterface"),
    AlgorithmSelectorInterface:Symbol.for("AlgorithmSelectorInterface"),
    FilterSelectorInterface:Symbol.for("FilterSelectorInterface")
}

export{ TYPES };
