import * as React from 'react';
import * as ReactDOM from 'react-dom';

import 'jquery-ui/themes/base/all.css'
import 'jquery-ui/demos/demos.css'
import 'jquery-ui/external/requirejs/require.js'
import 'jquery-ui/ui/widgets/slider.js'


import { HeaderComponent } from "../modulesForPages/header";
import {LineSelectorInterface,AlgorithmSelectorInterface,FilterSelectorInterface} from '../others/selector/selector'
import {StaticLanguageInterface} from '../others/staticLanguageData/staticLanguageData'
import {myContainer} from '../DIContainerConf/inversify.config'
import {TYPES} from '../DIContainerConf/types'

import { LineAjaxCall,AlgorithmDataAjaxCall,FilterDataAjaxCall,TrainingDataAjaxCall } from '../others/restCall/dbRestcall'
import {TrainingModelRestCall} from '../others/restCall/othersRestCall'
import {encoder} from "../others/encoder/encoder";

import {properties} from '../properties/properties'

var selectedList:any[] = [];
ReactDOM.render(
  <HeaderComponent />,
  document.getElementById("header")
);

class DeleteButtonInTable extends React.Component<{modelcode:string,},{}> {
    constructor(props:any) {
        super(props);
        this.deleteOnclicked = this.deleteOnclicked.bind(this);
    }

    deleteOnclicked() {
        var trainingDataAjaxCallObj = new TrainingDataAjaxCall();
        trainingDataAjaxCallObj.delete(
            [],
            [{ modelcode: this.props.modelcode}]
        );
    }
    render() {
        return (
            <button className="delete_button" onClick={this.deleteOnclicked}>
                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_delete_table")}
            </button>
        );
    }
}

class FilterInTable extends React.Component<{filterList:any[],filtercode:string},{}> {
    constructor(props:any) {
        super(props);
        this.filterCodeToFilterName = this.filterCodeToFilterName.bind(
            this
        );
    }

    filterCodeToFilterName() {
        for (var i = 0; i < this.props.filterList.length; i++) {
            if (this.props.filterList[i].filtercode == this.props.filtercode) {
                return (
                    this.props.filterList[i].filtername
                );
            }
        }
    }

    render() {
        return <div>{this.filterCodeToFilterName()}</div>;
    }
}

class AlgorithmInTable extends React.Component<{algorithmList:any[],algorithmcode:string},{}> {
    constructor(props:any) {
        super(props);
        this.algorithmCodeToAlgorithmName = this.algorithmCodeToAlgorithmName.bind(
            this
        );
    }

    algorithmCodeToAlgorithmName() {
        for (var i = 0; i < this.props.algorithmList.length; i++) {
            if (this.props.algorithmList[i].algorithmcode == this.props.algorithmcode) {
                return (
                    this.props.algorithmList[i].algorithmname
                );
            }
        }
    }

    render() {
        return <div>{this.algorithmCodeToAlgorithmName()}</div>;
    }
}

class StatusInPopup extends React.Component<{status:string,id:string},{}> {
    constructor(props:any) {
        super(props);
        this.initDate = this.initDate.bind(this);
    }

    initDate() {
        $("#" + this.props.id).val(this.props.status);
    }

    componentDidMount() {
        this.initDate();
    }

    render() {
        return <input type="text" className="status_input" id={this.props.id} />;
    }
}

class AlgorithmSelectBoxInPopup extends React.Component<{id:string,algorithm:string},{}> {

    componentDidMount() {
        var algorithmDataAjaxCallObj = new AlgorithmDataAjaxCall();
        var algorithmList = algorithmDataAjaxCallObj.retrieve([], [])["items"];
        let algorithmSelectorObj = myContainer.get<AlgorithmSelectorInterface>(TYPES.AlgorithmSelectorInterface);
        algorithmSelectorObj.setDivId(this.props.id)
        algorithmSelectorObj.setDatas(algorithmList);
        algorithmSelectorObj.makeSelector();
        $("#" + this.props.id).val(this.props.algorithm);
    }

    render() {
        return (
            <select className="algorithm_input" id={this.props.id}>
                <option value="all">all</option>
            </select>

        );
    }
}


class FilterSelectBoxInPopup extends React.Component<{id:string,filter:string},{}> {

    componentDidMount() {
        var filterDataAjaxCallObj = new FilterDataAjaxCall();
        var filterList = filterDataAjaxCallObj.retrieve([], [])["items"];
        let filterSelectorObj = myContainer.get<FilterSelectorInterface>(TYPES.FilterSelectorInterface);
        filterSelectorObj.setDivId(this.props.id)
        filterSelectorObj.setDatas(filterList);
        filterSelectorObj.makeSelector();
        $("#" + this.props.id).val(this.props.filter);
    }

    render() {
        return (
            <select className="filter_input" id={this.props.id}>
                <option value="all">all</option>
            </select>

        );
    }
}
class AccuracyBoxInPopup extends React.Component<{startAcid:string,endAcid:string,startAccuracy:number,endAccuracy:number},{}> {

    componentDidMount() {
        $( "#startAccuracyRange" ).val(this.props.startAccuracy);
        $( "#endAccuracyRange" ).val(this.props.endAccuracy);
        $( "#slider-range" ).slider({
          range: true,
          min: 0,
          max: 100,
          values: [ this.props.startAccuracy, this.props.endAccuracy ],
          slide: function( event, ui ) {
            $( "#startAccuracyRange" ).val(ui.values[ 0 ]);
            $( "#endAccuracyRange" ).val(ui.values[ 1 ]);

          }
        });

    }

    render() {
        return (
            <div>
                <input type="text" id={this.props.startAcid} readOnly/>
                <input type="text" id={this.props.endAcid} readOnly/>

                <div id="slider-range"></div>
            </div>
        );
    }
}
class CheckBoxInTable extends React.Component<{modelcode:string},{}> {
    constructor(props:any){
        super(props);
        this.checkBoxOnChange = this.checkBoxOnChange.bind(this);
    }
    checkBoxOnChange(e:any){
        if(e.target.checked==true){
            selectedList.push(this.props.modelcode);
        }else{
            var index = selectedList.indexOf(this.props.modelcode);
            selectedList.splice(index,1);
        }
    }
    render(){
        return(
            <input type="checkbox" onChange={this.checkBoxOnChange} className="checkbox"/>
        );
    }

}

class RegdateBoxInPopup extends React.Component<{startDate:string,endDate:string,startDateid:string,endDateid:string},{}> {

    componentDidMount() {
        var filterDataAjaxCallObj = new FilterDataAjaxCall();
        var filterList = filterDataAjaxCallObj.retrieve([], [])["items"];
        let filterSelectorObj = myContainer.get<FilterSelectorInterface>(TYPES.FilterSelectorInterface);
        filterSelectorObj.setDivId(this.props.id)
        filterSelectorObj.setDatas(filterList);
        filterSelectorObj.makeSelector();
        $("#" + this.props.id).val(this.props.filter);
    }

    render() {
        return (
            <div>
                <input type="date" id={this.props.startDateid}/>
                <input type="date" id={this.props.endDateid}/>
            </div>
        );
    }
}

class Tbody extends React.Component<{data:any[],algorithmList:any[],filterList:any[]},{}> {
    render() {
        console.log(this.props.data);
        const data = this.props.data.map(item => (
            <tr>
                <td><CheckBoxInTable modelcode={item.modelcode}/></td>
                <td>{item.modelname}</td>
                <td>
                    <AlgorithmInTable
                        algorithmcode={item.algorithm}
                        algorithmList={this.props.algorithmList}
                    />
                </td>
                <td>
                    <FilterInTable
                        filtercode={item.filter}
                        filterList={this.props.filterList}
                    />
                </td>
                <td className="number">{item.accuracy}</td>
                <td>{item.user}</td>
                <td className="number">{item.status}</td>
                <td>{item.contents}</td>
                <td>{item.regdate}</td>

            </tr>
        ));
        return data;
    }
}


class TableHeaderAndBox extends React.Component<{data:any[],algorithmList:any[],filterList:any[]},{isSelectallClicked:boolean}> {
        constructor(props:any){
            super(props);
            this.state = {
                isSelectallClicked:false
            }
            this.selectAllOnClick = this.selectAllOnClick.bind(this);

        }
        selectAllOnClick(){

            var checkboxs = document.getElementsByClassName("checkbox");
            if(this.state.isSelectallClicked==false){
                for(var i=0;i<this.props.data.length;i++){
                    selectedList.push(this.props.data[i].modelcode);
                }
                for(var i=0;i<checkboxs.length;i++){
                    checkboxs[i].checked=true;
                }
            }else{
                selectedList=[];
                for(var i=0;i<checkboxs.length;i++){
                    checkboxs[i].checked=false;
                }
            }

            this.setState(prevState => ({
                isSelectallClicked: !prevState.isSelectallClicked
            }));

        }
    render() {
        return (
            <div className="table_box">
                <table className="table">
                    <thead className="thead">
                        <tr>
                            <th onClick={this.selectAllOnClick}>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("table_select_name")}
                            </th>

                            <th className="table_modelname">
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "modelstatus_model_name"
                                )}
                            </th>
                            <th className="table_algorithm">
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_algorithm_name")}
                            </th>
                            <th className="table_filter">
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "modelstatus_filter_name"
                                )}
                            </th>
                            <th className="table_accuracy">
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_accuracy_name")}
                            </th>
                            <th className="table_user">
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_user_name")}
                            </th>
                            <th className="table_status">
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "modelstatus_status_name"
                                )}
                            </th>
                            <th className="table_content">
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "modelstatus_contents_name"
                                )}
                            </th>
                            <th className="table_regdate">
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "modelstatus_regdate_name"
                                )}
                            </th>
                        </tr>
                    </thead>
                    <tbody className="tbody">
                        <Tbody
                            algorithmList={this.props.algorithmList}
                            filterList={this.props.filterList}
                            data={this.props.data}
                        />
                    </tbody>
                </table>
            </div>
        );
    }
}

class SearchBoxAndTable extends React.Component<{},{modelcode:string,modelname:string,algorithm:string,filter:string,start_accuracy:number,end_accuracy:number,user:string,status:string,start_regdate:string,end_regdate:string,currentPage:number,maxPage:number,perpage:number,isfilterOpen:boolean,resultData:any[],algorithmList:any[],filterList:any[]}> {
    constructor(props:any) {
        super(props);
        this.state = {
            modelcode: "",
            modelname: "",
            user: "",
            status: "",
            algorithm: "all",
            filter: "all",
            start_accuracy: 0,
            end_accuracy: 100,
            currentPage: 1,
            maxPage: -1,
            perpage: 10,
            isfilterOpen: false,
            start_regdate:"",
            end_regdate:"",
            algorithmList:[],
            filterList:[],
            resultData:[]
        };

        this.getDataUsingRestCallFunc = this.getDataUsingRestCallFunc.bind(
            this
        );

        this.modelcodeOnChange = this.modelcodeOnChange.bind(this);
        this.modelnameOnChange = this.modelnameOnChange.bind(this);
        this.algorithmOnChange = this.algorithmOnChange.bind(this);
        this.filterOnChange = this.filterOnChange.bind(this);
        this.userOnChange = this.userOnChange.bind(this);
        this.statusOnChange = this.statusOnChange.bind(this);
        this.start_accuracyOnChange = this.start_accuracyOnChange.bind(this);
        this.end_accuracyOnChange = this.end_accuracyOnChange.bind(this);
        this.start_regdateOnChange = this.start_regdateOnChange.bind(this);
        this.end_regdateOnChange = this.end_regdateOnChange.bind(this);

        this.searchOnclick = this.searchOnclick.bind(this);
        this.filterOnclick = this.filterOnclick.bind(this);
        this.filterCloseOnclick = this.filterCloseOnclick.bind(this);
        this.setFilterOnclick = this.setFilterOnclick.bind(this);

        this.firstPageOnclick = this.firstPageOnclick.bind(this);
        this.lastPageOnclick = this.lastPageOnclick.bind(this);
        this.prePageOnclick = this.prePageOnclick.bind(this);
        this.nextPageOnclick = this.nextPageOnclick.bind(this);
        this.currentPageOnKeyPress = this.currentPageOnKeyPress.bind(this);
    }
    setFilterOnclick() {
        this.setState({ status: $("#status").val() as string });
        this.setState({ algorithm: $("#algorithm option:selected").val() as string });
        this.setState({ filter: $("#filter option:selected").val()  as string});
        this.setState({ start_accuracy: parseInt($("#startAccuracyRange").val() as string)});
        console.log($("#startAccuracyRange").val());
        console.log($("#endAccuracyRange").val());
        this.setState({ end_accuracy: parseInt($("#endAccuracyRange").val() as string)});
        this.setState({ start_regdate: $("#startRegDate").val() as string });
        this.setState({ end_regdate: $("#endRegDate").val() as string });
        this.setState(prevState => ({
            isfilterOpen: !prevState.isfilterOpen
        }));
    }
    filterCloseOnclick() {
        this.setState(prevState => ({
            isfilterOpen: !prevState.isfilterOpen
        }));
    }
    filterOnclick() {
        this.setState(prevState => ({
            isfilterOpen: !prevState.isfilterOpen
        }));
    }
    getDataUsingRestCallFunc(page:number) {
        var trainingDataAjaxCallResult;
        var option1:{[key:string]:any} = {};
        var option2:{[key:string]:any} = {};
        var option5:{[key:string]:any} = {};

        if (this.state.algorithm != "all") {
            option1["algorithm"] = this.state.algorithm;
        }
        if (this.state.modelcode != "") {
            option1["modelcode"] = this.state.modelcode;
        }
        if (this.state.modelname != "") {
            option1["modelname"] = this.state.modelname;
        }
        if (this.state.user != "") {
            option1["user"] = this.state.user;
        }
        if (this.state.status != "") {
            option1["status"] = this.state.status;
        }
        if (this.state.filter != "all") {
            option1["filter"] = this.state.filter;
        }
        if (this.state.start_accuracy != "0" || this.state.end_accuracy != "100") {
            option5["start_accuracy"] = this.state.start_accuracy;
            option5["end_accuracy"] = this.state.end_accuracy;
        }
        if (this.state.start_regdate != "" && this.state.end_regdate != "") {
            option5["start_regdate"] = encoder.dateToYYMMDDHHMMSS(
                this.state.start_regdate + "000000"
            );
            option2["end_regdate"] = encoder.dateToYYMMDDHHMMSS(
                this.state.end_regdate + "235959"
            );
        }
        var trainingDataAjaxCallObj = new TrainingDataAjaxCall();
        trainingDataAjaxCallResult = trainingDataAjaxCallObj.retrieve(
            [],
            [option1, option2, {}, { page: page, perpage: this.state.perpage },option5]
        );
        console.log(trainingDataAjaxCallResult);
        return trainingDataAjaxCallResult;
    }

    firstPageOnclick() {
        var trainingDataAjaxCallResult;
        if (this.state.currentPage != 1) {
            trainingDataAjaxCallResult = this.getDataUsingRestCallFunc(1);
            this.setState({
                resultData: trainingDataAjaxCallResult["items"],
                currentPage: 1
            });

            $("#current_page").val(1);
        }
    }

    lastPageOnclick() {
        var trainingDataAjaxCallResult;
        if (
            this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage
        ) {
            trainingDataAjaxCallResult = this.getDataUsingRestCallFunc(this.state.maxPage);
            this.setState({
                resultData: trainingDataAjaxCallResult["items"],
                currentPage: this.state.maxPage
            });

            $("#current_page").val(this.state.maxPage);
        }
    }
    prePageOnclick() {
        var trainingDataAjaxCallResult;
        if (this.state.maxPage != -1 && this.state.currentPage > 1) {
            trainingDataAjaxCallResult = this.getDataUsingRestCallFunc(
                this.state.currentPage - 1
            );
            this.setState({ resultData: trainingDataAjaxCallResult["items"] });
            $("#current_page").val(this.state.currentPage - 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage - 1
            }));
        }
    }
    nextPageOnclick() {
        var trainingDataAjaxCallResult;
        if (
            this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage
        ) {
            trainingDataAjaxCallResult = this.getDataUsingRestCallFunc(
                this.state.currentPage + 1
            );
            this.setState({ resultData: trainingDataAjaxCallResult["items"] });
            $("#current_page").val(this.state.currentPage + 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage + 1
            }));
        }
    }
    currentPageOnKeyPress(e:any) {
        if (e.which == 13) {
            var pageVal = $("#current_page").val() as string;
            var page = parseInt(pageVal);
            if (isNaN(page)) {
                alert(myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_is_not_number_alert"));
                $("#current_page").val(this.state.currentPage);
            } else {
                if (page > this.state.maxPage) {
                    alert(
                        myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_greater_than_max_alert")
                    );
                    $("#current_page").val(this.state.currentPage);
                } else {
                    if (
                        this.state.maxPage != -1 &&
                        this.state.currentPage != page
                    ) {
                        var trainingDataAjaxCallResult = this.getDataUsingRestCallFunc(
                            page
                        );
                        this.setState({
                            resultData: trainingDataAjaxCallResult["items"],
                            currentPage: page
                        });
                        $("#current_page").val(page);
                    }
                }
            }
        }
    }

    modelcodeOnChange(e:any) {
        this.setState({
            modelcode: e.target.value
        });
    }
    modelnameOnChange(e:any) {
        this.setState({
            modelname: e.target.value
        });
    }
    algorithmOnChange() {
        this.setState({
            algorithm: $("#algorithmSelector option:selected").val() as string
        });
    }
    filterOnChange() {
        this.setState({
            filter: $("#filterSelector option:selected").val() as string
        });
    }
    userOnChange(e:any) {
        this.setState({
            user: e.target.value
        });
    }
    statusOnChange(e:any) {
        this.setState({
            status: e.target.value
        });
    }
    start_accuracyOnChange(e:any) {
        this.setState({
            start_accuracy: e.target.value
        });
    }
    end_accuracyOnChange(e:any) {
        this.setState({
            end_accuracy: e.target.value
        });
    }
    start_regdateOnChange(e:any) {
        this.setState({
            start_regdate: e.target.value
        });
    }
    end_regdateOnChange(e:any) {
        this.setState({
            end_regdate: e.target.value
        });
    }
    deleteOnClick(e:any){
        var trainingDataAjaxCallObj = new TrainingDataAjaxCall();
        var optionList:any[] = [];
        for(var i=0;i<selectedList.length;i++){
            optionList.push({modelcode:selectedList[i]});
        }
        trainingDataAjaxCallObj.delete(
            [],
            optionList
        );
        location.reload();
    }
    searchOnclick() {
        var trainingDataAjaxCallResult = this.getDataUsingRestCallFunc(1);

        this.setState({ resultData: trainingDataAjaxCallResult["items"] });

        var maxpage = Math.ceil(trainingDataAjaxCallResult["total"] / this.state.perpage);
        this.setState({ maxPage: maxpage });

        if (trainingDataAjaxCallResult["items"].length == 0) {
            $("#current_page").val(0);
            $("#max_page").val(0);
            this.setState({ maxPage: 0, currentPage: 0 });
        } else {
            $("#current_page").val(1);
            $("#max_page").val(maxpage);
            this.setState({ maxPage: maxpage, currentPage: 1 });
        }
    }

    componentDidMount() {
        var filterDataAjaxCallObj = new FilterDataAjaxCall();
        var filterList = filterDataAjaxCallObj.retrieve([], [])["items"];
        var algorithmDataAjaxCallObj = new AlgorithmDataAjaxCall();
        var algorithmList = algorithmDataAjaxCallObj.retrieve([], [])["items"];
        this.setState({
            algorithmList:algorithmList,
            filterList:filterList
        });

        this.searchOnclick();

    }
    render() {
        const popup = (
            <div className="optional_settings_box">
                <div className="popup_close_button">
                    <i
                        className="fas fa-window-close"
                        onClick={this.filterCloseOnclick}
                    />
                </div>
                <div className="status_input_box">
                    <label>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_status_name")}
                        <StatusInPopup id="status" status={this.state.status} />
                    </label>
                </div>
                <div className="algorithm_input_box">
                    <label>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_algorithm_name")}
                        <AlgorithmSelectBoxInPopup algorithm={this.state.algorithm} id="algorithm" />
                    </label>
                </div>
                <div className="filter_input_box">
                    <label>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_filter_name")}
                        <FilterSelectBoxInPopup filter={this.state.algorithm} id="filter" />
                    </label>
                </div>
                <div className="accuracy_input_box">
                    <label>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_accuracy_name")}
                        <AccuracyBoxInPopup
                            startAccuracy={this.state.start_accuracy}
                            endAccuracy={this.state.end_accuracy}
                            startAcid="startAccuracyRange"
                            endAcid="endAccuracyRange"
                        />
                    </label>
                </div>
                <div className="regDate_input_box">
                    <label>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_regdate_name")}
                        <RegdateBoxInPopup
                            startDate={this.state.start_regdate}
                            endDate={this.state.end_regdate}
                            startDateid="startRegDate"
                            endDateid="endRegDate"
                        />
                    </label>
                </div>
                <div className="filter_set_button">
                    <button
                        className="set_button"
                        onClick={this.setFilterOnclick}
                    >
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("filter_setting_button")}
                    </button>
                </div>
            </div>
        );
        if (this.state.isfilterOpen) {
            return (
                <div className="main_box">
                    <div className="current_page_info">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_model_root_name")}
                        <i className="fas fa-angle-right"></i>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_traintable_name")}
                    </div>
                    <div>{popup}</div>
                    <div className="searchBox">
                        {/* <div className="modelcode_input_box">
                            <label>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "modelstatus_modelcode_name"
                                )}
                                <input
                                    type="text"
                                    className="modelcode_input"
                                    onChange={this.modelcodeOnChange}
                                    defaultValue={this.state.modelcode}
                                />
                            </label>
                        </div> */}
                        <div className="modelname_input_box">
                            <label>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "modelstatus_model_name"
                                )}
                                <input
                                    type="text"
                                    className="model_name_input"
                                    onChange={this.modelnameOnChange}
                                    defaultValue={this.state.modelname}
                                />
                            </label>
                        </div>
                        <div className="user_input_box">
                            <label>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "modelstatus_user_name"
                                )}
                                <input
                                    type="text"
                                    className="user_input"
                                    onChange={this.userOnChange}
                                    defaultValue={this.state.user}
                                />
                            </label>
                        </div>
                        <div className="button_box">
                            <div className="set_specific_filter_button_box">
                                <button
                                    className="set_specific_filter_button"
                                    onClick={this.filterOnclick}
                                >
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "imagefiletable_set_specific_filter_button_box_name"
                                    )}
                                </button>
                            </div>
                            <div className="search_button_box">
                                <button
                                    className="searchButton"
                                    onClick={this.searchOnclick}
                                >
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "dashtable_search_button_name"
                                    )}
                                </button>
                            </div>
                            <div>
                                <button className="items_delete_button" onClick={this.deleteOnClick}>{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "table_selected_item_delete_button"
                                )}</button>
                            </div>
                        </div>
                    </div>

                    <div>
                        <TableHeaderAndBox
                            algorithmList={this.state.algorithmList}
                            filterList={this.state.filterList}
                            data={this.state.resultData}
                        />
                    </div>
                    <div className="page_box">
                        <div className="pre_page_box">
                            <i
                                className="fas fa-angle-double-left"
                                onClick={this.firstPageOnclick}
                            />
                            <i
                                className="fas fa-angle-left"
                                onClick={this.prePageOnclick}
                            />
                        </div>
                        <div className="page_input_box">
                            <input
                                type="text"
                                className="current_page_input"
                                id="current_page"
                                onKeyPress={this.currentPageOnKeyPress}
                            />
                            <div className="slash">
                            </div>
                            <input
                                type="text"
                                className="max_page_input"
                                readOnly="true"
                                id="max_page"
                            />
                        </div>
                        <div className="post_page_box">
                            <i
                                className="fas fa-angle-right"
                                onClick={this.nextPageOnclick}
                            />
                            <i
                                className="fas fa-angle-double-right"
                                onClick={this.lastPageOnclick}
                            />
                        </div>
                    </div>
                </div>
            );
        } else {
            return (
                <div className="main_box">
                    <div className="current_page_info">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_model_root_name")}
                        <i className="fas fa-angle-right"></i>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_traintable_name")}
                    </div>
                    <div className="searchBox">
                        {/* <div className="modelcode_input_box">
                            <label>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "modelstatus_modelcode_name"
                                )}
                                <input
                                    type="text"
                                    className="modelcode_input"
                                    id="modelcode"
                                    onChange={this.modelcodeOnChange}
                                    defaultValue={this.state.modelcode}
                                />
                            </label>
                        </div> */}
                        <div className="modelname_input_box">
                            <label>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "modelstatus_model_name"
                                )}
                                <input
                                    type="text"
                                    className="model_name_input"
                                    onChange={this.modelnameOnChange}
                                    defaultValue={this.state.modelname}
                                />
                            </label>
                        </div>
                        <div className="user_input_box">
                            <label>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "modelstatus_user_name"
                                )}
                                <input
                                    type="text"
                                    className="user_input"
                                    onChange={this.userOnChange}
                                    defaultValue={this.state.user}
                                />
                            </label>
                        </div>
                        <div className="button_box">
                            <div className="set_specific_filter_button_box">
                                <button
                                    className="set_specific_filter_button"
                                    onClick={this.filterOnclick}
                                >
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "imagefiletable_set_specific_filter_button_box_name"
                                    )}
                                </button>
                            </div>
                            <div className="search_button_box">
                                <button
                                    className="searchButton"
                                    onClick={this.searchOnclick}
                                >
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "dashtable_search_button_name"
                                    )}
                                </button>
                            </div>
                            <div>
                                <button className="items_delete_button" onClick={this.deleteOnClick}>{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "table_selected_item_delete_button"
                                )}</button>
                            </div>
                        </div>
                    </div>

                    <div>
                        <TableHeaderAndBox
                            algorithmList={this.state.algorithmList}
                            filterList={this.state.filterList}
                            data={this.state.resultData}
                        />
                    </div>
                    <div className="page_box">
                        <div className="pre_page_box">
                            <i
                                className="fas fa-angle-double-left"
                                onClick={this.firstPageOnclick}
                            />
                            <i
                                className="fas fa-angle-left"
                                onClick={this.prePageOnclick}
                            />
                        </div>
                        <div className="page_input_box">
                            <input
                                type="text"
                                className="current_page_input"
                                id="current_page"
                                onKeyPress={this.currentPageOnKeyPress}
                            />
                            <div className="slash">
                            </div>
                            <input
                                type="text"
                                className="max_page_input"
                                readOnly="true"
                                id="max_page"
                            />
                        </div>
                        <div className="post_page_box">
                            <i
                                className="fas fa-angle-right"
                                onClick={this.nextPageOnclick}
                            />
                            <i
                                className="fas fa-angle-double-right"
                                onClick={this.lastPageOnclick}
                            />
                        </div>
                    </div>
                </div>
            );
        }
    }
}

ReactDOM.render(
    <div>
        <SearchBoxAndTable />
    </div>,
    document.getElementById("main")
);
