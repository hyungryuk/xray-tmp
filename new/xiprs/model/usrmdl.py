# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 10:48:59 2018

@author: ryuk
"""
from .. import db
from flask_login import UserMixin

def create_user(item):
    db.session.add(item)


class UserInfo(db.Model):
    __tablename__ = 'iis_user'
    __table_args__ = {'mysql_collate': 'utf8_general_ci', 'mysql_engine': 'MyISAM'}

    userid = db.Column(db.VARCHAR(32), primary_key=True, nullable=False)
    password = db.Column(db.VARCHAR(128), nullable=False)
    username = db.Column(db.VARCHAR(32), nullable=False)
    department = db.Column(db.VARCHAR(32), nullable=False)
    phone = db.Column(db.VARCHAR(32), nullable=False)
    email = db.Column(db.VARCHAR(128), nullable=False)
    grade = db.Column(db.Integer, nullable=False)
    regdate = db.Column(db.DateTime, nullable=False)

    def __init__(self, userid, password, username, department, phone, email, grade):
        self.userid = userid
        self.password = password
        self.username = username
        self.department = department
        self.phone = phone
        self.email = email
        self.grade = grade


    def __repr__(self):
        return repr((self.userid, self.password, self.username, self.department, self.phone, self.email, self.grade, self.regdate))

