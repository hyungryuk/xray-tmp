"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const inversify_1 = require("inversify");
const languageProperties_1 = require("../../../language/languageProperties");
const properties_1 = require("../../properties/properties");
let StaticLanguageWithCsv = class StaticLanguageWithCsv {
    constructor() {
        try {
            let metaTags = document.getElementsByTagName("meta");
            for (var i = 0; i < metaTags.length; i++) {
                if (metaTags[i].hasAttribute("language")) {
                    this.languageType = metaTags[i].getAttribute("language");
                    break;
                }
            }
        }
        catch (err) {
            location.href = properties_1.properties["WEB_PATH"] + "";
        }
    }
    getTextUsingKey(key) {
        return languageProperties_1.language[this.languageType][key];
    }
};
StaticLanguageWithCsv = __decorate([
    inversify_1.injectable()
], StaticLanguageWithCsv);
exports.StaticLanguageWithCsv = StaticLanguageWithCsv;
