"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("jstree");
const dbRestcall_1 = require("../restCall/dbRestcall");
const othersRestcall_1 = require("../restCall/othersRestcall");
const properties_1 = require("../../properties/properties");
var selectedPath;
exports.jsTreeModule = {
    reportMenu: function (node) {
        return {
            deleteItem: {
                "label": "Remove",
                "action": function (obj) {
                    var ImageFileAjaxCallObj = new dbRestcall_1.ImageFileAjaxCall();
                    ImageFileAjaxCallObj.delete([{ "isremoved": 1 }], [{ "filepath": selectedPath }]);
                    $("#tree").jstree(true).delete_node(node);
                }
            }
        };
    },
    setDoubleClickFunc: function (divId, imageDivId) {
        $('#' + divId).bind("dblclick.jstree", function (data) {
            var OpenImageRestCallObj = new othersRestcall_1.OpenImageRestCall();
            var image = OpenImageRestCallObj.openImageWithJPG([selectedPath]);
            image[0].setAttribute("class", "image");
            image[0].setAttribute("id", selectedPath);
            document.getElementById(imageDivId).innerHTML = "";
            document.getElementById(imageDivId).appendChild(image[0]);
        });
    },
    setOnChangeFunction: function (divId) {
        $('#' + divId).on("changed.jstree", function (e, data) {
            if (data.selected[0] != undefined) {
                var imagepath = data.selected[0].replace(/@/gi, properties_1.properties["DIRECTORY_SEPERATOR"]);
                selectedPath = imagepath;
            }
        });
    },
    setLazyLoadingJsTree: function (divId, urlToGetTreeData) {
        if (!document.getElementById(divId)) {
            return false;
        }
        else {
            $('#' + divId).jstree({
                'core': {
                    'check_callback': true,
                    'data': {
                        'url': function (node) {
                            if (node.id === '#') {
                                return urlToGetTreeData + "?nodeId=0";
                            }
                            else {
                                return urlToGetTreeData + "?nodeId=" + node.id;
                            }
                        },
                        'data': function (node) {
                            return { 'id': node.id };
                        },
                    }
                },
                'plugins': ["contextmenu"],
                "contextmenu": {
                    "items": exports.jsTreeModule.reportMenu
                }
            });
            return true;
        }
    }
};
