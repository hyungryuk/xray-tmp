import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { HeaderComponent } from "../modulesForPages/header";
import {LineSelectorInterface} from '../others/selector/selector'
import {StaticLanguageInterface} from '../others/staticLanguageData/staticLanguageData'
import {myContainer} from '../DIContainerConf/inversify.config'
import {TYPES} from '../DIContainerConf/types'

import { ImageFileAjaxCall,LineAjaxCall } from '../others/restCall/dbRestcall'
import {encoder} from "../others/encoder/encoder";
import { jsTreeModule } from "../others/jsTreeModule/jsTreeModule";

import {properties} from '../properties/properties'

ReactDOM.render(
  <HeaderComponent />,
  document.getElementById("header")
);

class FileInfo extends React.Component<{filename:string,filedate:string,line:string,filepath:string,lotno:string,ng:string,ng_by_user:string,ng_by_mes:string},{}> {
    render() {
        return (
            <div className="info_box">

                <div className="each_info_div">
                    <label>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletree_filedate_name")}
                    </label>
                    <div>{this.props.filedate}</div>
                </div>
                <div className="each_info_div">
                    <label>{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletree_line_name")}</label>
                    <div>{this.props.line}</div>
                </div>
                <div className="each_info_div">
                    <label>{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletree_lotno_name")}</label>
                    <div>{this.props.lotno}</div>
                </div>
                <div className="each_info_div">
                    <label>{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletree_ng_name")}</label>
                    <div>{this.props.ng}</div>
                </div>
                <div className="each_info_div">
                    <label>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletree_ng_by_user_name")}
                    </label>
                    <div>{this.props.ng_by_user}</div>
                </div>
                <div className="each_info_div">
                    <label>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletree_ng_by_mes_name")}
                    </label>
                    <div>{this.props.ng_by_mes}</div>
                </div>
            </div>
        );
    }
}
class ImageShow extends React.Component {
    componentDidMount() {
        jsTreeModule.setDoubleClickFunc("tree", "show_image");
    }

    render() {
        return <div id="show_image" />;
    }
}
class FileTree extends React.Component {
    componentDidMount() {
        jsTreeModule.setOnChangeFunction("tree");
        jsTreeModule.setLazyLoadingJsTree(
            "tree",
            properties["WEB_PATH"] + "/rest/db/imagefile"
        );
    }

    render() {
        return <div id="tree" />;
    }
}

class FileTreeAndImageView extends React.Component<{},{filename:string,filedate:string,line:string,filepath:string,lotno:string,ng:string,ng_by_user:string,ng_by_mes:string}> {
    constructor(props:any) {
        super(props);
        this.state = {
            filepath: "",
            filename: "",
            filedate: "",
            line: "",
            lotno: "",
            ng: "",
            ng_by_user: "",
            ng_by_mes: ""
        };
        this.imageDivMouseHover = this.imageDivMouseHover.bind(this);
        this.mouseOnDoubleClick = this.mouseOnDoubleClick.bind(this);
    }
    imageDivMouseHover() {
        if ($("#show_image").children.length > 0) {
            if (
                $("img")
                    .first()
                    .attr("id") != this.state.filepath
            ) {
                this.setState({
                    filepath: "",
                    filename: "",
                    filedate: "",
                    line: "",
                    lotno: "",
                    ng: "",
                    ng_by_user: "",
                    ng_by_mes: ""
                });
            }
        }
    }
    mouseOnDoubleClick() {
        if($("img").length){
            if ($("#show_image").children.length > 0) {
                var imageFileAjaxCallObj = new ImageFileAjaxCall();
                var imageRestCallResult = imageFileAjaxCallObj.retrieve(
                    [],
                    [
                        {
                            filepath: $("img")
                                .first()
                                .attr("id")
                        }
                    ]
                );
                if (imageRestCallResult["items"].length > 0) {
                    this.setState({
                        filename: imageRestCallResult["items"][0].filename,
                        filedate: imageRestCallResult["items"][0].filedate,
                        filepath: imageRestCallResult["items"][0].filepath,
                        lotno: imageRestCallResult["items"][0].lotno
                    });
                    var lineAjaxCallObj = new LineAjaxCall();
                    var lineRestCallResult = lineAjaxCallObj.retrieve([], []);
                    for (var i = 0; i < lineRestCallResult["items"].length; i++) {
                        if (
                            lineRestCallResult["items"][i].linecode ==
                            imageRestCallResult["items"][0].linecode
                        ) {
                            this.setState({
                                line:
                                    lineRestCallResult["items"][i].factory +
                                    " " +
                                    lineRestCallResult["items"][i].line
                            });
                            break;
                        }
                    }

                    if (imageRestCallResult["items"][0].ng == 0) {
                        this.setState({
                            ng: myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ok_name")
                        });
                    } else if (imageRestCallResult["items"][0].ng == 1) {
                        this.setState({
                            ng: myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_name")
                        });
                    } else {
                        this.setState({
                            ng: myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_nr_name")
                        });
                    }

                    if (imageRestCallResult["items"][0].ng_by_user == 0) {
                        this.setState({
                            ng_by_user: myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ok_name")
                        });
                    } else if (imageRestCallResult["items"][0].ng_by_user == 1) {
                        this.setState({
                            ng_by_user: myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_name")
                        });
                    } else {
                        this.setState({
                            ng_by_user: myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_nr_name")
                        });
                    }

                    if (imageRestCallResult["items"][0].ng_by_mes == 0) {
                        this.setState({
                            ng_by_mes: myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ok_name")
                        });
                    } else if (imageRestCallResult["items"][0].ng_by_mes == 1) {
                        this.setState({
                            ng_by_mes: myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_name")
                        });
                    } else {
                        this.setState({
                            ng_by_mes: myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_nr_name")
                        });
                    }
                } else {
                    this.setState({
                        filepath: "",
                        filename: "",
                        filedate: "",
                        line: "",
                        lotno: "",
                        ng: "",
                        ng_by_user: "",
                        ng_by_mes: ""
                    });
                }
            }
        }

    }
    render() {
        return (
            <div className="main_box" onDoubleClick={this.mouseOnDoubleClick}>
                <div className="current_page_info">
                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_file_root_name")}
                    <i className="fas fa-angle-right"></i>
                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_filetree_name")}
                </div>
                <div className="file_tree_box">
                    <FileTree />
                </div>
                <div
                    className="image_view_box"
                    onMouseEnter={this.imageDivMouseHover}
                >
                    <ImageShow />
                </div>
                <FileInfo
                    filepath={this.state.filepath}
                    filename={this.state.filename}
                    filedate={this.state.filedate}
                    line={this.state.line}
                    lotno={this.state.lotno}
                    ng={this.state.ng}
                    ng_by_user={this.state.ng_by_user}
                    ng_by_mes={this.state.ng_by_mes}
                />
            </div>
        );
    }
}

ReactDOM.render(
    <div>
        <FileTreeAndImageView />
    </div>,

    document.getElementById("main")
);
