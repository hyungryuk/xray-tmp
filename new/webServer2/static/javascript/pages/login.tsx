import * as React from 'react';
import * as ReactDOM from 'react-dom';

import {StaticLanguageInterface} from '../others/staticLanguageData/staticLanguageData'
import {myContainer} from '../DIContainerConf/inversify.config'
import {TYPES} from '../DIContainerConf/types'

import { LoginRestCall } from '../others/restCall/othersRestCall'

import {properties} from '../properties/properties'

class LoginHeader extends React.Component<{},{}> {

    render(){
        return(

            <div>
                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("login_main_title")}
            </div>

        );
    }

}
ReactDOM.render(
  <LoginHeader />,
  document.getElementById("header")
);

class LoginMain extends React.Component<{},{id:string,pw:string}> {
    constructor(props:any){
        super(props);
        this.state={
            id:"",
            pw:""
        };

        this.idOnChange = this.idOnChange.bind(this);
        this.pwOnChange = this.pwOnChange.bind(this);
        this.loginOnClick = this.loginOnClick.bind(this);
        this.onKeyPress = this.onKeyPress.bind(this);

    }
    idOnChange(e:any){
        this.setState({
            id:e.target.value
        })
    }
    pwOnChange(e:any){
        this.setState({
            pw:e.target.value
        })
    }

    loginOnClick(){
        if(this.state.id ==""){
            myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("login_id_empty_message");
        }else if(this.state.pw ==""){
            myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("login_pw_empty_message");
        }else{
            var loginRestCallObj = new LoginRestCall();
            var loginRestResult = loginRestCallObj.login(this.state.id,this.state.pw);
            console.log(loginRestResult);
            if(loginRestResult["message"]=="login_success"){
                location.href="/web/dashGraph";
            }else{
                alert(myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("login_wrong"));
            }
        }
    }
    onKeyPress(e:any){
        console.log("asdad");
        if (e.which == 13) {
            this.loginOnClick();
        }
    }
    render(){
        return(

            <div className="login_box">
                <div className="inside_login_box">
                    <div className="id_box">
                        <div className="id_name">
                            {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("login_id_name")}
                        </div>
                        <input type="text" className="id_input" placeholder={myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("login_id_empty_message")} onChange={this.idOnChange} onKeyPress={this.onKeyPress} />
                    </div>
                    <div className="pw_box">
                        <div className="pw_name">
                            {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("login_pw_name")}
                        </div>
                        <input type="password" className="pw_input" placeholder={myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("login_pw_empty_message")} onChange={this.pwOnChange} onKeyPress={this.onKeyPress}/>
                    </div>

                </div>
                <div className="login_button_div">
                    <button className="login_button" onClick={this.loginOnClick}>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("login")}
                    </button>
                </div>
            </div>

        );
    }

}
ReactDOM.render(
  <LoginMain />,
  document.getElementById("main")
);
