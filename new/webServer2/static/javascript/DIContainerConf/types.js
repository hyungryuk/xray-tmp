"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const TYPES = {
    StaticLanguageInterface: Symbol.for("StaticLanguageInterface"),
    LineSelectorInterface: Symbol.for("LineSelectorInterface"),
    AlgorithmSelectorInterface: Symbol.for("AlgorithmSelectorInterface"),
    FilterSelectorInterface: Symbol.for("FilterSelectorInterface")
};
exports.TYPES = TYPES;
