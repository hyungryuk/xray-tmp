# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 11:04:06 2018

@author: ryuk

@des :  -모델정보에 대한 REST API를 설정한다.
"""
from flask_restful import Resource
from flask import request
import json
from datetime import datetime
from .. import db
from ..model.dpmmdl import *
from ..helper.messages import *
from ..helper.util import converter
from ..model.logmdl import *


class DeployModelInfoRESTApi(Resource):

    def post(self):

        request_data = request.get_json()
        method_type = request_data.get('method_type')
        items = request_data.get('items')

        if method_type is not None:
            if items is not None:
                if method_type == 'view':
                    return view_processing(request_data)
                elif method_type == 'insert':
                    return insert_processing(request_data)
                elif method_type == 'update':
                    return update_processing(request_data)
                else:
                    pass
            else:
                message = COMMON_ITEMS_NOT_FOUND
                return json.dumps(message, ensure_ascii=True)
        else:
            message = COMMON_METHOD_TYPE_NOT_FOUND
            message.update({'items': request.get_json().get('items')})
            return json.dumps(message, ensure_ascii=True)

def update_processing(request_data):
    try:
        items = request_data.get('items')
        options = request_data.get('options')
        if items is not None and options is not None:
            if type(items) == list:
                len_val = len(items)
                for i in range (0,len(items)):
                    query_input_element = options[i]
                    query_result = db.session.query(DPModelInfo).filter_by(**query_input_element).all()
                    update_element = items[i]
                    for query_result_element in query_result:
                        for update_element_key in update_element:
                            setattr(query_result_element, update_element_key, update_element[update_element_key])
                db.session.commit()

                message = DLM_UPDATE_SUCCESS
                message.update({'items': items})
                return json.dumps(message, ensure_ascii=True)
        else:
            message = COMMON_ITEMS_NOT_FOUND
            return json.dumps(message, ensure_ascii=True)
    except Exception as e:
        db.session.rollback()
        db.session.remove()
        message = DLM_UPDATE_SUCCESS_FAIL
        message.update({'cause': str(e)})
        message.update({'items': request.get_json()['items']})
        return json.dumps(message, ensure_ascii=True)

def insert_processing(request_data):
    try:
        items = request_data.get('items')

        if type(items) == list:
            for item in items:
                dpm_info = DPModelInfo(item.get('linecode'),
                                           item.get('filtercode'), item.get('modelcode'), item.get('deploydate'),
                                           item.get('isrunning'),item.get('accuracy'),item.get('modelname'),item.get('user'))

                create_dpm(dpm_info)

            log_info = LogInfo(LOG_DELETE_LOG["kind"],LOG_DELETE_LOG["title"], LOG_DELETE_LOG["contents"]+" by "+request_data.get('userid'))
            create_log(log_info)
            db.session.commit()

            message = DLM_CREATE_SUCCESS
            message.update({'items': items})
            return json.dumps(message, ensure_ascii=True)

    except Exception as e:
        db.session.rollback()
        db.session.remove()
        message = DLM_CREATE_SUCCESS_FAIL
        message.update({'cause': str(e)})
        message.update({'items': request.get_json()['items']})
        print(message)
        return json.dumps(message, ensure_ascii=True)

def view_processing(request_data):
    try:
        items = request_data.get('items')
        options = request_data.get('options')

        if len(options) > 0:
            filter_params = make_filter_params(options[0])
            if len(options) == 1:
                result = db.session.query(DPModelInfo).filter_by(**filter_params).all()
            elif len(options) == 3:
                orderby = options[2].get('orderby')
                if not orderby is None:
                    result = db.session.query(DPModelInfo).filter_by(**filter_params).order_by(orderby).all()
            elif len(options) == 4:
                page = options[3].get('page')
                per_page = options[3].get('perpage')
                orderby = options[2].get('orderby')
                if not orderby is None:
                    result = db.session.query(DPModelInfo).filter_by(**filter_params).order_by(orderby).paginate(
                            int(page), int(per_page))
                else:
                    result = db.session.query(DPModelInfo).filter_by(**filter_params).paginate(int(page),
                                                                                                   int(per_page))

            else:
                pass

            if len(options) == 4:
                return json.dumps({'items': [item.__dict__ for item in result.items], 'total': result.total,
                                   'return_code': DLM_VIEW_SUCCESS.get('return_code'),
                                   'message': DLM_VIEW_SUCCESS.get('message')}, default=converter)
            else:
                return json.dumps({'items': [item.__dict__ for item in result],
                                   'return_code': DLM_VIEW_SUCCESS.get('return_code'),
                                   'message': DLM_VIEW_SUCCESS.get('message')}, default=converter)
        else:
            result = db.session.query(DPModelInfo).all()
            return json.dumps({'items': [item.__dict__ for item in result],
                               'return_code': DLM_VIEW_SUCCESS.get('return_code'),
                               'message': DLM_VIEW_SUCCESS.get('message')}, default=converter)

    except Exception as e:
        message = DLM_VIEW_SUCCESS_FAIL
        message.update({'cause': str(e)})
        message.update({'items': request_data['items']})
        return json.dumps(message, ensure_ascii=True)


def make_filter_params(item):
    filter_params = {}
    if not item.get('linecode') is None:
        filter_params['linecode'] = item.get('linecode')

    if not item.get('filtercode') is None:
        filter_params['filtercode'] = item.get('filtercode')

    if not item.get('modelcode') is None:
        filter_params['modelcode'] = item.get('modelcode')

    if not item.get('deploydate') is None:
        filter_params['deploydate'] = item.get('deploydate')

    if not item.get('isrunning') is None:
        filter_params['isrunning'] = item.get('isrunning')

    if not item.get('accuracy') is None:
        filter_params['accuracy'] = item.get('accuracy')

    if not item.get('modelname') is None:
        filter_params['modelname'] = item.get('modelname')

    if not item.get('user') is None:
        filter_params['user'] = item.get('user')

    return filter_params
