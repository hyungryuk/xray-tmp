﻿var webpack = require("webpack");
var path = require('path');
var SOURCE_DIR = path.resolve(__dirname, 'javascript/');

module.exports = {
    mode: 'development',
    entry: {
            dashGraphPage : './static/javascript/pages/dashGraphPage.js',
            algorithmSettingPage : './static/javascript/pages/algorithmSettingPage.js',
            dashTablePage : './static/javascript/pages/dashTablePage.js',
            filterSettingPage : './static/javascript/pages/filterSettingPage.js',
            imageFileLabelingPage : './static/javascript/pages/imageFileLabelingPage.js',
            imageFileTablePage : './static/javascript/pages/imageFileTablePage.js',
            imageFileTreePage : './static/javascript/pages/imageFileTreePage.js',
            lineSettingPage : './static/javascript/pages/lineSettingPage.js',
            login : './static/javascript/pages/login.js',
            logManagingPage : './static/javascript/pages/logManagingPage.js',
            modelDeployPage : './static/javascript/pages/modelDeployPage.js',
            modelStatusPage : './static/javascript/pages/modelStatusPage.js',
            modelTrainingPage : './static/javascript/pages/modelTrainingPage.js',
            settings : './static/javascript/pages/settings.js',
            userSettingPage : './static/javascript/pages/userSettingPage.js'
    },
    output: {
        path: path.resolve(__dirname, 'static/dist'),
        filename: '[name].bundle.js'
    },
    module: {
        rules: [
            { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" },
            { test: /\.jsx$/, exclude: /node_modules/, loader: "babel-loader" },
            { test: /\.css$/, use: [ 'style-loader', 'css-loader' ]},
            { test: /\.(gif|png|jpe?g|svg)$/i, use: ['file-loader',{loader: 'image-webpack-loader',  options: {bypassOnDebug: true, // webpack@1.x
                        disable: true, // webpack@2.x and newer
           },
    },
  ],
}

        ]
    },
    devtool: 'inline-source-map',
    devServer: {
      contentBase: './static'
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery",
            'Waves': 'node-waves'
        }),

    ]

};
