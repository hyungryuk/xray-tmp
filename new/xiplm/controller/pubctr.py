# -*- coding: utf-8 -*-

from flask_restful import Resource
from flask import request
import json
import requests as rq
import datetime

from ..helper.messages import *
from ..configs.config import *
import shutil



class PublishingModelRESTApi(Resource):

    def post(self):

        request_data = request.get_json()
        method_type = request_data.get('method_type')
        items = request_data.get('items')

        if method_type is not None:
            if items is not None:
                if method_type == 'pub':
                    return pub_processing(request_data)
                else:
                    pass
            else:
                message = COMMON_ITEMS_NOT_FOUND
                return json.dumps(message, ensure_ascii=True)
        else:
            message = COMMON_METHOD_TYPE_NOT_FOUND
            message.update({'items': request.get_json().get('items')})
            return json.dumps(message, ensure_ascii=True)


def pub_processing(request_data):
    try:

        options = request_data.get('options')

        modelcode = options[0].get('modelcode')
        linecode = options[0].get('linecode')
        userid = options[0].get('userid')
        src = (MDL_DIR % (modelcode))
        dst = (MDL_PUB_DIR % (linecode))
        for item in os.listdir(src):
            s = os.path.join(src, item)
            d = os.path.join(dst, item)
            if os.path.isdir(s):
                shutil.copytree(s, d, False, None)
            else:
                shutil.copy2(s, d)

        now = datetime.datetime.now()

        result = rq.post(REST_SERVER_URL+'/rest/db/deploy',json={'items': [{"isrunning":False}],
                   'method_type': 'update',
                   'options': [{"linecode":linecode}]})
        print(result.json())
        result = rq.post(REST_SERVER_URL+'/rest/db/modelHandler',json={'items': [],
                           'method_type': 'view',
                           'options': [{"modelcode":modelcode}]})

        model = json.loads(result.json())
        model = model.get("items")[0]
        rq.post(REST_SERVER_URL+'/rest/db/deploy',json={'items': [{"isrunning":True,"linecode":linecode,"modelcode":modelcode,"deploydate":now.strftime('%Y-%m-%d %H:%M:%S'),"filtercode":model.get("filter"),"accuracy":model.get("accuracy"),"modelname":model.get("modelname"),"user":userid}],
                           'method_type': 'insert',
                           'options': []})

        rq.post(REST_SERVER_URL+'/rest/db/log',json={'items': [{"kind":3,"title":"deploy","contents":str(modelcode) +" model deployed to "+str(linecode)+" by "+str(userid)}],
                   'method_type': 'insert',
                   'options': []})



        return json.dumps({'items': [],
                           'return_code': MDL_TRAIN_SUCCESS.get('return_code'),
                           'message': MDL_TRAIN_SUCCESS.get('message')},ensure_ascii=True)

    except Exception as e:
        rq.post(REST_SERVER_URL+'/rest/db/log',json={'items': [{"kind":3,"title":"deploy","contents":str(modelcode) +"error occured when  model deployed  to "+str(linecode)+" by "+str(userid)}],
                   'method_type': 'insert',
                   'options': []})
        message = MDL_TRAIN_SUCCESS_FAIL
        message.update({'cause': str(e)})
        message.update({'items': request_data['items']})
        return json.dumps(message, ensure_ascii=True)
