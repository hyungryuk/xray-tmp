"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const ReactDOM = require("react-dom");
const header_1 = require("../modulesForPages/header");
const inversify_config_1 = require("../DIContainerConf/inversify.config");
const types_1 = require("../DIContainerConf/types");
const dbRestcall_1 = require("../others/restCall/dbRestcall");
const encoder_1 = require("../others/encoder/encoder");
const properties_1 = require("../properties/properties");
var selectedList = [];
ReactDOM.render(React.createElement(header_1.HeaderComponent, null), document.getElementById("header"));
class DeleteButtonInTable extends React.Component {
    constructor(props) {
        super(props);
        this.deleteOnclicked = this.deleteOnclicked.bind(this);
    }
    deleteOnclicked(e) {
        var algorithmDataAjaxCallObj = new dbRestcall_1.AlgorithmDataAjaxCall();
        algorithmDataAjaxCallObj.delete([], [{ algorithmcode: this.props.algorithmcode }]);
        location.reload();
    }
    render() {
        return (React.createElement("button", { className: "delete_button", onClick: this.deleteOnclicked }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_delete_table")));
    }
}
class AvailableBoxInPopup extends React.Component {
    constructor(props) {
        super(props);
        this.initDate = this.initDate.bind(this);
    }
    initDate() {
        $("#" + this.props.id).val(this.props.available);
    }
    componentDidMount() {
        this.initDate();
    }
    render() {
        return (React.createElement("select", { id: "available" },
            React.createElement("option", { value: "all" }, "All"),
            React.createElement("option", { value: "true" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("filter_available_true")),
            React.createElement("option", { value: "false" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("filter_available_false"))));
    }
}
class VersionBoxInPopup extends React.Component {
    constructor(props) {
        super(props);
        this.initDate = this.initDate.bind(this);
    }
    initDate() {
        $("#" + this.props.id).val(this.props.version);
    }
    componentDidMount() {
        this.initDate();
    }
    render() {
        return React.createElement("input", { type: "text", className: "version_input", id: this.props.id });
    }
}
class AvailableInTable extends React.Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
    }
    onChange() {
        var algorithmDataAjaxCallObj = new dbRestcall_1.AlgorithmDataAjaxCall();
        var available;
        if ($("#" + this.props.primKey + " option:selected").val() == "1") {
            available = true;
        }
        else {
            available = false;
        }
        algorithmDataAjaxCallObj.update([{ available: available }], [{ algorithmcode: this.props.primKey }]);
    }
    componentDidMount() {
        if (this.props.available == 1) {
            $("#" + this.props.primKey).val("1");
        }
        else {
            $("#" + this.props.primKey).val("0");
        }
    }
    render() {
        return (React.createElement("select", { id: this.props.primKey, onChange: this.onChange },
            React.createElement("option", { value: "1" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("filter_available_true")),
            React.createElement("option", { value: "0" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("filter_available_false"))));
    }
}
class CheckBoxInTable extends React.Component {
    constructor(props) {
        super(props);
        this.checkBoxOnChange = this.checkBoxOnChange.bind(this);
    }
    checkBoxOnChange(e) {
        if (e.target.checked == true) {
            selectedList.push(this.props.algorithmcode);
        }
        else {
            var index = selectedList.indexOf(this.props.algorithmcode);
            selectedList.splice(index, 1);
        }
    }
    render() {
        return (React.createElement("input", { type: "checkbox", onChange: this.checkBoxOnChange, className: "checkbox" }));
    }
}
class TableContents extends React.Component {
    constructor(props) {
        super(props);
        this.valueOnclick = this.valueOnclick.bind(this);
        this.valueOnKeyPress = this.valueOnKeyPress.bind(this);
        this.state = {
            isEditState: false,
            value: this.props.value
        };
    }
    valueOnclick() {
        if (this.state.isEditState == false) {
            this.setState(prevState => ({
                isEditState: !prevState.isEditState
            }));
        }
    }
    valueOnKeyPress(e) {
        if (e.key == 'Enter') {
            var algorithmDataAjaxCallObj = new dbRestcall_1.AlgorithmDataAjaxCall();
            var optionList = [];
            var option = {};
            var item = {};
            option["algorithmcode"] = this.props.primKey;
            item[this.props.id] = e.target.value;
            algorithmDataAjaxCallObj.update([item], [option]);
            this.setState({
                value: e.target.value,
                isEditState: false
            });
        }
        if (e.keyCode === 27) {
            this.setState({
                isEditState: false
            });
        }
    }
    render() {
        if (this.state.isEditState) {
            return (React.createElement("div", { onClick: this.valueOnclick },
                React.createElement("input", { type: "text", defaultValue: this.state.value, onKeyDown: this.valueOnKeyPress })));
        }
        else {
            return (React.createElement("div", { onClick: this.valueOnclick }, this.state.value));
        }
    }
}
class TableBody extends React.Component {
    render() {
        const data = this.props.data.map(item => (React.createElement("tr", null,
            React.createElement("td", null,
                React.createElement(CheckBoxInTable, { algorithmcode: item.algorithmcode })),
            React.createElement("td", null,
                React.createElement(TableContents, { primKey: item.algorithmcode, id: "algorithmname", value: item.algorithmname })),
            React.createElement("td", { className: "number" },
                React.createElement(TableContents, { primKey: item.algorithmcode, id: "version", value: item.version })),
            React.createElement("td", null,
                React.createElement(TableContents, { primKey: item.algorithmcode, id: "fullpath", value: item.fullpath })),
            React.createElement("td", null,
                React.createElement(AvailableInTable, { available: item.available, primKey: item.algorithmcode })))));
        return data;
    }
}
class TableHeaderAndBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isSelectallClicked: false
        };
        this.selectAllOnClick = this.selectAllOnClick.bind(this);
    }
    selectAllOnClick() {
        var checkboxs = document.getElementsByClassName("checkbox");
        if (this.state.isSelectallClicked == false) {
            for (var i = 0; i < this.props.data.length; i++) {
                selectedList.push(this.props.data[i].algorithmcode);
            }
            for (var i = 0; i < checkboxs.length; i++) {
                checkboxs[i].checked = true;
            }
        }
        else {
            selectedList = [];
            for (var i = 0; i < checkboxs.length; i++) {
                checkboxs[i].checked = false;
            }
        }
        this.setState(prevState => ({
            isSelectallClicked: !prevState.isSelectallClicked
        }));
    }
    render() {
        return (React.createElement("div", { className: "table_box" },
            React.createElement("table", { className: "table" },
                React.createElement("thead", { className: "thead" },
                    React.createElement("tr", null,
                        React.createElement("th", { onClick: this.selectAllOnClick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("table_select_name")),
                        React.createElement("th", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("algorithmtable_algorithmname_name")),
                        React.createElement("th", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("algorithmtable_version_name")),
                        React.createElement("th", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("algorithmtable_fullpath_name")),
                        React.createElement("th", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("algorithmtable_available_name")))),
                React.createElement("tbody", { className: "tbody" },
                    React.createElement(TableBody, { data: this.props.data })))));
    }
}
class Addpopup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            close: true
        };
        this.closeOnClick = this.closeOnClick.bind(this);
    }
    addOnClick() {
        var inputs = $(".addpopupBox :input");
        var item = {};
        var file;
        for (var i = 0; i < inputs.length; i++) {
            if (inputs[i].getAttribute("name") == "fullpath") {
                file = inputs[i].files[0];
            }
            else {
                item[inputs[i].getAttribute("name")] = inputs[i].value;
            }
        }
        item["algorithmcode"] = Date.now().toString();
        item["fullpath"] = properties_1.properties["AlgorithmDownloadPath"] + item["algorithmcode"] + ".py";
        if (item["available"] == "1") {
            item["available"] = true;
        }
        else {
            item["available"] = false;
        }
        var filereader = new FileReader();
        filereader.readAsText(file);
        filereader.onloadend = function (event) {
            item["data"] = filereader.result;
            var algorithmDataAjaxCallObj = new dbRestcall_1.AlgorithmDataAjaxCall();
            var algorithmDataAjaxResult = algorithmDataAjaxCallObj.create([item], []);
            if (algorithmDataAjaxResult["return_code"] == "0000") {
                alert("success");
            }
            else {
                alert(algorithmDataAjaxResult["cause"]);
            }
        };
        this.setState(prevState => ({
            close: !prevState.close
        }));
        location.reload();
    }
    closeOnClick() {
        this.setState(prevState => ({
            close: !prevState.close
        }));
    }
    render() {
        if (this.props.open != this.state.close) {
            return (React.createElement("div", null));
        }
        else {
            return (React.createElement("div", { className: "addpopupBox" },
                React.createElement("div", { className: "addpopup_inside_Box" },
                    React.createElement("label", null,
                        inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("algorithmtable_algorithmname_name"),
                        React.createElement("input", { type: "text", name: "algorithmname", placeholder: "name" })),
                    React.createElement("label", null,
                        inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("algorithmtable_version_name"),
                        React.createElement("input", { type: "text", name: "version", placeholder: "version" })),
                    React.createElement("label", null,
                        inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("algorithmtable_fullpath_name"),
                        React.createElement("input", { type: "file", name: "fullpath" })),
                    React.createElement("label", null,
                        inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("algorithmtable_available_name"),
                        React.createElement("select", { name: "available" },
                            React.createElement("option", { value: "1" }, "true"),
                            React.createElement("option", { value: "0" }, "false"))),
                    React.createElement("div", { className: "add_popup_button_group" },
                        React.createElement("button", { onClick: this.addOnClick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("table_selected_item_add_button")),
                        React.createElement("button", { onClick: this.closeOnClick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("popup_close"))))));
        }
    }
}
class SearchBoxAndTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            startDate: "",
            endDate: "",
            algorithmcode: "",
            algorithmname: "",
            available: "all",
            version: "",
            fullpath: "",
            currentPage: 1,
            maxPage: -1,
            perpage: 10,
            isfilterOpen: false,
            resultData: [],
            isAddClicked: false
        };
        this.getDataUsingRestCallFunc = this.getDataUsingRestCallFunc.bind(this);
        this.startDateOnChange = this.startDateOnChange.bind(this);
        this.endDateOnChange = this.endDateOnChange.bind(this);
        this.algorithmcodeOnChange = this.algorithmcodeOnChange.bind(this);
        this.searchOnclick = this.searchOnclick.bind(this);
        this.filterOnclick = this.filterOnclick.bind(this);
        this.filterCloseOnclick = this.filterCloseOnclick.bind(this);
        this.setFilterOnclick = this.setFilterOnclick.bind(this);
        this.addOnClick = this.addOnClick.bind(this);
        this.firstPageOnclick = this.firstPageOnclick.bind(this);
        this.lastPageOnclick = this.lastPageOnclick.bind(this);
        this.prePageOnclick = this.prePageOnclick.bind(this);
        this.nextPageOnclick = this.nextPageOnclick.bind(this);
        this.currentPageOnKeyPress = this.currentPageOnKeyPress.bind(this);
    }
    setFilterOnclick() {
        this.setState({ version: $("#version").val() });
        this.setState({ available: $("#available option:selected").val() });
        this.setState(prevState => ({
            isfilterOpen: !prevState.isfilterOpen
        }));
    }
    filterCloseOnclick() {
        this.setState(prevState => ({
            isfilterOpen: !prevState.isfilterOpen
        }));
    }
    filterOnclick() {
        this.setState(prevState => ({
            isfilterOpen: !prevState.isfilterOpen
        }));
    }
    getDataUsingRestCallFunc(page) {
        var algorithmDataResult;
        var option1 = {};
        var option2 = {};
        if (this.state.available != "all") {
            if (this.state.available == "true")
                option1["available"] = 1;
            else {
                option1["available"] = 0;
            }
        }
        if (this.state.fullpath != "") {
            option1["fullpath"] = this.state.fullpath;
        }
        if (this.state.version != "") {
            option1["version"] = this.state.version;
        }
        if (this.state.algorithmcode != "") {
            option1["algorithmcode"] = this.state.algorithmcode;
        }
        if (this.state.algorithmname != "") {
            option1["algorithmname"] = this.state.algorithmname;
        }
        if (this.state.startDate != "" && this.state.endDate != "") {
            option2["startdate"] = encoder_1.encoder.dateToYYMMDDHHMMSS(this.state.startDate + "000000");
            option2["enddate"] = encoder_1.encoder.dateToYYMMDDHHMMSS(this.state.endDate + "235959");
        }
        var algorithmDataAjaxCallObj = new dbRestcall_1.AlgorithmDataAjaxCall();
        algorithmDataResult = algorithmDataAjaxCallObj.retrieve([], [option1, option2, {}, { page: page, perpage: this.state.perpage }]);
        var logDataAjaxCallObj = new dbRestcall_1.LogDataAjaxCall();
        logDataAjaxCallObj.create([properties_1.properties["Algorithm_view_log"]], []);
        return algorithmDataResult;
    }
    firstPageOnclick() {
        var filterDateResult;
        if (this.state.currentPage != 1) {
            filterDateResult = this.getDataUsingRestCallFunc(1);
            this.setState({
                resultData: filterDateResult["items"],
                currentPage: 1
            });
            $("#current_page").val(1);
        }
    }
    lastPageOnclick() {
        var filterDateResult;
        if (this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage) {
            filterDateResult = this.getDataUsingRestCallFunc(this.state.maxPage);
            this.setState({
                resultData: filterDateResult["items"],
                currentPage: this.state.maxPage
            });
            $("#current_page").val(this.state.maxPage);
        }
    }
    prePageOnclick() {
        var filterDateResult;
        if (this.state.maxPage != -1 && this.state.currentPage > 1) {
            filterDateResult = this.getDataUsingRestCallFunc(this.state.currentPage - 1);
            this.setState({ resultData: filterDateResult["items"] });
            $("#current_page").val(this.state.currentPage - 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage - 1
            }));
        }
    }
    nextPageOnclick() {
        var filterDateResult;
        if (this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage) {
            filterDateResult = this.getDataUsingRestCallFunc(this.state.currentPage + 1);
            this.setState({ resultData: filterDateResult["items"] });
            $("#current_page").val(this.state.currentPage + 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage + 1
            }));
        }
    }
    currentPageOnKeyPress(e) {
        if (e.which == 13) {
            var pageVal = $("#current_page").val();
            var page = parseInt(pageVal);
            if (isNaN(page)) {
                alert(inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_is_not_number_alert"));
                $("#current_page").val(this.state.currentPage);
            }
            else {
                if (page > this.state.maxPage) {
                    alert(inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_greater_than_max_alert"));
                    $("#current_page").val(this.state.currentPage);
                }
                else {
                    if (this.state.maxPage != -1 &&
                        this.state.currentPage != page) {
                        var algorithmDataResult = this.getDataUsingRestCallFunc(page);
                        this.setState({
                            resultData: algorithmDataResult["items"],
                            currentPage: page
                        });
                        $("#current_page").val(page);
                    }
                }
            }
        }
    }
    startDateOnChange(e) {
        this.setState({
            startDate: e.target.value
        });
    }
    endDateOnChange(e) {
        this.setState({
            endDate: e.target.value
        });
    }
    algorithmcodeOnChange(e) {
        this.setState({
            algorithmcode: e.target.value
        });
    }
    algorithmnameOnChange(e) {
        this.setState({
            algorithmname: e.target.value
        });
    }
    deleteOnClick(e) {
        var algorithmDataAjaxCallObj = new dbRestcall_1.AlgorithmDataAjaxCall();
        var optionList = [];
        for (var i = 0; i < selectedList.length; i++) {
            optionList.push({ algorithmcode: selectedList[i] });
        }
        algorithmDataAjaxCallObj.delete([], optionList);
        location.reload();
    }
    addOnClick() {
        this.setState(prevState => ({
            isAddClicked: !prevState.isAddClicked
        }));
    }
    searchOnclick() {
        var algorithmDateResult = this.getDataUsingRestCallFunc(1);
        this.setState({ resultData: algorithmDateResult["items"] });
        var maxpage = Math.ceil(algorithmDateResult["total"] / this.state.perpage);
        this.setState({ maxPage: maxpage });
        if (algorithmDateResult["items"].length == 0) {
            $("#current_page").val(0);
            $("#max_page").val(0);
            this.setState({ maxPage: 0, currentPage: 0 });
        }
        else {
            $("#current_page").val(1);
            $("#max_page").val(maxpage);
            this.setState({ maxPage: maxpage, currentPage: 1 });
        }
    }
    componentDidMount() {
        this.searchOnclick();
    }
    render() {
        const popup = (React.createElement("div", { className: "optional_settings_box" },
            React.createElement("div", { className: "popup_close_button" },
                React.createElement("i", { className: "fas fa-window-close", onClick: this.filterCloseOnclick })),
            React.createElement("div", { className: "version_input_box" },
                React.createElement("label", null,
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("algorithmtable_version_name"),
                    React.createElement(VersionBoxInPopup, { id: "version", version: this.state.version }))),
            React.createElement("div", { className: "available_input_box" },
                React.createElement("label", null,
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("algorithmtable_available_name"),
                    React.createElement(AvailableBoxInPopup, { available: this.state.available, id: "available" }))),
            React.createElement("div", { className: "filter_set_button" },
                React.createElement("button", { className: "set_button", onClick: this.setFilterOnclick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("filter_setting_button")))));
        if (this.state.isfilterOpen) {
            return (React.createElement("div", { className: "main_box" },
                React.createElement("div", { className: "current_page_info" },
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_setting_root_name"),
                    React.createElement("i", { className: "fas fa-angle-right" }),
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_algorithmsetting_name")),
                React.createElement("div", null, popup),
                React.createElement("div", { className: "searchBox" },
                    React.createElement("div", { className: "algorithmname_input_box" },
                        React.createElement("label", null,
                            inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("algorithmtable_algorithmname_name"),
                            React.createElement("input", { type: "text", className: "algorithmname_input", id: "algorithmname", onChange: this.algorithmnameOnChange, defaultValue: this.state.algorithmname }))),
                    React.createElement("div", { className: "button_box" },
                        React.createElement("div", { className: "set_specific_filter_button_box" },
                            React.createElement("button", { className: "set_specific_filter_button", onClick: this.filterOnclick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_set_specific_filter_button_box_name"))),
                        React.createElement("div", { className: "search_button_box" },
                            React.createElement("button", { className: "searchButton", onClick: this.searchOnclick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_search_button_name"))),
                        React.createElement("div", null,
                            React.createElement("button", { className: "items_delete_button", onClick: this.deleteOnClick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("table_selected_item_delete_button"))),
                        React.createElement("div", null,
                            React.createElement("button", { className: "items_add_button", onClick: this.addOnClick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("table_selected_item_add_button")),
                            React.createElement(Addpopup, { open: this.state.isAddClicked })))),
                React.createElement("div", null,
                    React.createElement(TableHeaderAndBox, { data: this.state.resultData })),
                React.createElement("div", { className: "page_box" },
                    React.createElement("div", { className: "pre_page_box" },
                        React.createElement("i", { className: "fas fa-angle-double-left", onClick: this.firstPageOnclick }),
                        React.createElement("i", { className: "fas fa-angle-left", onClick: this.prePageOnclick })),
                    React.createElement("div", { className: "page_input_box" },
                        React.createElement("input", { type: "text", className: "current_page_input", id: "current_page", onKeyPress: this.currentPageOnKeyPress }),
                        "/",
                        React.createElement("input", { type: "text", className: "max_page_input", readOnly: "true", id: "max_page" })),
                    React.createElement("div", { className: "post_page_box" },
                        React.createElement("i", { className: "fas fa-angle-right", onClick: this.nextPageOnclick }),
                        React.createElement("i", { className: "fas fa-angle-double-right", onClick: this.lastPageOnclick })))));
        }
        else {
            return (React.createElement("div", { className: "main_box" },
                React.createElement("div", { className: "current_page_info" },
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_setting_root_name"),
                    React.createElement("i", { className: "fas fa-angle-right" }),
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_algorithmsetting_name")),
                React.createElement("div", { className: "searchBox" },
                    React.createElement("div", { className: "algorithmname_input_box" },
                        React.createElement("label", null,
                            inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("algorithmtable_algorithmname_name"),
                            React.createElement("input", { type: "text", className: "algorithmname_input", id: "algorithmname", onChange: this.algorithmnameOnChange, defaultValue: this.state.algorithmname }))),
                    React.createElement("div", { className: "button_box" },
                        React.createElement("div", { className: "set_specific_filter_button_box" },
                            React.createElement("button", { className: "set_specific_filter_button", onClick: this.filterOnclick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_set_specific_filter_button_box_name"))),
                        React.createElement("div", { className: "search_button_box" },
                            React.createElement("button", { className: "searchButton", onClick: this.searchOnclick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_search_button_name"))),
                        React.createElement("div", null,
                            React.createElement("button", { className: "items_delete_button", onClick: this.deleteOnClick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("table_selected_item_delete_button"))),
                        React.createElement("div", null,
                            React.createElement("button", { className: "items_add_button", onClick: this.addOnClick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("table_selected_item_add_button")),
                            React.createElement(Addpopup, { open: this.state.isAddClicked })))),
                React.createElement("div", null,
                    React.createElement(TableHeaderAndBox, { data: this.state.resultData })),
                React.createElement("div", { className: "page_box" },
                    React.createElement("div", { className: "pre_page_box" },
                        React.createElement("i", { className: "fas fa-angle-double-left", onClick: this.firstPageOnclick }),
                        React.createElement("i", { className: "fas fa-angle-left", onClick: this.prePageOnclick })),
                    React.createElement("div", { className: "page_input_box" },
                        React.createElement("input", { type: "text", className: "current_page_input", id: "current_page", onKeyPress: this.currentPageOnKeyPress }),
                        React.createElement("div", { className: "slash" }),
                        React.createElement("input", { type: "text", className: "max_page_input", readOnly: "true", id: "max_page" })),
                    React.createElement("div", { className: "post_page_box" },
                        React.createElement("i", { className: "fas fa-angle-right", onClick: this.nextPageOnclick }),
                        React.createElement("i", { className: "fas fa-angle-double-right", onClick: this.lastPageOnclick })))));
        }
    }
}
ReactDOM.render(React.createElement("div", null,
    React.createElement(SearchBoxAndTable, null)), document.getElementById("main"));
