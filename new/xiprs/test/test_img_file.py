import json
from datetime import datetime

from .conftest import post_json, get_json
from ..helper import messages


from .. import *
from ..configs import config


def test_create_image_file_none_method_type(client):
    image_dict = {'items': [{'filename': 'mnist99992', 'filedate': '2018-03-25',
                             'linecode': 1, 'filepath': 'C:\\xray\\image\\mnist99992.bmp',
                             'lotno': '2', 'ng': 1,
                             'isremoved': 0, 'ng_by_user': 1,
                             'ng_by_mes': 0}]}

    rv = post_json(client, IMG_CTR_URL, image_dict)
    resp = json.loads(rv.data)
    resp_dic = json.loads(resp)
    assert rv.status_code == 200
    assert resp_dic['return_code'] == messages.COMMON_METHOD_TYPE_NOT_FOUND['return_code']


def test_create_image_file_none_items(client):
    image_dict = {'method_type': 'insert'}

    rv = post_json(client, IMG_CTR_URL, image_dict)
    resp = json.loads(rv.data)
    resp_dic = json.loads(resp)
    assert rv.status_code == 200
    assert resp_dic['return_code'] == messages.COMMON_ITEMS_NOT_FOUND['return_code']


def test_create_image_file_success_with_one_item(client):
    image_dict = {'method_type': 'insert',
                  'items': [{'filename': 'mnist99992', 'filedate': '{:%Y%m%d%H%M%S}'.format(datetime.now()),
                             'linecode': 1, 'filepath': 'C:\\xray\\image\\mnist99992.bmp',
                             'lotno': '2', 'ng': 1,
                             'isremoved': 0, 'ng_by_user': 1,
                             'ng_by_mes': 0
                             }]
                  }

    rv = post_json(client, IMG_CTR_URL, image_dict)
    resp = json.loads(rv.data)
    resp_dic = json.loads(resp)
    print(resp_dic)
    assert rv.status_code == 200
    assert resp_dic['return_code'] == messages.IMG_CREATE_SUCCESS['return_code']
    assert resp_dic['items'][0]['filename'] == 'mnist99992'


def test_create_image_file_fail_with_one_item(client):
    image_dict = {'method_type': 'insert',
                  'items': [{'filename': 'mnist99992', 'filedate': None,
                             'linecode': 1, 'filepath': 'C:\\xray\\image\\mnist99992.bmp',
                             'lotno': '2', 'ng': 1,
                             'isremoved': 0, 'ng_by_user': 1,
                             'ng_by_mes': 0
                             }]
                  }

    rv = post_json(client, IMG_CTR_URL, image_dict)
    resp = json.loads(rv.data)
    resp_dic = json.loads(resp)
    print(resp_dic)
    assert rv.status_code == 200
    assert resp_dic['return_code'] == messages.IMG_CREATE_SUCCESS_FAIL['return_code']
    assert resp_dic['items'][0]['filename'] == 'mnist99992'


def test_create_image_file_success_with_two_item(client):
    image_dict = {'method_type': 'insert',
                  'items': [{'filename': 'mnist99991', 'filedate': '{:%Y%m%d%H%M%S}'.format(datetime.now()),
                             'linecode': 1, 'filepath': 'C:\\xray\\image\\mnist99991.bmp',
                             'lotno': '2', 'ng': 1,
                             'isremoved': 0, 'ng_by_user': 1,
                             'ng_by_mes': 0
                             },
                            {'filename': 'mnist99990', 'filedate': '{:%Y%m%d%H%M%S}'.format(datetime.now()),
                             'linecode': 1, 'filepath': 'C:\\xray\\image\\mnist99990.bmp',
                             'lotno': '2', 'ng': 1,
                             'isremoved': 0, 'ng_by_user': 1,
                             'ng_by_mes': 0
                             }]
                  }

    rv = post_json(client, IMG_CTR_URL, image_dict)
    resp = json.loads(rv.data)
    resp_dic = json.loads(resp)
    print(resp_dic)
    assert rv.status_code == 200
    assert resp_dic['return_code'] == messages.IMG_CREATE_SUCCESS['return_code']
    assert resp_dic['items'][1]['filename'] == 'mnist99990'


def test_view_image_file_success_with_options(client):
    image_dict = {'method_type': 'view',
                  'items': [],
                  'options': [{'filename': 'mnist99991'}]
                  }

    rv = post_json(client, IMG_CTR_URL, image_dict)
    resp = json.loads(rv.data)
    resp_dic = json.loads(resp)
    assert rv.status_code == 200
    assert resp_dic.get('return_code') == messages.IMG_VIEW_SUCCESS.get('return_code')
    assert len(resp_dic.get('items')) == 11


def test_view_image_file_success_with_options_with_date(client):
    image_dict = {'method_type': 'view',
                  'items': [],
                  'options': [{'filename': 'mnist99991'},{'startdate':'20180326130000', 'enddate':'20180326141000'}]
                  }

    rv = post_json(client, IMG_CTR_URL, image_dict)
    resp = json.loads(rv.data)
    resp_dic = json.loads(resp)
    assert rv.status_code == 200
    assert resp_dic.get('return_code') == messages.IMG_VIEW_SUCCESS.get('return_code')
    assert len(resp_dic.get('items')) == 4


def test_view_image_file_success_with_options_with_date_and_orderby(client):
    image_dict = {'method_type': 'view',
                  'items': [],
                  'options': [{'filename': 'mnist99991'},{'startdate':'20180326130000', 'enddate':'20180326141000'},
                              {'orderby': 'filedate'}]
                  }

    rv = post_json(client, IMG_CTR_URL, image_dict)
    resp = json.loads(rv.data)
    resp_dic = json.loads(resp)
    assert rv.status_code == 200
    assert resp_dic.get('return_code') == messages.IMG_VIEW_SUCCESS.get('return_code')
    assert len(resp_dic.get('items')) == 4
    assert resp_dic.get('items')[0].get('lotno') == '1'


def test_view_image_file_success_with_options_without_date_and_orderby(client):
    image_dict = {'method_type': 'view',
                  'items': [],
                  'options': [{'filename': 'mnist99991'},{},
                              {'orderby': 'filedate'}]
                  }

    rv = post_json(client, IMG_CTR_URL, image_dict)
    resp = json.loads(rv.data)
    resp_dic = json.loads(resp)
    assert rv.status_code == 200
    assert resp_dic.get('return_code') == messages.IMG_VIEW_SUCCESS.get('return_code')
    assert resp_dic.get('items')[0].get('lotno') == '3'


def test_view_image_file_success_with_options_with_date_and_orderby_and_pagination(client):
    image_dict = {'method_type': 'view',
                  'items': [],
                  'options': [{'filename': 'mnist99991'},{'startdate':'20180326130000', 'enddate':'20180326141000'},
                              {'orderby': 'filedate'}, {'page':'2', 'perpage': '2'}]
                  }

    rv = post_json(client, IMG_CTR_URL, image_dict)
    resp = json.loads(rv.data)
    resp_dic = json.loads(resp)
    assert rv.status_code == 200
    assert resp_dic.get('return_code') == messages.IMG_VIEW_SUCCESS.get('return_code')
    assert resp_dic.get('items')[0].get('filedate') == '2018-03-26 13:43:02'


def test_view_image_file_success_with_file_name_and_start_end_date(client):
    image_dict = {'method_type': 'view',
                  'items': [{'filename': 'mnist99991'}],
                  'startdate': '20180301000000',
                  'enddate': '{:%Y%m%d%H%M%S}'.format(datetime.now())
                  }

    rv = post_json(client, IMG_CTR_URL, image_dict)
    resp = json.loads(rv.data)
    resp_dic = json.loads(resp)
    assert rv.status_code == 200
    assert resp_dic.get('return_code') == messages.IMG_VIEW_SUCCESS.get('return_code')


def test_view_all(client):
    image_dict = {'method_type': 'view',
                  'items': [],
                  'options': []
                  }

    rv = post_json(client, IMG_CTR_URL, image_dict)
    resp = json.loads(rv.data)
    resp_dic = json.loads(resp)
    assert rv.status_code == 200
    assert resp_dic.get('return_code') == messages.IMG_VIEW_SUCCESS.get('return_code')
    assert len(resp_dic.get('items')) == 36

def test_count_image_file_success_with_options(client):
    image_dict = {'method_type': 'count',
                  'items': [],
                  'options': [{'ng':1 , 'lotno': '2'}]
                  }

    rv = post_json(client, IMG_CTR_URL, image_dict)
    resp = json.loads(rv.data)
    resp_dic = json.loads(resp)
    assert rv.status_code == 200
    assert resp_dic.get('return_code') == messages.IMG_COUNT_SUCCESS.get('return_code')
    assert len(resp_dic.get('items')) == 1
    assert resp_dic.get('items')[0].get('count') == 32



def test_view_image_file_success_with_pagination_option_and_file_name_and_start_end_date(client):
    image_dict = {'method_type': 'view',
                  'items': [{'filename': 'mnist99991'}],
                  'startdate': '20180301000000',
                  'enddate': '{:%Y%m%d%H%M%S}'.format(datetime.now()),
                  'option': 'pagination', 'page': 1, 'perpage': 10
                  }

    rv = post_json(client, IMG_CTR_URL, image_dict)
    resp = json.loads(rv.data)
    resp_dic = json.loads(resp)
    assert rv.status_code == 200
    assert resp_dic.get('total') == 10
    assert resp_dic.get('return_code') == messages.IMG_VIEW_SUCCESS.get('return_code')


def test_view_image_file_success_with_pagination_option_and_file_name(client):
    image_dict = {'method_type': 'view',
                  'items': [{'filename': 'mnist99991'}],
                  'option': 'pagination', 'page': 1, 'perpage': 10
                  }

    rv = post_json(client, IMG_CTR_URL, image_dict)
    resp = json.loads(rv.data)
    resp_dic = json.loads(resp)
    assert rv.status_code == 200
    assert resp_dic.get('total') == 10
    assert resp_dic.get('return_code') == messages.IMG_VIEW_SUCCESS.get('return_code')


def test_view_image_file_success_with_pagination_option_and_orderby_and_file_name(client):
    image_dict = {'method_type': 'view',
                  'items': [{'filename': 'mnist99991'}],
                  'option': 'pagination', 'page': 1, 'perpage': 10,
                  'orderby': 'filedate'
                  }

    rv = post_json(client, IMG_CTR_URL, image_dict)
    resp = json.loads(rv.data)
    resp_dic = json.loads(resp)
    assert rv.status_code == 200
    assert resp_dic.get('total') == 10
    assert resp_dic.get('return_code') == messages.IMG_VIEW_SUCCESS.get('return_code')


def test_view_image_file_success_with_pagination_option_and_orderby_and_file_name_and_start_end_date(client):
    image_dict = {'method_type': 'view',
                  'items': [{'filename': 'mnist99991'}],
                  'startdate': '20180301000000',
                  'enddate': '{:%Y%m%d%H%M%S}'.format(datetime.now()),
                  'option': 'pagination', 'page': 1, 'perpage': 10,
                  'orderby': 'filedate'
                  }

    rv = post_json(client, IMG_CTR_URL, image_dict)
    resp = json.loads(rv.data)
    resp_dic = json.loads(resp)
    assert rv.status_code == 200
    assert resp_dic.get('total') == 10
    assert resp_dic.get('return_code') == messages.IMG_VIEW_SUCCESS.get('return_code')


def test_file_tree_root(client):
    rv = get_json(client, IMG_CTR_URL+'?nodeId=0')
    resp = json.loads(rv.data)
    assert rv.status_code == 200
#    assert resp_dic.get('return_code') == messages.IMG_TREE_SUCCESS.get('return_code')

def test_file_tree_open(client):
    rv = get_json(client, IMG_CTR_URL+'?imagePath={0}'.format('/Users/ymith/PycharmProjects/Xray_image_processing/new/xiprs/Users/ymith/PycharmProjects/Xray_image_processing/new/xiprs/routes.py'))
    print(rv.data)
    assert rv.status_code == 200

def test_update_image_file_success_with_one_item(client):
    test_dict = {'method_type': 'update',
                  'items': [{'isremoved': 2 }],
                  'options': [{'filename': 'mnist99990', 'filedate': '20180326132528'}]
                  }

    rv = post_json(client, IMG_CTR_URL, test_dict)
    resp = json.loads(rv.data)
    resp_dic = json.loads(resp)
    print(resp_dic)
    assert rv.status_code == 200
    assert resp_dic['return_code'] == messages.IMG_UPDATE_SUCCESS['return_code']


def test_update_image_file_success_with_one_item_and_delete_file(client):
    test_dict = {'method_type': 'delete',
                  'items': [{'isremoved': 5 }],
                  'options': [{'filename': 'mnist99990', 'filedate': '20180326132528'}],
                  'filePath': config.FILE_TREE_ROOT_PATH + '/temp1'
                  }

    rv = post_json(client, IMG_CTR_URL, test_dict)
    resp = json.loads(rv.data)
    resp_dic = json.loads(resp)
    print(resp_dic)
    assert rv.status_code == 200
    assert resp_dic['return_code'] == messages.IMG_UPDATE_SUCCESS['return_code']

def test_update_image_file_fail_with_one_item(client):
    image_dict = {'method_type': 'update',
                  'items': [{'filename': 'mnist99990', 'filedate': '20180326132528',
                             'linecode': None, 'filepath': 'C:\\xray\\image\\mnist00000.bmp',
                             'lotno': '2', 'ng': 2,
                             'isremoved': 0, 'ng_by_user': 1,
                             'ng_by_mes': 0
                             }]
                  }

    rv = post_json(client, IMG_CTR_URL, image_dict)
    resp = json.loads(rv.data)
    resp_dic = json.loads(resp)
    print(resp_dic)
    assert rv.status_code == 200
    assert resp_dic['return_code'] == messages.IMG_UPDATE_SUCCESS_FAIL['return_code']