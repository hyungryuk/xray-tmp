from .configs.config import configure_app
from flask_restful import Api
from flask_cors import CORS

__version__ = '0.1.0'

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

configure_app(app)

db = SQLAlchemy(app)

api = Api(app)

CORS(app)

db.create_all()

from .controller.tmctr import TrainingModelRESTApi
from .controller.pubctr import PublishingModelRESTApi
from .controller.glgctr import ModelLogRESTApi

TRN_CTR_URL = '/ml/trainning'
api.add_resource(TrainingModelRESTApi, TRN_CTR_URL)
PUB_CTR_URL = '/ml/publishing'
api.add_resource(PublishingModelRESTApi, PUB_CTR_URL)
LOG_CTR_URL = '/ml/trainingLog'
api.add_resource(ModelLogRESTApi, LOG_CTR_URL)
