"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const ReactDOM = require("react-dom");
const header_1 = require("../modulesForPages/header");
const inversify_config_1 = require("../DIContainerConf/inversify.config");
const types_1 = require("../DIContainerConf/types");
const dbRestcall_1 = require("../others/restCall/dbRestcall");
const encoder_1 = require("../others/encoder/encoder");
const properties_1 = require("../properties/properties");
var selectedList = [];
ReactDOM.render(React.createElement(header_1.HeaderComponent, null), document.getElementById("header"));
Date.prototype.yyyymmddhhmmss = function () {
    var mm = this.getMonth() + 1;
    var dd = this.getDate();
    var hh = this.getHours();
    var minutes = this.getMinutes();
    var ss = this.getSeconds();
    return [
        this.getFullYear(),
        (mm > 9 ? "" : "0") + mm,
        (dd > 9 ? "" : "0") + dd,
        (hh > 9 ? "" : "0") + hh,
        (minutes > 9 ? "" : "0") + minutes,
        (ss > 9 ? "" : "0") + ss
    ].join("");
};
var csvModule = {
    onclickCsvButton: function (kind, title, startdate, enddate) {
        var csvData = csvModule.makeCsvData(title, startdate, enddate);
        csvModule.downloadCsvFile(csvData, startdate, enddate);
    },
    downloadCsvFile: function (data, startdate, enddate) {
        var encodedStartdate = encoder_1.encoder.dateToYYMMDDHHMMSS(startdate);
        var encodedEnddate = encoder_1.encoder.dateToYYMMDDHHMMSS(enddate);
        let csvContent = "data:text/csv;charset=euc-kr,";
        data.forEach(function (rowArray) {
            let row = rowArray.join(",");
            csvContent += row + "\r\n";
        });
        var encodedUri = encodeURI(csvContent);
        var link = document.createElement("a");
        link.setAttribute("href", encodedUri);
        link.setAttribute("download", encodedStartdate + "_" + encodedEnddate + ".csv");
        document.body.appendChild(link);
        link.click();
    },
    makeCsvData: function (title, startdate_orin, enddate_orin) {
        var startdate = new Date(startdate_orin + " 00:00:00");
        var enddate = new Date(enddate_orin + " 23:59:59");
        var option1 = {};
        if (title != "") {
            option1["title"] = title;
        }
        var option2 = {};
        option2["startdate"] = startdate.yyyymmddhhmmss();
        option2["enddate"] = enddate.yyyymmddhhmmss();
        let logDataAjaxCallObj = new dbRestcall_1.LogDataAjaxCall();
        var logDataAjaxResult;
        if (startdate_orin == "" || enddate_orin == "") {
            logDataAjaxResult = logDataAjaxCallObj.retrieve([], [option1]);
        }
        else {
            logDataAjaxResult = logDataAjaxCallObj.retrieve([], [option1, option2]);
        }
        var csvData = [];
        var firstRow = [];
        firstRow.push(inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("logtable_kind_name"));
        firstRow.push(inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("logtable_title_name"));
        firstRow.push(inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("logtable_contents_name"));
        firstRow.push(inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_regdate_name"));
        csvData.push(firstRow);
        var logsDataList = logDataAjaxResult["items"];
        for (var i = 0; i < logsDataList.length; i++) {
            var row = [];
            if (logsDataList[i].kind == 0) {
                row.push("error");
            }
            else if (logsDataList[i].kind == 1) {
                row.push("warn");
            }
            else if (logsDataList[i].kind == 2) {
                row.push("info");
            }
            else {
                row.push("");
            }
            row.push(logsDataList[i].title);
            row.push(logsDataList[i].contents);
            row.push(logsDataList[i].logdate);
            csvData.push(row);
        }
        return csvData;
    }
};
class CheckBoxInTable extends React.Component {
    constructor(props) {
        super(props);
        this.checkBoxOnChange = this.checkBoxOnChange.bind(this);
    }
    checkBoxOnChange(e) {
        if (e.target.checked == true) {
            selectedList.push(this.props.logdate);
        }
        else {
            var index = selectedList.indexOf(this.props.logdate);
            selectedList.splice(index, 1);
        }
    }
    render() {
        return (React.createElement("input", { type: "checkbox", onChange: this.checkBoxOnChange, className: "checkbox" }));
    }
}
class KindInTable extends React.Component {
    render() {
        if (this.props.kind == "1") {
            return (React.createElement("div", null, "error"));
        }
        if (this.props.kind == "2") {
            return (React.createElement("div", null, "warn"));
        }
        if (this.props.kind == "3") {
            return (React.createElement("div", null, "info"));
        }
        return (React.createElement("div", null, "unknown"));
    }
}
class TableBody extends React.Component {
    render() {
        const data = this.props.data.map(item => (React.createElement("tr", null,
            React.createElement("td", null,
                React.createElement(CheckBoxInTable, { logdate: item.logdate })),
            React.createElement("td", null,
                React.createElement(KindInTable, { kind: item.kind })),
            React.createElement("td", null, item.title),
            React.createElement("td", null, item.contents),
            React.createElement("td", null, item.logdate))));
        return data;
    }
}
class TableHeaderAndBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isSelectallClicked: false
        };
        this.selectAllOnClick = this.selectAllOnClick.bind(this);
    }
    selectAllOnClick() {
        var checkboxs = document.getElementsByClassName("checkbox");
        if (this.state.isSelectallClicked == false) {
            for (var i = 0; i < this.props.data.length; i++) {
                selectedList.push(this.props.data[i].logdate);
            }
            for (var i = 0; i < checkboxs.length; i++) {
                checkboxs[i].checked = true;
            }
        }
        else {
            selectedList = [];
            for (var i = 0; i < checkboxs.length; i++) {
                checkboxs[i].checked = false;
            }
        }
        this.setState(prevState => ({
            isSelectallClicked: !prevState.isSelectallClicked
        }));
    }
    render() {
        return (React.createElement("div", { className: "table_box" },
            React.createElement("table", { className: "table" },
                React.createElement("thead", { className: "thead" },
                    React.createElement("tr", null,
                        React.createElement("th", { onClick: this.selectAllOnClick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("table_select_name")),
                        React.createElement("th", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("logtable_kind_name")),
                        React.createElement("th", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("logtable_title_name")),
                        React.createElement("th", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("logtable_contents_name")),
                        React.createElement("th", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("usertable_regdate_name")))),
                React.createElement("tbody", { className: "tbody" },
                    React.createElement(TableBody, { data: this.props.data })))));
    }
}
class SearchBoxAndTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            startDate: "",
            endDate: "",
            logdate: "",
            kind: "",
            title: "",
            contents: "",
            currentPage: 1,
            maxPage: -1,
            perpage: 10,
            resultData: []
        };
        this.getDataUsingRestCallFunc = this.getDataUsingRestCallFunc.bind(this);
        this.startDateOnChange = this.startDateOnChange.bind(this);
        this.endDateOnChange = this.endDateOnChange.bind(this);
        this.kindOnChange = this.kindOnChange.bind(this);
        this.titleOnChange = this.titleOnChange.bind(this);
        this.searchOnclick = this.searchOnclick.bind(this);
        this.csvOnclick = this.csvOnclick.bind(this);
        this.firstPageOnclick = this.firstPageOnclick.bind(this);
        this.lastPageOnclick = this.lastPageOnclick.bind(this);
        this.prePageOnclick = this.prePageOnclick.bind(this);
        this.nextPageOnclick = this.nextPageOnclick.bind(this);
        this.currentPageOnKeyPress = this.currentPageOnKeyPress.bind(this);
    }
    getDataUsingRestCallFunc(page) {
        var logDataAjaxResult;
        var option1 = {};
        var option2 = {};
        if (this.state.kind != "") {
            option1["kind"] = this.state.kind;
        }
        if (this.state.title != "") {
            option1["title"] = this.state.title;
        }
        if (this.state.startDate != "" && this.state.endDate != "") {
            option2["startdate"] = encoder_1.encoder.dateToYYMMDDHHMMSS(this.state.startDate + "000000");
            option2["enddate"] = encoder_1.encoder.dateToYYMMDDHHMMSS(this.state.endDate + "235959");
        }
        var logDataAjaxCallObj = new dbRestcall_1.LogDataAjaxCall();
        logDataAjaxCallObj.create([properties_1.properties["Log_view_log"]], []);
        logDataAjaxResult = logDataAjaxCallObj.retrieve([], [option1, option2, { orderby: "logdate" }, { page: page, perpage: this.state.perpage }]);
        return logDataAjaxResult;
    }
    firstPageOnclick() {
        var logDataResult;
        if (this.state.currentPage != 1) {
            logDataResult = this.getDataUsingRestCallFunc(1);
            this.setState({
                resultData: logDataResult["items"],
                currentPage: 1
            });
            $("#current_page").val(1);
        }
    }
    lastPageOnclick() {
        var logDataResult;
        if (this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage) {
            logDataResult = this.getDataUsingRestCallFunc(this.state.maxPage);
            this.setState({
                resultData: logDataResult["items"],
                currentPage: this.state.maxPage
            });
            $("#current_page").val(this.state.maxPage);
        }
    }
    prePageOnclick() {
        var logDataResult;
        if (this.state.maxPage != -1 && this.state.currentPage > 1) {
            logDataResult = this.getDataUsingRestCallFunc(this.state.currentPage - 1);
            this.setState({ resultData: logDataResult["items"] });
            $("#current_page").val(this.state.currentPage - 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage - 1
            }));
        }
    }
    nextPageOnclick() {
        var logDataResult;
        if (this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage) {
            logDataResult = this.getDataUsingRestCallFunc(this.state.currentPage + 1);
            this.setState({ resultData: logDataResult["items"] });
            $("#current_page").val(this.state.currentPage + 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage + 1
            }));
        }
    }
    currentPageOnKeyPress(e) {
        if (e.which == 13) {
            var pageVal = $("#current_page").val();
            var page = parseInt(pageVal);
            if (isNaN(page)) {
                alert(inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_is_not_number_alert"));
                $("#current_page").val(this.state.currentPage);
            }
            else {
                if (page > this.state.maxPage) {
                    alert(inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_greater_than_max_alert"));
                    $("#current_page").val(this.state.currentPage);
                }
                else {
                    if (this.state.maxPage != -1 &&
                        this.state.currentPage != page) {
                        var logDataResult = this.getDataUsingRestCallFunc(page);
                        this.setState({
                            resultData: logDataResult["items"],
                            currentPage: page
                        });
                        $("#current_page").val(page);
                    }
                }
            }
        }
    }
    startDateOnChange(e) {
        this.setState({
            startDate: e.target.value
        });
    }
    endDateOnChange(e) {
        this.setState({
            endDate: e.target.value
        });
    }
    kindOnChange(e) {
        this.setState({
            kind: e.target.value
        });
    }
    titleOnChange(e) {
        this.setState({
            title: e.target.value
        });
    }
    csvOnclick() {
        csvModule.onclickCsvButton(this.state.kind, this.state.title, this.state.startDate, this.state.endDate);
    }
    deleteOnClick(e) {
        var logDataAjaxCallObj = new dbRestcall_1.LogDataAjaxCall();
        var optionList = [];
        for (var i = 0; i < selectedList.length; i++) {
            optionList.push({ logdate: selectedList[i] });
        }
        logDataAjaxCallObj.delete([], optionList);
        location.reload();
    }
    searchOnclick() {
        var logDataAjaxResult = this.getDataUsingRestCallFunc(1);
        this.setState({ resultData: logDataAjaxResult["items"] });
        var maxpage = Math.ceil(logDataAjaxResult["total"] / this.state.perpage);
        this.setState({ maxPage: maxpage });
        if (logDataAjaxResult["items"].length == 0) {
            $("#current_page").val(0);
            $("#max_page").val(0);
            this.setState({ maxPage: 0, currentPage: 0 });
        }
        else {
            $("#current_page").val(1);
            $("#max_page").val(maxpage);
            this.setState({ maxPage: maxpage, currentPage: 1 });
        }
    }
    componentDidMount() {
        this.searchOnclick();
    }
    render() {
        return (React.createElement("div", { className: "main_box" },
            React.createElement("div", { className: "current_page_info" },
                inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_setting_root_name"),
                React.createElement("i", { className: "fas fa-angle-right" }),
                inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_logmanager_name")),
            React.createElement("div", { className: "searchBox" },
                React.createElement("div", { className: "title_input_box" },
                    React.createElement("label", null,
                        inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("logtable_title_name"),
                        React.createElement("input", { type: "text", className: "title_input", id: "title", onChange: this.titleOnChange, defaultValue: this.state.title }))),
                React.createElement("div", { className: "date_input_box" },
                    React.createElement("div", { className: "start_date_input_box" },
                        React.createElement("label", null,
                            inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_startdate_label"),
                            React.createElement("input", { type: "date", className: "start_date_input", onChange: this.startDateOnChange, defaultValue: this.state.startDate }))),
                    React.createElement("div", { className: "end_date_input_box" },
                        React.createElement("label", null,
                            inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_enddate_label"),
                            React.createElement("input", { type: "date", className: "end_date_input", onChange: this.endDateOnChange, defaultValue: this.state.endDate })))),
                React.createElement("div", { className: "button_box" },
                    React.createElement("div", { className: "csv_button_box" },
                        React.createElement("button", { className: "csv_button", onClick: this.csvOnclick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_csv_button_name"))),
                    React.createElement("div", { className: "search_button_box" },
                        React.createElement("button", { className: "searchButton", onClick: this.searchOnclick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_search_button_name"))),
                    React.createElement("div", null,
                        React.createElement("button", { className: "items_delete_button", onClick: this.deleteOnClick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("table_selected_item_delete_button"))))),
            React.createElement("div", null,
                React.createElement(TableHeaderAndBox, { data: this.state.resultData })),
            React.createElement("div", { className: "page_box" },
                React.createElement("div", { className: "pre_page_box" },
                    React.createElement("i", { className: "fas fa-angle-double-left", onClick: this.firstPageOnclick }),
                    React.createElement("i", { className: "fas fa-angle-left", onClick: this.prePageOnclick })),
                React.createElement("div", { className: "page_input_box" },
                    React.createElement("input", { type: "text", className: "current_page_input", id: "current_page", onKeyPress: this.currentPageOnKeyPress }),
                    React.createElement("div", { className: "slash" }),
                    React.createElement("input", { type: "text", className: "max_page_input", readOnly: "true", id: "max_page" })),
                React.createElement("div", { className: "post_page_box" },
                    React.createElement("i", { className: "fas fa-angle-right", onClick: this.nextPageOnclick }),
                    React.createElement("i", { className: "fas fa-angle-double-right", onClick: this.lastPageOnclick })))));
    }
}
ReactDOM.render(React.createElement("div", null,
    React.createElement(SearchBoxAndTable, null)), document.getElementById("main"));
