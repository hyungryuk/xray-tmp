import {injectable} from "inversify";

export interface LineSelectorInterface{
    datas:any[];
    divId:string;
    setDatas(datas:any[]):void;
    setDivId(divId:string):void;
    makeSelector():void;
}

@injectable()
export class LineSelector implements LineSelectorInterface{
    datas:any[];
    divId:string;
    setDivId(divId:string){
        this.divId = divId;
    }
    setDatas(datas:any[]){
        this.datas = datas;
    }
    makeSelector(){
        for (var i = 0; i < this.datas.length; i++) {
            $("#" + this.divId).append(
                new Option(
                    this.datas[i].factory + " " + this.datas[i].line,
                    this.datas[i].linecode
                )
            );
        }
    }
}

export interface AlgorithmSelectorInterface{
    datas:any[];
    divId:string;
    setDatas(datas:any[]):void;
    setDivId(divId:string):void;
    makeSelector():void;
}

@injectable()
export class AlgorithmSelector implements AlgorithmSelectorInterface{
    datas:any[];
    divId:string;
    setDivId(divId:string){
        this.divId = divId;
    }
    setDatas(datas:any[]){
        this.datas = datas;
    }
    makeSelector(){
        for (var i = 0; i < this.datas.length; i++) {
            $("#" + this.divId).append(
                new Option(
                    this.datas[i].algorithmname + " " + this.datas[i].version,
                    this.datas[i].algorithmcode
                )
            );
        }
    }
}

export interface FilterSelectorInterface{
    datas:any[];
    divId:string;
    setDatas(datas:any[]):void;
    setDivId(divId:string):void;
    makeSelector():void;
}

@injectable()
export class FilterSelector implements FilterSelectorInterface{
    datas:any[];
    divId:string;
    setDivId(divId:string){
        this.divId = divId;
    }
    setDatas(datas:any[]){
        this.datas = datas;
    }
    makeSelector(){
        for (var i = 0; i < this.datas.length; i++) {
            $("#" + this.divId).append(
                new Option(
                    this.datas[i].filtername + " " + this.datas[i].version,
                    this.datas[i].filtercode
                )
            );
        }
    }
}
