# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 10:50:08 2018

@author: ryuk
"""
from .. import db

def create_mdl(item):
    db.session.add(item)

class DLModelInfo(db.Model):
    __tablename__ = 'iis_model'
    __table_args__ = {'mysql_collate': 'utf8_general_ci', 'mysql_engine': 'MyISAM'}

    modelcode = db.Column(db.VARCHAR(16), primary_key=True, nullable=False)
    modelname = db.Column(db.VARCHAR(128), nullable=False)
    version = db.Column(db.VARCHAR(16), nullable=False)
    algorithm = db.Column(db.VARCHAR(16), nullable=False)
    filter = db.Column(db.VARCHAR(16), nullable=False)
    fullpath = db.Column(db.VARCHAR(256), nullable=False)
    accuracy = db.Column(db.FLOAT, nullable=False)
    available = db.Column(db.Boolean, nullable=False)
    regdate = db.Column(db.DateTime, nullable=True)

    def __init__(self, model_code, model_name, version, algorithm, filter, full_path, accuracy, available):
        self.modelcode = model_code
        self.modelname = model_name
        self.version = version
        self.algorithm = algorithm
        self.filter = filter
        self.fullpath = full_path
        self.accuracy = accuracy
        self.available = available


    def __repr__(self):
        return repr((self.modelcode, self.modelname, self.version, self.algorithm, self.filter,
                     self.fullpath, self.accuracy, self.available, self.regdate))
