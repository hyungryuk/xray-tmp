export var properties :{[key:string] :any} = {};

properties = {
    WEB_PATH:"http://192.168.0.115:80",
    DIRECTORY_SEPERATOR:"/",
    LabelingPageItemSlideSpeed:"1000",
    AlgorithmDownloadPath : "/user/xray/xrayDir/algorithms/",
    filterDownloadPath : "/user/xray/xrayDir/filters/",
    Algorithm_view_log:{kind:3,title:"algorithm_retrieve_title_log",contents:"algorithm_retrieve_title_log_by_"},
    Log_view_log:{kind:3,title:"log_retrieve_title_log",contents:"log_retrieve_title_log_by_"},
    Filter_view_log:{kind:3,title:"filter_retrieve_title_log",contents:"filter_retrieve_title_log_by_"},
    Line_view_log:{kind:3,title:"line_retrieve_title_log",contents:"line_retrieve_title_log_by_"},
    User_view_log:{kind:3,title:"user_retrieve_title_log",contents:"user_retrieve_title_log_by_"},
    ImageFile_view_log:{kind:3,title:"imagefile_retrieve_title_log",contents:"imagefile_retrieve_title_log_by_"}
}
