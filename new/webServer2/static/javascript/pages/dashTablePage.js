"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const ReactDOM = require("react-dom");
const header_1 = require("../modulesForPages/header");
const inversify_config_1 = require("../DIContainerConf/inversify.config");
const types_1 = require("../DIContainerConf/types");
const dbRestcall_1 = require("../others/restCall/dbRestcall");
const encoder_1 = require("../others/encoder/encoder");
var selectedList = [];
Date.prototype.yyyymmddhhmmss = function () {
    var mm = this.getMonth() + 1;
    var dd = this.getDate();
    var hh = this.getHours();
    var minutes = this.getMinutes();
    var ss = this.getSeconds();
    return [
        this.getFullYear(),
        (mm > 9 ? "" : "0") + mm,
        (dd > 9 ? "" : "0") + dd,
        (hh > 9 ? "" : "0") + hh,
        (minutes > 9 ? "" : "0") + minutes,
        (ss > 9 ? "" : "0") + ss
    ].join("");
};
ReactDOM.render(React.createElement(header_1.HeaderComponent, null), document.getElementById("header"));
var csvModule = {
    onclickCsvButton: function (linecode, startdate, enddate, unit) {
        var csvData = csvModule.makeCsvData(linecode, startdate, enddate, unit);
        csvModule.downloadCsvFile(csvData, startdate, enddate);
    },
    downloadCsvFile: function (data, startdate, enddate) {
        var encodedStartdate = encoder_1.encoder.dateToYYMMDDHHMMSS(startdate);
        var encodedEnddate = encoder_1.encoder.dateToYYMMDDHHMMSS(enddate);
        let csvContent = "data:text/csv;charset=euc-kr,";
        data.forEach(function (rowArray) {
            let row = rowArray.join(",");
            csvContent += row + "\r\n";
        });
        var encodedUri = encodeURI(csvContent);
        var link = document.createElement("a");
        link.setAttribute("href", encodedUri);
        link.setAttribute("download", encodedStartdate + "_" + encodedEnddate + ".csv");
        document.body.appendChild(link);
        link.click();
    },
    makeCsvData: function (linecode, startdate_orin, enddate_orin, unit) {
        var startdate = new Date(startdate_orin + " 00:00:00");
        var enddate = new Date(enddate_orin + " 23:59:59");
        var option1 = {};
        if (linecode != "all") {
            option1["linecode"] = linecode;
        }
        var option2 = {};
        option2["startdate"] = startdate.yyyymmddhhmmss();
        option2["enddate"] = enddate.yyyymmddhhmmss();
        let lineRestCall = new dbRestcall_1.LineAjaxCall();
        let lineRestCallRes = lineRestCall.retrieve([], [option1]);
        option1["kind"] = unit;
        let StatisticDataRestCall = new dbRestcall_1.StatisticDataAjaxCall();
        var statisticsResult;
        if (startdate_orin == "" || enddate_orin == "") {
            statisticsResult = StatisticDataRestCall.retrieve([], [option1]);
        }
        else {
            statisticsResult = StatisticDataRestCall.retrieve([], [option1, option2]);
        }
        var csvData = [];
        var firstRow = [];
        firstRow.push(inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_line_label_name"));
        firstRow.push(inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_table_term_name"));
        firstRow.push(inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_table_total_name"));
        firstRow.push(inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_table_ok_name"));
        firstRow.push(inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_table_ng_name"));
        firstRow.push(inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_table_nr_name"));
        csvData.push(firstRow);
        var statisticsDataList = statisticsResult["items"];
        for (var i = 0; i < statisticsDataList.length; i++) {
            var row = [];
            for (var j = 0; j < lineRestCallRes["items"].length; j++) {
                if (statisticsDataList[i].linecode ==
                    lineRestCallRes["items"][j].linecode) {
                    row.push(lineRestCallRes["items"][j].factory +
                        " " +
                        lineRestCallRes["items"][j].line);
                    break;
                }
            }
            row.push(statisticsDataList[i].date);
            row.push(statisticsDataList[i].ngcount +
                statisticsDataList[i].okcount +
                statisticsDataList[i].nrcount);
            row.push(statisticsDataList[i].okcount);
            row.push(statisticsDataList[i].ngcount);
            row.push(statisticsDataList[i].nrcount);
            csvData.push(row);
        }
        return csvData;
    }
};
class CheckBoxInTable extends React.Component {
    constructor(props) {
        super(props);
        this.checkBoxOnChange = this.checkBoxOnChange.bind(this);
    }
    checkBoxOnChange(e) {
        if (e.target.checked == true) {
            selectedList.push(this.props.date);
        }
        else {
            var index = selectedList.indexOf(this.props.date);
            selectedList.splice(index, 1);
        }
    }
    render() {
        return (React.createElement("input", { type: "checkbox", onChange: this.checkBoxOnChange, className: "checkbox" }));
    }
}
class LineNameInTable extends React.Component {
    render() {
        for (var i = 0; i < this.props.lineList.length; i++) {
            if (this.props.lineList[i].linecode == this.props.linecode) {
                return (this.props.lineList[i].factory +
                    " " +
                    this.props.lineList[i].line);
            }
        }
    }
}
class TableBody extends React.Component {
    render() {
        console.log(this.props.data);
        const data = this.props.data.map(item => (React.createElement("tr", null,
            React.createElement("td", null,
                React.createElement(CheckBoxInTable, { date: item.date })),
            React.createElement("td", null,
                React.createElement(LineNameInTable, { linecode: item.linecode, lineList: this.props.lineList })),
            React.createElement("td", null, item.date),
            React.createElement("td", { className: "number" }, item.ngcount + item.okcount + item.nrcount),
            React.createElement("td", { className: "number" }, item.okcount),
            React.createElement("td", { className: "number" }, item.ngcount),
            React.createElement("td", { className: "number" }, item.nrcount))));
        return data;
    }
}
class TableHeaderAndBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isSelectallClicked: false
        };
        this.selectAllOnClick = this.selectAllOnClick.bind(this);
    }
    selectAllOnClick() {
        var checkboxs = document.getElementsByClassName("checkbox");
        if (this.state.isSelectallClicked == false) {
            for (var i = 0; i < this.props.data.length; i++) {
                selectedList.push(this.props.data[i].algorithmcode);
            }
            for (var i = 0; i < checkboxs.length; i++) {
                checkboxs[i].checked = true;
            }
        }
        else {
            selectedList = [];
            for (var i = 0; i < checkboxs.length; i++) {
                checkboxs[i].checked = false;
            }
        }
        this.setState(prevState => ({
            isSelectallClicked: !prevState.isSelectallClicked
        }));
    }
    render() {
        return (React.createElement("div", { className: "table_box" },
            React.createElement("table", { className: "table" },
                React.createElement("thead", { className: "thead" },
                    React.createElement("tr", null,
                        React.createElement("th", { onClick: this.selectAllOnClick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("table_select_name")),
                        React.createElement("th", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_table_line_name")),
                        React.createElement("th", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_table_term_name")),
                        React.createElement("th", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_table_total_name")),
                        React.createElement("th", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_table_ok_name")),
                        React.createElement("th", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_table_ng_name")),
                        React.createElement("th", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_table_nr_name")))),
                React.createElement("tbody", { className: "tbody" },
                    React.createElement(TableBody, { lineList: this.props.lineList, data: this.props.data })))));
    }
}
class SearchBoxAndTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            startDate: "",
            endDate: "",
            unit: 0,
            line: "all",
            resultData: [],
            lineList: [],
            currentPage: 1,
            maxPage: -1,
            perpage: 2
        };
        this.getDataUsingRestCallFunc = this.getDataUsingRestCallFunc.bind(this);
        this.startDateOnChange = this.startDateOnChange.bind(this);
        this.endDateOnChange = this.endDateOnChange.bind(this);
        this.searchUnitOnChange = this.searchUnitOnChange.bind(this);
        this.lineSelectOnChange = this.lineSelectOnChange.bind(this);
        this.csvOnclick = this.csvOnclick.bind(this);
        this.searchOnclick = this.searchOnclick.bind(this);
        this.firstPageOnclick = this.firstPageOnclick.bind(this);
        this.lastPageOnclick = this.lastPageOnclick.bind(this);
        this.prePageOnclick = this.prePageOnclick.bind(this);
        this.nextPageOnclick = this.nextPageOnclick.bind(this);
        this.currentPageOnKeyPress = this.currentPageOnKeyPress.bind(this);
    }
    getDataUsingRestCallFunc(page) {
        var staticticsResult;
        let StatisticDataRestCall = new dbRestcall_1.StatisticDataAjaxCall();
        if (this.state.line == "all") {
            staticticsResult = StatisticDataRestCall.retrieve([], [
                { kind: this.state.unit },
                {
                    startdate: encoder_1.encoder.dateToYYMMDDHHMMSS(this.state.startDate),
                    enddate: encoder_1.encoder.dateToYYMMDDHHMMSS(this.state.endDate)
                },
                {},
                { page: page, perpage: this.state.perpage }
            ]);
        }
        else {
            staticticsResult = StatisticDataRestCall.retrieve([], [
                { linecode: this.state.line, kind: this.state.unit },
                {
                    startdate: encoder_1.encoder.dateToYYMMDDHHMMSS(this.state.startDate),
                    enddate: encoder_1.encoder.dateToYYMMDDHHMMSS(this.state.endDate)
                },
                {},
                { page: page, perpage: this.state.perpage }
            ]);
        }
        return staticticsResult;
    }
    firstPageOnclick() {
        var staticticsResult;
        if (this.state.currentPage != 1) {
            staticticsResult = this.getDataUsingRestCallFunc(1);
            this.setState({
                resultData: staticticsResult["items"],
                currentPage: 1
            });
            $("#current_page").val(1);
        }
    }
    lastPageOnclick() {
        var staticticsResult;
        if (this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage) {
            staticticsResult = this.getDataUsingRestCallFunc(this.state.maxPage);
            this.setState({
                resultData: staticticsResult["items"],
                currentPage: this.state.maxPage
            });
            $("#current_page").val(this.state.maxPage);
        }
    }
    prePageOnclick() {
        var staticticsResult;
        if (this.state.maxPage != -1 && this.state.currentPage != 1) {
            staticticsResult = this.getDataUsingRestCallFunc(this.state.currentPage - 1);
            this.setState({ resultData: staticticsResult["items"] });
            $("#current_page").val(this.state.currentPage - 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage - 1
            }));
        }
    }
    nextPageOnclick() {
        var staticticsResult;
        if (this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage) {
            staticticsResult = this.getDataUsingRestCallFunc(this.state.currentPage + 1);
            this.setState({ resultData: staticticsResult["items"] });
            $("#current_page").val(this.state.currentPage + 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage + 1
            }));
        }
    }
    currentPageOnKeyPress(e) {
        if (e.which == 13) {
            var pageVal = $("#current_page").val();
            var page = parseInt(pageVal);
            if (isNaN(page)) {
                alert(inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_is_not_number_alert"));
                $("#current_page").val(this.state.currentPage);
            }
            else {
                if (page > this.state.maxPage) {
                    alert(inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_greater_than_max_alert"));
                    $("#current_page").val(this.state.currentPage);
                }
                else {
                    if (this.state.maxPage != -1 &&
                        this.state.currentPage != page) {
                        var staticticsResult = this.getDataUsingRestCallFunc(page);
                        this.setState({
                            resultData: staticticsResult["items"],
                            currentPage: page
                        });
                        $("#current_page").val(page);
                    }
                }
            }
        }
    }
    startDateOnChange(e) {
        this.setState({
            startDate: e.target.value
        });
    }
    endDateOnChange(e) {
        this.setState({
            endDate: e.target.value
        });
    }
    searchUnitOnChange() {
        var value = $("#unitSelector option:selected").val();
        this.setState({
            unit: parseInt(value)
        });
    }
    lineSelectOnChange() {
        this.setState({
            line: $("#lineSelector option:selected").val()
        });
    }
    csvOnclick() {
        console.log(this.state.startDate);
        csvModule.onclickCsvButton(this.state.line, this.state.startDate, this.state.endDate, this.state.unit);
    }
    deleteOnClick(e) {
        var staticticsResultObj = new dbRestcall_1.StatisticDataAjaxCall();
        var optionList = [];
        for (var i = 0; i < selectedList.length; i++) {
            optionList.push({ algorithmcode: selectedList[i] });
        }
        staticticsResultObj.delete([], optionList);
    }
    searchOnclick() {
        var staticticsResult = this.getDataUsingRestCallFunc(1);
        this.setState({ resultData: staticticsResult["items"] });
        var maxpage = Math.ceil(staticticsResult["total"] / this.state.perpage);
        this.setState({ maxPage: maxpage });
        $("#current_page").val(this.state.currentPage);
        $("#max_page").val(maxpage);
    }
    componentDidMount() {
        let lineRestCall = new dbRestcall_1.LineAjaxCall();
        let lineRestCallRes = lineRestCall.retrieve([], []);
        var lineList = lineRestCallRes["items"];
        this.setState({ lineList: lineList });
        let selectorObj = inversify_config_1.myContainer.get(types_1.TYPES.LineSelectorInterface);
        selectorObj.setDivId("lineSelector");
        selectorObj.setDatas(lineList);
        selectorObj.makeSelector();
        this.searchOnclick();
    }
    render() {
        return (React.createElement("div", { className: "main_box" },
            React.createElement("div", { className: "current_page_info" },
                inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_dash_root_name"),
                React.createElement("i", { className: "fas fa-angle-right" }),
                inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_dashtable_name")),
            React.createElement("div", { className: "searchBox" },
                React.createElement("div", { className: "date_input_box" },
                    React.createElement("div", { className: "start_date_input_box" },
                        React.createElement("label", null,
                            inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_startdate_label_name"),
                            React.createElement("input", { type: "date", className: "start_date_input", onChange: this.startDateOnChange }))),
                    React.createElement("div", { className: "end_date_input_box" },
                        React.createElement("label", null,
                            inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_enddate_label_name"),
                            React.createElement("input", { type: "date", className: "end_date_input", onChange: this.endDateOnChange })))),
                React.createElement("div", { className: "unit_input_box" },
                    React.createElement("label", null,
                        inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_unit_name"),
                        React.createElement("select", { className: "unit_input", id: "unitSelector", onChange: this.searchUnitOnChange },
                            React.createElement("option", { value: "0" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_unit_hourly_option_name")),
                            React.createElement("option", { value: "1" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_unit_daily_option_name")),
                            React.createElement("option", { value: "2" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_unit_weekly_option_name")),
                            React.createElement("option", { value: "3" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_unit_monthly_option_name")),
                            React.createElement("option", { value: "4" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_unit_yearly_option_name"))))),
                React.createElement("div", { className: "line_input_box" },
                    React.createElement("label", null,
                        inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_line_name"),
                        React.createElement("select", { className: "line_input", id: "lineSelector", onChange: this.lineSelectOnChange },
                            React.createElement("option", { value: "all" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_line_selector_option_all_name"))))),
                React.createElement("div", { className: "button_box" },
                    React.createElement("div", { className: "csv_button_box" },
                        React.createElement("button", { className: "csv_button", onClick: this.csvOnclick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_csv_button_name"))),
                    React.createElement("div", { className: "search_button_box" },
                        React.createElement("button", { className: "searchButton", onClick: this.searchOnclick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_search_button_name"))))),
            React.createElement("div", null,
                React.createElement(TableHeaderAndBox, { lineList: this.state.lineList, data: this.state.resultData })),
            React.createElement("div", { className: "page_box" },
                React.createElement("div", { className: "pre_page_box" },
                    React.createElement("i", { className: "fas fa-angle-double-left", onClick: this.firstPageOnclick }),
                    React.createElement("i", { className: "fas fa-angle-left", onClick: this.prePageOnclick })),
                React.createElement("div", { className: "page_input_box" },
                    React.createElement("input", { type: "text", className: "current_page_input", id: "current_page", onKeyPress: this.currentPageOnKeyPress }),
                    React.createElement("div", { className: "slash" }),
                    React.createElement("input", { type: "text", className: "max_page_input", readOnly: "true", id: "max_page" })),
                React.createElement("div", { className: "post_page_box" },
                    React.createElement("i", { className: "fas fa-angle-right", onClick: this.nextPageOnclick }),
                    React.createElement("i", { className: "fas fa-angle-double-right", onClick: this.lastPageOnclick })))));
    }
}
ReactDOM.render(React.createElement("div", null,
    React.createElement(SearchBoxAndTable, null)), document.getElementById("main"));
