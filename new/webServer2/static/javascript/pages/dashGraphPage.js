"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const ReactDOM = require("react-dom");
const Chartjs = require("chart.js");
const header_1 = require("../modulesForPages/header");
const inversify_config_1 = require("../DIContainerConf/inversify.config");
const types_1 = require("../DIContainerConf/types");
const dbRestcall_1 = require("../others/restCall/dbRestcall");
Date.prototype.yyyymmddhhmmss = function () {
    var mm = this.getMonth() + 1;
    var dd = this.getDate();
    var hh = this.getHours();
    var minutes = this.getMinutes();
    var ss = this.getSeconds();
    return [
        this.getFullYear(),
        (mm > 9 ? "" : "0") + mm,
        (dd > 9 ? "" : "0") + dd,
        (hh > 9 ? "" : "0") + hh,
        (minutes > 9 ? "" : "0") + minutes,
        (ss > 9 ? "" : "0") + ss
    ].join("");
};
ReactDOM.render(React.createElement(header_1.HeaderComponent, null), document.getElementById("header"));
class MainComponent extends React.Component {
    constructor(props) {
        super(props);
        let lineRestCall = new dbRestcall_1.LineAjaxCall();
        let lineRestCallRes = lineRestCall.retrieve([], []);
        this.state = {
            lines: lineRestCallRes["items"],
        };
    }
    componentDidMount() {
    }
    render() {
        const chartTags = this.state.lines.map((line) => React.createElement("div", { className: "line_box" },
            React.createElement("div", { className: "line_name" }, line.factory + " " + line.line),
            React.createElement("div", { className: "charts_in_line_box" },
                React.createElement(DashChart, { categoryType: "daily", linename: line.line, linecode: line.linecode }),
                React.createElement(DashChart, { categoryType: "hourly", linename: line.line, linecode: line.linecode }))));
        return (React.createElement("div", null,
            React.createElement("div", { className: "current_page_info" },
                inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_dash_root_name"),
                React.createElement("i", { className: "fas fa-angle-right" }),
                inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_dashgraph_name")),
            chartTags));
    }
}
class DashChart extends React.Component {
    constructor(props) {
        super(props);
    }
    getStatisticsData(type, linecode) {
        var date = new Date();
        var weekAgoDate = new Date();
        var dayAgoDate = new Date();
        weekAgoDate.setDate(date.getDate() - 6);
        dayAgoDate.setHours(date.getHours() - 24);
        let StatisticDataRestCall = new dbRestcall_1.StatisticDataAjaxCall();
        if (type == "daily") {
            var staticticsResultForDailyChart = StatisticDataRestCall.retrieve([], [
                { linecode: linecode, kind: 1 },
                {
                    startdate: weekAgoDate.yyyymmddhhmmss(),
                    enddate: date.yyyymmddhhmmss()
                }
            ]);
            return staticticsResultForDailyChart["items"];
        }
        else {
            var staticticsResultForHourlyChart = StatisticDataRestCall.retrieve([], [
                { linecode: linecode, kind: 0 },
                {
                    startdate: dayAgoDate.yyyymmddhhmmss(),
                    enddate: date.yyyymmddhhmmss()
                }
            ]);
            return staticticsResultForHourlyChart["items"];
        }
    }
    componentDidMount() {
        var dateLabel = [];
        var datas = {};
        if (this.props.categoryType == "daily") {
            var todayForLabel = new Date();
            dateLabel.push(todayForLabel.getDate());
            for (var i = 0; i < 6; i++) {
                todayForLabel.setDate(todayForLabel.getDate() - 1);
                dateLabel.push(todayForLabel.getDate());
            }
            dateLabel.reverse();
            datas["ok"] = [0, 0, 0, 0, 0, 0, 0];
            datas["ng"] = [0, 0, 0, 0, 0, 0, 0];
            datas["nr"] = [0, 0, 0, 0, 0, 0, 0];
            var graphData = this.getStatisticsData("daily", this.props.linecode);
            for (var i = 0; i < graphData.length; i++) {
                for (var j = 0; j < dateLabel.length; j++) {
                    if (graphData[i].date.substring(8, 10) ==
                        dateLabel[j]) {
                        datas["ng"][j] = graphData[i].ngcount;
                        datas["ok"][j] = graphData[i].okcount;
                        datas["nr"][j] = graphData[i].nrcount;
                    }
                }
            }
        }
        else {
            var todayForLabel = new Date();
            dateLabel.push(todayForLabel.getHours());
            for (var i = 0; i < 23; i++) {
                todayForLabel.setHours(todayForLabel.getHours() - 1);
                dateLabel.push(todayForLabel.getHours());
            }
            dateLabel.reverse();
            datas["ok"] = [
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0
            ];
            datas["ng"] = [
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0
            ];
            datas["nr"] = [
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0
            ];
            var graphData = this.getStatisticsData("hourly", this.props.linecode);
            for (var i = 0; i < graphData.length; i++) {
                for (var j = 0; j < dateLabel.length; j++) {
                    if (graphData[i].date.substring(11, 13) ==
                        dateLabel[j]) {
                        datas["ng"][j] = graphData[i].ngcount;
                        datas["ok"][j] = graphData[i].okcount;
                        datas["nr"][j] = graphData[i].nrcount;
                    }
                }
            }
        }
        var dataObject = {
            labels: dateLabel,
            datasets: [
                {
                    label: "OK",
                    data: datas["ok"],
                    backgroundColor: "green",
                    borderColor: "lightgreen",
                    fill: false,
                    lineTension: 0,
                    radius: 5
                },
                {
                    label: "NG",
                    data: datas["ng"],
                    backgroundColor: "red",
                    borderColor: "lightred",
                    fill: false,
                    lineTension: 0,
                    radius: 5
                },
                {
                    label: "NR",
                    data: datas["nr"],
                    backgroundColor: "gray",
                    borderColor: "lightgray",
                    fill: false,
                    lineTension: 0,
                    radius: 5
                }
            ]
        };
        var chart_title = "";
        if (this.props.categoryType == "daily") {
            chart_title = inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("daily_chart_name");
        }
        else {
            chart_title = inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("hourly_chart_name");
        }
        var optionsObject = {
            responsive: true,
            title: {
                display: true,
                position: "top",
                text: chart_title,
                fontSize: 18,
                fontColor: "#111"
            },
            legend: {
                display: true,
                position: "bottom",
                labels: {
                    fontColor: "#333",
                    fontSize: 16
                }
            }
        };
        var ctx = $("#" + this.props.linecode + "_" + this.props.categoryType);
        new Chartjs.Chart(ctx, {
            type: "bar",
            data: dataObject,
            options: optionsObject
        });
    }
    render() {
        return (React.createElement("div", null,
            React.createElement("div", { className: "chart-container" },
                React.createElement("canvas", { id: this.props.linecode + "_" + this.props.categoryType }))));
    }
}
ReactDOM.render(React.createElement("div", null,
    React.createElement(MainComponent, null)), document.getElementById("main"));
