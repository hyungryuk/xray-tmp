"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
Date.prototype.yyyymmdd = function () {
    var mm = this.getMonth() + 1;
    var dd = this.getDate();
    return [
        this.getFullYear(),
        (mm > 9 ? "" : "0") + mm,
        (dd > 9 ? "" : "0") + dd
    ].join("");
};
Date.prototype.yyyymmddhhmmss = function () {
    var mm = this.getMonth() + 1;
    var dd = this.getDate();
    var hh = this.getHours();
    var minutes = this.getMinutes();
    var ss = this.getSeconds();
    return [
        this.getFullYear(),
        (mm > 9 ? "" : "0") + mm,
        (dd > 9 ? "" : "0") + dd,
        (hh > 9 ? "" : "0") + hh,
        (minutes > 9 ? "" : "0") + minutes,
        (ss > 9 ? "" : "0") + ss
    ].join("");
};
exports.encoder = {
    dateToYYMMDDHHMMSS: function (input) {
        if (typeof input === "string" || input instanceof String) {
            var result = input;
            result = result.replace(/-/g, "");
            result = result.replace("T", "");
            result = result.replace(/:/g, "");
            result = result.replace(" ", "");
            return result;
        }
        else {
            throw new Error("input is not String");
        }
    }
};
