from server import app
from flask import render_template,request
from flask_login import current_user
import requests as rq
import json

from server.configs.config import *

@app.route('/rest/db/statistics', methods = ['POST'])
def db_statistics():
    url=REST_SERVER_PATH+'/rest/db/statistics'
    jsonData = request.get_json()
    jsonData.update({'userid':current_user.userid})

    result = rq.post(url,json=jsonData)

    response = app.response_class(
        response=json.dumps(result.json()),
        status=200,
        mimetype='application/json'
    )

    return response

@app.route('/rest/db/line', methods = ['POST'])
def db_line():
    url=REST_SERVER_PATH+'/rest/db/line'

    jsonData = request.get_json()
    jsonData.update({'userid':current_user.userid})

    result = rq.post(url,json=jsonData)

    response = app.response_class(
        response=json.dumps(result.json()),
        status=200,
        mimetype='application/json'
    )

    return response

@app.route('/rest/db/imagefile', methods = ['POST'])
def db_imagefile():
    url=REST_SERVER_PATH+'/rest/db/imagefile'

    jsonData = request.get_json()
    jsonData.update({'userid':current_user.userid})

    result = rq.post(url,json=jsonData)

    response = app.response_class(
        response=json.dumps(result.json()),
        status=200,
        mimetype='application/json'
    )

    return response

@app.route('/rest/db/imagefile', methods = ['GET'])
def open_imagefile():
    url=REST_SERVER_PATH+'/rest/db/imagefile'

    args = request.args
    node_id = args.get("nodeId")
    image_path = args.get("imagePath")

    if image_path is None:
        result = rq.get(url+'?nodeId='+node_id)
        response = app.response_class(
            response=json.dumps(result.json()),
            status=200,
            mimetype='application/json'
        )

        return response
    else:
        result = rq.post(url + '?imagePath=' + image_path)
        response = app.response_class(
            response=json.dumps(result.json()),
            status=200,
            mimetype='application/json'
        )
        return response

@app.route('/rest/db/algorithm', methods = ['POST'])
def db_algorithm():
    url=REST_SERVER_PATH+'/rest/db/algorithm'

    jsonData = request.get_json()
    jsonData.update({'userid':current_user.userid})

    result = rq.post(url,json=jsonData)

    response = app.response_class(
        response=json.dumps(result.json()),
        status=200,
        mimetype='application/json'
    )

    return response

@app.route('/rest/db/filter', methods = ['POST'])
def db_filter():
    url=REST_SERVER_PATH+'/rest/db/filter'

    jsonData = request.get_json()
    jsonData.update({'userid':current_user.userid})

    result = rq.post(url,json=jsonData)

    response = app.response_class(
        response=json.dumps(result.json()),
        status=200,
        mimetype='application/json'
    )

    return response

@app.route('/ml/trainModel', methods = ['POST'])
def train_model():
    url=MODEL_SERVER_PATH+'/ml/trainning'

    jsonData = request.get_json()
    jsonData.get('items')[0].update({'userid':current_user.userid})

    result = rq.post(url,json=jsonData)

    response = app.response_class(
        response=json.dumps(result.json()),
        status=200,
        mimetype='application/json'
    )

    return response

@app.route('/rest/db/training', methods = ['POST'])
def db_training():
    url=REST_SERVER_PATH+'/rest/trn'

    jsonData = request.get_json()
    jsonData.update({'userid':current_user.userid})

    result = rq.post(url,json=jsonData)

    response = app.response_class(
        response=json.dumps(result.json()),
        status=200,
        mimetype='application/json'
    )

    return response



@app.route('/ml/publishing', methods = ['POST'])
def deploy_model():
    url=MODEL_SERVER_PATH+'/ml/publishing'

    jsonData = request.get_json()
    jsonData.update({'userid':current_user.userid})

    result = rq.post(url,json=jsonData)

    response = app.response_class(
        response=json.dumps(result.json()),
        status=200,
        mimetype='application/json'
    )

    return response


@app.route('/rest/db/user', methods = ['POST'])
def db_user():
    url=REST_SERVER_PATH+'/rest/db/user'

    jsonData = request.get_json()
    jsonData.update({'userid':current_user.userid})

    result = rq.post(url,json=jsonData)

    response = app.response_class(
        response=json.dumps(result.json()),
        status=200,
        mimetype='application/json'
    )

    return response

@app.route('/rest/db/log', methods = ['POST'])
def db_log():
    url=REST_SERVER_PATH+'/rest/db/log'

    jsonData = request.get_json()
    jsonData.update({'userid':current_user.userid})

    result = rq.post(url,json=jsonData)

    response = app.response_class(
        response=json.dumps(result.json()),
        status=200,
        mimetype='application/json'
    )

    return response

@app.route('/rest/db/model', methods = ['POST'])
def db_model():
    url=REST_SERVER_PATH+'/rest/db/modelHandler'

    jsonData = request.get_json()
    jsonData.update({'userid':current_user.userid})

    result = rq.post(url,json=jsonData)

    response = app.response_class(
        response=json.dumps(result.json()),
        status=200,
        mimetype='application/json'
    )

    return response


@app.route('/rest/db/deploy', methods = ['POST'])
def db_deploy():
    url=REST_SERVER_PATH+'/rest/db/deploy'

    jsonData = request.get_json()
    jsonData.update({'userid':current_user.userid})

    result = rq.post(url,json=jsonData)

    response = app.response_class(
        response=json.dumps(result.json()),
        status=200,
        mimetype='application/json'
    )

    return response
