# python DemoAM.py "{\"Option\":{\"Option1\":128,\"Option2\":10,\"Option3\":12,\"Option4\":28,\"Option5\":28},\"Setting\":{\"FilterFullPath\":\"Filter01.py\",\"AlgorithmFullPath\":\"Keras_Cnn_Test\",\"ModelName\":\"Model1_1\"},\"Data\":{\"CSVFile\":[\"/data/mnist/train/tmp.csv\"]}}"

# {\"Option\":{\"Option1\":128,\"Option2\":10,\"Option3\":12,\"Option4\":28,\"Option5\":28},\"Setting\":{\"AlgorithmName\":\"Keras_Cnn_Test_V05\",\"ModelName\":\"Model1_1\"},\"Data\":{\"CSVFile\":[\"/data/mnist/train/label.csv\"]}}

# {
# \"Option\":{\"Option1\":128,\"Option2\":10,\"Option3\":12,\"Option4\":28,\"Option5\":28},
# \"Setting\":{\"AlgorithmName\":\"Keras_Cnn_Test\",\"ModelName\":\"Model1_1\"}
# \"Data\":{\"CSVFile\":[\"/data/mnist/train/label.csv\"]}
# }

import os
import sys
import json

print("DemoAM")
# print('Number of arguments:', len(sys.argv), 'arguments.')
# print('Argument List:', str(sys.argv))

# Argv =
# ['DemoAM_V05.py',
# '{\"Option\":{\"Option1\":128,\"Option2\":10,\"Option3\":12,\"Option4\":28,\"Option5\":28},
# \"Setting\":{\"FilterFullPath\":\"Filter01.py\",\"AlgorithmName\":\"Keras_Cnn_Test\",\"ModelName\":\"Model1_1\"},
# \"Data\":{\"CSVFile\":[\"/data/mnist/train/label.csv\"]}}']
Argv = sys.argv

JsonStr = Argv[1]
Input = json.loads(JsonStr)

# 전처리 수행
if("FilterFullPath" in Input['Setting'].keys()):
    FilterFullPath = Input['Setting']["FilterFullPath"]

    CmdStr = "python " + FilterFullPath + " \"" + JsonStr.replace("\"","\\\"") + "\""
    print(CmdStr)
    os.system(CmdStr)

# 모델 학습
if(Input['Setting']['AlgorithmFullPath']=='Keras_Cnn_Test'):
    CmdStr = "python Keras_Cnn_mnist_Save.py \"" + JsonStr.replace("\"","\\\"") + "\""
    os.system(CmdStr)
elif(Input['Setting']['AlgorithmFullPath']=='Keras_Cnn_Test_V01'):
#   Option1 : batch_size = 128
#   Option2 : num_classes = 10
#   Option3 : epochs = 12
#   Option4 : img_rows = 28
#   Option5 : img_cols = 28

    CmdStr = "python Keras_Cnn_mnist_V01_Save.py \"" + JsonStr.replace("\"","\\\"") + "\""
    print(CmdStr)
    os.system(CmdStr)
elif(Input['Setting']['AlgorithmFullPath']=='Keras_Cnn_Test_V02'):
    CmdStr = "python Keras_Cnn_mnist_V02_Save.py \"" + JsonStr.replace("\"","\\\"") + "\""
    print(CmdStr)
    os.system(CmdStr)

elif(Input['Setting']['AlgorithmFullPath']=='Keras_Cnn_Test_V03'):
    CmdStr = "python Keras_Cnn_mnist_V03_Save.py \"" + JsonStr.replace("\"","\\\"") + "\""
    print(CmdStr)
    os.system(CmdStr)

elif(Input['Setting']['AlgorithmFullPath']=='Keras_Cnn_Test_V04'):
    CmdStr = "python Keras_Cnn_mnist_V04_Save.py \"" + JsonStr.replace("\"","\\\"") + "\""
    print(CmdStr)
    os.system(CmdStr)

elif(Input['Setting']['AlgorithmFullPath']=='Keras_Cnn_Test_V05'):
    CmdStr = "python Keras_Cnn_mnist_V05_Save.py \"" + JsonStr.replace("\"","\\\"") + "\""
    print(CmdStr)
    os.system(CmdStr)

else:
    CmdStr = "python " + Input['Setting']['AlgorithmFullPath'] + " \"" + JsonStr.replace("\"","\\\"") + "\""
    os.system(CmdStr)
