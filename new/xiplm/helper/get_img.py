# -*-coding: utf-8 -*-
from ..configs.config import *
from .messages import *

from flask import json
import requests as rq

def get_img(modelcode,path,filename,progress):
    try:
        status = 2

      

        print(path)
        image_response = rq.post(REST_SERVER_URL+'/rest/dwn',json={'path':path,
                           'method_type': 'download'
                           })
        print(filename.split("."))
        if len(filename.split(".")) >1 :
            with open(IMG_DOWNLOAD_DIR % (modelcode,filename), "wb") as img_file :
                img_file.write(image_response.content)

        else :
            path_comma_sep = path.split(".")
            with open(IMG_DOWNLOAD_DIR % (modelcode,filename+"."+path_comma_sep[1]), "wb") as img_file :
                img_file.write(image_response.content)

        message = GETFILE_IMG_SUCCESS
        return message

    except Exception as e:
        result = rq.post(REST_SERVER_URL+'/rest/trn',json={'items': [{"contents":"get_img_fail"}],
                           'method_type': 'update',
                           'option': [{"modelcode":modelcode}]})
        message = GETFILE_IMG_SUCCESS_FAIL
        message.update({'cause': str(e)})
        print(message)
        return message
