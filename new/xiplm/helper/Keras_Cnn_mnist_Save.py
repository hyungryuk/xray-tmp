'''Trains a simple convnet on the MNIST dataset.

Gets to 99.25% test accuracy after 12 epochs
(there is still a lot of margin for parameter tuning).
16 seconds per epoch on a GRID K520 GPU.
'''

import os
import json
import sys
import shutil
import pandas as pd
import numpy as np

import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K

import scipy.misc

print("Keras_Cnn_mnist_Save")

Argv = sys.argv

# Argv =
# ['DemoAM.py',
# '{\"Option\":{\"Option1\":128,\"Option2\":10,\"Option3\":12,\"Option4\":28,\"Option5\":28},
# \"Setting\":{\"FilterFullPath\":\"Filter01.py\",\"AlgorithmName\":\"Keras_Cnn_Test\",\"ModelName\":\"Model1_1\"},
# \"Data\":{\"CSVFile\":[\"/data/mnist/train/tmp.csv\"]}}']
JsonStr = Argv[1]
Input = json.loads(JsonStr)

Option = Input['Option']
Setting = Input['Setting']
Data = Input['Data']

FilterFullPath = Input['Setting']["FilterFullPath"]


print(Option)

batch_size = 128
num_classes = 10
epochs = 12

# input image dimensions
img_rows, img_cols = 28, 28

# the data, shuffled and split between train and test sets
if 0:
    from keras.datasets import mnist

    (x_train, y_train), (x_test, y_test) = mnist.load_data()

    if 0:
        for idx in range(len(x_train)):
            scipy.misc.imsave('/data/mnist/train/x/' + ('000000' + str(idx))[-5:] + '.bmp', x_train[idx].reshape([28,28]))
        
    if 0:
        np.savetxt('/data/mnist/train/y.csv',y_train,delimiter=',',fmt='%d', newline='\n')
        y_label = np.argmax(y_train, axis=1)
        
    if 0:
        df = pd.DataFrame(columns=['filename','type','start_x','start_y','width','height'])
            
        for idx in range(len(y_label)):
            df = df.append({'filename':'/data/mnist/train/x/' + ('000000' + str(idx))[-5:] + '.bmp',
                            'type':y_label[idx],
                            'start_x':0,
                            'start_y':0,
                            'width':28,
                            'heigth':28}, ignore_index=True)    
        
        df.to_csv('/data/mnist/train/label.csv')

PreProcessedFlag = False
if("FilterFullPath" in Input['Setting'].keys()):
    if(os.path.isfile(Data['CSVFile'][0].replace(".csv","_F.csv"))):
        CSVFile = Data['CSVFile'][0].replace(".csv","_F.csv")
        PreProcessedFlag = True
    else:
        CSVFile = Data['CSVFile'][0]
else:
    CSVFile = Data['CSVFile'][0]
df_label = pd.read_csv(CSVFile)

x_train = np.ndarray([len(df_label),img_rows, img_cols, 1])
y_train = np.array(df_label['isdefected'],dtype='uint8')
for idx in range(len(df_label)):
    x_train[idx] = scipy.misc.imread(df_label['filepath'][idx]).reshape([img_rows, img_cols, 1])

if K.image_data_format() == 'channels_first':
    x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
#    x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
    input_shape = (1, img_rows, img_cols)
else:
    x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
#    x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
    input_shape = (img_rows, img_cols, 1)

x_train = x_train.astype('float32')
# x_test = x_test.astype('float32')
x_train /= 255
# x_test /= 255
print('x_train shape:', x_train.shape)
print(x_train.shape[0], 'train samples')
# print(x_test.shape[0], 'test samples')

# convert class vectors to binary class matrices
y_train = keras.utils.to_categorical(y_train, num_classes)
# y_test = keras.utils.to_categorical(y_test, num_classes)

model = Sequential()
model.add(Conv2D(32, kernel_size=(3, 3),
                 activation='relu',
                 input_shape=input_shape))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))
model.add(Flatten())
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(num_classes, activation='softmax'))

model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer=keras.optimizers.Adadelta(),
              metrics=['accuracy'])

model.fit(x_train, y_train,
          batch_size=batch_size,
          epochs=epochs,
          verbose=1)

# serialize model to JSON
model_json = model.to_json()
with open("/xray/models/" + Setting['ModelName'] + ".json", "w") as json_file:
    json_file.write(model_json)
# serialize weights to HDF5
model.save_weights("/xray/models/" + Setting['ModelName'] + ".h5")
print("Saved model to disk")
FTPath = "/xray/models/Filter_" + Setting['ModelName'] + ".py"
shutil.copyfile(Input['Setting']["FilterFullPath"],FTPath)
print("Saved Filter to disk")

if(PreProcessedFlag == True):
    os.remove(CSVFile)

    RMPath = "/xray/temp/" + Setting['ModelName']
    shutil.rmtree(RMPath)
    
    print("Deleted temp file")