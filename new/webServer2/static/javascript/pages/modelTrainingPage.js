"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const ReactDOM = require("react-dom");
const header_1 = require("../modulesForPages/header");
const inversify_config_1 = require("../DIContainerConf/inversify.config");
const types_1 = require("../DIContainerConf/types");
const dbRestcall_1 = require("../others/restCall/dbRestcall");
const othersRestCall_1 = require("../others/restCall/othersRestCall");
const encoder_1 = require("../others/encoder/encoder");
const training_1 = require("../others/training/training");
const makeCSV_1 = require("../others/csvModule/makeCSV");
ReactDOM.render(React.createElement(header_1.HeaderComponent, null), document.getElementById("header"));
class LineNameInTable extends React.Component {
    constructor(props) {
        super(props);
        this.lineCodeToFactoryAndLineName = this.lineCodeToFactoryAndLineName.bind(this);
    }
    lineCodeToFactoryAndLineName() {
        for (var i = 0; i < this.props.lineList.length; i++) {
            if (this.props.lineList[i].linecode == this.props.linecode) {
                return (this.props.lineList[i].factory +
                    " " +
                    this.props.lineList[i].line);
            }
        }
    }
    render() {
        return React.createElement("div", null, this.lineCodeToFactoryAndLineName());
    }
}
class NgToString extends React.Component {
    render() {
        if (this.props.ng == 0) {
            return React.createElement("td", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ok_name"));
        }
        if (this.props.ng == 1) {
            return React.createElement("td", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_name"));
        }
        if (this.props.ng == 2) {
            return React.createElement("td", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_nr_name"));
        }
    }
}
class Tbody extends React.Component {
    constructor(props) {
        super(props);
        this.availableOnClick = this.availableOnClick.bind(this);
    }
    availableOnClick(e) {
        var all_checkbox_num = $("input:checkbox").length;
        var unAvailable_checkbox_num = $("input:checkbox:not(:checked)").length;
        $("#imagesNum").html(all_checkbox_num - unAvailable_checkbox_num + "");
    }
    componentDidUpdate() {
        var all_checkbox_num = $("input:checkbox").length;
        var unAvailable_checkbox_num = $("input:checkbox:not(:checked)").length;
        $("#imagesNum").html(all_checkbox_num - unAvailable_checkbox_num + "");
    }
    render() {
        const data = this.props.data.map(item => (React.createElement("tr", null,
            React.createElement("td", null,
                React.createElement("input", { type: "checkBox", defaultChecked: "checked", onClick: this.availableOnClick })),
            React.createElement("td", null, item.filename),
            React.createElement("td", null, item.filedate),
            React.createElement("td", { className: "table_filepath" }, item.filepath),
            React.createElement("td", null,
                React.createElement(LineNameInTable, { linecode: item.linecode, lineList: this.props.lineList })),
            React.createElement(NgToString, { ng: item.ng_by_user }))));
        return data;
    }
}
class Table extends React.Component {
    render() {
        return (React.createElement("div", { className: "table_box" },
            React.createElement("table", { className: "table" },
                React.createElement("thead", { className: "thead" },
                    React.createElement("tr", null,
                        React.createElement("th", { className: "table_file_available" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modeltraining_table_file_available")),
                        React.createElement("th", { className: "table_filename" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_filename_table")),
                        React.createElement("th", { className: "table_filedate" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_filedate_table")),
                        React.createElement("th", { className: "table_filepath" }),
                        React.createElement("th", { className: "table_line" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_line_table")),
                        React.createElement("th", { className: "table_ng" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_by_user_table")))),
                React.createElement("tbody", { className: "tbody", id: "tbody" },
                    React.createElement(Tbody, { lineList: this.props.lineList, data: this.props.data })))));
    }
}
class TrainingHistory extends React.Component {
    render() {
        return (React.createElement("select", { className: "history_select" }));
    }
}
class InputArgument extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            lineList: [],
            line: "all",
            startDate: "",
            endDate: "",
            filter: "",
            algorithm: "",
            modelName: "",
            modelVersion: "",
            isStart: false,
            modelcode: 0
        };
        this.getDataUsingRestCallFunc = this.getDataUsingRestCallFunc.bind(this);
        this.getImageOnclick = this.getImageOnclick.bind(this);
        this.lineSelectOnChange = this.lineSelectOnChange.bind(this);
        this.filterSelectOnChange = this.filterSelectOnChange.bind(this);
        this.algorithmSelectOnChange = this.algorithmSelectOnChange.bind(this);
        this.startDateOnChange = this.startDateOnChange.bind(this);
        this.endDateOnChange = this.endDateOnChange.bind(this);
        this.modelNameOnChange = this.modelNameOnChange.bind(this);
        this.modelVersionOnChange = this.modelVersionOnChange.bind(this);
        this.trainingOnclick = this.trainingOnclick.bind(this);
    }
    componentDidMount() {
        var lineAjaxCall = new dbRestcall_1.LineAjaxCall();
        var lineList = lineAjaxCall.retrieve([], [])["items"];
        this.setState({
            lineList: lineList
        });
        let lineSelectorObj = inversify_config_1.myContainer.get(types_1.TYPES.LineSelectorInterface);
        lineSelectorObj.setDivId("lineSelector");
        lineSelectorObj.setDatas(lineList);
        lineSelectorObj.makeSelector();
        this.setState({ lineList: lineList });
        var algorithmAjaxCall = new dbRestcall_1.AlgorithmDataAjaxCall();
        var algorithmList = algorithmAjaxCall.retrieve([], [{ available: 1 }])["items"];
        let algorithmSelectorObj = inversify_config_1.myContainer.get(types_1.TYPES.AlgorithmSelectorInterface);
        algorithmSelectorObj.setDivId("algorithmSelector");
        algorithmSelectorObj.setDatas(algorithmList);
        algorithmSelectorObj.makeSelector();
        var filterAjaxCall = new dbRestcall_1.FilterDataAjaxCall();
        var filterList = filterAjaxCall.retrieve([], [{ available: 1 }])["items"];
        let filterSelectorObj = inversify_config_1.myContainer.get(types_1.TYPES.FilterSelectorInterface);
        filterSelectorObj.setDivId("filterSelector");
        filterSelectorObj.setDatas(filterList);
        filterSelectorObj.makeSelector();
    }
    filterSelectOnChange() {
        this.setState({
            filter: $("#filterSelector option:selected").val()
        });
        if ($("#filterSelector option:selected").val() == "") {
            $("#filter_name_info").html("(empty)");
        }
        else {
            $("#filter_name_info").html($("#filterSelector option:selected").text());
        }
    }
    algorithmSelectOnChange() {
        this.setState({
            algorithm: $("#algorithmSelector option:selected").val()
        });
        if ($("#algorithmSelector option:selected").val() == "") {
            $("#algorithm_name_info").html("(empty)");
        }
        else {
            $("#algorithm_name_info").html($("#algorithmSelector option:selected").text());
        }
    }
    modelNameOnChange(e) {
        this.setState({
            modelName: e.target.value
        });
        if (e.target.value == "") {
            $("#modelname_info").html("(empty)");
        }
        else {
            $("#modelname_info").html(e.target.value);
        }
    }
    modelVersionOnChange(e) {
        this.setState({
            modelVersion: e.target.value
        });
        if (e.target.value == "") {
            $("#modelversion_info").html("(empty)");
        }
        else {
            $("#modelversion_info").html(e.target.value);
        }
    }
    startDateOnChange(e) {
        this.setState({
            startDate: e.target.value
        });
    }
    endDateOnChange(e) {
        this.setState({
            endDate: e.target.value
        });
    }
    lineSelectOnChange() {
        this.setState({
            line: $("#lineSelector option:selected").val()
        });
    }
    getDataUsingRestCallFunc() {
        var imageFileResult;
        var option1 = {};
        var option2 = {};
        if (this.state.line != "all") {
            option1["linecode"] = this.state.line;
        }
        if (this.state.startDate != "" && this.state.endDate != "") {
            option2["startdate"] = encoder_1.encoder.dateToYYMMDDHHMMSS(this.state.startDate + "000000");
            option2["enddate"] = encoder_1.encoder.dateToYYMMDDHHMMSS(this.state.endDate + "235959");
        }
        option1["isremoved"] = 0;
        var imageFileAjaxCall = new dbRestcall_1.ImageFileAjaxCall();
        imageFileResult = imageFileAjaxCall.retrieve([], [option1, option2]);
        return imageFileResult;
    }
    getImageOnclick() {
        var imageFileResult = this.getDataUsingRestCallFunc();
        this.setState({ data: imageFileResult["items"] });
    }
    trainingOnclick() {
        if (parseInt($("#imagesNum").html()) <= 0) {
        }
        else if ($("#modelname_info").html() == "(empty)") {
        }
        else if ($("#modelversion_info").html() == "(empty)") {
        }
        else if ($("#algorithm_name_info").html() == "(empty)") {
        }
        else if ($("#filter_name_info").html() == "(empty)") {
        }
        else {
            var imageDataToJsonObj = new training_1.ImageDataToJson("tbody");
            var imagesJson = imageDataToJsonObj.getJson();
            var csvRowList = makeCSV_1.csvModule.makeCSVRowsList(imagesJson);
            var itemsDict = {};
            var modelcode = Date.now();
            itemsDict["csvrowlist"] = csvRowList;
            itemsDict["modelcode"] = modelcode;
            itemsDict["modelname"] = this.state.modelName;
            itemsDict["algorithmcode"] = this.state.algorithm;
            itemsDict["filtercode"] = this.state.filter;
            var trainingModelRestCallObj = new othersRestCall_1.TrainingModelRestCall();
            this.setState({
                isStart: true,
                modelcode: modelcode
            });
            var restResult = trainingModelRestCallObj.trainModel([itemsDict], []);
        }
    }
    render() {
        return (React.createElement("div", { className: "main_box" },
            React.createElement("div", { className: "current_page_info" },
                inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_model_root_name"),
                React.createElement("i", { className: "fas fa-angle-right" }),
                inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_trainmodel_name")),
            React.createElement("div", { className: "image_data_inputs_box" },
                React.createElement("div", { className: "line_selector_box" },
                    React.createElement("label", null,
                        inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_line_label_name"),
                        React.createElement("select", { className: "line_selector", id: "lineSelector", onChange: this.lineSelectOnChange },
                            React.createElement("option", { value: "all" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_line_selector_option_all_name"))))),
                React.createElement("div", { className: "set_date_box" },
                    React.createElement("div", { className: "start_date_box" },
                        React.createElement("label", null,
                            inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_startdate_label"),
                            React.createElement("input", { type: "date", id: "startDate", onChange: this.startDateOnChange }))),
                    React.createElement("div", { className: "end_date_box" },
                        React.createElement("label", null,
                            inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_enddate_label"),
                            React.createElement("input", { type: "date", id: "endDate", onChange: this.endDateOnChange })))),
                React.createElement("div", { className: "training_history_box" },
                    React.createElement("label", null,
                        inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("training_history_name"),
                        React.createElement(TrainingHistory, null))),
                React.createElement("div", { className: "get_image_button_box" },
                    React.createElement("button", { className: "get_image_button", onClick: this.getImageOnclick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modeltraining_get_image_data_button_name"))),
                React.createElement("div", { className: "image_table_box" },
                    React.createElement(Table, { data: this.state.data, lineList: this.state.lineList }))),
            React.createElement("div", { className: "model_data_input_box" },
                React.createElement("div", { className: "model_name_box" },
                    React.createElement("label", null,
                        inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modeltraining_table_model_name"),
                        React.createElement("input", { type: "text", className: "modelName", onChange: this.modelNameOnChange }))),
                React.createElement("div", { className: "model_version_box" },
                    React.createElement("label", null,
                        inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modeltraining_table_model_version"),
                        React.createElement("input", { type: "text", className: "modelVersion", onChange: this.modelVersionOnChange }))),
                React.createElement("div", { className: "select_algorithm_box" },
                    React.createElement("label", null,
                        inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modeltraining_table_algorithm_name"),
                        React.createElement("select", { className: "algorithm_selector", id: "algorithmSelector", onChange: this.algorithmSelectOnChange },
                            React.createElement("option", { value: "" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("select_box_default_option"))))),
                React.createElement("div", { className: "select_filter_box" },
                    React.createElement("label", null,
                        inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modeltraining_table_filter_name"),
                        React.createElement("select", { className: "filter_selector", id: "filterSelector", onChange: this.filterSelectOnChange },
                            React.createElement("option", { value: "" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("select_box_default_option"))))),
                React.createElement("div", { className: "info_box" },
                    React.createElement("div", { className: "info_key_box" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_line_label_name")),
                    React.createElement("div", { className: "info_value_box" }, $("#lineSelector option:selected").text()),
                    React.createElement("div", { className: "info_key_box" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modeltraining_image_count_name")),
                    React.createElement("div", { className: "info_value_box" },
                        React.createElement("div", { id: "imagesNum" })),
                    React.createElement("div", { className: "info_key_box" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modeltraining_table_model_name")),
                    React.createElement("div", { className: "info_value_box" },
                        React.createElement("div", { id: "modelname_info" }, "(empty)")),
                    React.createElement("div", { className: "info_key_box" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modeltraining_table_model_version")),
                    React.createElement("div", { className: "info_value_box" },
                        React.createElement("div", { id: "modelversion_info" }, "(empty)")),
                    React.createElement("div", { className: "info_key_box" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modeltraining_table_algorithm_name")),
                    React.createElement("div", { className: "info_value_box" },
                        React.createElement("div", { id: "algorithm_name_info" }, "(empty)")),
                    React.createElement("div", { className: "info_key_box" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modeltraining_table_filter_name")),
                    React.createElement("div", { className: "info_value_box" },
                        React.createElement("div", { id: "filter_name_info" }, "(empty)")))),
            React.createElement("div", { className: "textLog_box" },
                React.createElement("textarea", { id: "logArea", defaultValue: "Log Here", readOnly: "true" })),
            React.createElement("div", { className: "buttons_box" },
                React.createElement("button", { id: "startTraining", onClick: this.trainingOnclick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modeltraining_start_training_button")))));
    }
}
ReactDOM.render(React.createElement(InputArgument, null), document.getElementById("main"));
