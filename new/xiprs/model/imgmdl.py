import json

from .. import db


def create_image_file(item):
    db.session.add(item)


def update_image_file(item):
    image = db.session.query(ImageFileInfo).filter_by(filename=item['filename'],
                                                      filedate=item['filedate']).first()

    image.linecode = item['linecode']
    image.filepath = item['filepath']
    image.lotno = item['lotno']
    image.ng = item['ng']
    image.isremoved = item['isremoved']
    image.ng_by_user = item['ng_by_user']
    image.ng_by_mes = item['ng_by_mes']



class ImageFileInfo(db.Model):
    __tablename__ = 'iis_file'
    __table_args__ = {'mysql_collate': 'utf8_general_ci', 'mysql_engine': 'MyISAM'}

    filename = db.Column(db.VARCHAR(256), primary_key=True, nullable=False)
    filedate = db.Column(db.DateTime, primary_key=True, nullable=False)
    linecode = db.Column(db.Integer, nullable=False)
    filepath = db.Column(db.VARCHAR(256), nullable=False)
    lotno = db.Column(db.VARCHAR(128), nullable=False)
    ng = db.Column(db.SmallInteger, nullable=False)
    isremoved = db.Column(db.SmallInteger, nullable=False)
    ng_by_user = db.Column(db.SmallInteger, nullable=True)
    ng_by_mes = db.Column(db.SmallInteger, nullable=True)

    def __init__(self, file_name, file_date, line_code, file_path, lot_no, ng,
                 is_removed, ng_by_user, ng_by_mes):

        self.filename = file_name
        self.filedate = file_date
        self.linecode = line_code
        self.filepath = file_path
        self.lotno = lot_no
        self.ng = ng
        self.isremoved = is_removed
        self.ng_by_user = ng_by_user
        self.ng_by_mes = ng_by_mes

    def __repr__(self):
        return repr((self.filename, self.filedate, self.linecode, self.filepath, self.lotno, self.isdefected,
                     self.isremoved, self.ng_by_user, self.ng_by_mes))
