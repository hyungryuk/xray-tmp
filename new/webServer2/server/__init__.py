from flask import Flask,request
from flask_cors import CORS
from flask_login import LoginManager, UserMixin, login_user,logout_user,current_user,login_required
import json
import requests as rq

from server.configs.config import *

app = Flask(__name__, template_folder="../static/html",static_folder="../static")
cors = CORS(app)

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'
app.config["SECRET_KEY"]='thisSSsecrett'

url = REST_SERVER_PATH + '/rest/db/user'
url_for_log = REST_SERVER_PATH + '/rest/db/log'

class User(UserMixin):

    def __init__(self, userid, password, username, department, phone, email, grade):
        self.userid = userid
        self.password = password
        self.username = username
        self.department = department
        self.phone = phone
        self.email = email
        self.grade = grade

    def get_id(self):
        return self.userid


@login_manager.user_loader
def load_user(user_id):
    result = rq.post(url, json={'items':[], 'method_type':'view', 'options':[{'userid':user_id}]})
    userinfo_response = json.loads(result.json())

    if userinfo_response["return_code"] == "0000":
        user = User(userinfo_response["items"][0]["userid"], userinfo_response["items"][0]["password"],userinfo_response["items"][0]["username"],userinfo_response["items"][0]["department"],userinfo_response["items"][0]["phone"],userinfo_response["items"][0]["email"],userinfo_response["items"][0]["grade"])

        return user
    else:
        return None


@app.route('/web/do_login', methods=['POST'])
def do_login():
    try:
        request_data = request.get_json()
        items = request_data.get('items')
        if type(items) == list:
            for item in items:
                result = rq.post(url, json={'items':[], 'method_type':'view', 'options':[{'userid':item['userid']}]})
                userinfo_response = json.loads(result.json())
                user = User(userinfo_response["items"][0]["userid"], userinfo_response["items"][0]["password"],userinfo_response["items"][0]["username"],userinfo_response["items"][0]["department"],userinfo_response["items"][0]["phone"],userinfo_response["items"][0]["email"],userinfo_response["items"][0]["grade"])
                if user.password == item["password"]:
                    login_user(user)
                    rq.post(url_for_log, json={'items':[{'kind':3,'title':'login','contents':'login'}], 'method_type':'insert', 'options':[],'userid':current_user.userid})
                else :
                    res_dict = {"message": "password incorrect"}
                    response = app.response_class(
                        response=json.dumps(res_dict),
                        status=200,
                    )
                    return response

            res_dict = {"message": "login_success"}
            response = app.response_class(
                response=json.dumps(res_dict),
                status=200,
            )

            return response

    except Exception as e:
        response = app.response_class(
            response=json.dumps({"message": "login_fail",'cause': str(e)}),
            status=200
        )
        return response


@app.route('/web/do_logout', methods=['POST'])
@login_required
def do_logout():
    try:
        rq.post(url_for_log, json={'items':[{'kind':3,'title':'logout','contents':'logout'}], 'method_type':'insert', 'options':[],'userid':current_user.userid})
        logout_user()
        response = app.response_class(
            response=json.dumps({"message": "logout_success"}),
            status=200
        )
        return response

    except Exception as e:
        response = app.response_class(
            response=json.dumps({"message": "logout_fail",'cause': str(e)}),
            status=200
        )
        return response

@app.route('/web/current_user_info', methods=['POST'])
@login_required
def current_user_info():
    try:
        response = app.response_class(
            response=json.dumps({"message": "get_current_user_info_success","items":[{'username' : current_user.username, 'userid':current_user.userid, 'department':current_user.department, 'phone':current_user.phone,'email':current_user.email,'grade':current_user.grade}]}),
            status=200
        )
        return response

    except Exception as e:
        response = app.response_class(
            response=json.dumps({"message": "get_current_user_info_success_fail",'cause': str(e)}),
            status=200
        )
        return response


import server.views_for_web
import server.views_for_ajax
