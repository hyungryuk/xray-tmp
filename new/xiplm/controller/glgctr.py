# -*- coding: utf-8 -*-

from flask_restful import Resource
from flask import request
import json
import requests as rq

from ..helper.messages import *
from ..configs.config import *


class ModelLogRESTApi(Resource):

    def post(self):

        request_data = request.get_json()
        method_type = request_data.get('method_type')
        items = request_data.get('items')

        if method_type is not None:
            if items is not None:
                if method_type == 'view':
                    return view_processing(request_data)
                else:
                    pass
            else:
                message = COMMON_ITEMS_NOT_FOUND
                return json.dumps(message, ensure_ascii=True)
        else:
            message = COMMON_METHOD_TYPE_NOT_FOUND
            message.update({'items': request.get_json().get('items')})
            return json.dumps(message, ensure_ascii=True)


def view_processing(request_data):
    try:
        options = request_data.get('options')
        modelcode = options[0].get('modelcode')

        f = open(TRAINING_LOG_DIR % (modelcode,"training_log.txt"), 'r')
        lines = f.readlines()

        f.close()

        return json.dumps({'items': lines,
                           'return_code': MDL_TRAIN_SUCCESS.get('return_code'),
                           'message': MDL_TRAIN_SUCCESS.get('message')})

    except Exception as e:
        message = MDL_TRAIN_SUCCESS_FAIL
        message.update({'cause': str(e)})
        message.update({'items': request_data['items']})
        return json.dumps(message, ensure_ascii=True)
