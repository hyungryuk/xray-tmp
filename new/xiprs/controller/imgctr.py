# -*- coding: utf-8 -*-


from flask import request
from flask_restful import Resource
from datetime import datetime

from ..model.imgmdl import *
from ..helper.util import converter, delete_image
from ..helper.messages import *
from .. import db
from ..helper.util import get_file_tree, open_image
from ..configs.config import *

import sqlalchemy as sa

class ImageFileRESTApi(Resource):

    def get(self):
        try:
            args = request.args
            node_id = args.get("nodeId")
            image_paths = args.get("items")

            if image_paths is None:
                return get_file_tree(node_id)
            else:
            
                result = open_image(image_paths[0])
                print("result : "+result)
                return result
        except Exception as e:
            message = IMG_TREE_SUCCESS_FAIL
            message.update({'cause': str(e)})
            return json.dumps(message, ensure_ascii=True)

    def post(self):
        request_data = request.get_json()
        method_type = request_data.get('method_type')
        items = request_data.get('items')
        print(request_data)
        if method_type is not None:
            if items is not None:
                if method_type == 'view':

                    return view_processing(request_data)
                elif method_type == 'insert':
                    return insert_processing(items)
                elif method_type == 'show':
                    return show_processing(request_data)
                elif method_type == 'update':
                    return update_processing(request_data)
                elif method_type == 'count':
                    return count_processing(request_data)
                # elif method_type == 'statistic':
                #     return statistic_processing(request_data)
                elif method_type == 'delete':
                    return delete_processing(request_data)
                else:
                    pass
            else:
                message = COMMON_ITEMS_NOT_FOUND
                return json.dumps(message, ensure_ascii=True)
        else:
            message = COMMON_METHOD_TYPE_NOT_FOUND
            message.update({'items': request.get_json().get('items')})
            return json.dumps(message, ensure_ascii=True)




def insert_processing(items):
    try:
        if type(items) == list:
            for item in items:
                image_file = ImageFileInfo(item.get('filename'),
                                           datetime.strptime(item.get('filedate'), "%Y%m%d%H%M%S"),
                                           item.get('linecode'), item.get('filepath'), item.get('lotno'),
                                           item.get('ng'), item.get('isremoved'), item.get('ng_by_user'),
                                           item.get('ng_by_mes'))

                create_image_file(image_file)
            db.session.commit()

            message = IMG_CREATE_SUCCESS
            message.update({'items': items})
            return json.dumps(message, ensure_ascii=True)

    except Exception as e:
        db.session.rollback()
        db.session.remove()
        message = IMG_CREATE_SUCCESS_FAIL
        message.update({'cause': str(e)})
        message.update({'items': request.get_json()['items']})
        return json.dumps(message, ensure_ascii=True)

def show_processing(request_data):
    try:
        imagePaths = request_data.get('items')
        imageDataList=[]
        for path in imagePaths:
            path = path.replace(FILE_ID_SEPERATOR, FILE_TREE_SEPERATOR)
            imageDataList.append(open_image(path).decode("utf-8") )
        message = IMG_SHOW_SUCCESS
        message.update({'items': imageDataList})
        return json.dumps(message, ensure_ascii=True)
    except Exception as e:
        message = IMG_SHOW_SUCCESS_FAIL
        message.update({'cause': str(e)})
        return json.dumps(message, ensure_ascii=True)



def update_processing(request_data):
    try:
        items = request_data.get('items')
        options = request_data.get('options')
        if items is not None and options is not None:
            if type(items) == list:
                len_val = len(items)
                for i in range (0,len(items)):
                    query_input_element = options[i]
                    query_result = db.session.query(ImageFileInfo).filter_by(**query_input_element).all()
                    update_element = items[i]
                    for query_result_element in query_result:
                        for update_element_key in update_element:
                            setattr(query_result_element, update_element_key, update_element[update_element_key])
                db.session.commit()

                message = IMG_UPDATE_SUCCESS
                message.update({'items': items})
                return json.dumps(message, ensure_ascii=True)
        else:
            message = COMMON_ITEMS_NOT_FOUND
            return json.dumps(message, ensure_ascii=True)
    except Exception as e:
        db.session.rollback()
        db.session.remove()
        message = IMG_UPDATE_SUCCESS_FAIL
        message.update({'cause': str(e)})
        message.update({'items': request.get_json()['items']})
        return json.dumps(message, ensure_ascii=True)


def view_processing(request_data):
    try:
        items = request_data.get('items')
        options = request_data.get('options')

        if len(options) > 0:
            filter_params = make_filter_params(options[0])
            if len(options) == 1:
                result = db.session.query(ImageFileInfo).filter_by(**filter_params).all()
            elif len(options) == 2:
                start_date = options[1].get('startdate')
                end_date = options[1].get('enddate')

                if not start_date is None and not end_date is None:
                    start_date = datetime.strptime(start_date, "%Y%m%d%H%M%S")
                    end_date = datetime.strptime(end_date, "%Y%m%d%H%M%S")
                    print(start_date)
                    print(end_date)
                    result = db.session.query(ImageFileInfo).filter_by(**filter_params).filter(
                        ImageFileInfo.filedate >= start_date, ImageFileInfo.filedate <= end_date).all()
            elif len(options) == 3:
                orderby = options[2].get('orderby')
                if not orderby is None:
                    start_date = options[1].get('startdate')
                    end_date = options[1].get('enddate')
                    if not start_date is None and not end_date is None:
                        start_date = datetime.strptime(start_date, "%Y%m%d%H%M%S")
                        end_date = datetime.strptime(end_date, "%Y%m%d%H%M%S")
                        result = db.session.query(ImageFileInfo).filter_by(**filter_params).filter(
                            ImageFileInfo.filedate >= start_date, ImageFileInfo.filedate <= end_date).order_by(
                            orderby).all()
                    else:
                        result = db.session.query(ImageFileInfo).filter_by(**filter_params).order_by(orderby).all()
            elif len(options) == 4:
                page = options[3].get('page')
                per_page = options[3].get('perpage')
                orderby = options[2].get('orderby')
                start_date = options[1].get('startdate')
                end_date = options[1].get('enddate')
                if not orderby is None:
                    if not start_date is None and not end_date is None:
                        start_date = datetime.strptime(start_date, "%Y%m%d%H%M%S")
                        end_date = datetime.strptime(end_date, "%Y%m%d%H%M%S")
                        result = db.session.query(ImageFileInfo).filter_by(**filter_params).filter(
                            ImageFileInfo.filedate >= start_date, ImageFileInfo.filedate <= end_date).order_by(
                            orderby).paginate(int(page), int(per_page))
                    else:
                        result = db.session.query(ImageFileInfo).filter_by(**filter_params).order_by(orderby).paginate(int(page), int(per_page))
                else:
                    if not start_date is None and not end_date is None:
                        start_date = datetime.strptime(start_date, "%Y%m%d%H%M%S")
                        end_date = datetime.strptime(end_date, "%Y%m%d%H%M%S")
                        result = db.session.query(ImageFileInfo).filter_by(**filter_params).filter(
                            ImageFileInfo.filedate >= start_date, ImageFileInfo.filedate <= end_date).paginate(int(page), int(per_page))
                    else:
                        result = db.session.query(ImageFileInfo).filter_by(**filter_params).paginate(int(page), int(per_page))
            else:
                pass

            if len(options) == 4:
                return json.dumps({'items': [item.__dict__ for item in result.items], 'total': result.total,
                                   'return_code': IMG_VIEW_SUCCESS.get('return_code'),
                                   'message': IMG_VIEW_SUCCESS.get('message')}, default=converter)
            else:
                return json.dumps({'items': [item.__dict__ for item in result],
                                   'return_code': IMG_VIEW_SUCCESS.get('return_code'),
                                   'message': IMG_VIEW_SUCCESS.get('message')}, default=converter)
        else:
            result = db.session.query(ImageFileInfo).all()
            return json.dumps({'items': [item.__dict__ for item in result],
                       'return_code': IMG_VIEW_SUCCESS.get('return_code'),
                       'message': IMG_VIEW_SUCCESS.get('message')}, default=converter)

    except Exception as e:
        message = IMG_VIEW_SUCCESS_FAIL
        message.update({'cause': str(e)})
        message.update({'items': request_data['items']})
        return json.dumps(message, ensure_ascii=True)

def count_processing(request_data):
    try:
        items = request_data.get('items')
        options = request_data.get('options')

        if len(options) > 0:
            filter_params = make_filter_params(options[0])
            if len(options) == 1:
                result = db.session.query(ImageFileInfo).filter_by(**filter_params).all()
            elif len(options) == 2:
                start_date = options[1].get('startdate')
                end_date = options[1].get('enddate')
                if not start_date is None and not end_date is None:
                    start_date = datetime.strptime(start_date, "%Y%m%d%H%M%S")
                    end_date = datetime.strptime(end_date, "%Y%m%d%H%M%S")
                    result = db.session.query(ImageFileInfo).filter_by(**filter_params).filter(
                        ImageFileInfo.filedate >= start_date, ImageFileInfo.filedate <= end_date).all()


        message = IMG_COUNT_SUCCESS
        result_dic = {'count': result}
        message.update({'items': [result_dic]})
        return json.dumps(message, ensure_ascii=True)

    except Exception as e:
        message = IMG_COUNT_SUCCESS_FAIL
        message.update({'cause': str(e)})
        message.update({'items': request_data['items']})
        return json.dumps(message, ensure_ascii=True)


def delete_processing(request_data):
    try:
        update_processing(request_data)
        file_path = request_data.get('options')[0].get('filepath')
        if file_path is not None:
            update_processing(request_data)
            delete_image(file_path)
        else:
            update_processing(request_data)
        message = IMG_DELETE_SUCCESS
        return json.dumps(message, ensure_ascii=True)

    except Exception as e:
        message = IMG_DELETE_SUCCESS_FAIL
        message.update({'cause': str(e)})
        message.update({'items': request_data['items']})
        return json.dumps(message, ensure_ascii=True)


def make_filter_params(item):
    filter_params = {}
    if not item.get('filename') is None:
        filter_params['filename'] = item.get('filename')

    if not item.get('filedate') is None:
        filter_params['filedate'] = str(datetime.strptime(item.get('filedate'), "%Y%m%d%H%M%S"))

    if not item.get('linecode') is None:
        filter_params['linecode'] = item.get('linecode')

    if not item.get('filepath') is None:
        filter_params['filepath'] = item.get('filepath')

    if not item.get('lotno') is None:
        filter_params['lotno'] = item.get('lotno')

    if not item.get('ng') is None:
        filter_params['ng'] = item.get('ng')

    if not item.get('ng_by_user') is None:
        filter_params['ng_by_user'] = item.get('ng_by_user')

    if not item.get('ng_by_mes') is None:
        filter_params['ng_by_mes'] = item.get('ng_by_mes')

    if not item.get('isremoved') is None:
        filter_params['isremoved'] = item.get('isremoved')

    return filter_params
