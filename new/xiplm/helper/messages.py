# -*-coding: utf-8 -*-

COMMON_METHOD_TYPE_NOT_FOUND = {'return_code': '3000', 'message': 'COMMON_METHOD_TYPE_NOT_FOUND'}
COMMON_ITEMS_NOT_FOUND = {'return_code': '3001', 'message': 'COMMON_ITEMS_NOT_FOUND'}


MDL_TRAIN_SUCCESS = {'return_code': '0000', 'message': 'MDL_TRAIN_SUCCESS.'}
MDL_TRAIN_SUCCESS_FAIL = {'return_code': '3101', 'message': 'MDL_TRAIN_SUCCESS_FAIL'}
MDL_STATUS_SUCCESS = {'return_code': '0000', 'message': 'MDL_STATUS_SUCCESS.'}
MDL_STATUS_SUCCESS_FAIL = {'return_code': '3102', 'message': 'MDL_STATUS_SUCCESS_FAIL.'}


MAKEFILE_CSV_SUCCESS = {'return_code': '0000', 'message': 'MAKEFILE_CSV_SUCCESS.'}
MAKEFILE_CSV_SUCCESS_FAIL = {'return_code': '3201', 'message': 'MAKEFILE_CSV_SUCCESS_FAIL.'}

GETFILE_ALGORITHM_SUCCESS = {'return_code': '0000', 'message': 'GETFILE_ALGORITHM_SUCCESS'}
GETFILE_ALGORITHM_SUCCESS_FAIL = {'return_code': '3301', 'message': 'GETFILE_ALGORITHM_SUCCESS_FAIL'}


GETFILE_IMG_SUCCESS = {'return_code': '0000', 'message': 'GETFILE_IMG_SUCCESS'}
GETFILE_IMG_SUCCESS_FAIL = {'return_code': '3401', 'message': 'GETFILE_IMG_SUCCESS_FAIL'}

GETFILE_FILTER_SUCCESS = {'return_code': '0000', 'message': 'GETFILE_FILTER_SUCCESS'}
GETFILE_FILTER_SUCCESS_FAIL = {'return_code': '3501', 'message': 'GETFILE_FILTER_SUCCESS_FAIL.'}
