
from werkzeug.wrappers import Request, Response
from werkzeug.serving import run_simple

import os
from shutil import copyfile

from jsonrpc import JSONRPCResponseManager, dispatcher
from os import listdir


@dispatcher.add_method
def copyModel(**kwargs):
    
    try:
        
        os.makedirs(os.path.dirname(kwargs["des"]), exist_ok=True)
    
        for f in listdir(kwargs["src"]):
            copyfile(kwargs["src"]+"\\"+f,kwargs["des"]+"\\"+f)
    
    except:
        return "fail"
    
    return "success"
    

    


@dispatcher.add_method
def runModel(**kwargs):

    os.system("python /Users/ryuk/git/Xray_image_processing/new/clientDemonRPC/Keras_Cnn_mnist_Load.py \npause");




@Request.application
def application(request):

    response = JSONRPCResponseManager.handle(
        request.data, dispatcher)
    return Response(response.json, mimetype='application/json')


if __name__ == '__main__':
    
    
    run_simple('localhost', 4000, application)