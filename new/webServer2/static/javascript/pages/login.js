"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const ReactDOM = require("react-dom");
const inversify_config_1 = require("../DIContainerConf/inversify.config");
const types_1 = require("../DIContainerConf/types");
const othersRestCall_1 = require("../others/restCall/othersRestCall");
class LoginHeader extends React.Component {
    render() {
        return (React.createElement("div", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("login_main_title")));
    }
}
ReactDOM.render(React.createElement(LoginHeader, null), document.getElementById("header"));
class LoginMain extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: "",
            pw: ""
        };
        this.idOnChange = this.idOnChange.bind(this);
        this.pwOnChange = this.pwOnChange.bind(this);
        this.loginOnClick = this.loginOnClick.bind(this);
        this.onKeyPress = this.onKeyPress.bind(this);
    }
    idOnChange(e) {
        this.setState({
            id: e.target.value
        });
    }
    pwOnChange(e) {
        this.setState({
            pw: e.target.value
        });
    }
    loginOnClick() {
        if (this.state.id == "") {
            inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("login_id_empty_message");
        }
        else if (this.state.pw == "") {
            inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("login_pw_empty_message");
        }
        else {
            var loginRestCallObj = new othersRestCall_1.LoginRestCall();
            var loginRestResult = loginRestCallObj.login(this.state.id, this.state.pw);
            console.log(loginRestResult);
            if (loginRestResult["message"] == "login_success") {
                location.href = "/web/dashGraph";
            }
            else {
                alert(inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("login_wrong"));
            }
        }
    }
    onKeyPress(e) {
        console.log("asdad");
        if (e.which == 13) {
            this.loginOnClick();
        }
    }
    render() {
        return (React.createElement("div", { className: "login_box" },
            React.createElement("div", { className: "inside_login_box" },
                React.createElement("div", { className: "id_box" },
                    React.createElement("div", { className: "id_name" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("login_id_name")),
                    React.createElement("input", { type: "text", className: "id_input", placeholder: inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("login_id_empty_message"), onChange: this.idOnChange, onKeyPress: this.onKeyPress })),
                React.createElement("div", { className: "pw_box" },
                    React.createElement("div", { className: "pw_name" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("login_pw_name")),
                    React.createElement("input", { type: "password", className: "pw_input", placeholder: inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("login_pw_empty_message"), onChange: this.pwOnChange, onKeyPress: this.onKeyPress }))),
            React.createElement("div", { className: "login_button_div" },
                React.createElement("button", { className: "login_button", onClick: this.loginOnClick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("login")))));
    }
}
ReactDOM.render(React.createElement(LoginMain, null), document.getElementById("main"));
