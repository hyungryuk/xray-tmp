"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const ReactDOM = require("react-dom");
require("jquery-ui/themes/base/all.css");
require("jquery-ui/demos/demos.css");
require("jquery-ui/external/requirejs/require.js");
require("jquery-ui/ui/widgets/slider.js");
const header_1 = require("../modulesForPages/header");
const inversify_config_1 = require("../DIContainerConf/inversify.config");
const types_1 = require("../DIContainerConf/types");
const dbRestcall_1 = require("../others/restCall/dbRestcall");
const encoder_1 = require("../others/encoder/encoder");
var selectedList = [];
ReactDOM.render(React.createElement(header_1.HeaderComponent, null), document.getElementById("header"));
class DeleteButtonInTable extends React.Component {
    constructor(props) {
        super(props);
        this.deleteOnclicked = this.deleteOnclicked.bind(this);
    }
    deleteOnclicked() {
        var trainingDataAjaxCallObj = new dbRestcall_1.TrainingDataAjaxCall();
        trainingDataAjaxCallObj.delete([], [{ modelcode: this.props.modelcode }]);
    }
    render() {
        return (React.createElement("button", { className: "delete_button", onClick: this.deleteOnclicked }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_delete_table")));
    }
}
class FilterInTable extends React.Component {
    constructor(props) {
        super(props);
        this.filterCodeToFilterName = this.filterCodeToFilterName.bind(this);
    }
    filterCodeToFilterName() {
        for (var i = 0; i < this.props.filterList.length; i++) {
            if (this.props.filterList[i].filtercode == this.props.filtercode) {
                return (this.props.filterList[i].filtername);
            }
        }
    }
    render() {
        return React.createElement("div", null, this.filterCodeToFilterName());
    }
}
class AlgorithmInTable extends React.Component {
    constructor(props) {
        super(props);
        this.algorithmCodeToAlgorithmName = this.algorithmCodeToAlgorithmName.bind(this);
    }
    algorithmCodeToAlgorithmName() {
        for (var i = 0; i < this.props.algorithmList.length; i++) {
            if (this.props.algorithmList[i].algorithmcode == this.props.algorithmcode) {
                return (this.props.algorithmList[i].algorithmname);
            }
        }
    }
    render() {
        return React.createElement("div", null, this.algorithmCodeToAlgorithmName());
    }
}
class StatusInPopup extends React.Component {
    constructor(props) {
        super(props);
        this.initDate = this.initDate.bind(this);
    }
    initDate() {
        $("#" + this.props.id).val(this.props.status);
    }
    componentDidMount() {
        this.initDate();
    }
    render() {
        return React.createElement("input", { type: "text", className: "status_input", id: this.props.id });
    }
}
class AlgorithmSelectBoxInPopup extends React.Component {
    componentDidMount() {
        var algorithmDataAjaxCallObj = new dbRestcall_1.AlgorithmDataAjaxCall();
        var algorithmList = algorithmDataAjaxCallObj.retrieve([], [])["items"];
        let algorithmSelectorObj = inversify_config_1.myContainer.get(types_1.TYPES.AlgorithmSelectorInterface);
        algorithmSelectorObj.setDivId(this.props.id);
        algorithmSelectorObj.setDatas(algorithmList);
        algorithmSelectorObj.makeSelector();
        $("#" + this.props.id).val(this.props.algorithm);
    }
    render() {
        return (React.createElement("select", { className: "algorithm_input", id: this.props.id },
            React.createElement("option", { value: "all" }, "all")));
    }
}
class FilterSelectBoxInPopup extends React.Component {
    componentDidMount() {
        var filterDataAjaxCallObj = new dbRestcall_1.FilterDataAjaxCall();
        var filterList = filterDataAjaxCallObj.retrieve([], [])["items"];
        let filterSelectorObj = inversify_config_1.myContainer.get(types_1.TYPES.FilterSelectorInterface);
        filterSelectorObj.setDivId(this.props.id);
        filterSelectorObj.setDatas(filterList);
        filterSelectorObj.makeSelector();
        $("#" + this.props.id).val(this.props.filter);
    }
    render() {
        return (React.createElement("select", { className: "filter_input", id: this.props.id },
            React.createElement("option", { value: "all" }, "all")));
    }
}
class AccuracyBoxInPopup extends React.Component {
    componentDidMount() {
        $("#startAccuracyRange").val(this.props.startAccuracy);
        $("#endAccuracyRange").val(this.props.endAccuracy);
        $("#slider-range").slider({
            range: true,
            min: 0,
            max: 100,
            values: [this.props.startAccuracy, this.props.endAccuracy],
            slide: function (event, ui) {
                $("#startAccuracyRange").val(ui.values[0]);
                $("#endAccuracyRange").val(ui.values[1]);
            }
        });
    }
    render() {
        return (React.createElement("div", null,
            React.createElement("input", { type: "text", id: this.props.startAcid, readOnly: true }),
            React.createElement("input", { type: "text", id: this.props.endAcid, readOnly: true }),
            React.createElement("div", { id: "slider-range" })));
    }
}
class CheckBoxInTable extends React.Component {
    constructor(props) {
        super(props);
        this.checkBoxOnChange = this.checkBoxOnChange.bind(this);
    }
    checkBoxOnChange(e) {
        if (e.target.checked == true) {
            selectedList.push(this.props.modelcode);
        }
        else {
            var index = selectedList.indexOf(this.props.modelcode);
            selectedList.splice(index, 1);
        }
    }
    render() {
        return (React.createElement("input", { type: "checkbox", onChange: this.checkBoxOnChange, className: "checkbox" }));
    }
}
class RegdateBoxInPopup extends React.Component {
    componentDidMount() {
        var filterDataAjaxCallObj = new dbRestcall_1.FilterDataAjaxCall();
        var filterList = filterDataAjaxCallObj.retrieve([], [])["items"];
        let filterSelectorObj = inversify_config_1.myContainer.get(types_1.TYPES.FilterSelectorInterface);
        filterSelectorObj.setDivId(this.props.id);
        filterSelectorObj.setDatas(filterList);
        filterSelectorObj.makeSelector();
        $("#" + this.props.id).val(this.props.filter);
    }
    render() {
        return (React.createElement("div", null,
            React.createElement("input", { type: "date", id: this.props.startDateid }),
            React.createElement("input", { type: "date", id: this.props.endDateid })));
    }
}
class Tbody extends React.Component {
    render() {
        console.log(this.props.data);
        const data = this.props.data.map(item => (React.createElement("tr", null,
            React.createElement("td", null,
                React.createElement(CheckBoxInTable, { modelcode: item.modelcode })),
            React.createElement("td", null, item.modelname),
            React.createElement("td", null,
                React.createElement(AlgorithmInTable, { algorithmcode: item.algorithm, algorithmList: this.props.algorithmList })),
            React.createElement("td", null,
                React.createElement(FilterInTable, { filtercode: item.filter, filterList: this.props.filterList })),
            React.createElement("td", { className: "number" }, item.accuracy),
            React.createElement("td", null, item.user),
            React.createElement("td", { className: "number" }, item.status),
            React.createElement("td", null, item.contents),
            React.createElement("td", null, item.regdate))));
        return data;
    }
}
class TableHeaderAndBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isSelectallClicked: false
        };
        this.selectAllOnClick = this.selectAllOnClick.bind(this);
    }
    selectAllOnClick() {
        var checkboxs = document.getElementsByClassName("checkbox");
        if (this.state.isSelectallClicked == false) {
            for (var i = 0; i < this.props.data.length; i++) {
                selectedList.push(this.props.data[i].modelcode);
            }
            for (var i = 0; i < checkboxs.length; i++) {
                checkboxs[i].checked = true;
            }
        }
        else {
            selectedList = [];
            for (var i = 0; i < checkboxs.length; i++) {
                checkboxs[i].checked = false;
            }
        }
        this.setState(prevState => ({
            isSelectallClicked: !prevState.isSelectallClicked
        }));
    }
    render() {
        return (React.createElement("div", { className: "table_box" },
            React.createElement("table", { className: "table" },
                React.createElement("thead", { className: "thead" },
                    React.createElement("tr", null,
                        React.createElement("th", { onClick: this.selectAllOnClick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("table_select_name")),
                        React.createElement("th", { className: "table_modelname" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_model_name")),
                        React.createElement("th", { className: "table_algorithm" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_algorithm_name")),
                        React.createElement("th", { className: "table_filter" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_filter_name")),
                        React.createElement("th", { className: "table_accuracy" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_accuracy_name")),
                        React.createElement("th", { className: "table_user" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_user_name")),
                        React.createElement("th", { className: "table_status" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_status_name")),
                        React.createElement("th", { className: "table_content" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_contents_name")),
                        React.createElement("th", { className: "table_regdate" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_regdate_name")))),
                React.createElement("tbody", { className: "tbody" },
                    React.createElement(Tbody, { algorithmList: this.props.algorithmList, filterList: this.props.filterList, data: this.props.data })))));
    }
}
class SearchBoxAndTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modelcode: "",
            modelname: "",
            user: "",
            status: "",
            algorithm: "all",
            filter: "all",
            start_accuracy: 0,
            end_accuracy: 100,
            currentPage: 1,
            maxPage: -1,
            perpage: 10,
            isfilterOpen: false,
            start_regdate: "",
            end_regdate: "",
            algorithmList: [],
            filterList: [],
            resultData: []
        };
        this.getDataUsingRestCallFunc = this.getDataUsingRestCallFunc.bind(this);
        this.modelcodeOnChange = this.modelcodeOnChange.bind(this);
        this.modelnameOnChange = this.modelnameOnChange.bind(this);
        this.algorithmOnChange = this.algorithmOnChange.bind(this);
        this.filterOnChange = this.filterOnChange.bind(this);
        this.userOnChange = this.userOnChange.bind(this);
        this.statusOnChange = this.statusOnChange.bind(this);
        this.start_accuracyOnChange = this.start_accuracyOnChange.bind(this);
        this.end_accuracyOnChange = this.end_accuracyOnChange.bind(this);
        this.start_regdateOnChange = this.start_regdateOnChange.bind(this);
        this.end_regdateOnChange = this.end_regdateOnChange.bind(this);
        this.searchOnclick = this.searchOnclick.bind(this);
        this.filterOnclick = this.filterOnclick.bind(this);
        this.filterCloseOnclick = this.filterCloseOnclick.bind(this);
        this.setFilterOnclick = this.setFilterOnclick.bind(this);
        this.firstPageOnclick = this.firstPageOnclick.bind(this);
        this.lastPageOnclick = this.lastPageOnclick.bind(this);
        this.prePageOnclick = this.prePageOnclick.bind(this);
        this.nextPageOnclick = this.nextPageOnclick.bind(this);
        this.currentPageOnKeyPress = this.currentPageOnKeyPress.bind(this);
    }
    setFilterOnclick() {
        this.setState({ status: $("#status").val() });
        this.setState({ algorithm: $("#algorithm option:selected").val() });
        this.setState({ filter: $("#filter option:selected").val() });
        this.setState({ start_accuracy: parseInt($("#startAccuracyRange").val()) });
        console.log($("#startAccuracyRange").val());
        console.log($("#endAccuracyRange").val());
        this.setState({ end_accuracy: parseInt($("#endAccuracyRange").val()) });
        this.setState({ start_regdate: $("#startRegDate").val() });
        this.setState({ end_regdate: $("#endRegDate").val() });
        this.setState(prevState => ({
            isfilterOpen: !prevState.isfilterOpen
        }));
    }
    filterCloseOnclick() {
        this.setState(prevState => ({
            isfilterOpen: !prevState.isfilterOpen
        }));
    }
    filterOnclick() {
        this.setState(prevState => ({
            isfilterOpen: !prevState.isfilterOpen
        }));
    }
    getDataUsingRestCallFunc(page) {
        var trainingDataAjaxCallResult;
        var option1 = {};
        var option2 = {};
        var option5 = {};
        if (this.state.algorithm != "all") {
            option1["algorithm"] = this.state.algorithm;
        }
        if (this.state.modelcode != "") {
            option1["modelcode"] = this.state.modelcode;
        }
        if (this.state.modelname != "") {
            option1["modelname"] = this.state.modelname;
        }
        if (this.state.user != "") {
            option1["user"] = this.state.user;
        }
        if (this.state.status != "") {
            option1["status"] = this.state.status;
        }
        if (this.state.filter != "all") {
            option1["filter"] = this.state.filter;
        }
        if (this.state.start_accuracy != "0" || this.state.end_accuracy != "100") {
            option5["start_accuracy"] = this.state.start_accuracy;
            option5["end_accuracy"] = this.state.end_accuracy;
        }
        if (this.state.start_regdate != "" && this.state.end_regdate != "") {
            option5["start_regdate"] = encoder_1.encoder.dateToYYMMDDHHMMSS(this.state.start_regdate + "000000");
            option2["end_regdate"] = encoder_1.encoder.dateToYYMMDDHHMMSS(this.state.end_regdate + "235959");
        }
        var trainingDataAjaxCallObj = new dbRestcall_1.TrainingDataAjaxCall();
        trainingDataAjaxCallResult = trainingDataAjaxCallObj.retrieve([], [option1, option2, {}, { page: page, perpage: this.state.perpage }, option5]);
        console.log(trainingDataAjaxCallResult);
        return trainingDataAjaxCallResult;
    }
    firstPageOnclick() {
        var trainingDataAjaxCallResult;
        if (this.state.currentPage != 1) {
            trainingDataAjaxCallResult = this.getDataUsingRestCallFunc(1);
            this.setState({
                resultData: trainingDataAjaxCallResult["items"],
                currentPage: 1
            });
            $("#current_page").val(1);
        }
    }
    lastPageOnclick() {
        var trainingDataAjaxCallResult;
        if (this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage) {
            trainingDataAjaxCallResult = this.getDataUsingRestCallFunc(this.state.maxPage);
            this.setState({
                resultData: trainingDataAjaxCallResult["items"],
                currentPage: this.state.maxPage
            });
            $("#current_page").val(this.state.maxPage);
        }
    }
    prePageOnclick() {
        var trainingDataAjaxCallResult;
        if (this.state.maxPage != -1 && this.state.currentPage > 1) {
            trainingDataAjaxCallResult = this.getDataUsingRestCallFunc(this.state.currentPage - 1);
            this.setState({ resultData: trainingDataAjaxCallResult["items"] });
            $("#current_page").val(this.state.currentPage - 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage - 1
            }));
        }
    }
    nextPageOnclick() {
        var trainingDataAjaxCallResult;
        if (this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage) {
            trainingDataAjaxCallResult = this.getDataUsingRestCallFunc(this.state.currentPage + 1);
            this.setState({ resultData: trainingDataAjaxCallResult["items"] });
            $("#current_page").val(this.state.currentPage + 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage + 1
            }));
        }
    }
    currentPageOnKeyPress(e) {
        if (e.which == 13) {
            var pageVal = $("#current_page").val();
            var page = parseInt(pageVal);
            if (isNaN(page)) {
                alert(inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_is_not_number_alert"));
                $("#current_page").val(this.state.currentPage);
            }
            else {
                if (page > this.state.maxPage) {
                    alert(inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_greater_than_max_alert"));
                    $("#current_page").val(this.state.currentPage);
                }
                else {
                    if (this.state.maxPage != -1 &&
                        this.state.currentPage != page) {
                        var trainingDataAjaxCallResult = this.getDataUsingRestCallFunc(page);
                        this.setState({
                            resultData: trainingDataAjaxCallResult["items"],
                            currentPage: page
                        });
                        $("#current_page").val(page);
                    }
                }
            }
        }
    }
    modelcodeOnChange(e) {
        this.setState({
            modelcode: e.target.value
        });
    }
    modelnameOnChange(e) {
        this.setState({
            modelname: e.target.value
        });
    }
    algorithmOnChange() {
        this.setState({
            algorithm: $("#algorithmSelector option:selected").val()
        });
    }
    filterOnChange() {
        this.setState({
            filter: $("#filterSelector option:selected").val()
        });
    }
    userOnChange(e) {
        this.setState({
            user: e.target.value
        });
    }
    statusOnChange(e) {
        this.setState({
            status: e.target.value
        });
    }
    start_accuracyOnChange(e) {
        this.setState({
            start_accuracy: e.target.value
        });
    }
    end_accuracyOnChange(e) {
        this.setState({
            end_accuracy: e.target.value
        });
    }
    start_regdateOnChange(e) {
        this.setState({
            start_regdate: e.target.value
        });
    }
    end_regdateOnChange(e) {
        this.setState({
            end_regdate: e.target.value
        });
    }
    deleteOnClick(e) {
        var trainingDataAjaxCallObj = new dbRestcall_1.TrainingDataAjaxCall();
        var optionList = [];
        for (var i = 0; i < selectedList.length; i++) {
            optionList.push({ modelcode: selectedList[i] });
        }
        trainingDataAjaxCallObj.delete([], optionList);
        location.reload();
    }
    searchOnclick() {
        var trainingDataAjaxCallResult = this.getDataUsingRestCallFunc(1);
        this.setState({ resultData: trainingDataAjaxCallResult["items"] });
        var maxpage = Math.ceil(trainingDataAjaxCallResult["total"] / this.state.perpage);
        this.setState({ maxPage: maxpage });
        if (trainingDataAjaxCallResult["items"].length == 0) {
            $("#current_page").val(0);
            $("#max_page").val(0);
            this.setState({ maxPage: 0, currentPage: 0 });
        }
        else {
            $("#current_page").val(1);
            $("#max_page").val(maxpage);
            this.setState({ maxPage: maxpage, currentPage: 1 });
        }
    }
    componentDidMount() {
        var filterDataAjaxCallObj = new dbRestcall_1.FilterDataAjaxCall();
        var filterList = filterDataAjaxCallObj.retrieve([], [])["items"];
        var algorithmDataAjaxCallObj = new dbRestcall_1.AlgorithmDataAjaxCall();
        var algorithmList = algorithmDataAjaxCallObj.retrieve([], [])["items"];
        this.setState({
            algorithmList: algorithmList,
            filterList: filterList
        });
        this.searchOnclick();
    }
    render() {
        const popup = (React.createElement("div", { className: "optional_settings_box" },
            React.createElement("div", { className: "popup_close_button" },
                React.createElement("i", { className: "fas fa-window-close", onClick: this.filterCloseOnclick })),
            React.createElement("div", { className: "status_input_box" },
                React.createElement("label", null,
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_status_name"),
                    React.createElement(StatusInPopup, { id: "status", status: this.state.status }))),
            React.createElement("div", { className: "algorithm_input_box" },
                React.createElement("label", null,
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_algorithm_name"),
                    React.createElement(AlgorithmSelectBoxInPopup, { algorithm: this.state.algorithm, id: "algorithm" }))),
            React.createElement("div", { className: "filter_input_box" },
                React.createElement("label", null,
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_filter_name"),
                    React.createElement(FilterSelectBoxInPopup, { filter: this.state.algorithm, id: "filter" }))),
            React.createElement("div", { className: "accuracy_input_box" },
                React.createElement("label", null,
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_accuracy_name"),
                    React.createElement(AccuracyBoxInPopup, { startAccuracy: this.state.start_accuracy, endAccuracy: this.state.end_accuracy, startAcid: "startAccuracyRange", endAcid: "endAccuracyRange" }))),
            React.createElement("div", { className: "regDate_input_box" },
                React.createElement("label", null,
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_regdate_name"),
                    React.createElement(RegdateBoxInPopup, { startDate: this.state.start_regdate, endDate: this.state.end_regdate, startDateid: "startRegDate", endDateid: "endRegDate" }))),
            React.createElement("div", { className: "filter_set_button" },
                React.createElement("button", { className: "set_button", onClick: this.setFilterOnclick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("filter_setting_button")))));
        if (this.state.isfilterOpen) {
            return (React.createElement("div", { className: "main_box" },
                React.createElement("div", { className: "current_page_info" },
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_model_root_name"),
                    React.createElement("i", { className: "fas fa-angle-right" }),
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_traintable_name")),
                React.createElement("div", null, popup),
                React.createElement("div", { className: "searchBox" },
                    React.createElement("div", { className: "modelname_input_box" },
                        React.createElement("label", null,
                            inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_model_name"),
                            React.createElement("input", { type: "text", className: "model_name_input", onChange: this.modelnameOnChange, defaultValue: this.state.modelname }))),
                    React.createElement("div", { className: "user_input_box" },
                        React.createElement("label", null,
                            inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_user_name"),
                            React.createElement("input", { type: "text", className: "user_input", onChange: this.userOnChange, defaultValue: this.state.user }))),
                    React.createElement("div", { className: "button_box" },
                        React.createElement("div", { className: "set_specific_filter_button_box" },
                            React.createElement("button", { className: "set_specific_filter_button", onClick: this.filterOnclick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_set_specific_filter_button_box_name"))),
                        React.createElement("div", { className: "search_button_box" },
                            React.createElement("button", { className: "searchButton", onClick: this.searchOnclick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_search_button_name"))),
                        React.createElement("div", null,
                            React.createElement("button", { className: "items_delete_button", onClick: this.deleteOnClick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("table_selected_item_delete_button"))))),
                React.createElement("div", null,
                    React.createElement(TableHeaderAndBox, { algorithmList: this.state.algorithmList, filterList: this.state.filterList, data: this.state.resultData })),
                React.createElement("div", { className: "page_box" },
                    React.createElement("div", { className: "pre_page_box" },
                        React.createElement("i", { className: "fas fa-angle-double-left", onClick: this.firstPageOnclick }),
                        React.createElement("i", { className: "fas fa-angle-left", onClick: this.prePageOnclick })),
                    React.createElement("div", { className: "page_input_box" },
                        React.createElement("input", { type: "text", className: "current_page_input", id: "current_page", onKeyPress: this.currentPageOnKeyPress }),
                        React.createElement("div", { className: "slash" }),
                        React.createElement("input", { type: "text", className: "max_page_input", readOnly: "true", id: "max_page" })),
                    React.createElement("div", { className: "post_page_box" },
                        React.createElement("i", { className: "fas fa-angle-right", onClick: this.nextPageOnclick }),
                        React.createElement("i", { className: "fas fa-angle-double-right", onClick: this.lastPageOnclick })))));
        }
        else {
            return (React.createElement("div", { className: "main_box" },
                React.createElement("div", { className: "current_page_info" },
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_model_root_name"),
                    React.createElement("i", { className: "fas fa-angle-right" }),
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_traintable_name")),
                React.createElement("div", { className: "searchBox" },
                    React.createElement("div", { className: "modelname_input_box" },
                        React.createElement("label", null,
                            inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_model_name"),
                            React.createElement("input", { type: "text", className: "model_name_input", onChange: this.modelnameOnChange, defaultValue: this.state.modelname }))),
                    React.createElement("div", { className: "user_input_box" },
                        React.createElement("label", null,
                            inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_user_name"),
                            React.createElement("input", { type: "text", className: "user_input", onChange: this.userOnChange, defaultValue: this.state.user }))),
                    React.createElement("div", { className: "button_box" },
                        React.createElement("div", { className: "set_specific_filter_button_box" },
                            React.createElement("button", { className: "set_specific_filter_button", onClick: this.filterOnclick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_set_specific_filter_button_box_name"))),
                        React.createElement("div", { className: "search_button_box" },
                            React.createElement("button", { className: "searchButton", onClick: this.searchOnclick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_search_button_name"))),
                        React.createElement("div", null,
                            React.createElement("button", { className: "items_delete_button", onClick: this.deleteOnClick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("table_selected_item_delete_button"))))),
                React.createElement("div", null,
                    React.createElement(TableHeaderAndBox, { algorithmList: this.state.algorithmList, filterList: this.state.filterList, data: this.state.resultData })),
                React.createElement("div", { className: "page_box" },
                    React.createElement("div", { className: "pre_page_box" },
                        React.createElement("i", { className: "fas fa-angle-double-left", onClick: this.firstPageOnclick }),
                        React.createElement("i", { className: "fas fa-angle-left", onClick: this.prePageOnclick })),
                    React.createElement("div", { className: "page_input_box" },
                        React.createElement("input", { type: "text", className: "current_page_input", id: "current_page", onKeyPress: this.currentPageOnKeyPress }),
                        React.createElement("div", { className: "slash" }),
                        React.createElement("input", { type: "text", className: "max_page_input", readOnly: "true", id: "max_page" })),
                    React.createElement("div", { className: "post_page_box" },
                        React.createElement("i", { className: "fas fa-angle-right", onClick: this.nextPageOnclick }),
                        React.createElement("i", { className: "fas fa-angle-double-right", onClick: this.lastPageOnclick })))));
        }
    }
}
ReactDOM.render(React.createElement("div", null,
    React.createElement(SearchBoxAndTable, null)), document.getElementById("main"));
