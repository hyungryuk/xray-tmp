# -*-coding: utf-8 -*-
from ..configs.config import *
from .messages import *

from flask import json
import requests as rq

def get_filter(modelcode,filtercode):
    try:
        status = 2

      

        filter_response = rq.post(REST_SERVER_URL+'/rest/db/filter',json={'items':[],
                           'method_type': 'view',
                           'options':[{'filtercode':filtercode}]
                           })
        filter = json.loads(filter_response.json())

        flt_dwn_response = rq.post(REST_SERVER_URL+'/rest/dwn',json={'path':filter.get('items')[0].get('fullpath'),
                           'method_type': 'download'
                           })

        with open(FIT_DOWNLOAD_DIR % (modelcode) + "filter.py", "wb") as filter_file :
            filter_file.write(flt_dwn_response.content)



        result = rq.post(REST_SERVER_URL+'/rest/trn',json={'items': [{"contents":"complete to download filter file"}],
                           'method_type': 'update',
                           'options': [{"modelcode":modelcode}]})

        message = GETFILE_ALGORITHM_SUCCESS
        return message

    except Exception as e:
        result = rq.post(REST_SERVER_URL+'/rest/trn',json={'items': [{"contents":"error occured in downloading filter"}],
                           'method_type': 'update',
                           'option': [{"modelcode":modelcode}]})
        message = GETFILE_ALGORITHM_SUCCESS_FAIL
        message.update({'cause': str(e)})
        print(message)
        return message
