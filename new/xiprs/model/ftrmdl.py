# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 10:50:08 2018

@author: ryuk
"""
from .. import db

def create_filter(item):
    db.session.add(item)


class FilterInfo(db.Model):
    __tablename__ = 'iis_filter'
    __table_args__ = {'mysql_collate': 'utf8_general_ci', 'mysql_engine': 'MyISAM'}

    filtercode = db.Column(db.VARCHAR(16), primary_key=True, nullable=False)
    filtername = db.Column(db.VARCHAR(128), nullable=False)
    version = db.Column(db.VARCHAR(16), nullable=False)
    fullpath = db.Column(db.VARCHAR(256), nullable=False)
    available = db.Column(db.Boolean, nullable=False)
    regdate = db.Column(db.DateTime, nullable=True)

    def __init__(self,filter_code,filter_name, version, full_path, available):
        self.filtercode = filter_code
        self.filtername = filter_name
        self.version = version
        self.fullpath = full_path
        self.available = available


    def __repr__(self):
        return repr((self.filtercode, self.filtername, self.version, self.available, self.fullpath, self.regdate))
