# -*- coding: utf-8 -*-
"""
Created on Mon Feb 26 14:17:37 2018

@author: Nuri
"""


import os
import sys
import json

import pandas as pd

import imageio
import numpy as np
import matplotlib.pyplot as plt

from scipy import ndimage

print("filter01")

# Argv =
# ['DemoAM_V07.py',
# '{\"Option\":{\"Option1\":128,\"Option2\":10,\"Option3\":12,\"Option4\":28,\"Option5\":28},
# \"Setting\":{\"FilterFullPath\":\"Filter01.py\",\"AlgorithmName\":\"Keras_Cnn_Test\",\"ModelName\":\"Model1_1\"},
# \"Data\":{\"CSVFile\":[\"/data/mnist/train/tmp.csv\"]}
# }']
Argv = sys.argv

JsonStr = Argv[1]
Input = json.loads(JsonStr)

Option = Input['Option']
Setting = Input['Setting']
Data = Input['Data']

FilterFullPath = Input['Setting']["FilterFullPath"]

ToPath = "/xray/temp/" + Setting['ModelName'] 

if not os.path.isdir(ToPath):
    os.mkdir(ToPath)
    
for idx_csv in range(len(Data['CSVFile'])):
    CSVFile = Data['CSVFile'][idx_csv]
    df_label = pd.read_csv(CSVFile)
    
    FromPath = os.path.dirname(df_label['filepath'][0])
    if not os.path.isdir(ToPath):
        os.makedirs(ToPath)

    for idx_img in range(len(df_label['filepath'])):
        OriImgPath = df_label['filepath'][idx_img]
        FltImgPath = df_label['filepath'][idx_img].replace(FromPath,ToPath)
        print(OriImgPath)

        img = imageio.imread(OriImgPath)
#        plt.imshow(img, interpolation='nearest')
#        plt.show()

        img = ndimage.gaussian_filter(img, (3), order=0)
#        plt.imshow(img, interpolation='nearest')
#        plt.show()
        
        img.tofile(FltImgPath)
        imageio.imwrite(FltImgPath, img)
    
        df_label['filepath'][idx_img] = FltImgPath

    df_label.to_csv(CSVFile.replace(".csv","ls -al v"), index=False)
    