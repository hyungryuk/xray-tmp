export var language = {
	english : { 
			key : "english",	
		common_main_title : "SKI Automatic Classification Controll System",	
		login_main_title : "SKI Automatic Classification Controll System",	
		login_pw_name : "PW",	
		login_id_name : "ID",	
		logout_sign : "log_out",	
		login_id_placeholder : "enter your id",	
		login_pw_placeholder : "enter your pw",	
		login : "sign in",	
		login_id_empty_message : "please enter id",	
		login_wrong : "Incorrect pw Or invalid Id",	
		login_pw_empty_message : "please enter pw",	
		dashGraph_select_line_name : "Select line",	
		dashGraph_hourly_data_graph_type_name : "hourly",	
		dashGraph_daily_data_graph_type_name : "daily",	
		menu_popup_info : "(Click Background to leave!)",	
		menu_dash_root_name : "DashBoard",	
		menu_file_root_name : "FileManager",	
		menu_model_root_name : "ModelManager",	
		menu_setting_root_name : "Settings",	
		menu_dashgraph_name : "Graph",	
		menu_dashtable_name : "Table",	
		menu_filetree_name : "FileTree",	
		menu_filetable_name : "FileTable",	
		menu_filelabel_name : "FileLabeling",	
		menu_trainmodel_name : "TrainModel",	
		menu_traintable_name : "TrainTable",	
		menu_deploymodel_name : "DeployModel",	
		menu_usersetting_name : "UserManager",	
		menu_linesetting_name : "LineManager",	
		menu_algorithmsetting_name : "AlgorithmManager",	
		menu_filtersetting_name : "FilterManager",	
		menu_logmanager_name : "LogManager",	
		menu_enviromentsetting_name : "Settings",	
		dashtable_startdate_label_name : "date",	
		dashtable_enddate_label_name : "~",	
		dashtable_unit_label_name : "unit",	
		dashtable_unit_hourly_option_name : "hourly",	
		dashtable_unit_daily_option_name : "daily",	
		dashtable_unit_weekly_option_name : "weekly",	
		dashtable_unit_monthly_option_name : "monthly",	
		dashtable_unit_yearly_option_name : "yearly",	
		dashtable_line_label_name : "line",	
		dashtable_csv_button_name : "save csv",	
		dashtable_search_button_name : "search",	
		dashtable_line_selector_option_all_name : "All",	
		dashtable_table_line_name : "line",	
		dashtable_table_term_name : "term",	
		dashtable_table_total_name : "total",	
		dashtable_table_ok_name : "ok",	
		dashtable_table_ng_name : "ng",	
		dashtable_table_nr_name : "nr",	
		dashtable_unit_name : "unit",	
		dashtable_line_name : "line",	
		input_page_num_greater_than_max_alert : "input value is greater than max page num",	
		input_page_num_is_not_number_alert : "Input must be number",	
		imagefiletable_filename_label : "filename",	
		imagefiletable_startdate_label : "startdate",	
		imagefiletable_enddate_label : "enddate",	
		imagefiletable_line_label : "line",	
		imagefiletable_set_specific_filter_button_box_name : "optional",	
		imagefiletable_filename_table : "filename",	
		imagefiletable_filedate_table : "filedate",	
		imagefiletable_line_table : "line",	
		imagefiletable_filepath_table : "path",	
		imagefiletable_lotno_table : "lotno",	
		imagefiletable_ng_table : "ng",	
		imagefiletable_ng_by_user_table : "ng_by_user",	
		imagefiletable_ng_by_mes_table : "ng_by_mes",	
		imagefiletable_delete_table : "delete",	
		imagefiletable_ng_name : "ng",	
		imagefiletable_ok_name : "ok",	
		imagefiletable_nr_name : "nr",	
		select_box_default_option : "select",	
		filter_setting_button : "Setting",	
		imagefiletree_filename_name : "filename",	
		imagefiletree_filedate_name : "filedate",	
		imagefiletree_line_name : "linename",	
		imagefiletree_filepath_name : "path",	
		imagefiletree_lotno_name : "lotno",	
		imagefiletree_ng_name : "ng",	
		imagefiletree_ng_by_user_name : "ng_by_user",	
		imagefiletree_ng_by_mes_name : "ng_by_mes",	
		imagefiletree_info_popup_message : "(Click image to update)",	
		imagelabeling_perpage_name : "num",	
		imagelabeling_three_perpage_name : "3",	
		imagelabeling_five_perpage_name : "5",	
		imagelabeling_ten_perpage_name : "10",	
		modeltraining_get_image_data_button_name : "Get Images",	
		modeltraining_table_file_available : "Available",	
		modeltraining_table_model_name : "model name",	
		modeltraining_table_algorithm_name : "Algorithm",	
		modeltraining_table_filter_name : "Filter",	
		modeltraining_table_model_version : "Version",	
		modeltraining_start_training_button : "Start Training",	
		modeltraining_image_count_name : "num of images",	
		modelstatus_status_name : "status",	
		modelstatus_algorithm_name : "algorithm",	
		modelstatus_filter_name : "filter",	
		modelstatus_regdate_name : "regdate",	
		modelstatus_user_name : "user",	
		modelstatus_model_name : "model_name",	
		modelstatus_modelcode_name : "model_code",	
		modelstatus_accuracy_name : "accuracy",	
		modelstatus_contents_name : "contents",	
		user_dept_0 : "0 department",	
		user_dept_1 : "1 department",	
		user_dept_2 : "2 department",	
		user_dept_3 : "3 department",	
		user_grade_0 : "0 grade",	
		user_grade_1 : "1 grade",	
		user_grade_2 : "2 grade",	
		user_grade_3 : "3 grade",	
		filter_available_true : "Available",	
		filter_available_false : "Not Available",	
		usertable_userid_name : "userid",	
		usertable_password_name : "password",	
		usertable_username_name : "username",	
		usertable_department_name : "department",	
		usertable_phone_name : "phone",	
		usertable_email_name : "email",	
		usertable_grade_name : "grade",	
		usertable_regdate_name : "regdate",	
		filtertable_filtercode_name : "filtercode",	
		filtertable_filtername_name : "filtername",	
		filtertable_version_name : "version",	
		filtertable_fullpath_name : "fullpath",	
		filtertable_available_name : "available",	
		algorithmtable_algorithmcode_name : "algorithmcode",	
		algorithmtable_algorithmname_name : "algorithmname",	
		algorithmtable_version_name : "version",	
		algorithmtable_fullpath_name : "fullpath",	
		algorithmtable_available_name : "available",	
		linetable_linecode_name : "linecode",	
		linetable_factory_name : "factory",	
		linetable_linename_name : "linename",	
		linetable_ipaddr_name : "ipaddr",	
		linetable_modelcode_name : "modelcode",	
		logtable_kind_name : "kind",	
		logtable_title_name : "title",	
		logtable_contents_name : "contents",	
		table_select_name : "select All",	
		table_selected_item_delete_button : "delete selected items",	
		table_selected_item_add_button : "add item",	
		check_if_deploy_message : "Are you sure to deploy?",	
		daily_chart_name : "last 7 days",	
		hourly_chart_name : "last 24 hours",	
		deploy_button_name : "deploy",	
		deploy_history_button_name : "history",	
		usertable_password_check_name : "check_password",	
		popup_close : "close",	
		password_check_error : "must input same password",	
		training_history_name : "training history",	
	},

	korean : { 
			key : "korean",	
		common_main_title : "SKI 자동 분류 제어 시스템",	
		login_main_title : "SKI 자동 분류 제어 시스템",	
		login_pw_name : "PW",	
		login_id_name : "ID",	
		logout_sign : "로그아웃",	
		login_id_placeholder : "아이디",	
		login_pw_placeholder : "비밀번호",	
		login : "로그인",	
		login_id_empty_message : "아이디를 입력하세요",	
		login_wrong : "아이디나 비밀번호를 확인해주세요",	
		login_pw_empty_message : "비밀번호를 입력하세요",	
		dashGraph_select_line_name : "라인 선택",	
		dashGraph_hourly_data_graph_type_name : "시간별",	
		dashGraph_daily_data_graph_type_name : "날짜별",	
		menu_popup_info : "(나가려면 빈 공간 클릭)",	
		menu_dash_root_name : "대시보드",	
		menu_file_root_name : "파일관리",	
		menu_model_root_name : "모델관리",	
		menu_setting_root_name : "설   정",	
		menu_dashgraph_name : "그래프",	
		menu_dashtable_name : "테이블",	
		menu_filetree_name : "파일트리",	
		menu_filetable_name : "파일테이블",	
		menu_filelabel_name : "파일라벨링",	
		menu_trainmodel_name : "모델학습",	
		menu_traintable_name : "모델종합테이블",	
		menu_deploymodel_name : "모델배포",	
		menu_usersetting_name : "사용자관리",	
		menu_linesetting_name : "라인관리",	
		menu_algorithmsetting_name : "알고리즘관리",	
		menu_filtersetting_name : "필터관리",	
		menu_logmanager_name : "로그관리",	
		menu_enviromentsetting_name : "설정",	
		dashtable_startdate_label_name : "날짜",	
		dashtable_enddate_label_name : "~",	
		dashtable_unit_label_name : "구간",	
		dashtable_unit_hourly_option_name : "시간별",	
		dashtable_unit_daily_option_name : "일별",	
		dashtable_unit_weekly_option_name : "주별",	
		dashtable_unit_monthly_option_name : "월별",	
		dashtable_unit_yearly_option_name : "년도별",	
		dashtable_line_label_name : "라인",	
		dashtable_csv_button_name : "csv로 저장",	
		dashtable_search_button_name : "조회",	
		dashtable_line_selector_option_all_name : "모든라인",	
		dashtable_table_line_name : "라인",	
		dashtable_table_term_name : "기간",	
		dashtable_table_total_name : "총계",	
		dashtable_table_ok_name : "정상",	
		dashtable_table_ng_name : "비정상",	
		dashtable_table_nr_name : "판단불가",	
		dashtable_unit_name : "단위",	
		dashtable_line_name : "라인",	
		input_page_num_greater_than_max_alert : "마지막 페이지 번호를 초과하였습니다",	
		input_page_num_is_not_number_alert : "페이지에 숫자를 입력바랍니다",	
		imagefiletable_filename_label : "파일이름",	
		imagefiletable_startdate_label : "날짜",	
		imagefiletable_enddate_label : "~",	
		imagefiletable_line_label : "라인",	
		imagefiletable_set_specific_filter_button_box_name : "고급설정",	
		imagefiletable_filename_table : "파일이름",	
		imagefiletable_filedate_table : "생성일",	
		imagefiletable_line_table : "라인",	
		imagefiletable_filepath_table : "경로",	
		imagefiletable_lotno_table : "lotno",	
		imagefiletable_ng_table : "판정",	
		imagefiletable_ng_by_user_table : "사용자판정",	
		imagefiletable_ng_by_mes_table : "MES판정",	
		imagefiletable_delete_table : "삭제",	
		imagefiletable_ng_name : "불량",	
		imagefiletable_ok_name : "정상",	
		imagefiletable_nr_name : "판단불가",	
		select_box_default_option : "선택",	
		filter_setting_button : "적용",	
		imagefiletree_filename_name : "파일이름",	
		imagefiletree_filedate_name : "생성일",	
		imagefiletree_line_name : "라인",	
		imagefiletree_filepath_name : "경로",	
		imagefiletree_lotno_name : "lotno",	
		imagefiletree_ng_name : "결함여부",	
		imagefiletree_ng_by_user_name : "결함여부(사용자)",	
		imagefiletree_ng_by_mes_name : "결함여부(MES)",	
		imagefiletree_info_popup_message : "(이미지 클릭하여 갱신)",	
		imagelabeling_perpage_name : "개수",	
		imagelabeling_three_perpage_name : "3개",	
		imagelabeling_five_perpage_name : "5개",	
		imagelabeling_ten_perpage_name : "10개",	
		modeltraining_get_image_data_button_name : "이미지 불러오기",	
		modeltraining_table_file_available : "추가",	
		modeltraining_table_model_name : "모델이름",	
		modeltraining_table_algorithm_name : "알고리즘",	
		modeltraining_table_filter_name : "필터",	
		modeltraining_table_model_version : "버전",	
		modeltraining_start_training_button : "학습시작",	
		modeltraining_image_count_name : "이미지 개수",	
		modelstatus_status_name : "상태",	
		modelstatus_algorithm_name : "알고리즘",	
		modelstatus_filter_name : "필터",	
		modelstatus_regdate_name : "등록일",	
		modelstatus_user_name : "사용자",	
		modelstatus_model_name : "모델이름",	
		modelstatus_modelcode_name : "모델코드",	
		modelstatus_accuracy_name : "정확도",	
		modelstatus_contents_name : "내용",	
		user_dept_0 : "0부서",	
		user_dept_1 : "1부서",	
		user_dept_2 : "2부서",	
		user_dept_3 : "3부서",	
		user_grade_0 : "0등급",	
		user_grade_1 : "1등급",	
		user_grade_2 : "2등급",	
		user_grade_3 : "3등급",	
		filter_available_true : "활성화",	
		filter_available_false : "비활성화",	
		usertable_userid_name : "아이디",	
		usertable_password_name : "비밀번호",	
		usertable_username_name : "이름",	
		usertable_department_name : "부서",	
		usertable_phone_name : "전화번호",	
		usertable_email_name : "이메일",	
		usertable_grade_name : "직급",	
		usertable_regdate_name : "등록일",	
		filtertable_filtercode_name : "필터코드",	
		filtertable_filtername_name : "이름",	
		filtertable_version_name : "버전",	
		filtertable_fullpath_name : "경로",	
		filtertable_available_name : "사용가능여부",	
		algorithmtable_algorithmcode_name : "알고리즘코드",	
		algorithmtable_algorithmname_name : "이름",	
		algorithmtable_version_name : "버전",	
		algorithmtable_fullpath_name : "경로",	
		algorithmtable_available_name : "사용가능여부",	
		linetable_linecode_name : "라인코드",	
		linetable_factory_name : "공장",	
		linetable_linename_name : "라인이름",	
		linetable_ipaddr_name : "ip주소",	
		linetable_modelcode_name : "배포모델코드",	
		logtable_kind_name : "분류",	
		logtable_title_name : "제목",	
		logtable_contents_name : "내용",	
		table_select_name : "모두선택",	
		table_selected_item_delete_button : "선택항목 삭제",	
		table_selected_item_add_button : "등록",	
		check_if_deploy_message : "배포하시겠습니까?",	
		daily_chart_name : "지난7일",	
		hourly_chart_name : "지난 24시간",	
		deploy_button_name : "배포",	
		deploy_history_button_name : "지난기록",	
		usertable_password_check_name : "비밀번호확인",	
		popup_close : "닫기",	
		password_check_error : "비밀번호가 다릅니다",	
		training_history_name : "학습이력",	
	},

	chinese : { 
			key : "chinese",	
		common_main_title : "SKI自?分?控制系?",	
		login_main_title : "SKI自?分?控制系?",	
		login_pw_name : "PW",	
		login_id_name : "ID",	
		logout_sign : "登出",	
		login_id_placeholder : "?入?的ID",	
		login_pw_placeholder : "?入?的密?",	
		login : "登?",	
		login_id_empty_message : "??入ID",	
		login_wrong : "unknown",	
		login_pw_empty_message : "??入密?",	
		dashGraph_select_line_name : "??工??",	
		dashGraph_hourly_data_graph_type_name : "每小?",	
		dashGraph_daily_data_graph_type_name : "每日子",	
		menu_popup_info : "想不玩了的? 点??幕",	
		menu_dash_root_name : "部件板",	
		menu_file_root_name : "文件管理",	
		menu_model_root_name : "模特?理",	
		menu_setting_root_name : "?置",	
		menu_dashgraph_name : "?表",	
		menu_dashtable_name : "?",	
		menu_filetree_name : "文件?",	
		menu_filetable_name : "文件表",	
		menu_filelabel_name : "文件??",	
		menu_trainmodel_name : "培?模式",	
		menu_traintable_name : "模特?",	
		menu_deploymodel_name : "部署模型",	
		menu_usersetting_name : "用?管理",	
		menu_linesetting_name : "工??管理",	
		menu_algorithmsetting_name : "算法管理",	
		menu_filtersetting_name : "??器管理",	
		menu_logmanager_name : "??管理",	
		menu_enviromentsetting_name : "?置",	
		dashtable_startdate_label_name : "unknown",	
		dashtable_enddate_label_name : "unknown",	
		dashtable_unit_label_name : "unknown",	
		dashtable_unit_hourly_option_name : "unknown",	
		dashtable_unit_daily_option_name : "unknown",	
		dashtable_unit_weekly_option_name : "unknown",	
		dashtable_unit_monthly_option_name : "unknown",	
		dashtable_unit_yearly_option_name : "unknown",	
		dashtable_line_label_name : "unknown",	
		dashtable_csv_button_name : "unknown",	
		dashtable_search_button_name : "unknown",	
		dashtable_line_selector_option_all_name : "unknown",	
		dashtable_table_line_name : "unknown",	
		dashtable_table_term_name : "unknown",	
		dashtable_table_total_name : "unknown",	
		dashtable_table_ok_name : "unknown",	
		dashtable_table_ng_name : "unknown",	
		dashtable_table_nr_name : "unknown",	
		dashtable_unit_name : "unknown",	
		dashtable_line_name : "unknown",	
		input_page_num_greater_than_max_alert : "unknown",	
		input_page_num_is_not_number_alert : "unknown",	
		imagefiletable_filename_label : "unknown",	
		imagefiletable_startdate_label : "unknown",	
		imagefiletable_enddate_label : "unknown",	
		imagefiletable_line_label : "unknown",	
		imagefiletable_set_specific_filter_button_box_name : "unknown",	
		imagefiletable_filename_table : "unknown",	
		imagefiletable_filedate_table : "unknown",	
		imagefiletable_line_table : "unknown",	
		imagefiletable_filepath_table : "unknown",	
		imagefiletable_lotno_table : "unknown",	
		imagefiletable_ng_table : "unknown",	
		imagefiletable_ng_by_user_table : "unknown",	
		imagefiletable_ng_by_mes_table : "unknown",	
		imagefiletable_delete_table : "unknown",	
		imagefiletable_ng_name : "unknown",	
		imagefiletable_ok_name : "unknown",	
		imagefiletable_nr_name : "unknown",	
		select_box_default_option : "unknown",	
		filter_setting_button : "unknown",	
		imagefiletree_filename_name : "unknown",	
		imagefiletree_filedate_name : "unknown",	
		imagefiletree_line_name : "unknown",	
		imagefiletree_filepath_name : "unknown",	
		imagefiletree_lotno_name : "unknown",	
		imagefiletree_ng_name : "unknown",	
		imagefiletree_ng_by_user_name : "unknown",	
		imagefiletree_ng_by_mes_name : "unknown",	
		imagefiletree_info_popup_message : "unknown",	
		imagelabeling_perpage_name : "unknown",	
		imagelabeling_three_perpage_name : "3",	
		imagelabeling_five_perpage_name : "5",	
		imagelabeling_ten_perpage_name : "10",	
		modeltraining_get_image_data_button_name : "unknown",	
		modeltraining_table_file_available : "unknown",	
		modeltraining_table_model_name : "unknown",	
		modeltraining_table_algorithm_name : "unknown",	
		modeltraining_table_filter_name : "unknown",	
		modeltraining_table_model_version : "unknown",	
		modeltraining_start_training_button : "unknown",	
		modeltraining_image_count_name : "unknown",	
		modelstatus_status_name : "unknown",	
		modelstatus_algorithm_name : "unknown",	
		modelstatus_filter_name : "unknown",	
		modelstatus_regdate_name : "unknown",	
		modelstatus_user_name : "unknown",	
		modelstatus_model_name : "unknown",	
		modelstatus_modelcode_name : "unknown",	
		modelstatus_accuracy_name : "unknown",	
		modelstatus_contents_name : "unknown",	
		user_dept_0 : "unknown",	
		user_dept_1 : "unknown",	
		user_dept_2 : "unknown",	
		user_dept_3 : "unknown",	
		user_grade_0 : "unknown",	
		user_grade_1 : "unknown",	
		user_grade_2 : "unknown",	
		user_grade_3 : "unknown",	
		filter_available_true : "unknown",	
		filter_available_false : "unknown",	
		usertable_userid_name : "unknown",	
		usertable_password_name : "unknown",	
		usertable_username_name : "unknown",	
		usertable_department_name : "unknown",	
		usertable_phone_name : "unknown",	
		usertable_email_name : "unknown",	
		usertable_grade_name : "unknown",	
		usertable_regdate_name : "unknown",	
		filtertable_filtercode_name : "unknown",	
		filtertable_filtername_name : "unknown",	
		filtertable_version_name : "unknown",	
		filtertable_fullpath_name : "unknown",	
		filtertable_available_name : "unknown",	
		algorithmtable_algorithmcode_name : "unknown",	
		algorithmtable_algorithmname_name : "unknown",	
		algorithmtable_version_name : "unknown",	
		algorithmtable_fullpath_name : "unknown",	
		algorithmtable_available_name : "unknown",	
		linetable_linecode_name : "unknown",	
		linetable_factory_name : "unknown",	
		linetable_linename_name : "unknown",	
		linetable_ipaddr_name : "unknown",	
		linetable_modelcode_name : "unknown",	
		logtable_kind_name : "unknown",	
		logtable_title_name : "unknown",	
		logtable_contents_name : "unknown",	
		table_select_name : "unknown",	
		table_selected_item_delete_button : "unknown",	
		table_selected_item_add_button : "unknown",	
		check_if_deploy_message : "unknown",	
		daily_chart_name : "unknown",	
		hourly_chart_name : "unknown",	
		deploy_button_name : "unknown",	
		deploy_history_button_name : "unknown",	
		usertable_password_check_name : "unknown",	
		popup_close : "unknown",	
		password_check_error : "unknown",	
		training_history_name : "unknown",	
	},


}