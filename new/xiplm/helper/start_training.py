# -*-coding: utf-8 -*-

import json
import csv
import requests as rq
import subprocess
from time import gmtime, strftime


from ..helper.messages import *
from ..configs.config import *
from ..helper.makecsv import make_tmp_csv,make_input_csv
from ..helper.get_img import get_img
from ..helper.get_algorithm import get_algorithm
from ..helper.get_filter import get_filter

import traceback


def start_training(request_data):

    f = None
    try:
        items = request_data.get('items')
        options = request_data.get('options')

        algorithmcode = items[0].get("algorithmcode")
        filtercode = items[0].get("filtercode")

        status = 1

        input = items[0]
        modelcode = input.get("modelcode")

        os.makedirs(TRAINING_LOG_DIR % (modelcode,""))
        f = open((TRAINING_LOG_DIR % (modelcode,"training_log.txt")), 'w')

        f.write("Start Training...\n")
        f.flush()
        rq.post(REST_SERVER_URL+'/rest/trn',json={'items': [{'modelcode':modelcode,'modelname':input.get("modelname"), "status":status,"accuracy":0,"filter":input.get("filtercode"),"algorithm":input.get("algorithmcode"),"user":input.get("userid"),"contents":"start_training"}],'method_type': 'insert','options': []})

        img_data_index=0
        print(len(items[0].get("csvrowlist")))
        img_data_length = len(items[0].get("csvrowlist"))
        progress=0
        f.write("Start to make tmp csv file...\n")
        f.flush()
        make_csv_result = make_tmp_csv(modelcode,items[0])

        os.makedirs(IMG_DOWNLOAD_DIR % (modelcode,""))

        f.write("Download Input Images from paths...\n")
        f.flush()
        with open(TMP_CSV_DIR % (modelcode) +"tmpCsv.csv", 'r') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                img_data_index+=1
                progress = (img_data_index/img_data_length)*100
                progress = round(progress,2)
                get_img_result = get_img(modelcode,row['filepath'],row['filename'],progress)
                if get_img_result.get('return_code') != '0000':

                    return get_img_result

            csvfile.close()

        f.write("Download Input Images done!\n")
        f.flush()
        rq.post(REST_SERVER_URL+'/rest/trn',json={'items': [{"contents":"input image download done"}],
                           'method_type': 'update',
                           'options': [{"modelcode":modelcode}]})

        f.write("Make CSV file...\n")
        f.flush()
        make_input_csv(modelcode,items[0])

        os.makedirs(ALR_DOWNLOAD_DIR % (modelcode))

        f.write("Download selected Algorithm file...\n")
        f.flush()
        get_algorithm(modelcode,algorithmcode)

        f.write("Download selected Filter file...\n")
        f.flush()
        os.makedirs(FIT_DOWNLOAD_DIR % (modelcode))

        get_filter(modelcode,filtercode)

        os.makedirs(MDL_DIR % (modelcode))



        f.write("Start filtering images...\n")
        f.flush()
        rq.post(REST_SERVER_URL+'/rest/trn',json={'items': [{"contents":"start filtering images"}],
                   'method_type': 'update',
                   'options': [{"modelcode":modelcode}]})


        filter_cmd_str = "python3 " + FIT_DOWNLOAD_DIR % (modelcode) + 'filter.py {\\\"Setting\\\":{\\\"FilteredImagePath\\\":\\\"%s\\\"}\\,\\\"Data\\\":{\\\"CSVFullPath\\\":[\\\"%s\\\"]}}' % (FIT_IMAGE_DIR_FOR_JSON % modelcode, INPUT_CSV_DIR_FOR_JSON % modelcode )

        print(filter_cmd_str)
        # os.system(filter_cmd_str)
        #p = subprocess.Popen(filter_cmd_str, stdout=subprocess.PIPE, bufsize=1, shell=True)

        #for line in iter(p.stdout.readline, b''):
         #   f.write(str(line)+"\n")
          #  f.flush()
           # rq.post(REST_SERVER_URL+'/rest/trn',json={'items': [{"contents":str(line)}],
            #           'method_type': 'update',
             #          'options': [{"modelcode":modelcode}]})
        #p.stdout.close()
        #p.wait()


        rq.post(REST_SERVER_URL+'/rest/trn',json={'items': [{"contents":"filtering images done"}],
                   'method_type': 'update',
                   'options': [{"modelcode":modelcode}]})


        f.write("Start Training ...\n")
        f.flush()
        algorithm_cmd_str = "python " + ALR_DOWNLOAD_DIR % (modelcode) + 'algorithm.py {\\\"Setting\\\":{\\\"FilterFullPath\\\":\\\"%s\\\",\\\"ModelFullPath\\\":\\\"%s\\\"}\\,\\\"Data\\\":{\\\"CSVFullPath\\\":[\\\"%s\\\"]}}' % (FIT_DOWNLOAD_DIR_FOR_JSON % modelcode, MDL_DIR_FOR_JSON % (modelcode, input.get("modelname")), INPUT_CSV_DIR_FOR_JSON % modelcode )

        print(algorithm_cmd_str)
        # os.system(algorithm_cmd_str)
        p = subprocess.Popen(algorithm_cmd_str, stdout=subprocess.PIPE, bufsize=1, shell=True)
        for line in iter(p.stdout.readline, b''):
            f.write(str(line)+"\n")
            f.flush()
            rq.post(REST_SERVER_URL+'/rest/trn',json={'items': [{"contents":str(line)}],
                       'method_type': 'update',
                       'options': [{"modelcode":modelcode}]})
        p.stdout.close()
        p.wait()

        f.write("Training Finished Successfully!\n")
        f.flush()
        rq.post(REST_SERVER_URL+'/rest/trn',json={'items': [{"contents":"Training Finished Successfully","status":0}],
                   'method_type': 'update',
                   'options': [{"modelcode":modelcode}]})




        rq.post(REST_SERVER_URL+'/rest/db/modelHandler',json={'items':[{"modelcode":modelcode,"modelname":input.get("modelname"),"version":"0","algorithm":algorithmcode,"filter":filtercode,"fullpath":MDL_DIR % modelcode,"accuracy":"0","available":1}],
                   'method_type': 'insert',
                   'options': []})

        rq.post(REST_SERVER_URL+'/rest/db/log',json={'items': [{"kind":3,"title":"trainning","contents":str(modelcode) +" model trainned by "+str(input.get("userid"))}],
                   'method_type': 'insert',
                   'options': []})

        # ?�습 ?�료 ??모델 table???�록?�기
        f.close()

    except Exception as e:
        print(str(e))
        print(traceback.print_exc())
        f.write("Error Occrurd!!\n")
        f.write(str(e))
        f.flush()
        f.close()
        
        rq.post(REST_SERVER_URL+'/rest/db/log',json={'items': [{"kind":3,"title":"trainning","contents":str(modelcode) +"error occured when model trainning by "+str(input.get("userid"))}],
                   'method_type': 'insert',
                   'options': []})

        rq.post(REST_SERVER_URL+'/rest/trn',json={'items': [{"contents":"Error Occured : "+str(e)}],
                           'method_type': 'update',
                           'options': [{"modelcode":modelcode}]})


        return "what"
