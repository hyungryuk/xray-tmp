from flask import request, send_file
from flask_restful import Resource
from datetime import datetime

from ..model.imgmdl import *
from ..helper.util import converter, delete_image
from ..helper.messages import *
from .. import db
from ..helper.util import get_file_tree, open_image
from ..configs.config import *

class DownloadFileRESTApi(Resource):

    def post(self):
        request_data = request.get_json()
        method_type = request_data.get('method_type')
        items = request_data.get('items')

        if method_type is not None:
            if method_type == 'download':
                return download_processing(request_data)
            else:
                pass
        else:
            message = COMMON_METHOD_TYPE_NOT_FOUND
            message.update({'items': request.get_json().get('items')})
            return json.dumps(message, ensure_ascii=True)


def download_processing(request_data):
    try:
        path = request_data.get('path')
        return send_file(path)
    except Exception as e:
        message = DWN_DOWNLOAD_SUCCESS_FAIL
        message.update({'cause': str(e)})
        return json.dumps(message, ensure_ascii=True)
