# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 11:04:06 2018

@author: ryuk

@des :  -모델정보에 대한 REST API를 설정한다.
"""
from flask_restful import Resource
from flask import request
import json
from datetime import datetime
from .. import db
from ..model.dlmmdl import *
from ..helper.messages import *
from ..helper.util import converter


class DLModelInfoRESTApi(Resource):

    def post(self):

        request_data = request.get_json()
        method_type = request_data.get('method_type')
        items = request_data.get('items')

        if method_type is not None:
            if items is not None:
                if method_type == 'view':
                    return view_processing(request_data)
                elif method_type == 'insert':
                    return insert_processing(request_data)
                elif method_type == 'update':
                    return update_processing(request_data)
                elif method_type == 'train':
                    return training_processing(request_data)
                elif method_type == 'count':
                    return count_processing(request_data)
                elif method_type == 'delete':
                    pass
                else:
                    pass
            else:
                message = COMMON_ITEMS_NOT_FOUND
                return json.dumps(message, ensure_ascii=True)
        else:
            message = COMMON_METHOD_TYPE_NOT_FOUND
            message.update({'items': request.get_json().get('items')})
            return json.dumps(message, ensure_ascii=True)

def update_processing(request_data):
    try:
        items = request_data.get('items')
        options = request_data.get('options')
        if items is not None and options is not None:
            if type(items) == list:
                len_val = len(items)
                for i in range (0,len(items)):
                    query_input_element = options[i]
                    query_result = db.session.query(DLModelInfo).filter_by(**query_input_element).all()
                    update_element = items[i]
                    for query_result_element in query_result:
                        for update_element_key in update_element:
                            setattr(query_result_element, update_element_key, update_element[update_element_key])
                db.session.commit()

                message = DLM_UPDATE_SUCCESS
                message.update({'items': items})
                return json.dumps(message, ensure_ascii=True)
        else:
            message = COMMON_ITEMS_NOT_FOUND
            return json.dumps(message, ensure_ascii=True)
    except Exception as e:
        db.session.rollback()
        db.session.remove()
        message = DLM_UPDATE_SUCCESS_FAIL
        message.update({'cause': str(e)})
        message.update({'items': request.get_json()['items']})
        return json.dumps(message, ensure_ascii=True)

def insert_processing(request_data):
    try:
        items = request_data.get('items')

        if type(items) == list:
            for item in items:
                mdl_info = DLModelInfo(item.get('modelcode'),
                                           item.get('modelname'), item.get('version'), item.get('algorithm'),
                                           item.get('filter'), item.get('fullpath'), item.get('accuracy'),item.get('available'))

                create_mdl(mdl_info)

            db.session.commit()

            message = DLM_CREATE_SUCCESS
            message.update({'items': items})
            return json.dumps(message, ensure_ascii=True)

    except Exception as e:
        db.session.rollback()
        db.session.remove()
        message = DLM_CREATE_SUCCESS_FAIL
        message.update({'cause': str(e)})
        message.update({'items': request.get_json()['items']})
        print(message)
        return json.dumps(message, ensure_ascii=True)


def training_processing(request_data):
    try:
        items = request_data.get('items')
        options = request_data.get('options')


        return json.dumps({'items': [item.__dict__ for item in result],
                           'return_code': DLM_TRAIN_SUCCESS.get('return_code'),
                           'message': DLM_TRAIN_SUCCESS.get('message')}, default=converter)

    except Exception as e:
        message = DLM_TRAIN_SUCCESS_FAIL
        message.update({'cause': str(e)})
        message.update({'items': request_data['items']})
        return json.dumps(message, ensure_ascii=True)


def view_processing(request_data):
    try:
        items = request_data.get('items')
        options = request_data.get('options')

        if len(options) > 0:
            filter_params = make_filter_params(options[0])
            if len(options) == 1:
                result = db.session.query(DLModelInfo).filter_by(**filter_params).all()
            elif len(options) == 2:
                start_date = options[1].get('startdate')
                end_date = options[1].get('enddate')
                if not start_date is None and not end_date is None:
                    start_date = datetime.strptime(start_date, "%Y%m%d%H%M%S")
                    end_date = datetime.strptime(end_date, "%Y%m%d%H%M%S")
                    result = db.session.query(DLModelInfo).filter_by(**filter_params).filter(
                        DLModelInfo.regdate >= start_date, DLModelInfo.regdate <= end_date).all()
            elif len(options) == 3:
                orderby = options[2].get('orderby')
                if not orderby is None:
                    start_date = options[1].get('startdate')
                    end_date = options[1].get('enddate')
                    if not start_date is None and not end_date is None:
                        start_date = datetime.strptime(start_date, "%Y%m%d%H%M%S")
                        end_date = datetime.strptime(end_date, "%Y%m%d%H%M%S")
                        result = db.session.query(DLModelInfo).filter_by(**filter_params).filter(
                            DLModelInfo.regdate >= start_date, DLModelInfo.regdate <= end_date).order_by(
                            orderby).all()
                    else:
                        result = db.session.query(DLModelInfo).filter_by(**filter_params).order_by(orderby).all()
            elif len(options) == 4:
                page = options[3].get('page')
                per_page = options[3].get('perpage')
                orderby = options[2].get('orderby')
                start_date = options[1].get('startdate')
                end_date = options[1].get('enddate')
                if not orderby is None:
                    if not start_date is None and not end_date is None:
                        start_date = datetime.strptime(start_date, "%Y%m%d%H%M%S")
                        end_date = datetime.strptime(end_date, "%Y%m%d%H%M%S")
                        result = db.session.query(DLModelInfo).filter_by(**filter_params).filter(
                            DLModelInfo.regdate >= start_date, DLModelInfo.regdate <= end_date).order_by(
                            orderby).paginate(int(page), int(per_page))
                    else:
                        result = db.session.query(DLModelInfo).filter_by(**filter_params).order_by(orderby).paginate(
                            int(page), int(per_page))
                else:
                    if not start_date is None and not end_date is None:
                        start_date = datetime.strptime(start_date, "%Y%m%d%H%M%S")
                        end_date = datetime.strptime(end_date, "%Y%m%d%H%M%S")
                        result = db.session.query(DLModelInfo).filter_by(**filter_params).filter(
                            DLModelInfo.regdate >= start_date, DLModelInfo.regdate <= end_date).paginate(
                            int(page), int(per_page))
                    else:
                        result = db.session.query(DLModelInfo).filter_by(**filter_params).paginate(int(page),
                                                                                                   int(per_page))

            else:
                pass

            if len(options) == 4:
                return json.dumps({'items': [item.__dict__ for item in result.items], 'total': result.total,
                                   'return_code': DLM_VIEW_SUCCESS.get('return_code'),
                                   'message': DLM_VIEW_SUCCESS.get('message')},ensure_ascii=True, default=converter)
            else:
                return json.dumps({'items': [item.__dict__ for item in result],
                                   'return_code': DLM_VIEW_SUCCESS.get('return_code'),
                                   'message': DLM_VIEW_SUCCESS.get('message')},ensure_ascii=True, default=converter)
        else:
            result = db.session.query(DLModelInfo).all()
            return json.dumps({'items': [item.__dict__ for item in result],
                               'return_code': DLM_VIEW_SUCCESS.get('return_code'),
                               'message': DLM_VIEW_SUCCESS.get('message')},ensure_ascii=True, default=converter)

    except Exception as e:
        message = DLM_VIEW_SUCCESS_FAIL
        message.update({'cause': str(e)})
        message.update({'items': request_data['items']})
        return json.dumps(message, ensure_ascii=True)


def count_processing(request_data):
    try:
        options = request_data.get('options')

        option_element = options[0]

        result = db.session.query(DLModelInfo).filter_by(**option_element).count()

        message = DLM_COUNT_SUCCESS
        result_dic = {'count': result}
        message.update({'items': [result_dic]})
        return json.dumps(message, ensure_ascii=True)

    except Exception as e:
        message = DLM_COUNT_SUCCESS_FAIL
        message.update({'cause': str(e)})
        message.update({'items': request_data['items']})
        return json.dumps(message, ensure_ascii=True)


def make_filter_params(item):
    filter_params = {}
    if not item.get('modelcode') is None:
        filter_params['modelcode'] = item.get('modelcode')

    if not item.get('regdate') is None:
        filter_params['regdate'] = str(datetime.strptime(item.get('regdate'), "%Y%m%d%H%M%S"))

    if not item.get('modelname') is None:
        filter_params['modelname'] = item.get('modelname')

    if not item.get('version') is None:
        filter_params['version'] = item.get('version')

    if not item.get('algorithm') is None:
        filter_params['algorithm'] = item.get('algorithm')

    if not item.get('filter') is None:
        filter_params['filter'] = item.get('filter')

    if not item.get('fullpath') is None:
        filter_params['fullpath'] = item.get('fullpath')

    if not item.get('accuracy') is None:
        filter_params['accuracy'] = item.get('accuracy')

    if not item.get('available') is None:
        filter_params['available'] = item.get('available')

    return filter_params
