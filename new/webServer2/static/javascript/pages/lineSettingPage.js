"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const ReactDOM = require("react-dom");
const header_1 = require("../modulesForPages/header");
const inversify_config_1 = require("../DIContainerConf/inversify.config");
const types_1 = require("../DIContainerConf/types");
const dbRestcall_1 = require("../others/restCall/dbRestcall");
const encoder_1 = require("../others/encoder/encoder");
const properties_1 = require("../properties/properties");
var selectedList = [];
ReactDOM.render(React.createElement(header_1.HeaderComponent, null), document.getElementById("header"));
class ModelCodeBoxInPopup extends React.Component {
    constructor(props) {
        super(props);
        this.initDate = this.initDate.bind(this);
    }
    initDate() {
        $("#" + this.props.id).val(this.props.modelcode);
    }
    componentDidMount() {
        this.initDate();
    }
    render() {
        return React.createElement("input", { type: "text", className: "modelcode_input", id: this.props.id });
    }
}
class FactoryBoxInPopup extends React.Component {
    constructor(props) {
        super(props);
        this.initDate = this.initDate.bind(this);
    }
    initDate() {
        $("#" + this.props.id).val(this.props.factory);
    }
    componentDidMount() {
        this.initDate();
    }
    render() {
        return React.createElement("input", { type: "text", className: "factory_input", id: this.props.id });
    }
}
class IpAddrBoxInPopup extends React.Component {
    constructor(props) {
        super(props);
        this.initDate = this.initDate.bind(this);
    }
    initDate() {
        $("#" + this.props.id).val(this.props.ipaddr);
    }
    componentDidMount() {
        this.initDate();
    }
    render() {
        return React.createElement("input", { type: "text", className: "ipAddr_input", id: this.props.id });
    }
}
class ModelNameAndCodeInTable extends React.Component {
    constructor(props) {
        super(props);
        this.modelcodeToModelnameAndVersion = this.modelcodeToModelnameAndVersion.bind(this);
    }
    modelcodeToModelnameAndVersion() {
        for (var i = 0; i < this.props.modelList.length; i++) {
            if (this.props.modelList[i].linecode == this.props.modelcode) {
                return (this.props.modelList[i].modelname +
                    " " +
                    this.props.modelList[i].version);
            }
        }
    }
    render() {
        return React.createElement("div", null, this.modelcodeToModelnameAndVersion());
    }
}
class CheckBoxInTable extends React.Component {
    constructor(props) {
        super(props);
        this.checkBoxOnChange = this.checkBoxOnChange.bind(this);
    }
    checkBoxOnChange(e) {
        if (e.target.checked == true) {
            selectedList.push(this.props.linecode);
        }
        else {
            var index = selectedList.indexOf(this.props.linecode);
            selectedList.splice(index, 1);
        }
    }
    render() {
        return (React.createElement("input", { type: "checkbox", onChange: this.checkBoxOnChange, className: "checkbox" }));
    }
}
class TableContents extends React.Component {
    constructor(props) {
        super(props);
        this.valueOnclick = this.valueOnclick.bind(this);
        this.valueOnKeyPress = this.valueOnKeyPress.bind(this);
        this.state = {
            isEditState: false,
            value: this.props.value
        };
    }
    valueOnclick() {
        if (this.state.isEditState == false) {
            this.setState(prevState => ({
                isEditState: !prevState.isEditState
            }));
        }
    }
    valueOnKeyPress(e) {
        if (e.key == 'Enter') {
            var lineAjaxCallObj = new dbRestcall_1.LineAjaxCall();
            var optionList = [];
            var option = {};
            var item = {};
            option["linecode"] = this.props.primKey;
            item[this.props.id] = e.target.value;
            lineAjaxCallObj.update([item], [option]);
            this.setState({
                value: e.target.value,
                isEditState: false
            });
        }
        if (e.keyCode === 27) {
            this.setState({
                isEditState: false
            });
        }
    }
    render() {
        if (this.state.isEditState) {
            return (React.createElement("div", { onClick: this.valueOnclick },
                React.createElement("input", { type: "text", defaultValue: this.state.value, onKeyDown: this.valueOnKeyPress })));
        }
        else {
            return (React.createElement("div", { onClick: this.valueOnclick }, this.state.value));
        }
    }
}
class TableBody extends React.Component {
    render() {
        const data = this.props.data.map(item => (React.createElement("tr", null,
            React.createElement("td", null,
                React.createElement(CheckBoxInTable, { linecode: item.linecode })),
            React.createElement("td", null,
                React.createElement(TableContents, { primKey: item.linecode, id: "factory", value: item.factory })),
            React.createElement("td", null,
                React.createElement(TableContents, { primKey: item.linecode, id: "line", value: item.line })),
            React.createElement("td", null,
                React.createElement(TableContents, { primKey: item.linecode, id: "ipaddr", value: item.ipaddr })),
            React.createElement("td", null,
                React.createElement(ModelNameAndCodeInTable, { modelList: this.props.modelList, modelcode: item.modelcode })))));
        return data;
    }
}
class TableHeaderAndBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isSelectallClicked: false
        };
        this.selectAllOnClick = this.selectAllOnClick.bind(this);
    }
    selectAllOnClick() {
        var checkboxs = document.getElementsByClassName("checkbox");
        if (this.state.isSelectallClicked == false) {
            for (var i = 0; i < this.props.data.length; i++) {
                selectedList.push(this.props.data[i].linecode);
            }
            for (var i = 0; i < checkboxs.length; i++) {
                checkboxs[i].checked = true;
            }
        }
        else {
            selectedList = [];
            for (var i = 0; i < checkboxs.length; i++) {
                checkboxs[i].checked = false;
            }
        }
        this.setState(prevState => ({
            isSelectallClicked: !prevState.isSelectallClicked
        }));
    }
    render() {
        return (React.createElement("div", { className: "table_box" },
            React.createElement("table", { className: "table" },
                React.createElement("thead", { className: "thead" },
                    React.createElement("tr", null,
                        React.createElement("th", { onClick: this.selectAllOnClick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("table_select_name")),
                        React.createElement("th", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("linetable_factory_name")),
                        React.createElement("th", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("linetable_linename_name")),
                        React.createElement("th", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("linetable_ipaddr_name")),
                        React.createElement("th", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("linetable_modelcode_name")))),
                React.createElement("tbody", { className: "tbody" },
                    React.createElement(TableBody, { data: this.props.data, modelList: this.props.modelList })))));
    }
}
class Addpopup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            close: true
        };
        this.closeOnClick = this.closeOnClick.bind(this);
    }
    addOnClick() {
        var passwords = $('input[type="password"]');
        var inputs = $(".addpopupBox :input");
        var item = {};
        for (var i = 0; i < inputs.length; i++) {
            item[inputs[i].getAttribute("name")] = inputs[i].value;
        }
        item["linecode"] = parseInt(Date.now().toString().substring(5, 13));
        var lineAjaxCallObj = new dbRestcall_1.LineAjaxCall();
        var lineAjaxResult = lineAjaxCallObj.create([item], []);
        if (lineAjaxResult["return_code"] == "0000") {
            alert("success");
        }
        else {
            alert(lineAjaxResult["cause"]);
        }
        location.reload();
    }
    closeOnClick() {
        this.setState(prevState => ({
            close: !prevState.close
        }));
    }
    render() {
        if (this.props.open != this.state.close) {
            return (React.createElement("div", null));
        }
        else {
            return (React.createElement("div", { className: "addpopupBox" },
                React.createElement("div", { className: "addpopup_inside_Box" },
                    React.createElement("label", null,
                        inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("linetable_factory_name"),
                        React.createElement("input", { type: "text", name: "factory", placeholder: "factory" })),
                    React.createElement("label", null,
                        inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("linetable_linename_name"),
                        React.createElement("input", { type: "text", name: "line", placeholder: "linename" })),
                    React.createElement("label", null,
                        inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("linetable_ipaddr_name"),
                        React.createElement("input", { type: "text", name: "ipaddr", placeholder: "ipaddr" })),
                    React.createElement("div", { className: "add_popup_button_group" },
                        React.createElement("button", { onClick: this.addOnClick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("table_selected_item_add_button")),
                        React.createElement("button", { onClick: this.closeOnClick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("popup_close"))))));
        }
    }
}
class SearchBoxAndTable extends React.Component {
    constructor(props) {
        super(props);
        var modelDataAjaxCall = new dbRestcall_1.ModelDataAjaxCall();
        var modelList = modelDataAjaxCall.retrieve([], [])["items"];
        this.state = {
            startDate: "",
            endDate: "",
            linecode: "",
            line: "",
            factory: "",
            ipaddr: "",
            modelcode: "",
            modelList: modelList,
            currentPage: 1,
            maxPage: -1,
            perpage: 10,
            isfilterOpen: false,
            resultData: [],
            isAddClicked: false
        };
        this.getDataUsingRestCallFunc = this.getDataUsingRestCallFunc.bind(this);
        this.startDateOnChange = this.startDateOnChange.bind(this);
        this.endDateOnChange = this.endDateOnChange.bind(this);
        this.linecodeOnChange = this.linecodeOnChange.bind(this);
        this.searchOnclick = this.searchOnclick.bind(this);
        this.filterOnclick = this.filterOnclick.bind(this);
        this.filterCloseOnclick = this.filterCloseOnclick.bind(this);
        this.setFilterOnclick = this.setFilterOnclick.bind(this);
        this.addOnClick = this.addOnClick.bind(this);
        this.firstPageOnclick = this.firstPageOnclick.bind(this);
        this.lastPageOnclick = this.lastPageOnclick.bind(this);
        this.prePageOnclick = this.prePageOnclick.bind(this);
        this.nextPageOnclick = this.nextPageOnclick.bind(this);
        this.currentPageOnKeyPress = this.currentPageOnKeyPress.bind(this);
    }
    setFilterOnclick() {
        this.setState({ factory: $("#factory").val() });
        this.setState({ ipaddr: $("#ipaddr").val() });
        this.setState({ modelcode: $("#modelcode").val() });
        this.setState(prevState => ({
            isfilterOpen: !prevState.isfilterOpen
        }));
    }
    filterCloseOnclick() {
        this.setState(prevState => ({
            isfilterOpen: !prevState.isfilterOpen
        }));
    }
    filterOnclick() {
        this.setState(prevState => ({
            isfilterOpen: !prevState.isfilterOpen
        }));
    }
    getDataUsingRestCallFunc(page) {
        var lineDataResult;
        var option1 = {};
        var option2 = {};
        if (this.state.linecode != "") {
            option1["linecode"] = this.state.linecode;
        }
        if (this.state.factory != "") {
            option1["factory"] = this.state.factory;
        }
        if (this.state.line != "") {
            option1["line"] = this.state.line;
        }
        if (this.state.ipaddr != "") {
            option1["ipaddr"] = this.state.ipaddr;
        }
        if (this.state.modelcode != "") {
            option1["modelcode"] = this.state.modelcode;
        }
        if (this.state.startDate != "" && this.state.endDate != "") {
            option2["startdate"] = encoder_1.encoder.dateToYYMMDDHHMMSS(this.state.startDate + "000000");
            option2["enddate"] = encoder_1.encoder.dateToYYMMDDHHMMSS(this.state.endDate + "235959");
        }
        var lineAjaxCallObj = new dbRestcall_1.LineAjaxCall();
        lineDataResult = lineAjaxCallObj.retrieve([], [option1, option2, {}, { page: page, perpage: this.state.perpage }]);
        var logDataAjaxCallObj = new dbRestcall_1.LogDataAjaxCall();
        logDataAjaxCallObj.create([properties_1.properties["Line_view_log"]], []);
        return lineDataResult;
    }
    firstPageOnclick() {
        var lineDataResult;
        if (this.state.currentPage != 1) {
            lineDataResult = this.getDataUsingRestCallFunc(1);
            this.setState({
                resultData: lineDataResult["items"],
                currentPage: 1
            });
            $("#current_page").val(1);
        }
    }
    lastPageOnclick() {
        var lineDataResult;
        if (this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage) {
            lineDataResult = this.getDataUsingRestCallFunc(this.state.maxPage);
            this.setState({
                resultData: lineDataResult["items"],
                currentPage: this.state.maxPage
            });
            $("#current_page").val(this.state.maxPage);
        }
    }
    prePageOnclick() {
        var lineDataResult;
        if (this.state.maxPage != -1 && this.state.currentPage > 1) {
            lineDataResult = this.getDataUsingRestCallFunc(this.state.currentPage - 1);
            this.setState({ resultData: lineDataResult["items"] });
            $("#current_page").val(this.state.currentPage - 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage - 1
            }));
        }
    }
    nextPageOnclick() {
        var lineDataResult;
        if (this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage) {
            lineDataResult = this.getDataUsingRestCallFunc(this.state.currentPage + 1);
            this.setState({ resultData: lineDataResult["items"] });
            $("#current_page").val(this.state.currentPage + 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage + 1
            }));
        }
    }
    currentPageOnKeyPress(e) {
        if (e.which == 13) {
            var pageVal = $("#current_page").val();
            var page = parseInt(pageVal);
            if (isNaN(page)) {
                alert(inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_is_not_number_alert"));
                $("#current_page").val(this.state.currentPage);
            }
            else {
                if (page > this.state.maxPage) {
                    alert(inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_greater_than_max_alert"));
                    $("#current_page").val(this.state.currentPage);
                }
                else {
                    if (this.state.maxPage != -1 &&
                        this.state.currentPage != page) {
                        var lineDataResult = this.getDataUsingRestCallFunc(page);
                        this.setState({
                            resultData: lineDataResult["items"],
                            currentPage: page
                        });
                        $("#current_page").val(page);
                    }
                }
            }
        }
    }
    startDateOnChange(e) {
        this.setState({
            startDate: e.target.value
        });
    }
    endDateOnChange(e) {
        this.setState({
            endDate: e.target.value
        });
    }
    linecodeOnChange(e) {
        this.setState({
            linecode: e.target.value
        });
    }
    lineOnChange(e) {
        this.setState({
            line: e.target.value
        });
    }
    deleteOnClick(e) {
        var lineAjaxCallObj = new dbRestcall_1.LineAjaxCall();
        var optionList = [];
        for (var i = 0; i < selectedList.length; i++) {
            optionList.push({ linecode: selectedList[i] });
        }
        lineAjaxCallObj.delete([], optionList);
        location.reload();
    }
    addOnClick() {
        this.setState(prevState => ({
            isAddClicked: !prevState.isAddClicked
        }));
    }
    searchOnclick() {
        var lineDataResult = this.getDataUsingRestCallFunc(1);
        this.setState({ resultData: lineDataResult["items"] });
        var maxpage = Math.ceil(lineDataResult["total"] / this.state.perpage);
        this.setState({ maxPage: maxpage });
        if (lineDataResult["items"].length == 0) {
            $("#current_page").val(0);
            $("#max_page").val(0);
            this.setState({ maxPage: 0, currentPage: 0 });
        }
        else {
            $("#current_page").val(1);
            $("#max_page").val(maxpage);
            this.setState({ maxPage: maxpage, currentPage: 1 });
        }
    }
    componentDidMount() {
        this.searchOnclick();
    }
    render() {
        const popup = (React.createElement("div", { className: "optional_settings_box" },
            React.createElement("div", { className: "popup_close_button" },
                React.createElement("i", { className: "fas fa-window-close", onClick: this.filterCloseOnclick })),
            React.createElement("div", { className: "factory_input_box" },
                React.createElement("label", null,
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("linetable_factory_name"),
                    React.createElement(FactoryBoxInPopup, { id: "factory", factory: this.state.factory }))),
            React.createElement("div", { className: "ipaddr_input_box" },
                React.createElement("label", null,
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("linetable_ipaddr_name"),
                    React.createElement(IpAddrBoxInPopup, { ipaddr: this.state.ipaddr, id: "ipaddr" }))),
            React.createElement("div", { className: "modelcode_input_box" },
                React.createElement("label", null,
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("linetable_modelcode_name"),
                    React.createElement(ModelCodeBoxInPopup, { modelcode: this.state.modelcode, id: "modelcode" }))),
            React.createElement("div", { className: "filter_set_button" },
                React.createElement("button", { className: "set_button", onClick: this.setFilterOnclick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("filter_setting_button")))));
        if (this.state.isfilterOpen) {
            return (React.createElement("div", { className: "main_box" },
                React.createElement("div", { className: "current_page_info" },
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_setting_root_name"),
                    React.createElement("i", { className: "fas fa-angle-right" }),
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_linesetting_name")),
                React.createElement("div", null, popup),
                React.createElement("div", { className: "searchBox" },
                    React.createElement("div", { className: "line_input_box" },
                        React.createElement("label", null,
                            inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("linetable_linename_name"),
                            React.createElement("input", { type: "text", className: "line_input", id: "line", onChange: this.lineOnChange, defaultValue: this.state.line }))),
                    React.createElement("div", { className: "date_input_box" },
                        React.createElement("div", { className: "start_date_input_box" },
                            React.createElement("label", null,
                                inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_startdate_label"),
                                React.createElement("input", { type: "date", className: "start_date_input", onChange: this.startDateOnChange, defaultValue: this.state.startDate }))),
                        React.createElement("div", { className: "end_date_input_box" },
                            React.createElement("label", null,
                                inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_enddate_label"),
                                React.createElement("input", { type: "date", className: "end_date_input", onChange: this.endDateOnChange, defaultValue: this.state.endDate })))),
                    React.createElement("div", { className: "button_box" },
                        React.createElement("div", { className: "set_specific_filter_button_box" },
                            React.createElement("button", { className: "set_specific_filter_button", onClick: this.filterOnclick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_set_specific_filter_button_box_name"))),
                        React.createElement("div", { className: "search_button_box" },
                            React.createElement("button", { className: "searchButton", onClick: this.searchOnclick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_search_button_name"))),
                        React.createElement("div", null,
                            React.createElement("button", { className: "items_delete_button", onClick: this.deleteOnClick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("table_selected_item_delete_button"))),
                        React.createElement("div", null,
                            React.createElement("button", { className: "items_add_button", onClick: this.addOnClick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("table_selected_item_add_button")),
                            React.createElement(Addpopup, { open: this.state.isAddClicked })))),
                React.createElement("div", null,
                    React.createElement(TableHeaderAndBox, { data: this.state.resultData, modelList: this.state.modelList })),
                React.createElement("div", { className: "page_box" },
                    React.createElement("div", { className: "pre_page_box" },
                        React.createElement("i", { className: "fas fa-angle-double-left", onClick: this.firstPageOnclick }),
                        React.createElement("i", { className: "fas fa-angle-left", onClick: this.prePageOnclick })),
                    React.createElement("div", { className: "page_input_box" },
                        React.createElement("input", { type: "text", className: "current_page_input", id: "current_page", onKeyPress: this.currentPageOnKeyPress }),
                        React.createElement("div", { className: "slash" }),
                        React.createElement("input", { type: "text", className: "max_page_input", readOnly: "true", id: "max_page" })),
                    React.createElement("div", { className: "post_page_box" },
                        React.createElement("i", { className: "fas fa-angle-right", onClick: this.nextPageOnclick }),
                        React.createElement("i", { className: "fas fa-angle-double-right", onClick: this.lastPageOnclick })))));
        }
        else {
            return (React.createElement("div", { className: "main_box" },
                React.createElement("div", { className: "current_page_info" },
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_setting_root_name"),
                    React.createElement("i", { className: "fas fa-angle-right" }),
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_linesetting_name")),
                React.createElement("div", { className: "searchBox" },
                    React.createElement("div", { className: "line_input_box" },
                        React.createElement("label", null,
                            inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("linetable_linename_name"),
                            React.createElement("input", { type: "text", className: "line_input", id: "line", onChange: this.lineOnChange, defaultValue: this.state.line }))),
                    React.createElement("div", { className: "date_input_box" },
                        React.createElement("div", { className: "start_date_input_box" },
                            React.createElement("label", null,
                                inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_startdate_label"),
                                React.createElement("input", { type: "date", className: "start_date_input", onChange: this.startDateOnChange, defaultValue: this.state.startDate }))),
                        React.createElement("div", { className: "end_date_input_box" },
                            React.createElement("label", null,
                                inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_enddate_label"),
                                React.createElement("input", { type: "date", className: "end_date_input", onChange: this.endDateOnChange, defaultValue: this.state.endDate })))),
                    React.createElement("div", { className: "button_box" },
                        React.createElement("div", { className: "set_specific_filter_button_box" },
                            React.createElement("button", { className: "set_specific_filter_button", onClick: this.filterOnclick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_set_specific_filter_button_box_name"))),
                        React.createElement("div", { className: "search_button_box" },
                            React.createElement("button", { className: "searchButton", onClick: this.searchOnclick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_search_button_name"))),
                        React.createElement("div", null,
                            React.createElement("button", { className: "items_delete_button", onClick: this.deleteOnClick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("table_selected_item_delete_button"))),
                        React.createElement("div", null,
                            React.createElement("button", { className: "items_add_button", onClick: this.addOnClick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("table_selected_item_add_button")),
                            React.createElement(Addpopup, { open: this.state.isAddClicked })))),
                React.createElement("div", null,
                    React.createElement(TableHeaderAndBox, { data: this.state.resultData, modelList: this.state.modelList })),
                React.createElement("div", { className: "page_box" },
                    React.createElement("div", { className: "pre_page_box" },
                        React.createElement("i", { className: "fas fa-angle-double-left", onClick: this.firstPageOnclick }),
                        React.createElement("i", { className: "fas fa-angle-left", onClick: this.prePageOnclick })),
                    React.createElement("div", { className: "page_input_box" },
                        React.createElement("input", { type: "text", className: "current_page_input", id: "current_page", onKeyPress: this.currentPageOnKeyPress }),
                        React.createElement("div", { className: "slash" }),
                        React.createElement("input", { type: "text", className: "max_page_input", readOnly: "true", id: "max_page" })),
                    React.createElement("div", { className: "post_page_box" },
                        React.createElement("i", { className: "fas fa-angle-right", onClick: this.nextPageOnclick }),
                        React.createElement("i", { className: "fas fa-angle-double-right", onClick: this.lastPageOnclick })))));
        }
    }
}
ReactDOM.render(React.createElement("div", null,
    React.createElement(SearchBoxAndTable, null)), document.getElementById("main"));
