import os

class BaseConfig(object):
    DEBUG = False



class DevelopmentConfig(BaseConfig):
    DEBUG = False



class MacDevelopmentConfig(BaseConfig):
    DEBUG = False


TMP_CSV_DIR = os.getenv('TMP_CSV_DIR', 'C:\\xray\\modeltraining\\%s\\tmpCsv\\')
INPUT_CSV_DIR = os.getenv('INPUT_CSV_DIR', 'C:\\xray\\modeltraining\\%s\\inputCsv\\')
IMG_DOWNLOAD_DIR = os.getenv('IMG_DOWNLOAD_DIR', 'C:\\xray\\modeltraining\\%s\\images\\%s')
TRAINING_LOG_DIR = os.getenv('TRAINING_LOG_DIR', 'C:\\xray\\modeltraining\\%s\\logs\\%s')
ALR_DOWNLOAD_DIR = os.getenv('ALR_DOWNLOAD_DIR', 'C:\\xray\\modeltraining\\%s\\algorithm\\')
FIT_DOWNLOAD_DIR = os.getenv('FIT_DOWNLOAD_DIR', 'C:\\xray\\modeltraining\\%s\\filter\\')
FIT_IMAGE_DIR = os.getenv('FIT_IMAGE_DIR', 'C:\\xray\\modeltraining\\%s\\filteredImage\\')
MDL_DIR = os.getenv('MDL_DIR', 'C:\\xray\\modeltraining\\%s\\model\\')
MDL_PUB_DIR = os.getenv('MDL_PUB_DIR', 'C:\\xray\\lines\\%s\\model\\')


FIT_IMAGE_DIR_FOR_JSON = os.getenv('FIT_IMAGE_DIR_FOR_JSON', 'C:\\\\\\\\xray\\\\\\\\modeltraining\\\\\\\\%s\\\\\\\\filteredImage\\\\\\\\')
INPUT_CSV_DIR_FOR_JSON = os.getenv('INPUT_CSV_DIR_FOR_JSON', 'C:\\\\\\\\xray\\\\\\\\modeltraining\\\\\\\\%s\\\\\\\\inputCsv\\\\\\\\inputCsv.csv')
FIT_DOWNLOAD_DIR_FOR_JSON = os.getenv('FIT_DOWNLOAD_DIR_FOR_JSON', 'C:\\\\\\\\xray\\\\\\\\modeltraining\\\\\\\\%s\\\\\\\\filter\\\\\\\\filter.py')
REST_SERVER_URL = os.getenv('REST_SERVER_URL', 'http://localhost:5000')
MDL_DIR_FOR_JSON = os.getenv('MDL_DIR_FOR_JSON', 'C:\\\\\\\\xray\\\\\\\\modeltraining\\\\\\\\%s\\\\\\\\model\\\\\\\\%s')


config = {
    "development": "xiplm.configs.config.DevelopmentConfig",
    "mac_development": "xiplm.configs.config.MacDevelopmentConfig",
    "product": "xiplm.configs.config.BaseConfig",
}

def configure_app(app):
    config_name = os.getenv('FLASK_CONFIGURATION', 'development')
    app.config.from_object(config[config_name])
