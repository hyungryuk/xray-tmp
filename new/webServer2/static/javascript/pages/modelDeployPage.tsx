import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { HeaderComponent } from "../modulesForPages/header";
import {StaticLanguageInterface} from '../others/staticLanguageData/staticLanguageData'
import {myContainer} from '../DIContainerConf/inversify.config'
import {TYPES} from '../DIContainerConf/types'

import { LineAjaxCall,DeployDataAjaxCall,FilterDataAjaxCall,ModelDataAjaxCall  } from '../others/restCall/dbRestcall'
import { DeployModelRestCall } from '../others/restCall/othersRestCall'
import {encoder} from "../others/encoder/encoder";

ReactDOM.render(
  <HeaderComponent />,
  document.getElementById("header")
);

class ModelNameInTable extends React.Component<{linecode:string, modelcode:any,modeldata:any[]},{}> {
    constructor(props:any) {
        super(props);
        this.modelOnChange = this.modelOnChange.bind(this);
    }
    modelOnChange(){
        //selectedModel:$("#"+this.props.linecode+" option:selected").val() as string
        // if(confirm(myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("check_if_deploy_message"))==true){
        //     var deployModelRestCallObj = new DeployModelRestCall();
        //     var result = deployModelRestCallObj.deployModel([],[{modelcode:selectedModel,linecode:this.props.linecode}]);
        //     console.log(result);
        // }else{
        //
        // }
    }
    componentDidMount(){
        $("#"+this.props.linecode).val(this.props.modelcode+"");
    }

    render() {
        const selectItems = this.props.modeldata.map((model) =>
           <option value={model.modelcode}>
             {model.modelname}
         </option>
         );

        return(
            <div>
                <select id={this.props.linecode} onChange={this.modelOnChange}>
                    {selectItems}
                </select>
            </div>
        );

    }
}
class FilterNameInTable extends React.Component<{filtercode:any,filterdata:any[]},{}> {
    render() {
        for (var i=0;i<this.props.filterdata.length;i++){
            if(this.props.filterdata[i].filtercode == this.props.filtercode){
                return(
                    <div>
                        {this.props.filterdata[i].filtername}
                    </div>
                );
            }
        }
        return(
            <div></div>
        );

    }
}
class LineNameInTable extends React.Component<{linecode:any,linedata:any[]},{}> {
    render() {
        for (var i=0;i<this.props.linedata.length;i++){
            if(parseInt(this.props.linedata[i].linecode) == parseInt(this.props.linecode)){
                return(
                    <div>
                        {this.props.linedata[i].factory+" "+this.props.linedata[i].line}
                    </div>
                );
            }
        }
        return(
            <div></div>
        );

    }
}
class DeployButton extends React.Component<{linecode:string,modelSelectorId:string},{}> {
    constructor(props:any){
        super(props);
        this.deployOnClick = this.deployOnClick.bind(this);
    }
    deployOnClick(){
        var selectedModel = $("#"+this.props.modelSelectorId+" option:selected").val() as string
        if(confirm(myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("check_if_deploy_message"))==true){
            var deployModelRestCallObj = new DeployModelRestCall();
            var result = deployModelRestCallObj.deployModel([],[{modelcode:selectedModel,linecode:this.props.linecode,userid:"haha"}]);
            if(result["resturn_code"]=="0000"){
                alert("complete");
            }else{
                alert(result["cause"]);
            }
        }else{

        }
    }
    render() {
        return(
            <button onClick={this.deployOnClick}>{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("deploy_button_name")}</button>
        );
    }
}
class Historypopup extends React.Component<{open:boolean, data:any[]},{close:boolean}>{
    constructor(props:any){
        super(props);
        this.state={
            close:true
        }
        this.closeOnClick = this.closeOnClick.bind(this);
    }
    closeOnClick(){
        this.setState(prevState => ({
            close: !prevState.close
        }));
    }

    render(){
        if(this.props.open!=this.state.close){
            return(
                <div>
                </div>
            );
        }
        else{
            var historyList= this.props.data.map((item) =>
               <tr>
                   <td>{item.modelname}</td>
                   <td>{item.accuracy}</td>
                   <td>{item.user}</td>
                   <td>{item.deploydate}</td>
               </tr>
             );
            return(
                <div className="history_popup_box">
                    <div className="history_popup">
                        <div className="history_popup_table_box">
                            <table>
                                <thead>
                                    <th>
                                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_model_name")}
                                    </th>
                                    <th>
                                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_accuracy_name")}
                                    </th>
                                    <th>
                                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_user_name")}
                                    </th>
                                    <th>
                                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_regdate_name")}
                                    </th>
                                </thead>
                                <tbody>
                                    {historyList}
                                </tbody>
                            </table>
                        </div>
                        <div className="history_popup_button_group">
                            <button onClick={this.closeOnClick}>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "popup_close"
                                )}
                            </button>
                        </div>
                    </div>
                </div>
            );
        }
    }
}
class HistoryButton extends React.Component<{linecode:string},{isOpened:boolean,data:any[]}> {
    constructor(props:any){
        super(props);
        this.state={
            data:[],
            isOpened:false
        }
        this.historyOnClick = this.historyOnClick.bind(this);
    }
    historyOnClick(){

        if(this.state.data.length==0){
            var deployDataAjaxCallObj = new DeployDataAjaxCall();
            var deployDataRestCallResult = deployDataAjaxCallObj.retrieve(
                [],
                [{linecode:this.props.linecode}]
            );
            this.setState({
                data:deployDataRestCallResult["items"]
            });
        }
        this.setState(prevState => ({
            isOpened: !prevState.isOpened
        }));


    }
    render() {
        return(
            <div>
                <button onClick={this.historyOnClick}>{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("deploy_history_button_name")}</button>
                <Historypopup open={this.state.isOpened} data={this.state.data}/>
            </div>
        );

    }
}
class TableBody extends React.Component<{data:any[],linedata:any[],filterdata:any[],modeldata:any[]},{currentDeployDatas:any[]}> {
    //조회시 라인데이터를 기준으로하는것으로 변경하였다
    //이제 여기서 filtercode나 modelcode등을 어떻게 넘길것인지 해결해야함
    constructor(props:any){
        super(props);
        this.state={
            currentDeployDatas:[]
        };
        this.getDeployColDataWithLineCode = this.getDeployColDataWithLineCode.bind(this);
    }
    componentDidMount(){
        var deployDataAjaxCallObj = new DeployDataAjaxCall();
        var deployDataAjaxRusult = deployDataAjaxCallObj.retrieve([],[{isrunning:1}]);
        this.setState({
            currentDeployDatas:deployDataAjaxRusult["items"]
        });
    }
    getDeployColDataWithLineCode(linecode:string,colName:string){
        for(var i=0;i<this.state.currentDeployDatas.length;i++){
            if(this.state.currentDeployDatas[i]["linecode"]==linecode){
                return this.state.currentDeployDatas[i][colName];
            }
        }
        return "";
    }

    render() {
        const data = this.props.data.map(item => (
            <tr>
                <td><LineNameInTable linecode={item.linecode} linedata={this.props.linedata}/></td>
                <td><FilterNameInTable filtercode={this.getDeployColDataWithLineCode(item.linecode,"filtercode")} filterdata={this.props.filterdata}/></td>
                <td><ModelNameInTable modelcode={this.getDeployColDataWithLineCode(item.linecode,"modelcode")} modeldata={this.props.modeldata} linecode={item.linecode}/></td>
                <td><DeployButton linecode={item.linecode} modelSelectorId={item.linecode} /></td>
                <td><HistoryButton linecode={item.linecode}/></td>
            </tr>
        ));
        return data;
    }
}
class TableHeaderAndBox extends React.Component<{data:any[], linedata:any[], filterdata:any[], modeldata:any[]},{}> {
    render() {
        return (
            <div className="table_box">
                <table className="table">
                    <thead className="thead">
                        <tr>
                            <th>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("linetable_linename_name")}
                            </th>
                            <th>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("modeltraining_table_filter_name")}
                            </th>
                            <th>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("modelstatus_model_name")}
                            </th>
                            <th>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("deploy_button_name")}
                            </th>
                            <th>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("deploy_history_button_name")}
                            </th>
                        </tr>
                    </thead>
                    <tbody className="tbody">
                        <TableBody
                            data={this.props.data} linedata={this.props.linedata} filterdata={this.props.filterdata} modeldata={this.props.modeldata}
                        />
                    </tbody>
                </table>
            </div>
        );
    }
}

class SearchBoxAndTable extends React.Component<{},{modeldata:any[],linedata:any[],filterdata:any[],factory:string,linename:string,resultData:any[],currentPage:number,maxPage:number,perpage:number}> {
    constructor(props:any) {
        super(props);
        this.state = {
            linedata:[],
            factory: "",
            linename: "",
            currentPage: 1,
            maxPage: -1,
            perpage: 10,
            resultData:[],
            filterdata:[],
            modeldata:[]

        };

        this.getDataUsingRestCallFunc = this.getDataUsingRestCallFunc.bind(
            this
        );

        this.factoryOnChange = this.factoryOnChange.bind(this);
        this.linenameOnChange = this.linenameOnChange.bind(this);

        this.searchOnclick = this.searchOnclick.bind(this);

        this.firstPageOnclick = this.firstPageOnclick.bind(this);
        this.lastPageOnclick = this.lastPageOnclick.bind(this);
        this.prePageOnclick = this.prePageOnclick.bind(this);
        this.nextPageOnclick = this.nextPageOnclick.bind(this);
        this.currentPageOnKeyPress = this.currentPageOnKeyPress.bind(this);
    }

    getDataUsingRestCallFunc(page:number) {
        var lineDataRestCallResult;
        var deployDataRestCallResult;
        var filterDataRestCallResult;
        var modelDataRestCallResult;

        var option1:{[key:string]:any} = {};

        if (this.state.factory != "") {
            option1["factory"] = this.state.factory;
        }
        if (this.state.linename != "") {
            option1["line"] = this.state.linename;
        }
        var lineAjaxCallObj = new LineAjaxCall();
        lineDataRestCallResult = lineAjaxCallObj.retrieve(
            [],
            [option1]
        );
        var linecode = lineDataRestCallResult["items"][0]["linecode"];
        this.setState({
            linedata:lineDataRestCallResult["items"]
        });
        lineDataRestCallResult = lineAjaxCallObj.retrieve(
            [],
            [option1,{},{},{ page: page, perpage: this.state.perpage }]
        );

        var filterDataAjaxCallObj = new FilterDataAjaxCall();
        filterDataRestCallResult = filterDataAjaxCallObj.retrieve(
            [],
            []
        );
        this.setState({
            filterdata:filterDataRestCallResult["items"]
        });

        var modelDataAjaxCallObj = new ModelDataAjaxCall();
        modelDataRestCallResult = modelDataAjaxCallObj.retrieve(
            [],
            [{available:1}]
        );
        this.setState({
            modeldata:modelDataRestCallResult["items"]
        });

        return lineDataRestCallResult;

    }

    firstPageOnclick() {
        var deployDataResult;
        if (this.state.currentPage != 1) {
            deployDataResult = this.getDataUsingRestCallFunc(1);
            this.setState({
                resultData: deployDataResult["items"],
                currentPage: 1
            });

            $("#current_page").val(1);
        }
    }

    lastPageOnclick() {
        var deployDataResult;
        if (
            this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage
        ) {
            deployDataResult = this.getDataUsingRestCallFunc(this.state.maxPage);
            this.setState({
                resultData: deployDataResult["items"],
                currentPage: this.state.maxPage
            });

            $("#current_page").val(this.state.maxPage);
        }
    }
    prePageOnclick() {
        var deployDataResult;
        if (this.state.maxPage != -1 && this.state.currentPage > 1) {
            deployDataResult = this.getDataUsingRestCallFunc(
                this.state.currentPage - 1
            );
            this.setState({ resultData: deployDataResult["items"] });
            $("#current_page").val(this.state.currentPage - 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage - 1
            }));
        }
    }
    nextPageOnclick() {
        var deployDataResult;
        if (
            this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage
        ) {
            deployDataResult = this.getDataUsingRestCallFunc(
                this.state.currentPage + 1
            );
            this.setState({ resultData: deployDataResult["items"] });
            $("#current_page").val(this.state.currentPage + 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage + 1
            }));
        }
    }
    currentPageOnKeyPress(e:any) {
        if (e.which == 13) {
            var pageVal = $("#current_page").val() as string;
            var page = parseInt(pageVal);
            if (isNaN(page)) {
                alert(myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_is_not_number_alert"));
                $("#current_page").val(this.state.currentPage);
            } else {
                if (page > this.state.maxPage) {
                    alert(
                        myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_greater_than_max_alert")
                    );
                    $("#current_page").val(this.state.currentPage);
                } else {
                    if (
                        this.state.maxPage != -1 &&
                        this.state.currentPage != page
                    ) {
                        var deployDataResult = this.getDataUsingRestCallFunc(
                            page
                        );
                        this.setState({
                            resultData: deployDataResult["items"],
                            currentPage: page
                        });
                        $("#current_page").val(page);
                    }
                }
            }
        }
    }

    factoryOnChange(e:any) {
        this.setState({
            factory: e.target.value
        });
    }
    linenameOnChange(e:any) {
        this.setState({
            linename: e.target.value
        });
    }
    searchOnclick() {
        var deployDataResult = this.getDataUsingRestCallFunc(1);

        this.setState({ resultData: deployDataResult["items"] });

        var maxpage = Math.ceil(deployDataResult["total"] / this.state.perpage);
        this.setState({ maxPage: maxpage });

        if (deployDataResult["items"].length == 0) {
            $("#current_page").val(0);
            $("#max_page").val(0);
            this.setState({ maxPage: 0, currentPage: 0 });
        } else {
            $("#current_page").val(1);
            $("#max_page").val(maxpage);
            this.setState({ maxPage: maxpage, currentPage: 1 });
        }
    }
    componentDidMount(){
        this.searchOnclick();
    }
    render() {
            return (
                <div className="main_box">
                    <div className="current_page_info">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_model_root_name")}
                        <i className="fas fa-angle-right"></i>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_deploymodel_name")}
                    </div>

                    <div className="searchBox">
                        <div className="userid_input_box">
                            <label>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "linetable_factory_name"
                                )}
                                <input
                                    type="text"
                                    className="userid_input"
                                    id="factory"
                                    onChange={this.factoryOnChange}
                                    defaultValue={this.state.factory}
                                />
                            </label>
                        </div>
                        <div className="username_input_box">
                            <label>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "linetable_linename_name"
                                )}
                                <input
                                    type="text"
                                    className="username_input"
                                    id="username"
                                    onChange={this.linenameOnChange}
                                    defaultValue={this.state.linename}
                                />
                            </label>
                        </div>
                        <div className="button_box">
                            <div className="search_button_box">
                                <button
                                    className="searchButton"
                                    onClick={this.searchOnclick}
                                >
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "dashtable_search_button_name"
                                    )}
                                </button>
                            </div>
                        </div>
                    </div>

                    <div>
                        <TableHeaderAndBox
                            data={this.state.resultData} linedata={this.state.linedata} filterdata={this.state.filterdata} modeldata={this.state.modeldata}
                        />
                    </div>
                    <div className="page_box">
                        <div className="pre_page_box">
                            <i
                                className="fas fa-angle-double-left"
                                onClick={this.firstPageOnclick}
                            />
                            <i
                                className="fas fa-angle-left"
                                onClick={this.prePageOnclick}
                            />
                        </div>
                        <div className="page_input_box">
                            <input
                                type="text"
                                className="current_page_input"
                                id="current_page"
                                onKeyPress={this.currentPageOnKeyPress}
                            />
                            <div className="slash">
                            </div>
                            <input
                                type="text"
                                className="max_page_input"
                                readOnly="true"
                                id="max_page"
                            />
                        </div>
                        <div className="post_page_box">
                            <i
                                className="fas fa-angle-right"
                                onClick={this.nextPageOnclick}
                            />
                            <i
                                className="fas fa-angle-double-right"
                                onClick={this.lastPageOnclick}
                            />
                        </div>
                    </div>
                </div>
            );
        }
}

ReactDOM.render(
    <div>
        <SearchBoxAndTable />
    </div>,
    document.getElementById("main")
);
