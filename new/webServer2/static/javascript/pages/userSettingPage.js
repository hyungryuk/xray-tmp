"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const ReactDOM = require("react-dom");
const header_1 = require("../modulesForPages/header");
const inversify_config_1 = require("../DIContainerConf/inversify.config");
const types_1 = require("../DIContainerConf/types");
const dbRestcall_1 = require("../others/restCall/dbRestcall");
const properties_1 = require("../properties/properties");
var selectedList = [];
ReactDOM.render(React.createElement(header_1.HeaderComponent, null), document.getElementById("header"));
class GradeInPopup extends React.Component {
    constructor(props) {
        super(props);
        this.initDate = this.initDate.bind(this);
    }
    initDate() {
        $("#" + this.props.id).val(this.props.grade);
    }
    componentDidMount() {
        this.initDate();
    }
    render() {
        return (React.createElement("select", { id: "grade" },
            React.createElement("option", { value: "all" }, "All"),
            React.createElement("option", { value: "0" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("user_grade_0")),
            React.createElement("option", { value: "1" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("user_grade_1")),
            React.createElement("option", { value: "2" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("user_grade_2")),
            React.createElement("option", { value: "3" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("user_grade_3"))));
    }
}
class EmailBoxInPopup extends React.Component {
    constructor(props) {
        super(props);
        this.initDate = this.initDate.bind(this);
    }
    initDate() {
        $("#" + this.props.id).val(this.props.email);
    }
    componentDidMount() {
        this.initDate();
    }
    render() {
        return React.createElement("input", { type: "text", className: "email_input", id: this.props.id });
    }
}
class PhoneBoxInPopup extends React.Component {
    constructor(props) {
        super(props);
        this.initDate = this.initDate.bind(this);
    }
    initDate() {
        $("#" + this.props.id).val(this.props.phone);
    }
    componentDidMount() {
        this.initDate();
    }
    render() {
        return React.createElement("input", { type: "text", className: "phone_input", id: this.props.id });
    }
}
class DeptBoxInPopup extends React.Component {
    constructor(props) {
        super(props);
        this.initDate = this.initDate.bind(this);
    }
    initDate() {
        $("#" + this.props.id).val(this.props.department);
    }
    componentDidMount() {
        this.initDate();
    }
    render() {
        return (React.createElement("select", { id: "department" },
            React.createElement("option", { value: "all" }, "All"),
            React.createElement("option", { value: "0" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("user_dept_0")),
            React.createElement("option", { value: "1" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("user_dept_1")),
            React.createElement("option", { value: "2" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("user_dept_2")),
            React.createElement("option", { value: "3" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("user_dept_3"))));
    }
}
class GradeNameInTable extends React.Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
    }
    onChange() {
        var userDataAjaxCallObj = new dbRestcall_1.UserDataAjaxCall();
        userDataAjaxCallObj.update([{ grade: $("#" + this.props.primKey + " option:selected").val() }], [{ userid: this.props.primKey }]);
    }
    componentDidMount() {
        $("#" + this.props.primKey).val(this.props.grade);
    }
    render() {
        return (React.createElement("select", { id: this.props.primKey, onChange: this.onChange },
            React.createElement("option", { value: "0" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("user_grade_0")),
            React.createElement("option", { value: "1" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("user_grade_1")),
            React.createElement("option", { value: "2" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("user_grade_2")),
            React.createElement("option", { value: "3" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("user_grade_3"))));
    }
}
class CheckBoxInTable extends React.Component {
    constructor(props) {
        super(props);
        this.checkBoxOnChange = this.checkBoxOnChange.bind(this);
    }
    checkBoxOnChange(e) {
        if (e.target.checked == true) {
            selectedList.push(this.props.userid);
        }
        else {
            var index = selectedList.indexOf(this.props.userid);
            selectedList.splice(index, 1);
        }
    }
    render() {
        return (React.createElement("input", { type: "checkbox", onChange: this.checkBoxOnChange, className: "checkbox" }));
    }
}
class TableContents extends React.Component {
    constructor(props) {
        super(props);
        this.valueOnclick = this.valueOnclick.bind(this);
        this.valueOnKeyPress = this.valueOnKeyPress.bind(this);
        this.state = {
            isEditState: false,
            value: this.props.value
        };
    }
    valueOnclick() {
        if (this.state.isEditState == false) {
            this.setState(prevState => ({
                isEditState: !prevState.isEditState
            }));
        }
    }
    valueOnKeyPress(e) {
        if (e.key == 'Enter') {
            var userDataAjaxCallObj = new dbRestcall_1.UserDataAjaxCall();
            var optionList = [];
            var option = {};
            var item = {};
            option["userid"] = this.props.primKey;
            item[this.props.id] = e.target.value;
            userDataAjaxCallObj.update([item], [option]);
            this.setState({
                value: e.target.value,
                isEditState: false
            });
        }
        if (e.keyCode === 27) {
            this.setState({
                isEditState: false
            });
        }
    }
    render() {
        if (this.state.isEditState) {
            return (React.createElement("div", { onClick: this.valueOnclick },
                React.createElement("input", { type: "text", defaultValue: this.state.value, onKeyDown: this.valueOnKeyPress })));
        }
        else {
            return (React.createElement("div", { onClick: this.valueOnclick }, this.state.value));
        }
    }
}
class TableBody extends React.Component {
    render() {
        const data = this.props.data.map(item => (React.createElement("tr", null,
            React.createElement("td", null,
                React.createElement(CheckBoxInTable, { userid: item.userid })),
            React.createElement("td", null, item.userid),
            React.createElement("td", null,
                React.createElement(TableContents, { primKey: item.userid, id: "username", value: item.username })),
            React.createElement("td", null,
                React.createElement(TableContents, { primKey: item.userid, id: "department", value: item.department })),
            React.createElement("td", null,
                React.createElement(TableContents, { primKey: item.userid, id: "phone", value: item.phone })),
            React.createElement("td", null,
                React.createElement(TableContents, { primKey: item.userid, id: "email", value: item.email })),
            React.createElement("td", null,
                React.createElement(GradeNameInTable, { primKey: item.userid, grade: item.grade })))));
        return data;
    }
}
class TableHeaderAndBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isSelectallClicked: false
        };
        this.selectAllOnClick = this.selectAllOnClick.bind(this);
    }
    selectAllOnClick() {
        var checkboxs = document.getElementsByClassName("checkbox");
        if (this.state.isSelectallClicked == false) {
            for (var i = 0; i < this.props.data.length; i++) {
                selectedList.push(this.props.data[i].userid);
            }
            for (var i = 0; i < checkboxs.length; i++) {
                checkboxs[i].checked = true;
            }
        }
        else {
            selectedList = [];
            for (var i = 0; i < checkboxs.length; i++) {
                checkboxs[i].checked = false;
            }
        }
        this.setState(prevState => ({
            isSelectallClicked: !prevState.isSelectallClicked
        }));
    }
    render() {
        return (React.createElement("div", { className: "table_box" },
            React.createElement("table", { className: "table" },
                React.createElement("thead", { className: "thead" },
                    React.createElement("tr", null,
                        React.createElement("th", { onClick: this.selectAllOnClick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("table_select_name")),
                        React.createElement("th", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("usertable_userid_name")),
                        React.createElement("th", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("usertable_username_name")),
                        React.createElement("th", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("usertable_department_name")),
                        React.createElement("th", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("usertable_phone_name")),
                        React.createElement("th", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("usertable_email_name")),
                        React.createElement("th", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("usertable_grade_name")))),
                React.createElement("tbody", { className: "tbody" },
                    React.createElement(TableBody, { data: this.props.data })))));
    }
}
class Addpopup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            close: true
        };
        this.closeOnClick = this.closeOnClick.bind(this);
    }
    addOnClick() {
        var passwords = $('input[type="password"]');
        if (passwords[0].value == passwords[1].value) {
            var inputs = $(".addpopupBox :input");
            var item = {};
            for (var i = 0; i < inputs.length; i++) {
                item[inputs[i].getAttribute("name")] = inputs[i].value;
            }
            var userDateAjaxCallObj = new dbRestcall_1.UserDataAjaxCall();
            var userDateAjaxResult = userDateAjaxCallObj.create([item], []);
            if (userDateAjaxResult["return_code"] == "0000") {
                alert("success");
            }
            else {
                alert(userDateAjaxResult["cause"]);
            }
        }
        else {
            alert(inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("password_check_error"));
        }
        location.reload();
    }
    closeOnClick() {
        this.setState(prevState => ({
            close: !prevState.close
        }));
    }
    render() {
        if (this.props.open != this.state.close) {
            return (React.createElement("div", null));
        }
        else {
            return (React.createElement("div", { className: "addpopupBox" },
                React.createElement("div", { className: "addpopup_inside_Box" },
                    React.createElement("label", null,
                        inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("usertable_userid_name"),
                        React.createElement("input", { type: "text", name: "userid", placeholder: "userid" })),
                    React.createElement("label", null,
                        inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("usertable_password_name"),
                        React.createElement("input", { type: "password", name: "password", placeholder: "password" })),
                    React.createElement("label", null,
                        inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("usertable_password_check_name"),
                        React.createElement("input", { type: "password", name: "password_check", placeholder: "check_password" })),
                    React.createElement("label", null,
                        inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("usertable_username_name"),
                        React.createElement("input", { type: "text", name: "username", placeholder: "username" })),
                    React.createElement("label", null,
                        inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("usertable_department_name"),
                        React.createElement("input", { type: "text", name: "department", placeholder: "department" })),
                    React.createElement("label", null,
                        inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("usertable_phone_name"),
                        React.createElement("input", { type: "text", name: "phone", placeholder: "phone" })),
                    React.createElement("label", null,
                        inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("usertable_email_name"),
                        React.createElement("input", { type: "text", name: "email", placeholder: "email" })),
                    React.createElement("label", null,
                        inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("usertable_grade_name"),
                        React.createElement("input", { type: "text", name: "grade", placeholder: "grade" })),
                    React.createElement("div", { className: "add_popup_button_group" },
                        React.createElement("button", { onClick: this.addOnClick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("table_selected_item_add_button")),
                        React.createElement("button", { onClick: this.closeOnClick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("popup_close"))))));
        }
    }
}
class SearchBoxAndTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            startDate: "",
            endDate: "",
            userid: "",
            username: "",
            grade: "all",
            email: "",
            phone: "",
            department: "all",
            currentPage: 1,
            maxPage: -1,
            perpage: 10,
            resultData: [],
            isAddClicked: false
        };
        this.getDataUsingRestCallFunc = this.getDataUsingRestCallFunc.bind(this);
        this.useridOnChange = this.useridOnChange.bind(this);
        this.usernameOnChange = this.usernameOnChange.bind(this);
        this.addOnClick = this.addOnClick.bind(this);
        this.searchOnclick = this.searchOnclick.bind(this);
        this.firstPageOnclick = this.firstPageOnclick.bind(this);
        this.lastPageOnclick = this.lastPageOnclick.bind(this);
        this.prePageOnclick = this.prePageOnclick.bind(this);
        this.nextPageOnclick = this.nextPageOnclick.bind(this);
        this.currentPageOnKeyPress = this.currentPageOnKeyPress.bind(this);
    }
    getDataUsingRestCallFunc(page) {
        var userDateResult;
        var option1 = {};
        if (this.state.department != "all") {
            option1["department"] = this.state.department;
        }
        if (this.state.userid != "") {
            option1["userid"] = this.state.userid;
        }
        if (this.state.username != "") {
            option1["username"] = this.state.username;
        }
        var userDateAjaxCallObj = new dbRestcall_1.UserDataAjaxCall();
        userDateResult = userDateAjaxCallObj.retrieve([], [option1, {}, {}, { page: page, perpage: this.state.perpage }]);
        var logDataAjaxCallObj = new dbRestcall_1.LogDataAjaxCall();
        logDataAjaxCallObj.create([properties_1.properties["User_view_log"]], []);
        return userDateResult;
    }
    firstPageOnclick() {
        var userDateResult;
        if (this.state.currentPage != 1) {
            userDateResult = this.getDataUsingRestCallFunc(1);
            this.setState({
                resultData: userDateResult["items"],
                currentPage: 1
            });
            $("#current_page").val(1);
        }
    }
    lastPageOnclick() {
        var userDateResult;
        if (this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage) {
            userDateResult = this.getDataUsingRestCallFunc(this.state.maxPage);
            this.setState({
                resultData: userDateResult["items"],
                currentPage: this.state.maxPage
            });
            $("#current_page").val(this.state.maxPage);
        }
    }
    prePageOnclick() {
        var userDateResult;
        if (this.state.maxPage != -1 && this.state.currentPage > 1) {
            userDateResult = this.getDataUsingRestCallFunc(this.state.currentPage - 1);
            this.setState({ resultData: userDateResult["items"] });
            $("#current_page").val(this.state.currentPage - 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage - 1
            }));
        }
    }
    nextPageOnclick() {
        var userDateResult;
        if (this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage) {
            userDateResult = this.getDataUsingRestCallFunc(this.state.currentPage + 1);
            this.setState({ resultData: userDateResult["items"] });
            $("#current_page").val(this.state.currentPage + 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage + 1
            }));
        }
    }
    currentPageOnKeyPress(e) {
        if (e.which == 13) {
            var pageVal = $("#current_page").val();
            var page = parseInt(pageVal);
            if (isNaN(page)) {
                alert(inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_is_not_number_alert"));
                $("#current_page").val(this.state.currentPage);
            }
            else {
                if (page > this.state.maxPage) {
                    alert(inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_greater_than_max_alert"));
                    $("#current_page").val(this.state.currentPage);
                }
                else {
                    if (this.state.maxPage != -1 &&
                        this.state.currentPage != page) {
                        var userDateResult = this.getDataUsingRestCallFunc(page);
                        this.setState({
                            resultData: userDateResult["items"],
                            currentPage: page
                        });
                        $("#current_page").val(page);
                    }
                }
            }
        }
    }
    useridOnChange(e) {
        this.setState({
            userid: e.target.value
        });
    }
    usernameOnChange(e) {
        this.setState({
            username: e.target.value
        });
    }
    deleteOnClick(e) {
        var userDataAjaxCallObj = new dbRestcall_1.UserDataAjaxCall();
        var optionList = [];
        for (var i = 0; i < selectedList.length; i++) {
            optionList.push({ userid: selectedList[i] });
        }
        userDataAjaxCallObj.delete([], optionList);
        location.reload();
    }
    addOnClick() {
        this.setState(prevState => ({
            isAddClicked: !prevState.isAddClicked
        }));
    }
    searchOnclick() {
        var userDateResult = this.getDataUsingRestCallFunc(1);
        this.setState({ resultData: userDateResult["items"] });
        var maxpage = Math.ceil(userDateResult["total"] / this.state.perpage);
        this.setState({ maxPage: maxpage });
        if (userDateResult["items"].length == 0) {
            $("#current_page").val(0);
            $("#max_page").val(0);
            this.setState({ maxPage: 0, currentPage: 0 });
        }
        else {
            $("#current_page").val(1);
            $("#max_page").val(maxpage);
            this.setState({ maxPage: maxpage, currentPage: 1 });
        }
    }
    componentDidMount() {
        this.searchOnclick();
    }
    render() {
        return (React.createElement("div", { className: "main_box" },
            React.createElement("div", { className: "current_page_info" },
                inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_setting_root_name"),
                React.createElement("i", { className: "fas fa-angle-right" }),
                inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_usersetting_name")),
            React.createElement("div", { className: "searchBox" },
                React.createElement("div", { className: "userid_input_box" },
                    React.createElement("label", null,
                        inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("usertable_userid_name"),
                        React.createElement("input", { type: "text", className: "userid_input", id: "userid", onChange: this.useridOnChange, defaultValue: this.state.userid }))),
                React.createElement("div", { className: "username_input_box" },
                    React.createElement("label", null,
                        inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("usertable_username_name"),
                        React.createElement("input", { type: "text", className: "username_input", id: "username", onChange: this.usernameOnChange, defaultValue: this.state.username }))),
                React.createElement("div", { className: "username_input_box" },
                    React.createElement("label", null,
                        inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("usertable_department_name"),
                        React.createElement(DeptBoxInPopup, { department: this.state.department, id: "department" }))),
                React.createElement("div", { className: "button_box" },
                    React.createElement("div", { className: "search_button_box" },
                        React.createElement("button", { className: "searchButton", onClick: this.searchOnclick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_search_button_name"))),
                    React.createElement("div", null,
                        React.createElement("button", { className: "items_delete_button", onClick: this.deleteOnClick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("table_selected_item_delete_button"))),
                    React.createElement("div", null,
                        React.createElement("button", { className: "items_add_button", onClick: this.addOnClick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("table_selected_item_add_button")),
                        React.createElement(Addpopup, { open: this.state.isAddClicked })))),
            React.createElement("div", null,
                React.createElement(TableHeaderAndBox, { data: this.state.resultData })),
            React.createElement("div", { className: "page_box" },
                React.createElement("div", { className: "pre_page_box" },
                    React.createElement("i", { className: "fas fa-angle-double-left", onClick: this.firstPageOnclick }),
                    React.createElement("i", { className: "fas fa-angle-left", onClick: this.prePageOnclick })),
                React.createElement("div", { className: "page_input_box" },
                    React.createElement("input", { type: "text", className: "current_page_input", id: "current_page", onKeyPress: this.currentPageOnKeyPress }),
                    React.createElement("div", { className: "slash" }),
                    React.createElement("input", { type: "text", className: "max_page_input", readOnly: "true", id: "max_page" })),
                React.createElement("div", { className: "post_page_box" },
                    React.createElement("i", { className: "fas fa-angle-right", onClick: this.nextPageOnclick }),
                    React.createElement("i", { className: "fas fa-angle-double-right", onClick: this.lastPageOnclick })))));
    }
}
ReactDOM.render(React.createElement("div", null,
    React.createElement(SearchBoxAndTable, null)), document.getElementById("main"));
