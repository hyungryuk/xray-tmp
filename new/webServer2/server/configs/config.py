import os

WEB_SER_PORT_NUM = os.getenv('WEB_SER_PORT_NUM', 20000)
WEB_SER_HOST_NUM = os.getenv('WEB_SER_HOST_NUM', '0.0.0.0')
REST_SERVER_PATH = os.getenv('REST_SERVER_PATH', 'http://localhost:5000')
MODEL_SERVER_PATH = os.getenv('MODEL_SERVER_PATH', 'http://localhost:10002')
FIRST_FRONT_PAGE_URL = os.getenv('FIRST_FRONT_PAGE_URL', 'dashGraphPage.html')
