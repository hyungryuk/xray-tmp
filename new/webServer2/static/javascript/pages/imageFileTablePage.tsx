import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { HeaderComponent } from "../modulesForPages/header";
import {LineSelectorInterface} from '../others/selector/selector'
import {StaticLanguageInterface} from '../others/staticLanguageData/staticLanguageData'
import {myContainer} from '../DIContainerConf/inversify.config'
import {TYPES} from '../DIContainerConf/types'

import { ImageFileAjaxCall,LineAjaxCall,LogDataAjaxCall } from '../others/restCall/dbRestcall'
import {encoder} from "../others/encoder/encoder";

import {properties} from '../properties/properties'

var selectedList:{[key:string]:any}[] = []
ReactDOM.render(
  <HeaderComponent />,
  document.getElementById("header")
);


class LineNameInTable extends React.Component<{lineList:any[],linecode:number},{}> {
    constructor(props:any) {
        super(props);
        this.lineCodeToFactoryAndLineName = this.lineCodeToFactoryAndLineName.bind(
            this
        );
    }

    lineCodeToFactoryAndLineName() {
        for (var i = 0; i < this.props.lineList.length; i++) {
            if (this.props.lineList[i].linecode == this.props.linecode) {
                return (
                    this.props.lineList[i].factory +
                    " " +
                    this.props.lineList[i].line
                );
            }
        }
    }

    render() {
        return <div>{this.lineCodeToFactoryAndLineName()}</div>;
    }
}

class LotnoInPopup extends React.Component<{lotno:string,id:string},{}> {
    constructor(props:any) {
        super(props);
        this.initDate = this.initDate.bind(this);
    }

    initDate() {
        $("#" + this.props.id).val(this.props.lotno);
    }

    componentDidMount() {
        this.initDate();
    }

    render() {
        return <input type="text" className="lotno_input" id={this.props.id} />;
    }
}
class CheckBoxInTable extends React.Component<{filedate:string, filename:string},{}> {
    constructor(props:any){
        super(props);
        this.checkBoxOnChange = this.checkBoxOnChange.bind(this);
    }
    checkBoxOnChange(e:any){
        if(e.target.checked==true){
            selectedList.push(
                {filename:this.props.filename, filedate:this.props.filedate}
            );
        }else{
            var index = selectedList.indexOf({filename:this.props.filename, filedate:this.props.filedate});
            selectedList.splice(index,1);
        }
    }
    render(){
        return(
            <input type="checkbox" onChange={this.checkBoxOnChange} className="checkbox"/>
        );
    }

}

class NgSelectBoxInPopup extends React.Component<{ng:string,id:string},{}> {
    render() {
        if (this.props.ng == "-1") {
            return (
                <select className="ng_input" id={this.props.id}>
                    <option value="-1">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("select_box_default_option")}
                    </option>
                    <option value="0">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ok_name")}
                    </option>
                    <option value="1">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_name")}
                    </option>
                    <option value="2">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_nr_name")}
                    </option>
                </select>
            );
        }
        if (this.props.ng == "0") {
            return (
                <select className="ng_input" id={this.props.id}>
                    <option value="-1">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("select_box_default_option")}
                    </option>
                    <option value="0" selected>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ok_name")}
                    </option>
                    <option value="1">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_name")}
                    </option>
                    <option value="2">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_nr_name")}
                    </option>
                </select>
            );
        }
        if (this.props.ng == "1") {
            return (
                <select className="ng_input" id={this.props.id}>
                    <option value="-1">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("select_box_default_option")}
                    </option>
                    <option value="0">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ok_name")}
                    </option>
                    <option value="1" selected>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_name")}
                    </option>
                    <option value="2">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_nr_name")}
                    </option>
                </select>
            );
        }
        if (this.props.ng == "2") {
            return (
                <select className="ng_input" id={this.props.id}>
                    <option value="-1">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("select_box_default_option")}
                    </option>
                    <option value="0">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ok_name")}
                    </option>
                    <option value="1">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_name")}
                    </option>
                    <option value="2" selected>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_nr_name")}
                    </option>
                </select>
            );
        }
    }
}

class NgToString extends React.Component<{ng:number},{}> {
    render() {
        if (this.props.ng == 0) {
            return <td>{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ok_name")}</td>;
        }
        if (this.props.ng == 1) {
            return <td>{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_name")}</td>;
        }
        if (this.props.ng == 2) {
            return <td>{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_nr_name")}</td>;
        }
    }
}

class Tbody extends React.Component<{data:any[],lineList:any[]},{}> {
    render() {
        console.log(this.props.data);
        const data = this.props.data.map(item => (
            <tr>
                <td><CheckBoxInTable filedate={item.filedate} filename={item.filename}/></td>
                <td>{item.filename}</td>
                <td>
                    <LineNameInTable
                        linecode={item.linecode}
                        lineList={this.props.lineList}
                    />
                </td>
                <td>{item.filepath}</td>
                <td className="number">{item.lotno}</td>

                <NgToString ng={item.ng_by_user} />
                <NgToString ng={item.ng_by_mes} />
                <NgToString ng={item.ng} />
                <td>{item.filedate}</td>
            </tr>
        ));
        return data;
    }
}

class TableHeaderAndBox extends React.Component<{data:any[],lineList:any[]},{isSelectallClicked:boolean}> {
    constructor(props:any){
        super(props);
        this.state = {
            isSelectallClicked:false
        }
        this.selectAllOnClick = this.selectAllOnClick.bind(this);

    }
    selectAllOnClick(){

        var checkboxs = document.getElementsByClassName("checkbox");
        if(this.state.isSelectallClicked==false){
            for(var i=0;i<this.props.data.length;i++){
                selectedList.push(
                    {filename:this.props.data[i].filename, filedate:this.props.data[i].filedate}
                );
            }
            for(var i=0;i<checkboxs.length;i++){
                checkboxs[i].checked=true;
            }
        }else{
            selectedList=[];
            for(var i=0;i<checkboxs.length;i++){
                checkboxs[i].checked=false;
            }
        }

        this.setState(prevState => ({
            isSelectallClicked: !prevState.isSelectallClicked
        }));

    }
    render() {
        return (
            <div className="table_box">
                <table className="table">
                    <thead className="thead">
                        <tr>
                            <th onClick={this.selectAllOnClick}>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("table_select_name")}
                            </th>
                            <th className="table_filename">
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "imagefiletable_filename_table"
                                )}
                            </th>

                            <th className="table_line">
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_line_table")}
                            </th>
                            <th className="table_filepath">
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "imagefiletable_filepath_table"
                                )}
                            </th>
                            <th className="table_lotno">
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_lotno_table")}
                            </th>

                            <th className="table_ng">
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "imagefiletable_ng_by_user_table"
                                )}
                            </th>
                            <th className="table_ng">
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "imagefiletable_ng_by_mes_table"
                                )}
                            </th>
                            <th className="table_ng">
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_table")}
                            </th>
                            <th className="table_filedate">
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "imagefiletable_filedate_table"
                                )}
                            </th>
                        </tr>
                    </thead>
                    <tbody className="tbody">
                        <Tbody
                            lineList={this.props.lineList}
                            data={this.props.data}
                        />
                    </tbody>
                </table>
            </div>
        );
    }
}
class LineOptions extends React.Component<{line:string,lineList:any[]},{}>{
    componentDidMount(){
        let selectorObj = myContainer.get<LineSelectorInterface>(TYPES.LineSelectorInterface);
        selectorObj.setDivId("lineSelector")
        selectorObj.setDatas(this.props.lineList);
        selectorObj.makeSelector();
        $("#lineSelector").val(this.props.line);
    }
    render() {
        return (
                <option value="all">
                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                        "dashtable_line_selector_option_all_name"
                    )}
                </option>

        );
    }
}

class SearchBoxAndTable extends React.Component<{},{fileName:string,line:string,lotno:string,ng:string,ng_by_user:string,ng_by_mes:string,startDate:string,endDate:string,resultData:any[],lineList:any[],currentPage:number,maxPage:number,perpage:number,isfilterOpen:boolean}> {
    constructor(props:any) {
        super(props);
        var lineAjaxCall = new LineAjaxCall();
        var lineList = lineAjaxCall.retrieve([], [])["items"];
        this.state = {
            startDate: "",
            endDate: "",
            fileName: "",
            line: "all",
            resultData: [],
            lotno: "",
            lineList: lineList,
            currentPage: 1,
            maxPage: -1,
            perpage: 10,
            isfilterOpen: false,
            ng: "-1",
            ng_by_user: "-1",
            ng_by_mes: "-1"
        };

        this.getDataUsingRestCallFunc = this.getDataUsingRestCallFunc.bind(
            this
        );

        this.startDateOnChange = this.startDateOnChange.bind(this);
        this.endDateOnChange = this.endDateOnChange.bind(this);
        this.fileNameOnChange = this.fileNameOnChange.bind(this);
        this.lineSelectOnChange = this.lineSelectOnChange.bind(this);
        this.lotnoOnChange = this.lotnoOnChange.bind(this);
        this.searchOnclick = this.searchOnclick.bind(this);
        this.filterOnclick = this.filterOnclick.bind(this);
        this.filterCloseOnclick = this.filterCloseOnclick.bind(this);
        this.setFilterOnclick = this.setFilterOnclick.bind(this);
        this.initFilterBox = this.initFilterBox.bind(this);

        this.firstPageOnclick = this.firstPageOnclick.bind(this);
        this.lastPageOnclick = this.lastPageOnclick.bind(this);
        this.prePageOnclick = this.prePageOnclick.bind(this);
        this.nextPageOnclick = this.nextPageOnclick.bind(this);
        this.currentPageOnKeyPress = this.currentPageOnKeyPress.bind(this);
    }
    initFilterBox() {
        $("#lotno").val(this.state.lotno);
        $("#ng option:selected").val(this.state.ng);
        $("#ng_by_user option:selected").val(this.state.ng_by_user);
        $("#ng_by_mes option:selected").val(this.state.ng_by_mes);
    }
    setFilterOnclick() {
        this.setState({ lotno: $("#lotno").val() as string });
        this.setState({ ng: $("#ng option:selected").val() as string });
        this.setState({ ng_by_user: $("#ng_by_user option:selected").val()  as string});
        this.setState({ ng_by_mes: $("#ng_by_mes option:selected").val()  as string});
        this.setState(prevState => ({
            isfilterOpen: !prevState.isfilterOpen
        }));
    }
    filterCloseOnclick() {
        this.setState(prevState => ({
            isfilterOpen: !prevState.isfilterOpen
        }));
    }
    filterOnclick() {
        this.setState(prevState => ({
            isfilterOpen: !prevState.isfilterOpen
        }));
    }
    getDataUsingRestCallFunc(page:number) {
        var imageFileResult;
        var option1:{[key:string]:any} = {};
        var option2:{[key:string]:any} = {};

        if (this.state.line != "all") {
            option1["linecode"] = this.state.line;
        }
        if (this.state.fileName != "") {
            option1["filename"] = this.state.fileName;
        }
        if (this.state.lotno != "") {
            option1["lotno"] = this.state.lotno;
        }
        if (this.state.ng != "-1") {
            option1["ng"] = this.state.ng;
        }
        if (this.state.ng_by_user != "-1") {
            option1["ng_by_user"] = this.state.ng_by_user;
        }
        if (this.state.ng_by_mes != "-1") {
            option1["ng_by_mes"] = this.state.ng_by_mes;
        }
        if (this.state.startDate != "" && this.state.endDate != "") {
            option2["startdate"] = encoder.dateToYYMMDDHHMMSS(
                this.state.startDate + "000000"
            );
            option2["enddate"] = encoder.dateToYYMMDDHHMMSS(
                this.state.endDate + "235959"
            );
        }

        option1["isremoved"] = 0;
        var imageFileAjaxCall = new ImageFileAjaxCall();
        imageFileResult = imageFileAjaxCall.retrieve(
            [],
            [option1, option2, {}, { page: page, perpage: this.state.perpage }]
        );
        var logDataAjaxCallObj = new LogDataAjaxCall();
        logDataAjaxCallObj.create([properties["ImageFile_view_log"]],[]);
        console.log(imageFileResult);
        return imageFileResult;
    }

    firstPageOnclick() {
        var imageFileResult;
        if (this.state.currentPage != 1) {
            imageFileResult = this.getDataUsingRestCallFunc(1);
            this.setState({
                resultData: imageFileResult["items"],
                currentPage: 1
            });

            $("#current_page").val(1);
        }
    }

    lastPageOnclick() {
        var imageFileResult;
        if (
            this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage
        ) {
            imageFileResult = this.getDataUsingRestCallFunc(this.state.maxPage);
            this.setState({
                resultData: imageFileResult["items"],
                currentPage: this.state.maxPage
            });

            $("#current_page").val(this.state.maxPage);
        }
    }
    prePageOnclick() {
        var imageFileResult;
        if (this.state.maxPage != -1 && this.state.currentPage > 1) {
            imageFileResult = this.getDataUsingRestCallFunc(
                this.state.currentPage - 1
            );
            this.setState({ resultData: imageFileResult["items"] });
            $("#current_page").val(this.state.currentPage - 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage - 1
            }));
        }
    }
    nextPageOnclick() {
        var imageFileResult;
        if (
            this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage
        ) {
            imageFileResult = this.getDataUsingRestCallFunc(
                this.state.currentPage + 1
            );
            this.setState({ resultData: imageFileResult["items"] });
            $("#current_page").val(this.state.currentPage + 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage + 1
            }));
        }
    }
    currentPageOnKeyPress(e:any) {
        if (e.which == 13) {
            var pageVal = $("#current_page").val() as string;
            var page = parseInt(pageVal);
            if (isNaN(page)) {
                alert(myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_is_not_number_alert"));
                $("#current_page").val(this.state.currentPage);
            } else {
                if (page > this.state.maxPage) {
                    alert(
                        myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_greater_than_max_alert")
                    );
                    $("#current_page").val(this.state.currentPage);
                } else {
                    if (
                        this.state.maxPage != -1 &&
                        this.state.currentPage != page
                    ) {
                        var imageFileResult = this.getDataUsingRestCallFunc(
                            page
                        );
                        this.setState({
                            resultData: imageFileResult["items"],
                            currentPage: page
                        });
                        $("#current_page").val(page);
                    }
                }
            }
        }
    }
    startDateOnChange(e:any) {
        this.setState({
            startDate: e.target.value
        });
    }
    endDateOnChange(e:any) {
        this.setState({
            endDate: e.target.value
        });
    }
    fileNameOnChange() {
        this.setState({
            fileName: $("#fileName").val() as string
        });
    }
    lineSelectOnChange() {
        this.setState({
            line: $("#lineSelector option:selected").val() as string
        });
    }
    lotnoOnChange() {
        this.setState({
            lotno: $("#lotno").val() as string
        });
    }
    deleteOnClick(e:any){
        var imageFileAjaxCallObj = new ImageFileAjaxCall();
        var optionList:any[] = [];
        var itemList:any[] = [];
        for(var i=0;i<selectedList.length;i++){
            optionList.push({filename:selectedList[i].filename,filepath:selectedList[i].filepath});
            itemList.push({isremoved:1})
        }
        imageFileAjaxCallObj.delete(
            itemList,
            optionList
        );
        location.reload();
    }
    searchOnclick() {
        var imageFileResult = this.getDataUsingRestCallFunc(1);

        this.setState({ resultData: imageFileResult["items"] });

        var maxpage = Math.ceil(imageFileResult["total"] / this.state.perpage);
        this.setState({ maxPage: maxpage });

        if (imageFileResult["items"].length == 0) {
            $("#current_page").val(0);
            $("#max_page").val(0);
            this.setState({ maxPage: 0, currentPage: 0 });
        } else {
            $("#current_page").val(1);
            $("#max_page").val(maxpage);
            this.setState({ maxPage: maxpage, currentPage: 1 });
        }
    }

    componentDidMount(){
        this.searchOnclick();
    }
    render() {
        const popup = (
            <div className="optional_settings_box">
                <div className="popup_close_button">
                    <i
                        className="fas fa-window-close"
                        onClick={this.filterCloseOnclick}
                    />
                </div>
                <div className="lotno_input_box">
                    <label>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_lotno_table")}
                        <LotnoInPopup id="lotno" lotno={this.state.lotno} />
                    </label>
                </div>
                <div className="ng_input_box">
                    <label>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_table")}
                        <NgSelectBoxInPopup ng={this.state.ng} id="ng" />
                    </label>
                </div>
                <div className="ng_user_input_box">
                    <label>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_by_user_table")}
                        <NgSelectBoxInPopup
                            ng={this.state.ng_by_user}
                            id="ng_by_user"
                        />
                    </label>
                </div>
                <div className="ng_mes_input_box">
                    <label>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_by_mes_table")}
                        <NgSelectBoxInPopup
                            ng={this.state.ng_by_mes}
                            id="ng_by_mes"
                        />
                    </label>
                </div>
                <div className="filter_set_button">
                    <button
                        className="set_button"
                        onClick={this.setFilterOnclick}
                    >
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("filter_setting_button")}
                    </button>
                </div>
            </div>
        );
        if (this.state.isfilterOpen) {
            return (
                <div className="main_box">
                    <div className="current_page_info">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_file_root_name")}
                        <i className="fas fa-angle-right"></i>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_filetable_name")}
                    </div>
                    <div>{popup}</div>
                    <div className="searchBox">
                        <div className="filename_input_box">
                            <label>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "imagefiletable_filename_label"
                                )}
                                <input
                                    type="text"
                                    className="filename_input"
                                    id="fileName"
                                    onChange={this.fileNameOnChange}
                                    defaultValue={this.state.fileName}
                                />
                            </label>
                        </div>
                        <div className="date_input_box">
                            <div className="start_date_input_box">
                                <label>
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "imagefiletable_startdate_label"
                                    )}
                                    <input
                                        type="date"
                                        className="start_date_input"
                                        onChange={this.startDateOnChange}
                                        defaultValue={this.state.startDate}
                                    />
                                </label>
                            </div>
                            <div className="end_date_input_box">
                                <label>
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "imagefiletable_enddate_label"
                                    )}
                                    <input
                                        type="date"
                                        className="end_date_input"
                                        onChange={this.endDateOnChange}
                                        defaultValue={this.state.endDate}
                                    />
                                </label>
                            </div>
                        </div>
                        <div className="line_input_box">
                            <label>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_line_label")}
                                <select
                                    className="line_input"
                                    id="lineSelector"
                                    onChange={this.lineSelectOnChange}>
                                    <LineOptions line={this.state.line}  lineList={this.state.lineList}/>
                                </select>

                            </label>
                        </div>
                        <div className="button_box">
                            <div className="set_specific_filter_button_box">
                                <button
                                    className="set_specific_filter_button"
                                    onClick={this.filterOnclick}
                                >
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "imagefiletable_set_specific_filter_button_box_name"
                                    )}
                                </button>
                            </div>
                            <div className="search_button_box">
                                <button
                                    className="searchButton"
                                    onClick={this.searchOnclick}
                                >
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "dashtable_search_button_name"
                                    )}
                                </button>
                            </div>
                            <div>
                                <button className="items_delete_button" onClick={this.deleteOnClick}>{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "table_selected_item_delete_button"
                                )}</button>
                            </div>
                        </div>
                    </div>

                    <div>
                        <TableHeaderAndBox
                            lineList={this.state.lineList}
                            data={this.state.resultData}
                        />
                    </div>
                    <div className="page_box">
                        <div className="pre_page_box">
                            <i
                                className="fas fa-angle-double-left"
                                onClick={this.firstPageOnclick}
                            />
                            <i
                                className="fas fa-angle-left"
                                onClick={this.prePageOnclick}
                            />
                        </div>
                        <div className="page_input_box">
                            <input
                                type="text"
                                className="current_page_input"
                                id="current_page"
                                onKeyPress={this.currentPageOnKeyPress}
                            />
                            <div className="slash">
                            </div>
                            <input
                                type="text"
                                className="max_page_input"
                                readOnly="true"
                                id="max_page"
                            />
                        </div>
                        <div className="post_page_box">
                            <i
                                className="fas fa-angle-right"
                                onClick={this.nextPageOnclick}
                            />
                            <i
                                className="fas fa-angle-double-right"
                                onClick={this.lastPageOnclick}
                            />
                        </div>
                    </div>
                </div>
            );
        } else {
            return (
                <div className="main_box">
                    <div className="current_page_info">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_file_root_name")}
                        <i className="fas fa-angle-right"></i>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_filetable_name")}
                    </div>
                    <div className="searchBox">
                        <div className="filename_input_box">
                            <label>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "imagefiletable_filename_label"
                                )}
                                <input
                                    type="text"
                                    className="filename_input"
                                    id="fileName"
                                    onChange={this.fileNameOnChange}
                                    defaultValue={this.state.fileName}
                                />
                            </label>
                        </div>
                        <div className="date_input_box">
                            <div className="start_date_input_box">
                                <label>
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "imagefiletable_startdate_label"
                                    )}
                                    <input
                                        type="date"
                                        className="start_date_input"
                                        onChange={this.startDateOnChange}
                                        defaultValue={this.state.startDate}
                                    />
                                </label>
                            </div>
                            <div className="end_date_input_box">
                                <label>
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "imagefiletable_enddate_label"
                                    )}
                                    <input
                                        type="date"
                                        className="end_date_input"
                                        onChange={this.endDateOnChange}
                                        defaultValue={this.state.endDate}
                                    />
                                </label>
                            </div>
                        </div>
                        <div className="line_input_box">
                            <label>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_line_label")}
                                <select
                                    className="line_input"
                                    id="lineSelector"
                                    onChange={this.lineSelectOnChange}>
                                    <LineOptions line={this.state.line}  lineList={this.state.lineList}/>
                                </select>
                            </label>
                        </div>
                        <div className="button_box">
                            <div className="set_specific_filter_button_box">
                                <button
                                    className="set_specific_filter_button"
                                    onClick={this.filterOnclick}
                                >
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "imagefiletable_set_specific_filter_button_box_name"
                                    )}
                                </button>
                            </div>
                            <div className="search_button_box">
                                <button
                                    className="searchButton"
                                    onClick={this.searchOnclick}
                                >
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "dashtable_search_button_name"
                                    )}
                                </button>
                            </div>
                            <div>
                                <button className="items_delete_button" onClick={this.deleteOnClick}>{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "table_selected_item_delete_button"
                                )}</button>
                            </div>
                        </div>
                    </div>

                    <div>
                        <TableHeaderAndBox
                            lineList={this.state.lineList}
                            data={this.state.resultData}
                        />
                    </div>
                    <div className="page_box">
                        <div className="pre_page_box">
                            <i
                                className="fas fa-angle-double-left"
                                onClick={this.firstPageOnclick}
                            />
                            <i
                                className="fas fa-angle-left"
                                onClick={this.prePageOnclick}
                            />
                        </div>
                        <div className="page_input_box">
                            <input
                                type="text"
                                className="current_page_input"
                                id="current_page"
                                onKeyPress={this.currentPageOnKeyPress}
                            />
                            <div className="slash">
                            </div>
                            <input
                                type="text"
                                className="max_page_input"
                                readOnly="true"
                                id="max_page"
                            />
                        </div>
                        <div className="post_page_box">
                            <i
                                className="fas fa-angle-right"
                                onClick={this.nextPageOnclick}
                            />
                            <i
                                className="fas fa-angle-double-right"
                                onClick={this.lastPageOnclick}
                            />
                        </div>
                    </div>
                </div>
            );
        }
    }
}

ReactDOM.render(
    <div>
        <SearchBoxAndTable />
    </div>,
    document.getElementById("main")
);
