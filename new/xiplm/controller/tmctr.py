# -*- coding: utf-8 -*-

from flask_restful import Resource
from flask import request
import json
import csv
import requests as rq
import threading




from ..helper.messages import *
from ..configs.config import *
from ..helper.start_training import start_training


class TrainingModelRESTApi(Resource):

    def post(self):

        request_data = request.get_json()
        method_type = request_data.get('method_type')
        items = request_data.get('items')

        if method_type is not None:
            if items is not None:
                if method_type == 'train':
                    return trining_processing(request_data)
                elif method_type == 'check':
                    pass
                else:
                    pass
            else:
                message = COMMON_ITEMS_NOT_FOUND
                return json.dumps(message, ensure_ascii=True)
        else:
            message = COMMON_METHOD_TYPE_NOT_FOUND
            message.update({'items': request.get_json().get('items')})
            return json.dumps(message, ensure_ascii=True)


def trining_processing(request_data):
    try:
        print(request_data)        
        t = threading.Thread(target=start_training, args=(request_data,))
        t.start()
        return json.dumps({'items': [],
                           'return_code': MDL_TRAIN_SUCCESS.get('return_code'),
                           'message': MDL_TRAIN_SUCCESS.get('message')},ensure_ascii=True)

    except Exception as e:
        message = MDL_TRAIN_SUCCESS_FAIL
        message.update({'cause': str(e)})
        message.update({'items': request_data['items']})
        return json.dumps(message, ensure_ascii=True)
