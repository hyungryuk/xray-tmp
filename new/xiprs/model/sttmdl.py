import json

from .. import db


def create_statistic(item):
    db.session.add(item)

class StatisticInfo(db.Model):
    __tablename__ = 'iis_statistics'
    __table_args__ = {'mysql_collate': 'utf8_general_ci', 'mysql_engine': 'MyISAM'}

    date = db.Column(db.DateTime, primary_key=True, nullable=False)
    linecode = db.Column(db.Integer, primary_key=True, nullable=False)
    ngcount = db.Column(db.Integer, nullable=False)
    okcount = db.Column(db.Integer, nullable=False)
    nrcount = db.Column(db.Integer, nullable=False)
    kind = db.Column(db.Integer, nullable=False)
    regdate = db.Column(db.DateTime, nullable=False)

    def __init__(self, date, linecode, ngcount, okcount, nrcount, kind):

        self.date = date
        self.linecode = linecode
        self.ngcount = ngcount
        self.okcount = okcount
        self.nrcount = nrcount
        self.kind = kind

    def __repr__(self):
        return repr((self.date, self.linecode, self.ngcount, self.okcount, self.nrcount, self.kind, self.regdate))
