import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { HeaderComponent } from "../modulesForPages/header";
import {LineSelectorInterface, AlgorithmSelectorInterface, FilterSelectorInterface} from '../others/selector/selector'
import {StaticLanguageInterface} from '../others/staticLanguageData/staticLanguageData'
import {myContainer} from '../DIContainerConf/inversify.config'
import {TYPES} from '../DIContainerConf/types'

import { ImageFileAjaxCall,LineAjaxCall,AlgorithmDataAjaxCall,FilterDataAjaxCall,TrainingDataAjaxCall } from '../others/restCall/dbRestcall'
import {TrainingModelRestCall} from '../others/restCall/othersRestCall'
import {encoder} from "../others/encoder/encoder";

import {properties} from '../properties/properties'
import {ImageDataToJson} from '../others/training/training'
import {csvModule} from '../others/csvModule/makeCSV'

ReactDOM.render(
  <HeaderComponent />,
  document.getElementById("header")
);

class LineNameInTable extends React.Component<{lineList:any[],linecode:number},{}> {
    constructor(props:any) {
        super(props);
        this.lineCodeToFactoryAndLineName = this.lineCodeToFactoryAndLineName.bind(
            this
        );
    }

    lineCodeToFactoryAndLineName() {
        for (var i = 0; i < this.props.lineList.length; i++) {
            if (this.props.lineList[i].linecode == this.props.linecode) {
                return (
                    this.props.lineList[i].factory +
                    " " +
                    this.props.lineList[i].line
                );
            }
        }
    }

    render() {
        return <div>{this.lineCodeToFactoryAndLineName()}</div>;
    }
}

class NgToString extends React.Component<{ng:number},{}> {
    render() {
        if (this.props.ng == 0) {
            return <td>{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ok_name")}</td>;
        }
        if (this.props.ng == 1) {
            return <td>{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_name")}</td>;
        }
        if (this.props.ng == 2) {
            return <td>{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_nr_name")}</td>;
        }
    }
}

class Tbody extends React.Component<{data:any[],lineList:any[]},{}> {

    constructor(props:any){
        super(props);

        this.availableOnClick = this.availableOnClick.bind(this);
    }
    availableOnClick(e:any){
        var all_checkbox_num = $("input:checkbox").length;
        var unAvailable_checkbox_num = $("input:checkbox:not(:checked)").length;
        $("#imagesNum").html(all_checkbox_num-unAvailable_checkbox_num+"");
    }
    componentDidUpdate(){
        var all_checkbox_num = $("input:checkbox").length;
        var unAvailable_checkbox_num = $("input:checkbox:not(:checked)").length;
        $("#imagesNum").html(all_checkbox_num-unAvailable_checkbox_num+"");
    }


    render() {
        const data = this.props.data.map(item => (
            <tr>
                <td><input type="checkBox" defaultChecked="checked" onClick={this.availableOnClick}/></td>
                <td>{item.filename}</td>
                <td>{item.filedate}</td>
                <td className="table_filepath">{item.filepath}</td>
                <td>
                    <LineNameInTable
                        linecode={item.linecode}
                        lineList={this.props.lineList}
                    />
                </td>
                <NgToString ng={item.ng_by_user} />
            </tr>
        ));
        return data;
    }
}

class Table extends React.Component<{data:any[],lineList:any[]},{}> {
    render() {
        return (
            <div className="table_box">
                <table className="table">
                    <thead className="thead">
                        <tr>
                            <th className="table_file_available">
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "modeltraining_table_file_available"
                                )}
                            </th>
                            <th className="table_filename">
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "imagefiletable_filename_table"
                                )}
                            </th>
                            <th className="table_filedate">
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "imagefiletable_filedate_table"
                                )}
                            </th>
                            <th className="table_filepath">

                            </th>
                            <th className="table_line">
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_line_table")}
                            </th>
                            <th className="table_ng">
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "imagefiletable_ng_by_user_table"
                                )}
                            </th>
                        </tr>
                    </thead>
                    <tbody className="tbody" id="tbody">
                        <Tbody
                            lineList={this.props.lineList}
                            data={this.props.data}
                        />
                    </tbody>
                </table>
            </div>
        );
    }
}

class TrainingHistory extends React.Component<{data:any[],lineList:any[]},{}> {
    render(){
        return(
            <select className="history_select">
            </select>
        );
    }

}
class InputArgument extends React.Component<{},{data:any[],lineList:any[],line:string,startDate:string,endDate:string,filter:string,algorithm:string,modelName:string,modelVersion:string,isStart:boolean,modelcode:number}> {

    constructor(props:any){
        super(props);
        this.state = {
            data:[],
            lineList:[],
            line:"all",
            startDate:"",
            endDate:"",
            filter:"",
            algorithm:"",
            modelName:"",
            modelVersion:"",
            isStart:false,
            modelcode:0
        };

        this.getDataUsingRestCallFunc = this.getDataUsingRestCallFunc.bind(this);
        this.getImageOnclick = this.getImageOnclick.bind(this);
        this.lineSelectOnChange = this.lineSelectOnChange.bind(this);
        this.filterSelectOnChange = this.filterSelectOnChange.bind(this);
        this.algorithmSelectOnChange = this.algorithmSelectOnChange.bind(this);
        this.startDateOnChange = this.startDateOnChange.bind(this);
        this.endDateOnChange = this.endDateOnChange.bind(this);
        this.modelNameOnChange = this.modelNameOnChange.bind(this);
        this.modelVersionOnChange = this.modelVersionOnChange.bind(this);
        this.trainingOnclick = this.trainingOnclick.bind(this);


    }
    componentDidMount() {
        var lineAjaxCall = new LineAjaxCall();
        var lineList = lineAjaxCall.retrieve([], [])["items"];
        this.setState({
            lineList:lineList
        });
        let lineSelectorObj = myContainer.get<LineSelectorInterface>(TYPES.LineSelectorInterface);
        lineSelectorObj.setDivId("lineSelector")
        lineSelectorObj.setDatas(lineList);
        lineSelectorObj.makeSelector();
        this.setState({ lineList: lineList });

        var algorithmAjaxCall = new AlgorithmDataAjaxCall();
        var algorithmList = algorithmAjaxCall.retrieve([], [{available:1}])["items"];
        let algorithmSelectorObj = myContainer.get<AlgorithmSelectorInterface>(TYPES.AlgorithmSelectorInterface);
        algorithmSelectorObj.setDivId("algorithmSelector")
        algorithmSelectorObj.setDatas(algorithmList);
        algorithmSelectorObj.makeSelector();

        var filterAjaxCall = new FilterDataAjaxCall();
        var filterList = filterAjaxCall.retrieve([], [{available:1}])["items"];
        let filterSelectorObj = myContainer.get<FilterSelectorInterface>(TYPES.FilterSelectorInterface);
        filterSelectorObj.setDivId("filterSelector")
        filterSelectorObj.setDatas(filterList);
        filterSelectorObj.makeSelector();


    }
    filterSelectOnChange(){
        this.setState({
            filter: $("#filterSelector option:selected").val() as string
        });
        if($("#filterSelector option:selected").val()==""){
            $("#filter_name_info").html("(empty)");
        }else{
            $("#filter_name_info").html($("#filterSelector option:selected").text());
        }
    }
    algorithmSelectOnChange(){
        this.setState({
            algorithm: $("#algorithmSelector option:selected").val() as string
        });
        if($("#algorithmSelector option:selected").val()==""){
            $("#algorithm_name_info").html("(empty)");
        }else{
            $("#algorithm_name_info").html($("#algorithmSelector option:selected").text());
        }

    }

    modelNameOnChange(e:any) {
        this.setState({
            modelName: e.target.value
        });
        if(e.target.value==""){
            $("#modelname_info").html("(empty)");
        }else{
            $("#modelname_info").html(e.target.value);
        }

    }
    modelVersionOnChange(e:any) {
        this.setState({
            modelVersion: e.target.value
        });
        if(e.target.value==""){
            $("#modelversion_info").html("(empty)");
        }else{
            $("#modelversion_info").html(e.target.value);
        }
    }
    startDateOnChange(e:any) {
        this.setState({
            startDate: e.target.value
        });
    }
    endDateOnChange(e:any) {
        this.setState({
            endDate: e.target.value
        });
    }

    lineSelectOnChange() {
        this.setState({
            line: $("#lineSelector option:selected").val() as string
        });
    }
    getDataUsingRestCallFunc() {
        var imageFileResult;
        var option1:{[key:string]:any} = {};
        var option2:{[key:string]:any} = {};

        if (this.state.line != "all") {
            option1["linecode"] = this.state.line;
        }
        if (this.state.startDate != "" && this.state.endDate != "") {
            option2["startdate"] = encoder.dateToYYMMDDHHMMSS(
                this.state.startDate + "000000"
            );
            option2["enddate"] = encoder.dateToYYMMDDHHMMSS(
                this.state.endDate + "235959"
            );
        }
        option1["isremoved"] = 0;
        var imageFileAjaxCall = new ImageFileAjaxCall();
        imageFileResult = imageFileAjaxCall.retrieve(
            [],
            [option1, option2]
        );
        return imageFileResult;
    }
    getImageOnclick(){
        var imageFileResult = this.getDataUsingRestCallFunc();
        this.setState({ data: imageFileResult["items"] });
    }
    trainingOnclick(){
        if(parseInt($("#imagesNum").html())<=0){

        }else if($("#modelname_info").html()=="(empty)"){

        }else if($("#modelversion_info").html()=="(empty)"){

        }else if($("#algorithm_name_info").html()=="(empty)"){

        }else if($("#filter_name_info").html()=="(empty)"){

        }else{
            var imageDataToJsonObj = new ImageDataToJson("tbody");
            var imagesJson = imageDataToJsonObj.getJson();
            var csvRowList=csvModule.makeCSVRowsList(imagesJson);

            var itemsDict:{[key:string]:any} = {};
            var modelcode = Date.now();
            itemsDict["csvrowlist"] = csvRowList;
            itemsDict["modelcode"] = modelcode;
            itemsDict["modelname"] = this.state.modelName;
            itemsDict["algorithmcode"] = this.state.algorithm;
            itemsDict["filtercode"] = this.state.filter;

            var trainingModelRestCallObj = new TrainingModelRestCall();
            this.setState({
                isStart:true,
                modelcode:modelcode
            });
            var restResult = trainingModelRestCallObj.trainModel([itemsDict],[]);


        }



    }

    render(){
        return(
            <div className="main_box">
                <div className="current_page_info">
                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_model_root_name")}
                    <i className="fas fa-angle-right"></i>
                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_trainmodel_name")}
                </div>
                 <div className="image_data_inputs_box">
                     <div className="line_selector_box">
                         <label>
                             {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_line_label_name")}
                             <select className="line_selector" id="lineSelector" onChange={this.lineSelectOnChange}>
                                 <option value="all">
                                     {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                         "dashtable_line_selector_option_all_name"
                                     )}
                                 </option>
                             </select>
                         </label>
                     </div>
                     <div className="set_date_box">
                         <div className="start_date_box">
                             <label>
                                 {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_startdate_label")}
                                 <input type="date" id="startDate" onChange={this.startDateOnChange}/>
                             </label>
                         </div>
                         <div className="end_date_box">
                             <label>
                                 {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_enddate_label")}
                                 <input type="date" id="endDate" onChange={this.endDateOnChange}/>
                             </label>
                         </div>
                     </div>
                     <div className="training_history_box">
                         <label>
                            {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("training_history_name")}
                            <TrainingHistory />
                         </label>
                     </div>
                     <div className="get_image_button_box">
                         <button className="get_image_button" onClick={this.getImageOnclick}>{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("modeltraining_get_image_data_button_name")}</button>
                     </div>
                     <div className="image_table_box">
                         <Table data={this.state.data} lineList={this.state.lineList}/>
                     </div>
                 </div>
                 <div className="model_data_input_box">
                     <div className="model_name_box">
                         <label>
                             {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("modeltraining_table_model_name")}
                             <input type="text" className="modelName" onChange={this.modelNameOnChange}/>
                         </label>
                     </div>
                     <div className="model_version_box">
                         <label>
                             {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("modeltraining_table_model_version")}
                             <input type="text" className="modelVersion" onChange={this.modelVersionOnChange}/>
                         </label>
                     </div>
                     <div className="select_algorithm_box">
                         <label>
                             {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("modeltraining_table_algorithm_name")}
                             <select className="algorithm_selector" id="algorithmSelector" onChange={this.algorithmSelectOnChange}>
                                 <option value="">
                                     {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("select_box_default_option")}
                                 </option>
                             </select>
                         </label>
                     </div>
                     <div className="select_filter_box">
                         <label>
                             {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("modeltraining_table_filter_name")}
                             <select className="filter_selector" id="filterSelector" onChange={this.filterSelectOnChange}>
                                 <option value="">
                                     {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("select_box_default_option")}
                                 </option>
                             </select>
                         </label>
                     </div>
                     <div className="info_box">
                         <div className="info_key_box">
                             {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_line_label_name")}
                         </div>
                         <div className="info_value_box">
                             {$("#lineSelector option:selected").text()}
                         </div>
                         <div className="info_key_box">
                             {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("modeltraining_image_count_name")}
                         </div>
                         <div className="info_value_box">
                             <div id="imagesNum"/>
                         </div>
                         <div className="info_key_box">
                             {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("modeltraining_table_model_name")}
                         </div>
                         <div className="info_value_box">
                             <div id="modelname_info">(empty)</div>
                         </div>
                         <div className="info_key_box">
                             {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("modeltraining_table_model_version")}
                         </div>
                         <div className="info_value_box">
                             <div id="modelversion_info">(empty)</div>
                         </div>
                         <div className="info_key_box">
                             {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("modeltraining_table_algorithm_name")}
                         </div>
                         <div className="info_value_box">
                             <div id="algorithm_name_info">(empty)</div>
                         </div>
                         <div className="info_key_box">
                             {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("modeltraining_table_filter_name")}
                         </div>
                         <div className="info_value_box">
                             <div id="filter_name_info">(empty)</div>
                         </div>

                     </div>
                 </div>
                 <div className="textLog_box">
                     <textarea id="logArea" defaultValue="Log Here" readOnly="true">
                     </textarea>
                 </div>
                 <div className="buttons_box">
                     <button id="startTraining" onClick={this.trainingOnclick}>
                         {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("modeltraining_start_training_button")}

                     </button>
                 </div>
            </div>
        );
    }
}

ReactDOM.render(
  <InputArgument />,
  document.getElementById("main")
);
