# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 10:50:08 2018

@author: ryuk
"""
from .. import db

def create_dpm(item):
    db.session.add(item)

class DPModelInfo(db.Model):
    __tablename__ = 'iis_deploy'
    __table_args__ = {'mysql_collate': 'utf8_general_ci', 'mysql_engine': 'MyISAM'}

    linecode = db.Column(db.VARCHAR(32), nullable=False)
    filtercode = db.Column(db.VARCHAR(128), nullable=False)
    modelcode = db.Column(db.VARCHAR(16), nullable=True)
    deploydate = db.Column(db.VARCHAR(16), nullable=True, primary_key=True)
    isrunning = db.Column(db.Boolean, nullable=False)
    accuracy = db.Column(db.Float, nullable=True)
    modelname = db.Column(db.VARCHAR(128), nullable=True)
    user = db.Column(db.VARCHAR(32), nullable=True)


    def __init__(self, linecode, filtercode, modelcode, deploydate, isrunning,accuracy,modelname,user):
        self.linecode = linecode
        self.filtercode = filtercode
        self.modelcode = modelcode
        self.deploydate = deploydate
        self.isrunning = isrunning
        self.accuracy = accuracy
        self.modelname = modelname
        self.user = user


    def __repr__(self):
        return repr((self.linecode, self.filtercode, self.modelcode, self.deploydate, self.isrunning,self.accuracy,self.modelname,self.user))
