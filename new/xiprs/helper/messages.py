# -*-coding: utf-8 -*-

COMMON_METHOD_TYPE_NOT_FOUND = {'return_code': '1000', 'message': '메소드 타입이 지정 되지 않았습니다.'}
COMMON_ITEMS_NOT_FOUND = {'return_code': '1001', 'message': '처리 할 자료가 없습니다.'}
IMG_CREATE_SUCCESS = {'return_code': '0000', 'message': '이미지 파일 등록에 성공 했습니다.'}
IMG_CREATE_SUCCESS_FAIL = {'return_code': '1100', 'message': '이미지 파일 등록에 실패 했습니다.'}
IMG_VIEW_SUCCESS = {'return_code': '0000', 'message': '이미지 파일 조회에 성공 했습니다.'}
IMG_VIEW_SUCCESS_FAIL = {'return_code': '1101', 'message': '이미지 파일 조회에 실패 했습니다.'}
IMG_COUNT_SUCCESS = {'return_code': '0000', 'message': '이미지 파일 개수 조회에 성공 했습니다.'}
IMG_COUNT_SUCCESS_FAIL = {'return_code': '1102', 'message': '이미지 개수 조회에 실패 했습니다.'}
IMG_UPDATE_SUCCESS = {'return_code': '0000', 'message': '이미지 파일 수정에 성공 했습니다.'}
IMG_UPDATE_SUCCESS_FAIL = {'return_code': '1103', 'message': '이미지 파일 수정에 실패 했습니다.'}
IMG_TREE_SUCCESS = {'return_code': '0000', 'message': '이미지 파일 트리에 성공 했습니다.'}
IMG_TREE_SUCCESS_FAIL = {'return_code': '1104', 'message': '이미지 파일 트리에 실패 했습니다.'}
IMG_DELETE_SUCCESS = {'return_code': '0000', 'message': '이미지 파일 삭제에 성공 했습니다.'}
IMG_DELETE_SUCCESS_FAIL = {'return_code': '1105', 'message': '이미지 파일 삭제에 실패 했습니다.'}
IMG_SHOW_SUCCESS = {'return_code': '0000', 'message': '이미지 파일 열기에 성공 했습니다.'}
IMG_SHOW_SUCCESS_FAIL = {'return_code': '1106', 'message': '이미지 파일 열기에 실패 했습니다.'}
IMG_STATISTIC_SUCCESS = {'return_code': '0000', 'message': '이미지 통계 조회에 성공 했습니다.'}
IMG_STATISTIC_SUCCESS_FAIL = {'return_code': '1107', 'message': '이미지 통계 조회에 실패 했습니다.'}


ALG_VIEW_SUCCESS = {'return_code': '0000', 'message': '알고리즘 조회에 성공 했습니다.'}
ALG_VIEW_SUCCESS_FAIL = {'return_code': '1201', 'message': '알고리즘 조회에 실패 했습니다.'}
ALG_UPDATE_SUCCESS = {'return_code': '0000', 'message': '알고리즘 수정에 성공 했습니다.'}
ALG_UPDATE_SUCCESS_FAIL = {'return_code': '1201', 'message': '알고리즘 수정에 실패 했습니다.'}
ALG_CREATE_SUCCESS = {'return_code': '0000', 'message': '알고리즘 생성에 성공 했습니다.'}
ALG_CREATE_SUCCESS_FAIL = {'return_code': '1201', 'message': '알고리즘 생성에 실패 했습니다.'}
ALG_DELETE_SUCCESS = {'return_code': '0000', 'message': '알고리즘 삭제에 성공 했습니다.'}
ALG_DELETE_SUCCESS_FAIL = {'return_code': '1201', 'message': '알고리즘 삭제에 실패 했습니다.'}
ALG_COUNT_SUCCESS = {'return_code': '0000', 'message': '알고리즘 개수 조회에 성공 했습니다.'}
ALG_COUNT_SUCCESS_FAIL = {'return_code': '1202', 'message': '알고리즘 개수 조회에 실패 했습니다.'}
DLM_VIEW_SUCCESS = {'return_code': '0000', 'message': '배포 모델 조회에 성공 했습니다.'}
DLM_VIEW_SUCCESS_FAIL = {'return_code': '1301', 'message': '배포 모델  조회에 실패 했습니다.'}
DLM_UPDATE_SUCCESS = {'return_code': '0000', 'message': '배포 모델 수정에 성공 했습니다.'}
DLM_UPDATE_SUCCESS_FAIL = {'return_code': '1301', 'message': '배포 모델  수정에 실패 했습니다.'}
DLM_COUNT_SUCCESS = {'return_code': '0000', 'message': '배포 모델  개수 조회에 성공 했습니다.'}
DLM_COUNT_SUCCESS_FAIL = {'return_code': '1302', 'message': '배포 모델  개수 조회에 실패 했습니다.'}
DLM_TRAIN_SUCCESS = {'return_code': '0000', 'message': '배포 모델 학습 시작에 성공 했습니다.'}
DLM_TRAIN_SUCCESS_FAIL = {'return_code': '1303', 'message': '배포 모델  학습 시작에 실패 했습니다.'}

DLM_CREATE_SUCCESS = {'return_code': '0000', 'message': '배포 모델 생성에 성공 했습니다.'}
DLM_CREATE_SUCCESS_FAIL = {'return_code': '1304', 'message': '배포 모델  생성에 실패 했습니다.'}

FTR_VIEW_SUCCESS = {'return_code': '0000', 'message': '필터 조회에 성공 했습니다.'}
FTR_VIEW_SUCCESS_FAIL = {'return_code': '1401', 'message': '필터  조회에 실패 했습니다.'}
FTR_CREATE_SUCCESS = {'return_code': '0000', 'message': '필터 생성에 성공 했습니다.'}
FTR_CREATE_SUCCESS_FAIL = {'return_code': '1401', 'message': '필터 생성에 실패 했습니다.'}
FTR_UPDATE_SUCCESS = {'return_code': '0000', 'message': '필터 수정에 성공 했습니다.'}
FTR_UPDATE_SUCCESS_FAIL = {'return_code': '1401', 'message': '필터 수정에 실패 했습니다.'}
FTR_DELETE_SUCCESS = {'return_code': '0000', 'message': '필터 삭제에 성공 했습니다.'}
FTR_DELETE_SUCCESS_FAIL = {'return_code': '1401', 'message': '필터 삭제에 실패 했습니다.'}
FTR_COUNT_SUCCESS = {'return_code': '0000', 'message': '필터  개수 조회에 성공 했습니다.'}
FTR_COUNT_SUCCESS_FAIL = {'return_code': '1402', 'message': '필터  개수 조회에 실패 했습니다.'}
LINE_VIEW_SUCCESS = {'return_code': '0000', 'message': '라인 조회에 성공 했습니다.'}
LINE_VIEW_SUCCESS_FAIL = {'return_code': '1501', 'message': '라인  조회에 실패 했습니다.'}
LINE_CREATE_SUCCESS_FAIL = {'return_code': '1501', 'message': '라인  조회에 실패 했습니다.'}
LINE_CREATE_SUCCESS = {'return_code': '0000', 'message': '라인 조회에 성공 했습니다.'}
LINE_UPDATE_SUCCESS_FAIL = {'return_code': '1501', 'message': '라인  조회에 실패 했습니다.'}
LINE_UPDATE_SUCCESS = {'return_code': '0000', 'message': '라인 조회에 성공 했습니다.'}
LINE_DELETE_SUCCESS_FAIL = {'return_code': '1501', 'message': '라인  조회에 실패 했습니다.'}
LINE_DELETE_SUCCESS = {'return_code': '0000', 'message': '라인 조회에 성공 했습니다.'}

LINE_COUNT_SUCCESS = {'return_code': '0000', 'message': '라인  개수 조회에 성공 했습니다.'}
LINE_COUNT_SUCCESS_FAIL = {'return_code': '1502', 'message': '라인  개수 조회에 실패 했습니다.'}
LINE_UPDATE_SUCCESS = {'return_code': '0000', 'message': '라인 정보 수정에 성공 했습니다.'}
LINE_UPDATE_SUCCESS_FAIL = {'return_code': '1502', 'message': '라인 정보 수정에 실패 했습니다.'}
USER_VIEW_SUCCESS = {'return_code': '0000', 'message': '유저 조회에 성공 했습니다.'}
USER_VIEW_SUCCESS_FAIL = {'return_code': '1601', 'message': '유저 조회에 실패 했습니다.'}
USER_CREATE_SUCCESS = {'return_code': '0000', 'message': '유저 생성에 성공 했습니다.'}
USER_CREATE_SUCCESS_FAIL = {'return_code': '1600', 'message': '유저 생성에 실패 했습니다.'}
USER_UPDATE_SUCCESS = {'return_code': '0000', 'message': '유저정보 수정에 성공 했습니다.'}
USER_UPDATE_SUCCESS_FAIL = {'return_code': '1603', 'message': '유저정보 수정에 실패 했습니다.'}
USER_DELETE_SUCCESS = {'return_code': '0000', 'message': '유저정보 삭제에 성공 했습니다.'}
USER_DELETE_SUCCESS_FAIL = {'return_code': '1603', 'message': '유저정보 삭제에 실패 했습니다.'}
LOG_VIEW_SUCCESS = {'return_code': '0000', 'message': '로그 조회에 성공 했습니다.'}
LOG_VIEW_SUCCESS_FAIL = {'return_code': '1701', 'message': '로그 조회에 실패 했습니다.'}
LOG_CREATE_SUCCESS = {'return_code': '0000', 'message': '로그 생성에 성공 했습니다.'}
LOG_CREATE_SUCCESS_FAIL = {'return_code': '1700', 'message': '로그 생성에 실패 했습니다.'}
LOG_UPDATE_SUCCESS = {'return_code': '0000', 'message': '로그정보 수정에 성공 했습니다.'}
LOG_UPDATE_SUCCESS_FAIL = {'return_code': '1703', 'message': '로그정보 수정에 실패 했습니다.'}

DIR_RENAME_SUCCESS = {'return_code': '0000', 'message': '디렉토리 이름 수정에 성공 했습니다.'}
DIR_RENAME_SUCCESS_FAIL = {'return_code': '1801', 'message': '디렉토리 이름 수정에 실패 했습니다.'}

LOGIN_SUCCESS = {'return_code': '0000', 'message': '로그인에 성공 했습니다.'}
LOGIN_SUCCESS_FAIL = {'return_code': '1907', 'message': '로그인에 실패 했습니다.'}
LOGOUT_SUCCESS = {'return_code': '0000', 'message': '로그아웃에 성공 했습니다.'}
LOGOUT_SUCCESS_FAIL = {'return_code': '1908', 'message': '로그아웃에 실패 했습니다.'}
CURRENT_USER_VIEW_SUCCESS = {'return_code': '0000', 'message': '로그인 유저 정보 조회에 성공 했습니다.'}
CURRENT_USER_VIEW_SUCCESS_FAIL = {'return_code': '1909', 'message': '로그인 유저 정보 조회에  실패 했습니다.'}


TRN_CREATE_SUCCESS = {'return_code': '0000', 'message': '학습 정보 생성에 성공 했습니다.'}
TRN_CREATE_SUCCESS_FAIL = {'return_code': '1107', 'message': '학습 정보 생성에 실패 했습니다.'}
TRN_UPDATE_SUCCESS = {'return_code': '0000', 'message': '학습 정보 수정에 성공 했습니다.'}
TRN_UPDATE_SUCCESS_FAIL = {'return_code': '1107', 'message': '학습 정보 수정에 실패 했습니다.'}
TRN_DELETE_SUCCESS = {'return_code': '0000', 'message': '학습 정보 삭제에 성공 했습니다.'}
TRN_DELETE_SUCCESS_FAIL = {'return_code': '1107', 'message': '학습 정보 삭제에 실패 했습니다.'}

DWN_DOWNLOAD_SUCCESS_FAIL = {'return_code': '1108', 'message': '이미지 다운로드에 실패 했습니다.'}


STATISTICS_VIEW_SUCCESS = {'return_code': '0000', 'message': '일별 통계 조회에 성공 했습니다.'}
STATISTICS_VIEW_SUCCESS_FAIL = {'return_code': '1701', 'message': '일별 통계 조회에 실패 했습니다.'}
