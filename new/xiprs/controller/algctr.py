# -*- coding: utf-8 -*-

from flask_restful import Resource
from flask import request
import json
from datetime import datetime
from .. import db
from ..model.algmdl import *
from ..helper.messages import *
from ..helper.util import converter

from time import gmtime, strftime

from ..helper.properties import *
from ..model.logmdl import *
from ..configs.config import *



class AlgorithmInfoRESTApi(Resource):

    def post(self):

        request_data = request.get_json()
        method_type = request_data.get('method_type')
        items = request_data.get('items')

        if method_type is not None:
            if items is not None:
                if method_type == 'view':
                    return view_processing(request_data)
                elif method_type == 'insert':
                    return insert_processing(request_data)
                elif method_type == 'update':
                    return update_processing(request_data)
                elif method_type == 'count':
                    return count_processing(request_data)
                elif method_type == 'delete':
                    return delete_processing(request_data)
                else:
                    pass
            else:
                message = COMMON_ITEMS_NOT_FOUND
                return json.dumps(message, ensure_ascii=True)
        else:
            message = COMMON_METHOD_TYPE_NOT_FOUND
            message.update({'items': request.get_json().get('items')})
            return json.dumps(message, ensure_ascii=True)


def delete_processing(request_data):
    try:
        options = request_data.get('options')

        for option in options:
            del_data = db.session.query(AlgorithmInfo).filter_by(**option).first()
            db.session.delete(del_data)
            db.session.commit()
        

        db.session.delete(del_data)
        db.session.commit()
        message = ALG_DELETE_SUCCESS

        log_info = LogInfo(ALGORITHM_DELETE_LOG["kind"],ALGORITHM_DELETE_LOG["title"], ALGORITHM_DELETE_LOG["contents"]+" by "+request_data.get('userid'))
        create_log(log_info)
        db.session.commit()


        return json.dumps(message, ensure_ascii=True)

    except Exception as e:
        db.session.rollback()
        db.session.remove()
        message = ALG_DELETE_SUCCESS_FAIL
        message.update({'cause': str(e)})
        return json.dumps(message, ensure_ascii=True)


def update_processing(request_data):
    try:
        items = request_data.get('items')
        options = request_data.get('options')
        if items is not None and options is not None:
            if type(items) == list:
                len_val = len(items)
                for i in range (0,len(items)):
                    query_input_element = options[i]
                    query_result = db.session.query(AlgorithmInfo).filter_by(**query_input_element).all()
                    update_element = items[i]
                    for query_result_element in query_result:
                        for update_element_key in update_element:
                            setattr(query_result_element, update_element_key, update_element[update_element_key])

                print(query_result[0])
                db.session.commit()

                message = ALG_UPDATE_SUCCESS
                message.update({'items': items})

                log_info = LogInfo(ALGORITHM_UPDATE_LOG["kind"],ALGORITHM_UPDATE_LOG["title"], ALGORITHM_UPDATE_LOG["contents"]+" by "+request_data.get('userid'))
                create_log(log_info)
                db.session.commit()


                return json.dumps(message, ensure_ascii=True)
        else:
            message = COMMON_ITEMS_NOT_FOUND
            return json.dumps(message, ensure_ascii=True)
    except Exception as e:
        db.session.rollback()
        db.session.remove()
        message = ALG_UPDATE_SUCCESS_FAIL
        message.update({'cause': str(e)})
        message.update({'items': request.get_json()['items']})
        return json.dumps(message, ensure_ascii=True)


def insert_processing(request_data):
    try:

        items = request_data.get('items')

        if type(items) == list:
            for item in items:
                algorithm_info = AlgorithmInfo(item.get('algorithmcode'),item.get('algorithmname'),
                                           item.get('version'), item.get('fullpath'), item.get('available'))
                if item.get('data') is not None :
                    f = open(ALGORITHM_PATH+item.get('algorithmcode')+".py", 'w')
                    f.write(item.get('data'))
                    f.flush()

                create_algorithm(algorithm_info)

            db.session.commit()

            message = ALG_CREATE_SUCCESS
            message.update({'items': items})

            log_info = LogInfo(ALGORITHM_INSERT_LOG["kind"],ALGORITHM_INSERT_LOG["title"], ALGORITHM_INSERT_LOG["contents"]+" by "+request_data.get('userid'))
            create_log(log_info)
            db.session.commit()


            return json.dumps(message, ensure_ascii=True)

    except Exception as e:
        message = ALG_CREATE_SUCCESS_FAIL
        message.update({'cause': str(e)})
        message.update({'items': request_data['items']})
        return json.dumps(message, ensure_ascii=True)

def view_processing(request_data):
    try:
        items = request_data.get('items')
        options = request_data.get('options')

        if len(options) > 0:
            filter_params = make_filter_params(options[0])
            if len(options) == 1:
                result = db.session.query(AlgorithmInfo).filter_by(**filter_params).all()
            elif len(options) == 2:
                start_date = options[1].get('startdate')
                end_date = options[1].get('enddate')

                if not start_date is None and not end_date is None:
                    start_date = datetime.strptime(start_date, "%Y%m%d%H%M%S")
                    end_date = datetime.strptime(end_date, "%Y%m%d%H%M%S")
                    result = db.session.query(AlgorithmInfo).filter_by(**filter_params).filter(
                        AlgorithmInfo.regdate >= start_date, AlgorithmInfo.regdate <= end_date).all()
                else:
                    result = db.session.query(AlgorithmInfo).filter_by(**filter_params).all()
            elif len(options) == 3:
                orderby = options[2].get('orderby')
                start_date = options[1].get('startdate')
                end_date = options[1].get('enddate')
                if not orderby is None:
                    if not start_date is None and not end_date is None:
                        start_date = datetime.strptime(start_date, "%Y%m%d%H%M%S")
                        end_date = datetime.strptime(end_date, "%Y%m%d%H%M%S")
                        result = db.session.query(AlgorithmInfo).filter_by(**filter_params).filter(
                            AlgorithmInfo.regdate >= start_date, AlgorithmInfo.regdate <= end_date).order_by(orderby).all()
                    else:
                        result = db.session.query(AlgorithmInfo).filter_by(**filter_params).order_by(orderby).all()
                else:
                    if not start_date is None and not end_date is None:
                        start_date = datetime.strptime(start_date, "%Y%m%d%H%M%S")
                        end_date = datetime.strptime(end_date, "%Y%m%d%H%M%S")
                        result = db.session.query(AlgorithmInfo).filter_by(**filter_params).filter(
                            AlgorithmInfo.regdate >= start_date, AlgorithmInfo.regdate <= end_date).all()
                    else:
                        result = db.session.query(AlgorithmInfo).filter_by(**filter_params).all()
            elif len(options) == 4:
                page = options[3].get('page')
                per_page = options[3].get('perpage')
                orderby = options[2].get('orderby')
                start_date = options[1].get('startdate')
                end_date = options[1].get('enddate')
                print(page)
                print(per_page)
                if not orderby is None:
                    if not start_date is None and not end_date is None:
                        start_date = datetime.strptime(start_date, "%Y%m%d%H%M%S")
                        end_date = datetime.strptime(end_date, "%Y%m%d%H%M%S")
                        result = db.session.query(AlgorithmInfo).filter_by(**filter_params).filter(
                            AlgorithmInfo.regdate >= start_date, AlgorithmInfo.regdate <= end_date).order_by(orderby).paginate(int(page), int(per_page))
                    else:
                        result = db.session.query(AlgorithmInfo).filter_by(**filter_params).order_by(orderby).paginate(int(page), int(per_page))
                else:
                    if not start_date is None and not end_date is None:
                        start_date = datetime.strptime(start_date, "%Y%m%d%H%M%S")
                        end_date = datetime.strptime(end_date, "%Y%m%d%H%M%S")
                        result = db.session.query(AlgorithmInfo).filter_by(**filter_params).filter(AlgorithmInfo.regdate >= start_date, AlgorithmInfo.regdate <=end_date).paginate(int(page), int(per_page))
                    else:
                        result = db.session.query(AlgorithmInfo).filter_by(**filter_params).paginate(int(page), int(per_page))
            else:
                pass

            if len(options) == 4:
                return json.dumps({'items': [item.__dict__ for item in result.items], 'total': result.total,
                                   'return_code': ALG_VIEW_SUCCESS.get('return_code'),
                                   'message': ALG_VIEW_SUCCESS.get('message')}, default=converter)
            else:
                return json.dumps({'items': [item.__dict__ for item in result],
                                   'return_code': ALG_VIEW_SUCCESS.get('return_code'),
                                   'message': ALG_VIEW_SUCCESS.get('message')}, default=converter)
        else:
            result = db.session.query(AlgorithmInfo).all()
            return json.dumps({'items': [item.__dict__ for item in result],
                               'return_code': ALG_VIEW_SUCCESS.get('return_code'),
                               'message': ALG_VIEW_SUCCESS.get('message')}, default=converter)

    except Exception as e:
        message = ALG_VIEW_SUCCESS_FAIL
        message.update({'cause': str(e)})
        message.update({'items': request_data['items']})
        return json.dumps(message, ensure_ascii=True)


def count_processing(request_data):
    try:
        options = request_data.get('options')

        option_element = options[0]

        result = db.session.query(AlgorithmInfo).filter_by(**option_element).count()

        message = ALG_COUNT_SUCCESS
        result_dic = {'count': result}
        message.update({'items': [result_dic]})
        return json.dumps(message, ensure_ascii=True)

    except Exception as e:
        message = ALG_COUNT_SUCCESS_FAIL
        message.update({'cause': str(e)})
        message.update({'items': request_data['items']})
        return json.dumps(message, ensure_ascii=True)


def make_filter_params(item):
    filter_params = {}
    if not item.get('algorithmcode') is None:
        filter_params['algorithmcode'] = item.get('algorithmcode')

    if not item.get('regdate') is None:
        filter_params['regdate'] = str(datetime.strptime(item.get('regdate'), "%Y%m%d%H%M%S"))

    if not item.get('algorithmname') is None:
        filter_params['algorithmname'] = item.get('algorithmname')

    if not item.get('version') is None:
        filter_params['version'] = item.get('version')

    if not item.get('fullpath') is None:
        filter_params['fullpath'] = item.get('fullpath')

    if not item.get('available') is None:
        filter_params['available'] = item.get('available')

    return filter_params
