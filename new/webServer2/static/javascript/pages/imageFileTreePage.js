"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const ReactDOM = require("react-dom");
const header_1 = require("../modulesForPages/header");
const inversify_config_1 = require("../DIContainerConf/inversify.config");
const types_1 = require("../DIContainerConf/types");
const dbRestcall_1 = require("../others/restCall/dbRestcall");
const jsTreeModule_1 = require("../others/jsTreeModule/jsTreeModule");
const properties_1 = require("../properties/properties");
ReactDOM.render(React.createElement(header_1.HeaderComponent, null), document.getElementById("header"));
class FileInfo extends React.Component {
    render() {
        return (React.createElement("div", { className: "info_box" },
            React.createElement("div", { className: "each_info_div" },
                React.createElement("label", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletree_filedate_name")),
                React.createElement("div", null, this.props.filedate)),
            React.createElement("div", { className: "each_info_div" },
                React.createElement("label", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletree_line_name")),
                React.createElement("div", null, this.props.line)),
            React.createElement("div", { className: "each_info_div" },
                React.createElement("label", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletree_lotno_name")),
                React.createElement("div", null, this.props.lotno)),
            React.createElement("div", { className: "each_info_div" },
                React.createElement("label", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletree_ng_name")),
                React.createElement("div", null, this.props.ng)),
            React.createElement("div", { className: "each_info_div" },
                React.createElement("label", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletree_ng_by_user_name")),
                React.createElement("div", null, this.props.ng_by_user)),
            React.createElement("div", { className: "each_info_div" },
                React.createElement("label", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletree_ng_by_mes_name")),
                React.createElement("div", null, this.props.ng_by_mes))));
    }
}
class ImageShow extends React.Component {
    componentDidMount() {
        jsTreeModule_1.jsTreeModule.setDoubleClickFunc("tree", "show_image");
    }
    render() {
        return React.createElement("div", { id: "show_image" });
    }
}
class FileTree extends React.Component {
    componentDidMount() {
        jsTreeModule_1.jsTreeModule.setOnChangeFunction("tree");
        jsTreeModule_1.jsTreeModule.setLazyLoadingJsTree("tree", properties_1.properties["WEB_PATH"] + "/rest/db/imagefile");
    }
    render() {
        return React.createElement("div", { id: "tree" });
    }
}
class FileTreeAndImageView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            filepath: "",
            filename: "",
            filedate: "",
            line: "",
            lotno: "",
            ng: "",
            ng_by_user: "",
            ng_by_mes: ""
        };
        this.imageDivMouseHover = this.imageDivMouseHover.bind(this);
        this.mouseOnDoubleClick = this.mouseOnDoubleClick.bind(this);
    }
    imageDivMouseHover() {
        if ($("#show_image").children.length > 0) {
            if ($("img")
                .first()
                .attr("id") != this.state.filepath) {
                this.setState({
                    filepath: "",
                    filename: "",
                    filedate: "",
                    line: "",
                    lotno: "",
                    ng: "",
                    ng_by_user: "",
                    ng_by_mes: ""
                });
            }
        }
    }
    mouseOnDoubleClick() {
        if ($("img").length) {
            if ($("#show_image").children.length > 0) {
                var imageFileAjaxCallObj = new dbRestcall_1.ImageFileAjaxCall();
                var imageRestCallResult = imageFileAjaxCallObj.retrieve([], [
                    {
                        filepath: $("img")
                            .first()
                            .attr("id")
                    }
                ]);
                if (imageRestCallResult["items"].length > 0) {
                    this.setState({
                        filename: imageRestCallResult["items"][0].filename,
                        filedate: imageRestCallResult["items"][0].filedate,
                        filepath: imageRestCallResult["items"][0].filepath,
                        lotno: imageRestCallResult["items"][0].lotno
                    });
                    var lineAjaxCallObj = new dbRestcall_1.LineAjaxCall();
                    var lineRestCallResult = lineAjaxCallObj.retrieve([], []);
                    for (var i = 0; i < lineRestCallResult["items"].length; i++) {
                        if (lineRestCallResult["items"][i].linecode ==
                            imageRestCallResult["items"][0].linecode) {
                            this.setState({
                                line: lineRestCallResult["items"][i].factory +
                                    " " +
                                    lineRestCallResult["items"][i].line
                            });
                            break;
                        }
                    }
                    if (imageRestCallResult["items"][0].ng == 0) {
                        this.setState({
                            ng: inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ok_name")
                        });
                    }
                    else if (imageRestCallResult["items"][0].ng == 1) {
                        this.setState({
                            ng: inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_name")
                        });
                    }
                    else {
                        this.setState({
                            ng: inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_nr_name")
                        });
                    }
                    if (imageRestCallResult["items"][0].ng_by_user == 0) {
                        this.setState({
                            ng_by_user: inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ok_name")
                        });
                    }
                    else if (imageRestCallResult["items"][0].ng_by_user == 1) {
                        this.setState({
                            ng_by_user: inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_name")
                        });
                    }
                    else {
                        this.setState({
                            ng_by_user: inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_nr_name")
                        });
                    }
                    if (imageRestCallResult["items"][0].ng_by_mes == 0) {
                        this.setState({
                            ng_by_mes: inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ok_name")
                        });
                    }
                    else if (imageRestCallResult["items"][0].ng_by_mes == 1) {
                        this.setState({
                            ng_by_mes: inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_name")
                        });
                    }
                    else {
                        this.setState({
                            ng_by_mes: inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_nr_name")
                        });
                    }
                }
                else {
                    this.setState({
                        filepath: "",
                        filename: "",
                        filedate: "",
                        line: "",
                        lotno: "",
                        ng: "",
                        ng_by_user: "",
                        ng_by_mes: ""
                    });
                }
            }
        }
    }
    render() {
        return (React.createElement("div", { className: "main_box", onDoubleClick: this.mouseOnDoubleClick },
            React.createElement("div", { className: "current_page_info" },
                inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_file_root_name"),
                React.createElement("i", { className: "fas fa-angle-right" }),
                inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_filetree_name")),
            React.createElement("div", { className: "file_tree_box" },
                React.createElement(FileTree, null)),
            React.createElement("div", { className: "image_view_box", onMouseEnter: this.imageDivMouseHover },
                React.createElement(ImageShow, null)),
            React.createElement(FileInfo, { filepath: this.state.filepath, filename: this.state.filename, filedate: this.state.filedate, line: this.state.line, lotno: this.state.lotno, ng: this.state.ng, ng_by_user: this.state.ng_by_user, ng_by_mes: this.state.ng_by_mes })));
    }
}
ReactDOM.render(React.createElement("div", null,
    React.createElement(FileTreeAndImageView, null)), document.getElementById("main"));
