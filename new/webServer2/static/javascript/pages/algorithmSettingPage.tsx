import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { HeaderComponent } from "../modulesForPages/header";
import {StaticLanguageInterface} from '../others/staticLanguageData/staticLanguageData'
import {myContainer} from '../DIContainerConf/inversify.config'
import {TYPES} from '../DIContainerConf/types'

import { AlgorithmDataAjaxCall,LogDataAjaxCall } from '../others/restCall/dbRestcall'
import {encoder} from "../others/encoder/encoder";
import {properties} from '../properties/properties'

var selectedList:string[] = [];
ReactDOM.render(
  <HeaderComponent />,
  document.getElementById("header")
);


class DeleteButtonInTable extends React.Component<{algorithmcode:string},{}> {
    constructor(props:any) {
        super(props);
        this.deleteOnclicked = this.deleteOnclicked.bind(this);
    }

    deleteOnclicked(e:any) {
        var algorithmDataAjaxCallObj = new AlgorithmDataAjaxCall();
        algorithmDataAjaxCallObj.delete(
            [],
            [{ algorithmcode: this.props.algorithmcode }]
        );
        location.reload();
    }
    render() {
        return (
            <button className="delete_button" onClick={this.deleteOnclicked}>
                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_delete_table")}
            </button>
        );
    }
}
class AvailableBoxInPopup extends React.Component<{available:string,id:string},{}> {
    constructor(props:any) {
        super(props);
        this.initDate = this.initDate.bind(this);
    }

    initDate() {
        $("#" + this.props.id).val(this.props.available);
    }

    componentDidMount() {
        this.initDate();
    }

    render() {
        return (
            <select id="available">
                <option value="all">
                    All
                </option>
                <option value="true">
                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("filter_available_true")}
                </option>
                <option value="false">
                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("filter_available_false")}
                </option>
            </select>
        );

    }
}
class VersionBoxInPopup extends React.Component<{version:string,id:string},{}> {
    constructor(props:any) {
        super(props);
        this.initDate = this.initDate.bind(this);
    }

    initDate() {
        $("#" + this.props.id).val(this.props.version);
    }

    componentDidMount() {
        this.initDate();
    }

    render() {
        return <input type="text" className="version_input" id={this.props.id} />;
    }
}
class AvailableInTable extends React.Component<{available:number ,primKey:string},{}> {
    constructor(props:any){
        super(props);
        this.onChange = this.onChange.bind(this);
    }
    onChange(){
        var algorithmDataAjaxCallObj = new AlgorithmDataAjaxCall();
        var available;
        if( $("#"+this.props.primKey+" option:selected").val()=="1"){
            available = true;
        }else{
            available = false;
        }
        algorithmDataAjaxCallObj.update(
            [{available: available}],
            [{algorithmcode:this.props.primKey}]
        );

    }
    componentDidMount(){
        if(this.props.available==1){
            $("#"+this.props.primKey).val("1");
        }else{
            $("#"+this.props.primKey).val("0");
        }

    }
    render() {
            return(
                <select id={this.props.primKey} onChange={this.onChange}>
                    <option value="1" >{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("filter_available_true")}</option>
                    <option value="0" >{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("filter_available_false")}</option>
                </select>
            );
        }


    }
}
class CheckBoxInTable extends React.Component<{algorithmcode:string},{}> {
    constructor(props:any){
        super(props);
        this.checkBoxOnChange = this.checkBoxOnChange.bind(this);
    }
    checkBoxOnChange(e:any){
        if(e.target.checked==true){
            selectedList.push(this.props.algorithmcode);
        }else{
            var index = selectedList.indexOf(this.props.algorithmcode);
            selectedList.splice(index,1);
        }
    }
    render(){
        return(
            <input type="checkbox" onChange={this.checkBoxOnChange} className="checkbox"/>
        );
    }

}
class TableContents extends React.Component<{primKey:string,id:string,value:string},{isEditState:boolean,value:string}> {
    constructor(props:any){
        super(props);
        this.valueOnclick = this.valueOnclick.bind(this);
        this.valueOnKeyPress = this.valueOnKeyPress.bind(this);
        this.state={
            isEditState:false,
            value:this.props.value
        };
    }
    valueOnclick(){
        if(this.state.isEditState==false){
            this.setState(prevState => ({
                isEditState: !prevState.isEditState
            }));
        }

    }
    valueOnKeyPress(e:any){
        if (e.key == 'Enter') {

            var algorithmDataAjaxCallObj = new AlgorithmDataAjaxCall();
            var optionList:any[] = [];
            var option:{[key:string]:string}={};
            var item:{[key:string]:string}={};
            option["algorithmcode"] = this.props.primKey;
            item[this.props.id]=e.target.value;
            algorithmDataAjaxCallObj.update(
                [item],
                [option]
            );
            this.setState({
                value:e.target.value,
                isEditState:false

            });
        }
        if(e.keyCode === 27){
            this.setState({
                isEditState:false
            });
        }
    }
    render(){
        if(this.state.isEditState){
            return(
                <div onClick={this.valueOnclick}>
                    <input type="text" defaultValue={this.state.value} onKeyDown={this.valueOnKeyPress}/>
                </div>
            );
        }else{
            return(
                <div onClick={this.valueOnclick}>
                    {this.state.value}
                </div>
            );
        }
    }

}
class TableBody extends React.Component<{data:any[]},{}> {
    render() {
        const data = this.props.data.map(item => (
            <tr>
                <td><CheckBoxInTable algorithmcode={item.algorithmcode}/></td>
                <td><TableContents primKey={item.algorithmcode} id="algorithmname" value={item.algorithmname}/></td>
                <td className="number"><TableContents primKey={item.algorithmcode} id="version" value={item.version}/></td>
                <td><TableContents primKey={item.algorithmcode} id="fullpath" value={item.fullpath}/></td>
                <td><AvailableInTable available={item.available} primKey={item.algorithmcode} /></td>

            </tr>
        ));
        return data;
    }
}
class TableHeaderAndBox extends React.Component<{data:any[]},{isSelectallClicked:boolean}> {
    constructor(props:any){
        super(props);
        this.state = {
            isSelectallClicked:false
        }
        this.selectAllOnClick = this.selectAllOnClick.bind(this);

    }
    selectAllOnClick(){

        var checkboxs = document.getElementsByClassName("checkbox");
        if(this.state.isSelectallClicked==false){
            for(var i=0;i<this.props.data.length;i++){
                selectedList.push(this.props.data[i].algorithmcode);
            }
            for(var i=0;i<checkboxs.length;i++){
                checkboxs[i].checked=true;
            }
        }else{
            selectedList=[];
            for(var i=0;i<checkboxs.length;i++){
                checkboxs[i].checked=false;
            }
        }

        this.setState(prevState => ({
            isSelectallClicked: !prevState.isSelectallClicked
        }));

    }
    render() {
        return (
            <div className="table_box">
                <table className="table">
                    <thead className="thead">
                        <tr>
                            <th onClick={this.selectAllOnClick}>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("table_select_name")}
                            </th>
                            <th>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("algorithmtable_algorithmname_name")}
                            </th>
                            <th>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("algorithmtable_version_name")}
                            </th>
                            <th>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("algorithmtable_fullpath_name")}
                            </th>
                            <th>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("algorithmtable_available_name")}
                            </th>
                        </tr>
                    </thead>
                    <tbody className="tbody">
                        <TableBody
                            data={this.props.data}
                        />
                    </tbody>
                </table>
            </div>
        );
    }
}
class Addpopup extends React.Component<{open:boolean},{close:boolean}>{
    constructor(props:any){
        super(props);
        this.state={
            close:true
        }
        this.closeOnClick = this.closeOnClick.bind(this);
    }
    addOnClick(){
        var inputs = $(".addpopupBox :input");
        var item:{[key:string]:any} = {};
        var file;
        for(var i=0;i<inputs.length;i++){
            if(inputs[i].getAttribute("name")=="fullpath"){
                file = inputs[i].files[0];
            }else{
                item[inputs[i].getAttribute("name")] = inputs[i].value;
            }
        }
        item["algorithmcode"] = Date.now().toString();
        item["fullpath"] = properties["AlgorithmDownloadPath"]+item["algorithmcode"]+".py";
        if(item["available"]=="1"){
            item["available"] = true;
        }else{
            item["available"] = false;
        }
        var filereader = new FileReader();
        filereader.readAsText(file);
        filereader.onloadend = function(event){
            item["data"]=filereader.result;
            var algorithmDataAjaxCallObj = new AlgorithmDataAjaxCall();
            var algorithmDataAjaxResult = algorithmDataAjaxCallObj.create(
                [item],
                []
            );

            if(algorithmDataAjaxResult["return_code"]=="0000"){
                    alert("success");
            }else{
                alert(algorithmDataAjaxResult["cause"]);
            }
        };
        this.setState(prevState => ({
            close: !prevState.close
        }));

        location.reload();

    }
    closeOnClick(){
        this.setState(prevState => ({
            close: !prevState.close
        }));
    }

    render(){
        if(this.props.open!=this.state.close){
            return(
                <div>
                </div>
            );
        }
        else{
            return(
                <div className="addpopupBox">
                    <div className="addpopup_inside_Box">
                        <label>
                            {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                "algorithmtable_algorithmname_name"
                            )}
                            <input type="text" name="algorithmname" placeholder="name"></input>
                        </label>
                        <label>
                            {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                "algorithmtable_version_name"
                            )}
                            <input type="text" name="version" placeholder="version"></input>
                        </label>
                        <label>
                            {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                "algorithmtable_fullpath_name"
                            )}
                            <input type="file" name="fullpath"></input>
                        </label>
                        <label>
                            {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                "algorithmtable_available_name"
                            )}
                            <select name="available">
                                <option value="1">true</option>
                                <option value="0">false</option>
                            </select>
                        </label>

                        <div className="add_popup_button_group">
                            <button onClick={this.addOnClick}>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "table_selected_item_add_button"
                                )}
                            </button>
                            <button onClick={this.closeOnClick}>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "popup_close"
                                )}
                            </button>
                        </div>
                    </div>
                </div>
            );
        }

    }
}

class SearchBoxAndTable extends React.Component<{},{algorithmcode:string,algorithmname:string,available:string,version:string,fullpath:string,startDate:string,endDate:string,resultData:any[],currentPage:number,maxPage:number,perpage:number,isfilterOpen:boolean,isAddClicked:boolean}> {

    constructor(props:any) {
        super(props);
        this.state = {
            startDate: "",
            endDate: "",
            algorithmcode: "",
            algorithmname: "",
            available: "all",
            version: "",
            fullpath: "",
            currentPage: 1,
            maxPage: -1,
            perpage: 10,
            isfilterOpen: false,
            resultData:[],
            isAddClicked:false
        };

        this.getDataUsingRestCallFunc = this.getDataUsingRestCallFunc.bind(this);

        this.startDateOnChange = this.startDateOnChange.bind(this);
        this.endDateOnChange = this.endDateOnChange.bind(this);
        this.algorithmcodeOnChange = this.algorithmcodeOnChange.bind(this);

        this.searchOnclick = this.searchOnclick.bind(this);
        this.filterOnclick = this.filterOnclick.bind(this);
        this.filterCloseOnclick = this.filterCloseOnclick.bind(this);
        this.setFilterOnclick = this.setFilterOnclick.bind(this);
        this.addOnClick = this.addOnClick.bind(this);

        this.firstPageOnclick = this.firstPageOnclick.bind(this);
        this.lastPageOnclick = this.lastPageOnclick.bind(this);
        this.prePageOnclick = this.prePageOnclick.bind(this);
        this.nextPageOnclick = this.nextPageOnclick.bind(this);
        this.currentPageOnKeyPress = this.currentPageOnKeyPress.bind(this);
    }
    setFilterOnclick() {
        this.setState({ version: $("#version").val() as string });
        this.setState({ available: $("#available option:selected").val() as string });
        this.setState(prevState => ({
            isfilterOpen: !prevState.isfilterOpen
        }));
    }
    filterCloseOnclick() {
        this.setState(prevState => ({
            isfilterOpen: !prevState.isfilterOpen
        }));
    }
    filterOnclick() {
        this.setState(prevState => ({
            isfilterOpen: !prevState.isfilterOpen
        }));
    }
    getDataUsingRestCallFunc(page:number) {
        var algorithmDataResult;
        var option1:{[key:string]:any} = {};
        var option2:{[key:string]:any} = {};

        if (this.state.available != "all") {
            if(this.state.available=="true")
                option1["available"] = 1;
            else{
                option1["available"] = 0;
            }
        }
        if (this.state.fullpath != "") {
            option1["fullpath"] = this.state.fullpath;
        }
        if (this.state.version != "") {
            option1["version"] = this.state.version;
        }
        if (this.state.algorithmcode != "") {
            option1["algorithmcode"] = this.state.algorithmcode;
        }
        if (this.state.algorithmname != "") {
            option1["algorithmname"] = this.state.algorithmname;
        }
        if (this.state.startDate != "" && this.state.endDate != "") {
            option2["startdate"] = encoder.dateToYYMMDDHHMMSS(
                this.state.startDate + "000000"
            );
            option2["enddate"] = encoder.dateToYYMMDDHHMMSS(
                this.state.endDate + "235959"
            );
        }
        var algorithmDataAjaxCallObj = new AlgorithmDataAjaxCall();
        algorithmDataResult = algorithmDataAjaxCallObj.retrieve(
            [],
            [option1, option2, {}, { page: page, perpage: this.state.perpage }]
        );
        var logDataAjaxCallObj = new LogDataAjaxCall();
        logDataAjaxCallObj.create([properties["Algorithm_view_log"]],[]);
        return algorithmDataResult;
    }

    firstPageOnclick() {
        var filterDateResult;
        if (this.state.currentPage != 1) {
            filterDateResult = this.getDataUsingRestCallFunc(1);
            this.setState({
                resultData: filterDateResult["items"],
                currentPage: 1
            });

            $("#current_page").val(1);
        }
    }

    lastPageOnclick() {
        var filterDateResult;
        if (
            this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage
        ) {
            filterDateResult = this.getDataUsingRestCallFunc(this.state.maxPage);
            this.setState({
                resultData: filterDateResult["items"],
                currentPage: this.state.maxPage
            });

            $("#current_page").val(this.state.maxPage);
        }
    }
    prePageOnclick() {
        var filterDateResult;
        if (this.state.maxPage != -1 && this.state.currentPage > 1) {
            filterDateResult = this.getDataUsingRestCallFunc(
                this.state.currentPage - 1
            );
            this.setState({ resultData: filterDateResult["items"] });
            $("#current_page").val(this.state.currentPage - 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage - 1
            }));
        }
    }
    nextPageOnclick() {
        var filterDateResult;
        if (
            this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage
        ) {
            filterDateResult = this.getDataUsingRestCallFunc(
                this.state.currentPage + 1
            );
            this.setState({ resultData: filterDateResult["items"] });
            $("#current_page").val(this.state.currentPage + 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage + 1
            }));
        }
    }
    currentPageOnKeyPress(e:any) {
        if (e.which == 13) {
            var pageVal = $("#current_page").val() as string;
            var page = parseInt(pageVal);
            if (isNaN(page)) {
                alert(myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_is_not_number_alert"));
                $("#current_page").val(this.state.currentPage);
            } else {
                if (page > this.state.maxPage) {
                    alert(
                        myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_greater_than_max_alert")
                    );
                    $("#current_page").val(this.state.currentPage);
                } else {
                    if (
                        this.state.maxPage != -1 &&
                        this.state.currentPage != page
                    ) {
                        var algorithmDataResult = this.getDataUsingRestCallFunc(
                            page
                        );
                        this.setState({
                            resultData: algorithmDataResult["items"],
                            currentPage: page
                        });
                        $("#current_page").val(page);
                    }
                }
            }
        }
    }

    startDateOnChange(e:any) {
        this.setState({
            startDate: e.target.value
        });
    }
    endDateOnChange(e:any) {
        this.setState({
            endDate: e.target.value
        });
    }
    algorithmcodeOnChange(e:any) {
        this.setState({
            algorithmcode: e.target.value
        });
    }
    algorithmnameOnChange(e:any) {
        this.setState({
            algorithmname: e.target.value
        });
    }
    deleteOnClick(e:any){
        var algorithmDataAjaxCallObj = new AlgorithmDataAjaxCall();
        var optionList:any[] = [];
        for(var i=0;i<selectedList.length;i++){
            optionList.push({algorithmcode:selectedList[i]});
        }
        algorithmDataAjaxCallObj.delete(
            [],
            optionList
        );
        location.reload();
    }
    addOnClick(){
        this.setState(prevState => ({
            isAddClicked: !prevState.isAddClicked
        }));
    }

    searchOnclick() {
        var algorithmDateResult = this.getDataUsingRestCallFunc(1);

        this.setState({ resultData: algorithmDateResult["items"] });

        var maxpage = Math.ceil(algorithmDateResult["total"] / this.state.perpage);
        this.setState({ maxPage: maxpage });

        if (algorithmDateResult["items"].length == 0) {
            $("#current_page").val(0);
            $("#max_page").val(0);
            this.setState({ maxPage: 0, currentPage: 0 });
        } else {
            $("#current_page").val(1);
            $("#max_page").val(maxpage);
            this.setState({ maxPage: maxpage, currentPage: 1 });
        }

    }
    componentDidMount(){
        this.searchOnclick();
    }
    render() {
        const popup = (
            <div className="optional_settings_box">
                <div className="popup_close_button">
                    <i
                        className="fas fa-window-close"
                        onClick={this.filterCloseOnclick}
                    />
                </div>
                <div className="version_input_box">
                    <label>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("algorithmtable_version_name")}
                        <VersionBoxInPopup id="version" version={this.state.version} />
                    </label>
                </div>
                <div className="available_input_box">
                    <label>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("algorithmtable_available_name")}
                        <AvailableBoxInPopup available={this.state.available} id="available" />
                    </label>
                </div>
                <div className="filter_set_button">
                    <button
                        className="set_button"
                        onClick={this.setFilterOnclick}
                    >
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("filter_setting_button")}
                    </button>
                </div>
            </div>
        );
        if (this.state.isfilterOpen) {
            return (
                <div className="main_box">
                    <div className="current_page_info">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_setting_root_name")}
                        <i className="fas fa-angle-right"></i>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_algorithmsetting_name")}
                    </div>
                    <div>{popup}</div>
                    <div className="searchBox">
                        {/* <div className="algorithmcode_input_box">
                            <label>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "algorithmtable_algorithmcode_name"
                                )}
                                <input
                                    type="text"
                                    className="algorithmcode_input"
                                    id="algorithmcode"
                                    onChange={this.algorithmcodeOnChange}
                                    defaultValue={this.state.algorithmcode}
                                />
                            </label>
                        </div> */}
                        <div className="algorithmname_input_box">
                            <label>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "algorithmtable_algorithmname_name"
                                )}
                                <input
                                    type="text"
                                    className="algorithmname_input"
                                    id="algorithmname"
                                    onChange={this.algorithmnameOnChange}
                                    defaultValue={this.state.algorithmname}
                                />
                            </label>
                        </div>
                        {/* <div className="date_input_box">
                            <div className="start_date_input_box">
                                <label>
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "imagefiletable_startdate_label"
                                    )}
                                    <input
                                        type="date"
                                        className="start_date_input"
                                        onChange={this.startDateOnChange}
                                        defaultValue={this.state.startDate}
                                    />
                                </label>
                            </div>
                            <div className="end_date_input_box">
                                <label>
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "imagefiletable_enddate_label"
                                    )}
                                    <input
                                        type="date"
                                        className="end_date_input"
                                        onChange={this.endDateOnChange}
                                        defaultValue={this.state.endDate}
                                    />
                                </label>
                            </div>
                        </div> */}
                        <div className="button_box">
                            <div className="set_specific_filter_button_box">
                                <button
                                    className="set_specific_filter_button"
                                    onClick={this.filterOnclick}
                                >
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "imagefiletable_set_specific_filter_button_box_name"
                                    )}
                                </button>
                            </div>
                            <div className="search_button_box">
                                <button
                                    className="searchButton"
                                    onClick={this.searchOnclick}
                                >
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "dashtable_search_button_name"
                                    )}
                                </button>
                            </div>
                            <div>
                                <button className="items_delete_button" onClick={this.deleteOnClick}>{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "table_selected_item_delete_button"
                                )}</button>
                            </div>
                            <div>
                                <button className="items_add_button" onClick={this.addOnClick}>{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "table_selected_item_add_button"
                                )}</button>
                                <Addpopup open={this.state.isAddClicked} />

                            </div>
                        </div>
                    </div>

                    <div>
                        <TableHeaderAndBox
                            data={this.state.resultData}
                        />
                    </div>
                    <div className="page_box">
                        <div className="pre_page_box">
                            <i
                                className="fas fa-angle-double-left"
                                onClick={this.firstPageOnclick}
                            />
                            <i
                                className="fas fa-angle-left"
                                onClick={this.prePageOnclick}
                            />
                        </div>
                        <div className="page_input_box">
                            <input
                                type="text"
                                className="current_page_input"
                                id="current_page"
                                onKeyPress={this.currentPageOnKeyPress}
                            />
                            /
                            <input
                                type="text"
                                className="max_page_input"
                                readOnly="true"
                                id="max_page"
                            />
                        </div>
                        <div className="post_page_box">
                            <i
                                className="fas fa-angle-right"
                                onClick={this.nextPageOnclick}
                            />
                            <i
                                className="fas fa-angle-double-right"
                                onClick={this.lastPageOnclick}
                            />
                        </div>
                    </div>

                </div>
            );
        } else {
            return (
                <div className="main_box">
                    <div className="current_page_info">
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_setting_root_name")}
                        <i className="fas fa-angle-right"></i>
                        {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_algorithmsetting_name")}
                    </div>
                    <div className="searchBox">
                        {/* <div className="algorithmcode_input_box">
                            <label>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "algorithmtable_algorithmcode_name"
                                )}
                                <input
                                    type="text"
                                    className="algorithmcode_input"
                                    id="algorithmcode"
                                    onChange={this.algorithmcodeOnChange}
                                    defaultValue={this.state.algorithmcode}
                                />
                            </label>
                        </div> */}
                        <div className="algorithmname_input_box">
                            <label>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "algorithmtable_algorithmname_name"
                                )}
                                <input
                                    type="text"
                                    className="algorithmname_input"
                                    id="algorithmname"
                                    onChange={this.algorithmnameOnChange}
                                    defaultValue={this.state.algorithmname}
                                />
                            </label>
                        </div>
                        {/* <div className="date_input_box">
                            <div className="start_date_input_box">
                                <label>
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "imagefiletable_startdate_label"
                                    )}
                                    <input
                                        type="date"
                                        className="start_date_input"
                                        onChange={this.startDateOnChange}
                                        defaultValue={this.state.startDate}
                                    />
                                </label>
                            </div>
                            <div className="end_date_input_box">
                                <label>
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "imagefiletable_enddate_label"
                                    )}
                                    <input
                                        type="date"
                                        className="end_date_input"
                                        onChange={this.endDateOnChange}
                                        defaultValue={this.state.endDate}
                                    />
                                </label>
                            </div>
                        </div> */}
                        <div className="button_box">
                            <div className="set_specific_filter_button_box">
                                <button
                                    className="set_specific_filter_button"
                                    onClick={this.filterOnclick}
                                >
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "imagefiletable_set_specific_filter_button_box_name"
                                    )}
                                </button>
                            </div>
                            <div className="search_button_box">
                                <button
                                    className="searchButton"
                                    onClick={this.searchOnclick}
                                >
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "dashtable_search_button_name"
                                    )}
                                </button>
                            </div>
                            <div>
                                <button className="items_delete_button" onClick={this.deleteOnClick}>{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "table_selected_item_delete_button"
                                )}</button>
                            </div>
                            <div>
                                <button className="items_add_button" onClick={this.addOnClick}>{myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "table_selected_item_add_button"
                                )}</button>
                                <Addpopup open={this.state.isAddClicked} />

                            </div>
                        </div>
                    </div>

                    <div>
                        <TableHeaderAndBox
                            data={this.state.resultData}
                        />
                    </div>
                    <div className="page_box">
                        <div className="pre_page_box">
                            <i
                                className="fas fa-angle-double-left"
                                onClick={this.firstPageOnclick}
                            />
                            <i
                                className="fas fa-angle-left"
                                onClick={this.prePageOnclick}
                            />
                        </div>
                        <div className="page_input_box">
                            <input
                                type="text"
                                className="current_page_input"
                                id="current_page"
                                onKeyPress={this.currentPageOnKeyPress}
                            />
                            <div className="slash">
                            </div>
                            <input
                                type="text"
                                className="max_page_input"
                                readOnly="true"
                                id="max_page"
                            />
                        </div>
                        <div className="post_page_box">
                            <i
                                className="fas fa-angle-right"
                                onClick={this.nextPageOnclick}
                            />
                            <i
                                className="fas fa-angle-double-right"
                                onClick={this.lastPageOnclick}
                            />
                        </div>
                    </div>
                </div>
            );
        }
    }
}

ReactDOM.render(
    <div>
        <SearchBoxAndTable />
    </div>,
    document.getElementById("main")
);
