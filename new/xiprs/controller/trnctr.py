﻿# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 10:56:25 2018

@author: ryuk

@des :  -DB 이미지파일정보에 대한 REST API를 설정한다.
"""

from flask import request, json
from flask_restful import Resource
from datetime import datetime

from ..model.trnmdl import *
from ..helper.util import converter
from ..helper.messages import *
from .. import db
from ..configs.config import *

import sqlalchemy as sa

def create_training_info(item):
    db.session.add(item)

class TrainingInfoRESTApi(Resource):

    def post(self):
        request_data = request.get_json()
        method_type = request_data.get('method_type')
        items = request_data.get('items')

        if method_type is not None:
            if items is not None:
                if method_type == 'view':
                    return view_processing(request_data)
                elif method_type == 'insert':
                    return insert_processing(items)
                elif method_type == 'update':
                    return update_processing(request_data)
                elif method_type == 'delete':
                    return delete_processing(request_data)
                else:
                    pass
            else:
                message = COMMON_ITEMS_NOT_FOUND
                return json.dumps(message, ensure_ascii=True)
        else:
            message = COMMON_METHOD_TYPE_NOT_FOUND
            message.update({'items': request.get_json().get('items')})
            return json.dumps(message, ensure_ascii=True)


def insert_processing(items):

    try:
        if type(items) == list:
            for item in items:
                training_info = TrainingInfo(item.get('modelcode'),item.get('modelname'),
                                           item.get('status'), item.get('accuracy'), item.get('filter'),
                                           item.get('algorithm'), item.get('user'),
                                           item.get('contents'))

                create_training_info(training_info)
            db.session.commit()

            message = TRN_CREATE_SUCCESS
            message.update({'items': items})
            return json.dumps(message, ensure_ascii=True)

    except Exception as e:
        db.session.rollback()
        db.session.remove()
        message = TRN_CREATE_SUCCESS_FAIL

        message.update({'cause': str(e)})
        message.update({'items': request.get_json()['items']})
        return json.dumps(message, ensure_ascii=True)

def update_processing(request_data):
    try:
        items = request_data.get('items')
        options = request_data.get('options')
        print(items)
        print(options)
        if items is not None and options is not None:
            if type(items) == list:
                len_val = len(items)
                for i in range (0,len(items)):
                    query_input_element = options[i]
                    query_result = db.session.query(TrainingInfo).filter_by(**query_input_element).all()
                    update_element = items[i]
                    for query_result_element in query_result:
                        for update_element_key in update_element:
                            setattr(query_result_element, update_element_key, update_element[update_element_key])
                db.session.commit()

                message = TRN_UPDATE_SUCCESS
                message.update({'items': items})
                return json.dumps(message, ensure_ascii=True)
        else:
            message = COMMON_ITEMS_NOT_FOUND
            return json.dumps(message, ensure_ascii=True)

    except Exception as e:
        print(str(e))
        db.session.rollback()
        db.session.remove()
        message = TRN_UPDATE_SUCCESS_FAIL
        message.update({'cause': str(e)})
        message.update({'items': request.get_json()['items']})
        return json.dumps(message, ensure_ascii=True)


def view_processing(request_data):
    try:
        items = request_data.get('items')
        options = request_data.get('options')

        if len(options) > 0:
            if len(options) == 1:
                result = db.session.query(TrainingInfo).filter_by(**options[0]).all()
            elif len(options) == 2:
                start_date = options[1].get('start_regdate')
                end_date = options[1].get('end_regdate')
                if not start_date is None and not end_date is None:
                    start_date = datetime.strptime(start_date, "%Y%m%d%H%M%S")
                    end_date = datetime.strptime(end_date, "%Y%m%d%H%M%S")
                    result = db.session.query(TrainingInfo).filter_by(**options[0]).filter(
                        TrainingInfo.regdate >= start_date, TrainingInfo.regdate <= end_date).all()
            elif len(options) == 3:
                orderby = options[2].get('orderby')
                if not orderby is None:
                    start_date = options[1].get('start_regdate')
                    end_date = options[1].get('end_regdate')
                    if not start_date is None and not end_date is None:
                        start_date = datetime.strptime(start_date, "%Y%m%d%H%M%S")
                        end_date = datetime.strptime(end_date, "%Y%m%d%H%M%S")
                        result = db.session.query(TrainingInfo).filter_by(**options[0]).filter(
                            TrainingInfo.regdate >= start_date, TrainingInfo.regdate <= end_date).order_by(
                            orderby).all()
                    else:
                        result = db.session.query(TrainingInfo).filter_by(**options[0]).order_by(orderby).all()
            elif len(options) == 4:
                page = options[3].get('page')
                per_page = options[3].get('perpage')
                orderby = options[2].get('orderby')
                start_date = options[1].get('start_regdate')
                end_date = options[1].get('end_regdate')
                if not orderby is None:
                    if not start_date is None and not end_date is None:
                        start_date = datetime.strptime(start_date, "%Y%m%d%H%M%S")
                        end_date = datetime.strptime(end_date, "%Y%m%d%H%M%S")
                        result = db.session.query(TrainingInfo).filter_by(**options[0]).filter(
                            TrainingInfo.regdate >= start_date, TrainingInfo.regdate <= end_date).order_by(
                            orderby).paginate(int(page), int(per_page))
                    else:
                        result = db.session.query(TrainingInfo).filter_by(**options[0]).order_by(orderby).paginate(int(page), int(per_page))
                else:
                    if not start_date is None and not end_date is None:
                        start_date = datetime.strptime(start_date, "%Y%m%d%H%M%S")
                        end_date = datetime.strptime(end_date, "%Y%m%d%H%M%S")
                        result = db.session.query(TrainingInfo).filter_by(**options[0]).filter(
                            TrainingInfo.regdate >= start_date, TrainingInfo.regdate <= end_date).paginate(int(page), int(per_page))
                    else:
                        result = db.session.query(TrainingInfo).filter_by(**options[0]).paginate(int(page), int(per_page))
            elif len(options) == 5:
                page = options[3].get('page')
                per_page = options[3].get('perpage')
                orderby = options[2].get('orderby')
                start_date = options[1].get('start_regdate')
                end_date = options[1].get('end_regdate')
                start_accuracy = options[4].get('start_accuracy')
                end_accuracy = options[4].get('end_accuracy')
                if not orderby is None:
                    if not start_date is None and not end_date is None:
                        if not start_accuracy is None and not end_accuracy is None:
                            start_date = datetime.strptime(start_date, "%Y%m%d%H%M%S")
                            end_date = datetime.strptime(end_date, "%Y%m%d%H%M%S")
                            result = db.session.query(TrainingInfo).filter_by(**options[0]).filter(
                                TrainingInfo.regdate >= start_date, TrainingInfo.regdate <= end_date).filter(
                                    TrainingInfo.accuracy >= start_accuracy, TrainingInfo.accuracy <= end_accuracy).order_by(
                                orderby).paginate(int(page), int(per_page))
                        else:
                            start_date = datetime.strptime(start_date, "%Y%m%d%H%M%S")
                            end_date = datetime.strptime(end_date, "%Y%m%d%H%M%S")
                            result = db.session.query(TrainingInfo).filter_by(**options[0]).filter(
                                TrainingInfo.regdate >= start_date, TrainingInfo.regdate <= end_date).order_by(
                                orderby).paginate(int(page), int(per_page))
                    else:
                        if not start_accuracy is None and not end_accuracy is None:
                            result = db.session.query(TrainingInfo).filter_by(**options[0]).filter(
                                    TrainingInfo.accuracy >= start_accuracy, TrainingInfo.accuracy <= end_accuracy).order_by(
                                orderby).paginate(int(page), int(per_page))
                        else:
                            result = db.session.query(TrainingInfo).filter_by(**options[0]).order_by(
                                orderby).paginate(int(page), int(per_page))

                else:
                    if not start_date is None and not end_date is None:
                        if not start_accuracy is None and not end_accuracy is None:
                            start_date = datetime.strptime(start_date, "%Y%m%d%H%M%S")
                            end_date = datetime.strptime(end_date, "%Y%m%d%H%M%S")
                            result = db.session.query(TrainingInfo).filter_by(**options[0]).filter(
                                TrainingInfo.regdate >= start_date, TrainingInfo.regdate <= end_date).filter(
                                    TrainingInfo.accuracy >= start_accuracy, TrainingInfo.accuracy <= end_accuracy).paginate(int(page), int(per_page))
                        else:
                            start_date = datetime.strptime(start_date, "%Y%m%d%H%M%S")
                            end_date = datetime.strptime(end_date, "%Y%m%d%H%M%S")
                            result = db.session.query(TrainingInfo).filter_by(**options[0]).filter(
                                TrainingInfo.regdate >= start_date, TrainingInfo.regdate <= end_date).paginate(int(page), int(per_page))
                    else:
                        if not start_accuracy is None and not end_accuracy is None:
                            result = db.session.query(TrainingInfo).filter_by(**options[0]).filter(
                                    TrainingInfo.accuracy >= start_accuracy, TrainingInfo.accuracy <= end_accuracy).paginate(int(page), int(per_page))
                        else:
                            result = db.session.query(TrainingInfo).filter_by(**options[0]).paginate(int(page), int(per_page))
            else:
                pass

            if len(options) >= 4 :
                return json.dumps({'items': [item.__dict__ for item in result.items], 'total': result.total,
                                   'return_code': TRN_UPDATE_SUCCESS.get('return_code'),
                                   'message': TRN_UPDATE_SUCCESS.get('message')}, default=converter)
            else:
                return json.dumps({'items': [item.__dict__ for item in result],
                                   'return_code': TRN_UPDATE_SUCCESS.get('return_code'),
                                   'message': TRN_UPDATE_SUCCESS.get('message')}, default=converter)
        else:
            result = db.session.query(TrainingInfo).all()
            return json.dumps({'items': [item.__dict__ for item in result],
                       'return_code': TRN_UPDATE_SUCCESS.get('return_code'),
                       'message': TRN_UPDATE_SUCCESS.get('message')}, default=converter)

    except Exception as e:
        message = TRN_UPDATE_SUCCESS_FAIL
        message.update({'cause': str(e)})
        message.update({'items': request_data['items']})
        return json.dumps(message, ensure_ascii=True)

def delete_processing(request_data):
    
    try:
        options = request_data.get('options')

        for option in options:
            del_data = db.session.query(TrainingInfo).filter_by(**option).first()
            db.session.delete(del_data)
            db.session.commit()


        message = LINE_DELETE_SUCCESS


        log_info = LogInfo(TRN_DELETE_LOG["kind"],TRN_DELETE_LOG["title"], TRN_DELETE_LOG["contents"]+" by "+request_data.get('userid'))
        create_log(log_info)
        db.session.commit()


        return json.dumps(message, ensure_ascii=True)

    except Exception as e:
        db.session.rollback()
        db.session.remove()
        message = LINE_DELETE_SUCCESS_FAIL
        message.update({'cause': str(e)})
        return json.dumps(message, ensure_ascii=True)
