# -*- coding: utf-8 -*-
"""
Created on Tue Feb  6 10:56:25 2018

@author: ryuk

@des :  -DB 이미지파일정보에 대한 REST API를 설정한다.
"""

from flask import request, json
from flask_restful import Resource
from datetime import datetime
from ..helper.util import converter

from ..model.sttmdl import *
from ..helper.messages import *
from .. import db


class StatisticRESTApi(Resource):

    def post(self):

        request_data = request.get_json()
        method_type = request_data.get('method_type')
        items = request_data.get('items')

        if method_type is not None:
            if items is not None:
                if method_type == 'view':
                    return view_processing(request_data)
                else:
                    pass
            else:
                message = COMMON_ITEMS_NOT_FOUND
                return json.dumps(message, ensure_ascii=True)
        else:
            message = COMMON_METHOD_TYPE_NOT_FOUND
            message.update({'items': request.get_json().get('items')})
            return json.dumps(message, ensure_ascii=True)

def view_processing(request_data):
    try:
        items = request_data.get('items')
        options = request_data.get('options')

        if len(options) > 0:
            filter_params = make_filter_params(options[0])
            if len(options) == 1:
                result = db.session.query(StatisticInfo).filter_by(**filter_params).all()
            elif len(options) == 2:
                start_date = options[1].get('startdate')
                end_date = options[1].get('enddate')

                if not start_date is None and not end_date is None:
                    start_date = datetime.strptime(start_date, "%Y%m%d%H%M%S")
                    end_date = datetime.strptime(end_date, "%Y%m%d%H%M%S")
                    result = db.session.query(StatisticInfo).filter_by(**filter_params).filter(
                        StatisticInfo.date >= start_date, StatisticInfo.date <= end_date).all()
            elif len(options) == 3:
                orderby = options[2].get('orderby')
                if not orderby is None:
                    result = db.session.query(StatisticInfo).filter_by(**filter_params).order_by(orderby).all()
            elif len(options) == 4:
                page = options[3].get('page')
                per_page = options[3].get('perpage')
                orderby = options[2].get('orderby')
                if not orderby is None:
                    result = db.session.query(StatisticInfo).filter_by(**filter_params).order_by(orderby).paginate(int(page), int(per_page))
                else:
                    result = db.session.query(StatisticInfo).filter_by(**filter_params).paginate(int(page), int(per_page))
            else:
                pass

            if len(options) == 4:
                return json.dumps({'items': [item.__dict__ for item in result.items], 'total': result.total,
                                   'return_code': STATISTICS_VIEW_SUCCESS.get('return_code'),
                                   'message': STATISTICS_VIEW_SUCCESS.get('message')}, default=converter)
            else:
                return json.dumps({'items': [item.__dict__ for item in result],
                                   'return_code': STATISTICS_VIEW_SUCCESS.get('return_code'),
                                   'message': STATISTICS_VIEW_SUCCESS.get('message')}, default=converter)
        else:
            return json.dumps({'items': [item.__dict__ for item in result],
                       'return_code': STATISTICS_VIEW_SUCCESS.get('return_code'),
                       'message': STATISTICS_VIEW_SUCCESS.get('message')}, default=converter)

    except Exception as e:
        message = STATISTICS_VIEW_SUCCESS_FAIL
        message.update({'cause': str(e)})
        message.update({'items': request_data['items']})
        return json.dumps(message, ensure_ascii=True)


def make_filter_params(item):
    filter_params = {}
    if not item.get('linecode') is None:
        filter_params['linecode'] = item.get('linecode')

    if not item.get('date') is None:
        filter_params['date'] = str(datetime.strptime(item.get('date'), "%Y%m%d%H%M%S"))

    if not item.get('ngcount') is None:
        filter_params['ngcount'] = item.get('ngcount')

    if not item.get('okcount') is None:
        filter_params['okcount'] = item.get('okcount')

    if not item.get('nrcount') is None:
        filter_params['nrcount'] = item.get('nrcount')

    if not item.get('kind') is None:
        filter_params['kind'] = item.get('kind')

    return filter_params
