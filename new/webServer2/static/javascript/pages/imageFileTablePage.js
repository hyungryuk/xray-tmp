"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const ReactDOM = require("react-dom");
const header_1 = require("../modulesForPages/header");
const inversify_config_1 = require("../DIContainerConf/inversify.config");
const types_1 = require("../DIContainerConf/types");
const dbRestcall_1 = require("../others/restCall/dbRestcall");
const encoder_1 = require("../others/encoder/encoder");
const properties_1 = require("../properties/properties");
var selectedList = [];
ReactDOM.render(React.createElement(header_1.HeaderComponent, null), document.getElementById("header"));
class LineNameInTable extends React.Component {
    constructor(props) {
        super(props);
        this.lineCodeToFactoryAndLineName = this.lineCodeToFactoryAndLineName.bind(this);
    }
    lineCodeToFactoryAndLineName() {
        for (var i = 0; i < this.props.lineList.length; i++) {
            if (this.props.lineList[i].linecode == this.props.linecode) {
                return (this.props.lineList[i].factory +
                    " " +
                    this.props.lineList[i].line);
            }
        }
    }
    render() {
        return React.createElement("div", null, this.lineCodeToFactoryAndLineName());
    }
}
class LotnoInPopup extends React.Component {
    constructor(props) {
        super(props);
        this.initDate = this.initDate.bind(this);
    }
    initDate() {
        $("#" + this.props.id).val(this.props.lotno);
    }
    componentDidMount() {
        this.initDate();
    }
    render() {
        return React.createElement("input", { type: "text", className: "lotno_input", id: this.props.id });
    }
}
class CheckBoxInTable extends React.Component {
    constructor(props) {
        super(props);
        this.checkBoxOnChange = this.checkBoxOnChange.bind(this);
    }
    checkBoxOnChange(e) {
        if (e.target.checked == true) {
            selectedList.push({ filename: this.props.filename, filedate: this.props.filedate });
        }
        else {
            var index = selectedList.indexOf({ filename: this.props.filename, filedate: this.props.filedate });
            selectedList.splice(index, 1);
        }
    }
    render() {
        return (React.createElement("input", { type: "checkbox", onChange: this.checkBoxOnChange, className: "checkbox" }));
    }
}
class NgSelectBoxInPopup extends React.Component {
    render() {
        if (this.props.ng == "-1") {
            return (React.createElement("select", { className: "ng_input", id: this.props.id },
                React.createElement("option", { value: "-1" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("select_box_default_option")),
                React.createElement("option", { value: "0" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ok_name")),
                React.createElement("option", { value: "1" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_name")),
                React.createElement("option", { value: "2" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_nr_name"))));
        }
        if (this.props.ng == "0") {
            return (React.createElement("select", { className: "ng_input", id: this.props.id },
                React.createElement("option", { value: "-1" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("select_box_default_option")),
                React.createElement("option", { value: "0", selected: true }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ok_name")),
                React.createElement("option", { value: "1" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_name")),
                React.createElement("option", { value: "2" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_nr_name"))));
        }
        if (this.props.ng == "1") {
            return (React.createElement("select", { className: "ng_input", id: this.props.id },
                React.createElement("option", { value: "-1" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("select_box_default_option")),
                React.createElement("option", { value: "0" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ok_name")),
                React.createElement("option", { value: "1", selected: true }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_name")),
                React.createElement("option", { value: "2" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_nr_name"))));
        }
        if (this.props.ng == "2") {
            return (React.createElement("select", { className: "ng_input", id: this.props.id },
                React.createElement("option", { value: "-1" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("select_box_default_option")),
                React.createElement("option", { value: "0" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ok_name")),
                React.createElement("option", { value: "1" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_name")),
                React.createElement("option", { value: "2", selected: true }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_nr_name"))));
        }
    }
}
class NgToString extends React.Component {
    render() {
        if (this.props.ng == 0) {
            return React.createElement("td", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ok_name"));
        }
        if (this.props.ng == 1) {
            return React.createElement("td", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_name"));
        }
        if (this.props.ng == 2) {
            return React.createElement("td", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_nr_name"));
        }
    }
}
class Tbody extends React.Component {
    render() {
        console.log(this.props.data);
        const data = this.props.data.map(item => (React.createElement("tr", null,
            React.createElement("td", null,
                React.createElement(CheckBoxInTable, { filedate: item.filedate, filename: item.filename })),
            React.createElement("td", null, item.filename),
            React.createElement("td", null,
                React.createElement(LineNameInTable, { linecode: item.linecode, lineList: this.props.lineList })),
            React.createElement("td", null, item.filepath),
            React.createElement("td", { className: "number" }, item.lotno),
            React.createElement(NgToString, { ng: item.ng_by_user }),
            React.createElement(NgToString, { ng: item.ng_by_mes }),
            React.createElement(NgToString, { ng: item.ng }),
            React.createElement("td", null, item.filedate))));
        return data;
    }
}
class TableHeaderAndBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isSelectallClicked: false
        };
        this.selectAllOnClick = this.selectAllOnClick.bind(this);
    }
    selectAllOnClick() {
        var checkboxs = document.getElementsByClassName("checkbox");
        if (this.state.isSelectallClicked == false) {
            for (var i = 0; i < this.props.data.length; i++) {
                selectedList.push({ filename: this.props.data[i].filename, filedate: this.props.data[i].filedate });
            }
            for (var i = 0; i < checkboxs.length; i++) {
                checkboxs[i].checked = true;
            }
        }
        else {
            selectedList = [];
            for (var i = 0; i < checkboxs.length; i++) {
                checkboxs[i].checked = false;
            }
        }
        this.setState(prevState => ({
            isSelectallClicked: !prevState.isSelectallClicked
        }));
    }
    render() {
        return (React.createElement("div", { className: "table_box" },
            React.createElement("table", { className: "table" },
                React.createElement("thead", { className: "thead" },
                    React.createElement("tr", null,
                        React.createElement("th", { onClick: this.selectAllOnClick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("table_select_name")),
                        React.createElement("th", { className: "table_filename" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_filename_table")),
                        React.createElement("th", { className: "table_line" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_line_table")),
                        React.createElement("th", { className: "table_filepath" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_filepath_table")),
                        React.createElement("th", { className: "table_lotno" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_lotno_table")),
                        React.createElement("th", { className: "table_ng" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_by_user_table")),
                        React.createElement("th", { className: "table_ng" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_by_mes_table")),
                        React.createElement("th", { className: "table_ng" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_table")),
                        React.createElement("th", { className: "table_filedate" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_filedate_table")))),
                React.createElement("tbody", { className: "tbody" },
                    React.createElement(Tbody, { lineList: this.props.lineList, data: this.props.data })))));
    }
}
class LineOptions extends React.Component {
    componentDidMount() {
        let selectorObj = inversify_config_1.myContainer.get(types_1.TYPES.LineSelectorInterface);
        selectorObj.setDivId("lineSelector");
        selectorObj.setDatas(this.props.lineList);
        selectorObj.makeSelector();
        $("#lineSelector").val(this.props.line);
    }
    render() {
        return (React.createElement("option", { value: "all" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_line_selector_option_all_name")));
    }
}
class SearchBoxAndTable extends React.Component {
    constructor(props) {
        super(props);
        var lineAjaxCall = new dbRestcall_1.LineAjaxCall();
        var lineList = lineAjaxCall.retrieve([], [])["items"];
        this.state = {
            startDate: "",
            endDate: "",
            fileName: "",
            line: "all",
            resultData: [],
            lotno: "",
            lineList: lineList,
            currentPage: 1,
            maxPage: -1,
            perpage: 10,
            isfilterOpen: false,
            ng: "-1",
            ng_by_user: "-1",
            ng_by_mes: "-1"
        };
        this.getDataUsingRestCallFunc = this.getDataUsingRestCallFunc.bind(this);
        this.startDateOnChange = this.startDateOnChange.bind(this);
        this.endDateOnChange = this.endDateOnChange.bind(this);
        this.fileNameOnChange = this.fileNameOnChange.bind(this);
        this.lineSelectOnChange = this.lineSelectOnChange.bind(this);
        this.lotnoOnChange = this.lotnoOnChange.bind(this);
        this.searchOnclick = this.searchOnclick.bind(this);
        this.filterOnclick = this.filterOnclick.bind(this);
        this.filterCloseOnclick = this.filterCloseOnclick.bind(this);
        this.setFilterOnclick = this.setFilterOnclick.bind(this);
        this.initFilterBox = this.initFilterBox.bind(this);
        this.firstPageOnclick = this.firstPageOnclick.bind(this);
        this.lastPageOnclick = this.lastPageOnclick.bind(this);
        this.prePageOnclick = this.prePageOnclick.bind(this);
        this.nextPageOnclick = this.nextPageOnclick.bind(this);
        this.currentPageOnKeyPress = this.currentPageOnKeyPress.bind(this);
    }
    initFilterBox() {
        $("#lotno").val(this.state.lotno);
        $("#ng option:selected").val(this.state.ng);
        $("#ng_by_user option:selected").val(this.state.ng_by_user);
        $("#ng_by_mes option:selected").val(this.state.ng_by_mes);
    }
    setFilterOnclick() {
        this.setState({ lotno: $("#lotno").val() });
        this.setState({ ng: $("#ng option:selected").val() });
        this.setState({ ng_by_user: $("#ng_by_user option:selected").val() });
        this.setState({ ng_by_mes: $("#ng_by_mes option:selected").val() });
        this.setState(prevState => ({
            isfilterOpen: !prevState.isfilterOpen
        }));
    }
    filterCloseOnclick() {
        this.setState(prevState => ({
            isfilterOpen: !prevState.isfilterOpen
        }));
    }
    filterOnclick() {
        this.setState(prevState => ({
            isfilterOpen: !prevState.isfilterOpen
        }));
    }
    getDataUsingRestCallFunc(page) {
        var imageFileResult;
        var option1 = {};
        var option2 = {};
        if (this.state.line != "all") {
            option1["linecode"] = this.state.line;
        }
        if (this.state.fileName != "") {
            option1["filename"] = this.state.fileName;
        }
        if (this.state.lotno != "") {
            option1["lotno"] = this.state.lotno;
        }
        if (this.state.ng != "-1") {
            option1["ng"] = this.state.ng;
        }
        if (this.state.ng_by_user != "-1") {
            option1["ng_by_user"] = this.state.ng_by_user;
        }
        if (this.state.ng_by_mes != "-1") {
            option1["ng_by_mes"] = this.state.ng_by_mes;
        }
        if (this.state.startDate != "" && this.state.endDate != "") {
            option2["startdate"] = encoder_1.encoder.dateToYYMMDDHHMMSS(this.state.startDate + "000000");
            option2["enddate"] = encoder_1.encoder.dateToYYMMDDHHMMSS(this.state.endDate + "235959");
        }
        option1["isremoved"] = 0;
        var imageFileAjaxCall = new dbRestcall_1.ImageFileAjaxCall();
        imageFileResult = imageFileAjaxCall.retrieve([], [option1, option2, {}, { page: page, perpage: this.state.perpage }]);
        var logDataAjaxCallObj = new dbRestcall_1.LogDataAjaxCall();
        logDataAjaxCallObj.create([properties_1.properties["ImageFile_view_log"]], []);
        console.log(imageFileResult);
        return imageFileResult;
    }
    firstPageOnclick() {
        var imageFileResult;
        if (this.state.currentPage != 1) {
            imageFileResult = this.getDataUsingRestCallFunc(1);
            this.setState({
                resultData: imageFileResult["items"],
                currentPage: 1
            });
            $("#current_page").val(1);
        }
    }
    lastPageOnclick() {
        var imageFileResult;
        if (this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage) {
            imageFileResult = this.getDataUsingRestCallFunc(this.state.maxPage);
            this.setState({
                resultData: imageFileResult["items"],
                currentPage: this.state.maxPage
            });
            $("#current_page").val(this.state.maxPage);
        }
    }
    prePageOnclick() {
        var imageFileResult;
        if (this.state.maxPage != -1 && this.state.currentPage > 1) {
            imageFileResult = this.getDataUsingRestCallFunc(this.state.currentPage - 1);
            this.setState({ resultData: imageFileResult["items"] });
            $("#current_page").val(this.state.currentPage - 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage - 1
            }));
        }
    }
    nextPageOnclick() {
        var imageFileResult;
        if (this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage) {
            imageFileResult = this.getDataUsingRestCallFunc(this.state.currentPage + 1);
            this.setState({ resultData: imageFileResult["items"] });
            $("#current_page").val(this.state.currentPage + 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage + 1
            }));
        }
    }
    currentPageOnKeyPress(e) {
        if (e.which == 13) {
            var pageVal = $("#current_page").val();
            var page = parseInt(pageVal);
            if (isNaN(page)) {
                alert(inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_is_not_number_alert"));
                $("#current_page").val(this.state.currentPage);
            }
            else {
                if (page > this.state.maxPage) {
                    alert(inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_greater_than_max_alert"));
                    $("#current_page").val(this.state.currentPage);
                }
                else {
                    if (this.state.maxPage != -1 &&
                        this.state.currentPage != page) {
                        var imageFileResult = this.getDataUsingRestCallFunc(page);
                        this.setState({
                            resultData: imageFileResult["items"],
                            currentPage: page
                        });
                        $("#current_page").val(page);
                    }
                }
            }
        }
    }
    startDateOnChange(e) {
        this.setState({
            startDate: e.target.value
        });
    }
    endDateOnChange(e) {
        this.setState({
            endDate: e.target.value
        });
    }
    fileNameOnChange() {
        this.setState({
            fileName: $("#fileName").val()
        });
    }
    lineSelectOnChange() {
        this.setState({
            line: $("#lineSelector option:selected").val()
        });
    }
    lotnoOnChange() {
        this.setState({
            lotno: $("#lotno").val()
        });
    }
    deleteOnClick(e) {
        var imageFileAjaxCallObj = new dbRestcall_1.ImageFileAjaxCall();
        var optionList = [];
        var itemList = [];
        for (var i = 0; i < selectedList.length; i++) {
            optionList.push({ filename: selectedList[i].filename, filepath: selectedList[i].filepath });
            itemList.push({ isremoved: 1 });
        }
        imageFileAjaxCallObj.delete(itemList, optionList);
        location.reload();
    }
    searchOnclick() {
        var imageFileResult = this.getDataUsingRestCallFunc(1);
        this.setState({ resultData: imageFileResult["items"] });
        var maxpage = Math.ceil(imageFileResult["total"] / this.state.perpage);
        this.setState({ maxPage: maxpage });
        if (imageFileResult["items"].length == 0) {
            $("#current_page").val(0);
            $("#max_page").val(0);
            this.setState({ maxPage: 0, currentPage: 0 });
        }
        else {
            $("#current_page").val(1);
            $("#max_page").val(maxpage);
            this.setState({ maxPage: maxpage, currentPage: 1 });
        }
    }
    componentDidMount() {
        this.searchOnclick();
    }
    render() {
        const popup = (React.createElement("div", { className: "optional_settings_box" },
            React.createElement("div", { className: "popup_close_button" },
                React.createElement("i", { className: "fas fa-window-close", onClick: this.filterCloseOnclick })),
            React.createElement("div", { className: "lotno_input_box" },
                React.createElement("label", null,
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_lotno_table"),
                    React.createElement(LotnoInPopup, { id: "lotno", lotno: this.state.lotno }))),
            React.createElement("div", { className: "ng_input_box" },
                React.createElement("label", null,
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_table"),
                    React.createElement(NgSelectBoxInPopup, { ng: this.state.ng, id: "ng" }))),
            React.createElement("div", { className: "ng_user_input_box" },
                React.createElement("label", null,
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_by_user_table"),
                    React.createElement(NgSelectBoxInPopup, { ng: this.state.ng_by_user, id: "ng_by_user" }))),
            React.createElement("div", { className: "ng_mes_input_box" },
                React.createElement("label", null,
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_by_mes_table"),
                    React.createElement(NgSelectBoxInPopup, { ng: this.state.ng_by_mes, id: "ng_by_mes" }))),
            React.createElement("div", { className: "filter_set_button" },
                React.createElement("button", { className: "set_button", onClick: this.setFilterOnclick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("filter_setting_button")))));
        if (this.state.isfilterOpen) {
            return (React.createElement("div", { className: "main_box" },
                React.createElement("div", { className: "current_page_info" },
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_file_root_name"),
                    React.createElement("i", { className: "fas fa-angle-right" }),
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_filetable_name")),
                React.createElement("div", null, popup),
                React.createElement("div", { className: "searchBox" },
                    React.createElement("div", { className: "filename_input_box" },
                        React.createElement("label", null,
                            inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_filename_label"),
                            React.createElement("input", { type: "text", className: "filename_input", id: "fileName", onChange: this.fileNameOnChange, defaultValue: this.state.fileName }))),
                    React.createElement("div", { className: "date_input_box" },
                        React.createElement("div", { className: "start_date_input_box" },
                            React.createElement("label", null,
                                inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_startdate_label"),
                                React.createElement("input", { type: "date", className: "start_date_input", onChange: this.startDateOnChange, defaultValue: this.state.startDate }))),
                        React.createElement("div", { className: "end_date_input_box" },
                            React.createElement("label", null,
                                inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_enddate_label"),
                                React.createElement("input", { type: "date", className: "end_date_input", onChange: this.endDateOnChange, defaultValue: this.state.endDate })))),
                    React.createElement("div", { className: "line_input_box" },
                        React.createElement("label", null,
                            inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_line_label"),
                            React.createElement("select", { className: "line_input", id: "lineSelector", onChange: this.lineSelectOnChange },
                                React.createElement(LineOptions, { line: this.state.line, lineList: this.state.lineList })))),
                    React.createElement("div", { className: "button_box" },
                        React.createElement("div", { className: "set_specific_filter_button_box" },
                            React.createElement("button", { className: "set_specific_filter_button", onClick: this.filterOnclick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_set_specific_filter_button_box_name"))),
                        React.createElement("div", { className: "search_button_box" },
                            React.createElement("button", { className: "searchButton", onClick: this.searchOnclick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_search_button_name"))),
                        React.createElement("div", null,
                            React.createElement("button", { className: "items_delete_button", onClick: this.deleteOnClick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("table_selected_item_delete_button"))))),
                React.createElement("div", null,
                    React.createElement(TableHeaderAndBox, { lineList: this.state.lineList, data: this.state.resultData })),
                React.createElement("div", { className: "page_box" },
                    React.createElement("div", { className: "pre_page_box" },
                        React.createElement("i", { className: "fas fa-angle-double-left", onClick: this.firstPageOnclick }),
                        React.createElement("i", { className: "fas fa-angle-left", onClick: this.prePageOnclick })),
                    React.createElement("div", { className: "page_input_box" },
                        React.createElement("input", { type: "text", className: "current_page_input", id: "current_page", onKeyPress: this.currentPageOnKeyPress }),
                        React.createElement("div", { className: "slash" }),
                        React.createElement("input", { type: "text", className: "max_page_input", readOnly: "true", id: "max_page" })),
                    React.createElement("div", { className: "post_page_box" },
                        React.createElement("i", { className: "fas fa-angle-right", onClick: this.nextPageOnclick }),
                        React.createElement("i", { className: "fas fa-angle-double-right", onClick: this.lastPageOnclick })))));
        }
        else {
            return (React.createElement("div", { className: "main_box" },
                React.createElement("div", { className: "current_page_info" },
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_file_root_name"),
                    React.createElement("i", { className: "fas fa-angle-right" }),
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_filetable_name")),
                React.createElement("div", { className: "searchBox" },
                    React.createElement("div", { className: "filename_input_box" },
                        React.createElement("label", null,
                            inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_filename_label"),
                            React.createElement("input", { type: "text", className: "filename_input", id: "fileName", onChange: this.fileNameOnChange, defaultValue: this.state.fileName }))),
                    React.createElement("div", { className: "date_input_box" },
                        React.createElement("div", { className: "start_date_input_box" },
                            React.createElement("label", null,
                                inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_startdate_label"),
                                React.createElement("input", { type: "date", className: "start_date_input", onChange: this.startDateOnChange, defaultValue: this.state.startDate }))),
                        React.createElement("div", { className: "end_date_input_box" },
                            React.createElement("label", null,
                                inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_enddate_label"),
                                React.createElement("input", { type: "date", className: "end_date_input", onChange: this.endDateOnChange, defaultValue: this.state.endDate })))),
                    React.createElement("div", { className: "line_input_box" },
                        React.createElement("label", null,
                            inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_line_label"),
                            React.createElement("select", { className: "line_input", id: "lineSelector", onChange: this.lineSelectOnChange },
                                React.createElement(LineOptions, { line: this.state.line, lineList: this.state.lineList })))),
                    React.createElement("div", { className: "button_box" },
                        React.createElement("div", { className: "set_specific_filter_button_box" },
                            React.createElement("button", { className: "set_specific_filter_button", onClick: this.filterOnclick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_set_specific_filter_button_box_name"))),
                        React.createElement("div", { className: "search_button_box" },
                            React.createElement("button", { className: "searchButton", onClick: this.searchOnclick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_search_button_name"))),
                        React.createElement("div", null,
                            React.createElement("button", { className: "items_delete_button", onClick: this.deleteOnClick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("table_selected_item_delete_button"))))),
                React.createElement("div", null,
                    React.createElement(TableHeaderAndBox, { lineList: this.state.lineList, data: this.state.resultData })),
                React.createElement("div", { className: "page_box" },
                    React.createElement("div", { className: "pre_page_box" },
                        React.createElement("i", { className: "fas fa-angle-double-left", onClick: this.firstPageOnclick }),
                        React.createElement("i", { className: "fas fa-angle-left", onClick: this.prePageOnclick })),
                    React.createElement("div", { className: "page_input_box" },
                        React.createElement("input", { type: "text", className: "current_page_input", id: "current_page", onKeyPress: this.currentPageOnKeyPress }),
                        React.createElement("div", { className: "slash" }),
                        React.createElement("input", { type: "text", className: "max_page_input", readOnly: "true", id: "max_page" })),
                    React.createElement("div", { className: "post_page_box" },
                        React.createElement("i", { className: "fas fa-angle-right", onClick: this.nextPageOnclick }),
                        React.createElement("i", { className: "fas fa-angle-double-right", onClick: this.lastPageOnclick })))));
        }
    }
}
ReactDOM.render(React.createElement("div", null,
    React.createElement(SearchBoxAndTable, null)), document.getElementById("main"));
