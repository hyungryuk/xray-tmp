"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const properties_1 = require("../../properties/properties");
class OpenImageRestCall {
    openImageWithJPG(paths) {
        var result = [];
        var object = {};
        object["method_type"] = 'show';
        object["items"] = paths;
        var jsonData = JSON.stringify(object);
        $.ajax({
            async: false,
            url: properties_1.properties["WEB_PATH"] + "/rest/db/imagefile",
            type: 'post',
            data: jsonData,
            accepts: {
                text: 'application/json'
            },
            contentType: 'application/json',
            success: function (data) {
                data = JSON.parse(data);
                data = data["items"];
                for (var i = 0; i < data.length; i++) {
                    var image = new Image();
                    var src = "data:image/jpg;base64," + data[i];
                    image.src = src.replace(/"/g, "");
                    result.push(image);
                    console.log(src);
                }
            },
            error: function (jqXHR, exception) {
            }
        });
        return result;
    }
    openImageWithBMP(paths) {
        var result = [];
        var object = { items: paths, method_type: "show" };
        var jsonData = JSON.stringify(object);
        $.ajax({
            async: false,
            url: properties_1.properties["WEB_PATH"] + "/rest/db/imagefile",
            type: 'post',
            data: jsonData,
            accepts: {
                text: 'application/json'
            },
            contentType: 'application/json',
            success: function (data) {
                data = JSON.parse(data);
                data = data["items"];
                for (var i = 0; i < data.length; i++) {
                    var image = new Image();
                    var src = "data:image/bmp;base64," + data[i];
                    image.src = src.replace(/"/g, "");
                    result.push(image);
                    console.log(src);
                }
            },
            error: function (jqXHR, exception) {
            }
        });
        return result;
    }
}
exports.OpenImageRestCall = OpenImageRestCall;
class TrainingModelRestCall {
    trainModel(items, options) {
        var result = [];
        var object = {};
        object["method_type"] = 'train';
        object["items"] = items;
        object["options"] = options;
        var jsonData = JSON.stringify(object);
        $.ajax({
            async: false,
            url: properties_1.properties["WEB_PATH"] + "/ml/trainModel",
            type: 'post',
            data: jsonData,
            accepts: {
                text: 'application/json'
            },
            contentType: 'application/json',
            success: function (data) {
                data = JSON.parse(data);
                result = data;
            },
            error: function (jqXHR, exception) {
            }
        });
        return result;
    }
    getTrainModelLog(items, options) {
        var result = [];
        var object = {};
        object["method_type"] = 'view';
        object["items"] = items;
        object["options"] = options;
        var jsonData = JSON.stringify(object);
        $.ajax({
            async: false,
            url: properties_1.properties["WEB_PATH"] + "/ml/trainModel",
            type: 'post',
            data: jsonData,
            accepts: {
                text: 'application/json'
            },
            contentType: 'application/json',
            success: function (data) {
                data = JSON.parse(data);
                result = data;
            },
            error: function (jqXHR, exception) {
            }
        });
        return result;
    }
}
exports.TrainingModelRestCall = TrainingModelRestCall;
class DeployModelRestCall {
    deployModel(items, options) {
        var result = [];
        var object = {};
        object["method_type"] = 'pub';
        object["items"] = items;
        object["options"] = options;
        var jsonData = JSON.stringify(object);
        $.ajax({
            async: false,
            url: properties_1.properties["WEB_PATH"] + "/ml/publishing",
            type: 'post',
            data: jsonData,
            accepts: {
                text: 'application/json'
            },
            contentType: 'application/json',
            success: function (data) {
                data = JSON.parse(data);
                result = data;
            },
            error: function (jqXHR, exception) {
            }
        });
        return result;
    }
}
exports.DeployModelRestCall = DeployModelRestCall;
class LoginRestCall {
    login(id, pw) {
        var result;
        var item = {};
        var object = {};
        item["userid"] = id;
        item["password"] = pw;
        object["items"] = [item];
        var jsonData = JSON.stringify(object);
        $.ajax({
            async: false,
            url: properties_1.properties["WEB_PATH"] + "/web/do_login",
            type: 'post',
            data: jsonData,
            accepts: {
                text: 'application/json'
            },
            contentType: 'application/json',
            success: function (data) {
                data = JSON.parse(data);
                result = data;
            },
            error: function (jqXHR, exception) {
            }
        });
        return result;
    }
}
exports.LoginRestCall = LoginRestCall;
