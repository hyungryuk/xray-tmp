"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const ReactDOM = require("react-dom");
const header_1 = require("../modulesForPages/header");
const inversify_config_1 = require("../DIContainerConf/inversify.config");
const types_1 = require("../DIContainerConf/types");
const dbRestcall_1 = require("../others/restCall/dbRestcall");
const othersRestCall_1 = require("../others/restCall/othersRestCall");
const encoder_1 = require("../others/encoder/encoder");
const properties_1 = require("../properties/properties");
require("owl.carousel");
ReactDOM.render(React.createElement(header_1.HeaderComponent, null), document.getElementById("header"));
class OwlContents extends React.Component {
    constructor(props) {
        super(props);
        this.setOwl = this.setOwl.bind(this);
        this.ngByUserOnChange = this.ngByUserOnChange.bind(this);
    }
    ngByUserOnChange(e) {
        var changedValue = $("#ngbyuser option:selected").val();
        var filename = e.target.parentNode.parentNode.getElementsByClassName("filename_div_in_item")[0].getElementsByTagName("input")[0].value;
        var filedate = e.target.parentNode.parentNode.getElementsByClassName("filedate_div_in_item")[0].getElementsByTagName("input")[0].value;
        filedate = encoder_1.encoder.dateToYYMMDDHHMMSS(filedate);
        var imageFileAjaxCallObj = new dbRestcall_1.ImageFileAjaxCall();
        imageFileAjaxCallObj.update([{ ng_by_user: parseInt(changedValue) }], [{ filename: filename, filedate: filedate }]);
    }
    setOwl() {
        if (document.getElementById("owlcarousel")) {
            document.getElementById("owlcarousel").innerHTML = "";
            $('.owl-carousel').trigger('destroy.owl.carousel').removeClass('owl-carousel owl-loaded');
            document.getElementById("owlcarousel").setAttribute("class", "owl-carousel owl-theme");
        }
        for (var i = 0; i < this.props.imagedatas.length; i++) {
            var imagediv = document.createElement("div");
            imagediv.setAttribute("class", "item");
            imagediv.appendChild(this.props.imagedatas[i]);
            var datadiv = document.createElement("div");
            var filenameLabel = document.createElement("label");
            var filedateLabel = document.createElement("label");
            var linenameLabel = document.createElement("label");
            var ngLabel = document.createElement("label");
            var ngByMesLabel = document.createElement("label");
            var ngByUserLabel = document.createElement("label");
            filenameLabel.innerHTML = inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_filename_table");
            filedateLabel.innerHTML = inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_filedate_table");
            linenameLabel.innerHTML = inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_line_table");
            ngLabel.innerHTML = inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_table");
            ngByMesLabel.innerHTML = inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_by_mes_table");
            ngByUserLabel.innerHTML = inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_by_user_table");
            var filename = document.createElement("input");
            var filedate = document.createElement("input");
            var linename = document.createElement("input");
            var ng = document.createElement("select");
            var ngByMes = document.createElement("select");
            var ngByUser = document.createElement("select");
            filename.setAttribute("type", "text");
            filedate.setAttribute("type", "date-time");
            linename.setAttribute("type", "text");
            filename.setAttribute("value", this.props.infoDatas[i].filename);
            filedate.setAttribute("value", this.props.infoDatas[i].filedate);
            for (var j = 0; j < this.props.lines.length; j++) {
                if (this.props.infoDatas[i].linecode == this.props.lines[j].linecode) {
                    linename.setAttribute("value", this.props.lines[j].line);
                    break;
                }
            }
            for (var j = 0; j < 3; j++) {
                var optionDefault = document.createElement("option");
                var optionOk = document.createElement("option");
                var optionNg = document.createElement("option");
                var optionNr = document.createElement("option");
                optionDefault.setAttribute("value", "-1");
                optionOk.setAttribute("value", "0");
                optionNg.setAttribute("value", "1");
                optionNr.setAttribute("value", "2");
                optionDefault.innerHTML = inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("select_box_default_option");
                optionOk.innerHTML = inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ok_name");
                optionNg.innerHTML = inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_name");
                optionNr.innerHTML = inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_nr_name");
                if (j == 0) {
                    ng.appendChild(optionDefault);
                    ng.appendChild(optionOk);
                    ng.appendChild(optionNg);
                    ng.appendChild(optionNr);
                }
                else if (j == 1) {
                    ngByUser.appendChild(optionDefault);
                    ngByUser.appendChild(optionOk);
                    ngByUser.appendChild(optionNg);
                    ngByUser.appendChild(optionNr);
                }
                else {
                    ngByMes.appendChild(optionDefault);
                    ngByMes.appendChild(optionOk);
                    ngByMes.appendChild(optionNg);
                    ngByMes.appendChild(optionNr);
                }
            }
            ng.value = this.props.infoDatas[i].ng + "";
            ngByUser.value = this.props.infoDatas[i].ng_by_user + "";
            ngByMes.value = this.props.infoDatas[i].ng_by_mes + "";
            datadiv.setAttribute("class", "data_box_div_in_item");
            filenameLabel.setAttribute("class", "filename_div_in_item");
            filedateLabel.setAttribute("class", "filedate_div_in_item");
            linenameLabel.setAttribute("class", "linename_div_in_item");
            ngLabel.setAttribute("class", "ng_div_in_item");
            ngByMesLabel.setAttribute("class", "ngbymes_div_in_item");
            ngByUserLabel.setAttribute("class", "ngbyuser_div_in_item");
            filename.setAttribute("class", "filename_in_item");
            filedate.setAttribute("class", "filedate_in_item");
            linename.setAttribute("class", "linename_in_item");
            ng.setAttribute("class", "ng_in_item");
            ngByMes.setAttribute("class", "ngbymes_in_item");
            ngByUser.setAttribute("class", "ngbyuser_in_item");
            ngByUser.setAttribute("id", "ngbyuser");
            ngByUser.onchange = this.ngByUserOnChange;
            filename.setAttribute("readonly", "true");
            filedate.setAttribute("readonly", "true");
            linename.setAttribute("readonly", "true");
            ng.setAttribute("disabled", "true");
            ngByMes.setAttribute("disabled", "true");
            filenameLabel.appendChild(filename);
            filedateLabel.appendChild(filedate);
            linenameLabel.appendChild(linename);
            ngLabel.appendChild(ng);
            ngByMesLabel.appendChild(ngByMes);
            ngByUserLabel.appendChild(ngByUser);
            datadiv.appendChild(filenameLabel);
            datadiv.appendChild(filedateLabel);
            datadiv.appendChild(linenameLabel);
            datadiv.appendChild(ngLabel);
            datadiv.appendChild(ngByMesLabel);
            datadiv.appendChild(ngByUserLabel);
            imagediv.appendChild(datadiv);
            document.getElementById("owlcarousel").appendChild(imagediv);
        }
        $('.owl-carousel').owlCarousel({
            loop: false,
            margin: 10,
            nav: true,
            autoplay: true,
            autoplayTimeout: parseInt(properties_1.properties["LabelingPageItemSlideSpeed"]),
            autoplayHoverPause: true,
        });
    }
    render() {
        return (React.createElement("div", null,
            React.createElement("div", { id: "owlcarousel" }),
            this.setOwl()));
    }
}
class LineNameInTable extends React.Component {
    constructor(props) {
        super(props);
        this.lineCodeToFactoryAndLineName = this.lineCodeToFactoryAndLineName.bind(this);
    }
    lineCodeToFactoryAndLineName() {
        for (var i = 0; i < this.props.lineList.length; i++) {
            if (this.props.lineList[i].linecode == this.props.linecode) {
                return (this.props.lineList[i].factory +
                    " " +
                    this.props.lineList[i].line);
            }
        }
    }
    render() {
        return React.createElement("div", null, this.lineCodeToFactoryAndLineName());
    }
}
class LotnoInPopup extends React.Component {
    constructor(props) {
        super(props);
        this.initDate = this.initDate.bind(this);
    }
    initDate() {
        $("#" + this.props.id).val(this.props.lotno);
    }
    componentDidMount() {
        this.initDate();
    }
    render() {
        return React.createElement("input", { type: "text", className: "lotno_input", id: this.props.id });
    }
}
class NgSelectBoxInPopup extends React.Component {
    render() {
        if (this.props.ng == "-1") {
            return (React.createElement("select", { className: "ng_input", id: this.props.id },
                React.createElement("option", { value: "-1" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("select_box_default_option")),
                React.createElement("option", { value: "0" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ok_name")),
                React.createElement("option", { value: "1" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_name")),
                React.createElement("option", { value: "2" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_nr_name"))));
        }
        if (this.props.ng == "0") {
            return (React.createElement("select", { className: "ng_input", id: this.props.id },
                React.createElement("option", { value: "-1" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("select_box_default_option")),
                React.createElement("option", { value: "0", selected: true }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ok_name")),
                React.createElement("option", { value: "1" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_name")),
                React.createElement("option", { value: "2" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_nr_name"))));
        }
        if (this.props.ng == "1") {
            return (React.createElement("select", { className: "ng_input", id: this.props.id },
                React.createElement("option", { value: "-1" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("select_box_default_option")),
                React.createElement("option", { value: "0" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ok_name")),
                React.createElement("option", { value: "1", selected: true }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_name")),
                React.createElement("option", { value: "2" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_nr_name"))));
        }
        if (this.props.ng == "2") {
            return (React.createElement("select", { className: "ng_input", id: this.props.id },
                React.createElement("option", { value: "-1" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("select_box_default_option")),
                React.createElement("option", { value: "0" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ok_name")),
                React.createElement("option", { value: "1" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_name")),
                React.createElement("option", { value: "2", selected: true }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_nr_name"))));
        }
    }
}
class NgToString extends React.Component {
    render() {
        if (this.props.ng == 0) {
            return React.createElement("td", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ok_name"));
        }
        if (this.props.ng == 1) {
            return React.createElement("td", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_name"));
        }
        if (this.props.ng == 2) {
            return React.createElement("td", null, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_nr_name"));
        }
    }
}
class SearchBoxAndTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            startDate: "",
            endDate: "",
            fileName: "",
            line: "all",
            resultImages: [],
            resultInfos: [],
            lotno: "",
            lineList: [],
            isfilterOpen: false,
            ng: "-1",
            ng_by_user: "-1",
            ng_by_mes: "-1",
            currentPage: 1,
            maxPage: -1,
            perpage: 3
        };
        this.getDataUsingRestCallFunc = this.getDataUsingRestCallFunc.bind(this);
        this.startDateOnChange = this.startDateOnChange.bind(this);
        this.endDateOnChange = this.endDateOnChange.bind(this);
        this.fileNameOnChange = this.fileNameOnChange.bind(this);
        this.lineSelectOnChange = this.lineSelectOnChange.bind(this);
        this.lotnoOnChange = this.lotnoOnChange.bind(this);
        this.searchOnclick = this.searchOnclick.bind(this);
        this.filterOnclick = this.filterOnclick.bind(this);
        this.filterCloseOnclick = this.filterCloseOnclick.bind(this);
        this.setFilterOnclick = this.setFilterOnclick.bind(this);
        this.initFilterBox = this.initFilterBox.bind(this);
        this.perpageOnChange = this.perpageOnChange.bind(this);
        this.firstPageOnclick = this.firstPageOnclick.bind(this);
        this.lastPageOnclick = this.lastPageOnclick.bind(this);
        this.prePageOnclick = this.prePageOnclick.bind(this);
        this.nextPageOnclick = this.nextPageOnclick.bind(this);
        this.currentPageOnKeyPress = this.currentPageOnKeyPress.bind(this);
    }
    initFilterBox() {
        $("#lotno").val(this.state.lotno);
        $("#ng option:selected").val(this.state.ng);
        $("#ng_by_user option:selected").val(this.state.ng_by_user);
        $("#ng_by_mes option:selected").val(this.state.ng_by_mes);
    }
    setFilterOnclick() {
        this.setState({ lotno: $("#lotno").val() });
        this.setState({ ng: $("#ng option:selected").val() });
        this.setState({ ng_by_user: $("#ng_by_user option:selected").val() });
        this.setState({ ng_by_mes: $("#ng_by_mes option:selected").val() });
        this.setState(prevState => ({
            isfilterOpen: !prevState.isfilterOpen
        }));
    }
    filterCloseOnclick() {
        this.setState(prevState => ({
            isfilterOpen: !prevState.isfilterOpen
        }));
    }
    filterOnclick() {
        this.setState(prevState => ({
            isfilterOpen: !prevState.isfilterOpen
        }));
    }
    getDataUsingRestCallFunc(page) {
        var imageFileResult;
        var option1 = {};
        var option2 = {};
        if (this.state.line != "all") {
            option1["linecode"] = this.state.line;
        }
        if (this.state.fileName != "") {
            option1["filename"] = this.state.fileName;
        }
        if (this.state.lotno != "") {
            option1["lotno"] = this.state.lotno;
        }
        if (this.state.ng != "-1") {
            option1["ng"] = this.state.ng;
        }
        if (this.state.ng_by_user != "-1") {
            option1["ng_by_user"] = this.state.ng_by_user;
        }
        if (this.state.ng_by_mes != "-1") {
            option1["ng_by_mes"] = this.state.ng_by_mes;
        }
        if (this.state.startDate != "" && this.state.endDate != "") {
            option2["startdate"] = encoder_1.encoder.dateToYYMMDDHHMMSS(this.state.startDate + "000000");
            option2["enddate"] = encoder_1.encoder.dateToYYMMDDHHMMSS(this.state.endDate + "235959");
        }
        option1["isremoved"] = 0;
        var imageFileAjaxCall = new dbRestcall_1.ImageFileAjaxCall();
        imageFileResult = imageFileAjaxCall.retrieve([], [option1, option2, {}, { page: page, perpage: this.state.perpage }]);
        var imageFiles = imageFileResult["items"];
        var paths = [];
        for (var i = 0; i < imageFiles.length; i++) {
            paths.push(imageFiles[i].filepath);
        }
        var openImageRestCallObj = new othersRestCall_1.OpenImageRestCall();
        var resultData = openImageRestCallObj.openImageWithBMP(paths);
        imageFileResult["images"] = resultData;
        return imageFileResult;
    }
    firstPageOnclick() {
        var imageFileResult;
        if (this.state.currentPage != 1) {
            imageFileResult = this.getDataUsingRestCallFunc(1);
            this.setState({
                resultImages: imageFileResult["images"],
                resultInfos: imageFileResult["items"],
                currentPage: 1
            });
            $("#current_page").val(1);
        }
    }
    lastPageOnclick() {
        var imageFileResult;
        if (this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage) {
            imageFileResult = this.getDataUsingRestCallFunc(this.state.maxPage);
            this.setState({
                resultImages: imageFileResult["images"],
                resultInfos: imageFileResult["items"],
                currentPage: this.state.maxPage
            });
            $("#current_page").val(this.state.maxPage);
        }
    }
    prePageOnclick() {
        var imageFileResult;
        if (this.state.maxPage != -1 && this.state.currentPage > 1) {
            imageFileResult = this.getDataUsingRestCallFunc(this.state.currentPage - 1);
            this.setState({
                resultImages: imageFileResult["images"],
                resultInfos: imageFileResult["items"]
            });
            $("#current_page").val(this.state.currentPage - 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage - 1
            }));
        }
    }
    nextPageOnclick() {
        var imageFileResult;
        if (this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage) {
            imageFileResult = this.getDataUsingRestCallFunc(this.state.currentPage + 1);
            this.setState({
                resultImages: imageFileResult["images"],
                resultInfos: imageFileResult["items"]
            });
            $("#current_page").val(this.state.currentPage + 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage + 1
            }));
        }
    }
    currentPageOnKeyPress(e) {
        if (e.which == 13) {
            var pageVal = $("#current_page").val();
            var page = parseInt(pageVal);
            if (isNaN(page)) {
                alert(inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_is_not_number_alert"));
                $("#current_page").val(this.state.currentPage);
            }
            else {
                if (page > this.state.maxPage) {
                    alert(inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_greater_than_max_alert"));
                    $("#current_page").val(this.state.currentPage);
                }
                else {
                    if (this.state.maxPage != -1 &&
                        this.state.currentPage != page) {
                        var imageFileResult = this.getDataUsingRestCallFunc(page);
                        this.setState({
                            resultImages: imageFileResult["images"],
                            resultInfos: imageFileResult["items"],
                            currentPage: page
                        });
                        $("#current_page").val(page);
                    }
                }
            }
        }
    }
    perpageOnChange(e) {
        this.setState({
            perpage: parseInt(e.target.value),
            resultImages: [],
            resultInfos: []
        });
    }
    startDateOnChange(e) {
        this.setState({
            startDate: e.target.value,
            resultImages: [],
            resultInfos: []
        });
    }
    endDateOnChange(e) {
        this.setState({
            endDate: e.target.value,
            resultImages: [],
            resultInfos: []
        });
    }
    fileNameOnChange() {
        this.setState({
            fileName: $("#fileName").val(),
            resultImages: [],
            resultInfos: []
        });
    }
    lineSelectOnChange() {
        this.setState({
            line: $("#lineSelector option:selected").val(),
            resultImages: [],
            resultInfos: []
        });
    }
    lotnoOnChange() {
        this.setState({
            lotno: $("#lotno").val(),
            resultImages: [],
            resultInfos: []
        });
    }
    searchOnclick() {
        var imageFileResult = this.getDataUsingRestCallFunc(1);
        this.setState({
            resultImages: imageFileResult["images"],
            resultInfos: imageFileResult["items"]
        });
        var maxpage = Math.ceil(imageFileResult["total"] / this.state.perpage);
        this.setState({ maxPage: maxpage });
        if (imageFileResult["items"].length == 0) {
            $("#current_page").val(0);
            $("#max_page").val(0);
            this.setState({ maxPage: 0, currentPage: 0 });
        }
        else {
            $("#current_page").val(1);
            $("#max_page").val(maxpage);
            this.setState({ maxPage: maxpage, currentPage: 1 });
        }
    }
    componentDidMount() {
        var lineAjaxCall = new dbRestcall_1.LineAjaxCall();
        var lineList = lineAjaxCall.retrieve([], [])["items"];
        let selectorObj = inversify_config_1.myContainer.get(types_1.TYPES.LineSelectorInterface);
        selectorObj.setDivId("lineSelector");
        selectorObj.setDatas(lineList);
        selectorObj.makeSelector();
        this.setState({ lineList: lineList });
    }
    render() {
        const popup = (React.createElement("div", { className: "optional_settings_box" },
            React.createElement("div", { className: "popup_close_button" },
                React.createElement("i", { className: "fas fa-window-close", onClick: this.filterCloseOnclick })),
            React.createElement("div", { className: "lotno_input_box" },
                React.createElement("label", null,
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_lotno_table"),
                    React.createElement(LotnoInPopup, { id: "lotno", lotno: this.state.lotno }))),
            React.createElement("div", { className: "ng_input_box" },
                React.createElement("label", null,
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_table"),
                    React.createElement(NgSelectBoxInPopup, { ng: this.state.ng, id: "ng" }))),
            React.createElement("div", { className: "ng_user_input_box" },
                React.createElement("label", null,
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_by_user_table"),
                    React.createElement(NgSelectBoxInPopup, { ng: this.state.ng_by_user, id: "ng_by_user" }))),
            React.createElement("div", { className: "ng_mes_input_box" },
                React.createElement("label", null,
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_ng_by_mes_table"),
                    React.createElement(NgSelectBoxInPopup, { ng: this.state.ng_by_mes, id: "ng_by_mes" }))),
            React.createElement("div", { className: "filter_set_button" },
                React.createElement("button", { className: "set_button", onClick: this.setFilterOnclick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("filter_setting_button")))));
        if (this.state.isfilterOpen) {
            return (React.createElement("div", { className: "main_box" },
                React.createElement("div", { className: "current_page_info" },
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_file_root_name"),
                    React.createElement("i", { className: "fas fa-angle-right" }),
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_filelabel_name")),
                React.createElement("div", null, popup),
                React.createElement("div", { className: "searchBox" },
                    React.createElement("div", { className: "filename_input_box" },
                        React.createElement("label", null,
                            inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_filename_label"),
                            React.createElement("input", { type: "text", className: "filename_input", id: "fileName", onChange: this.fileNameOnChange }))),
                    React.createElement("div", { className: "date_input_box" },
                        React.createElement("div", { className: "start_date_input_box" },
                            React.createElement("label", null,
                                inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_startdate_label"),
                                React.createElement("input", { type: "date", className: "start_date_input", onChange: this.startDateOnChange }))),
                        React.createElement("div", { className: "end_date_input_box" },
                            React.createElement("label", null,
                                inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_enddate_label"),
                                React.createElement("input", { type: "date", className: "end_date_input", onChange: this.endDateOnChange })))),
                    React.createElement("div", { className: "line_input_box" },
                        React.createElement("label", null,
                            inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_line_label"),
                            React.createElement("select", { className: "line_input", id: "lineSelector", onChange: this.lineSelectOnChange },
                                React.createElement("option", { value: "all" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_line_selector_option_all_name"))))),
                    React.createElement("div", { className: "perpage_input_box" },
                        React.createElement("label", null,
                            inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagelabeling_perpage_name"),
                            React.createElement("select", { className: "perpage", id: "perpage", onChange: this.perpageOnChange },
                                React.createElement("option", { value: "3" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagelabeling_three_perpage_name")),
                                React.createElement("option", { value: "5" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagelabeling_five_perpage_name")),
                                React.createElement("option", { value: "10" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagelabeling_ten_perpage_name"))))),
                    React.createElement("div", { className: "button_box" },
                        React.createElement("div", { className: "set_specific_filter_button_box" },
                            React.createElement("button", { className: "set_specific_filter_button", onClick: this.filterOnclick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_set_specific_filter_button_box_name"))),
                        React.createElement("div", { className: "search_button_box" },
                            React.createElement("button", { className: "searchButton", onClick: this.searchOnclick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_search_button_name"))))),
                React.createElement("div", { className: "owlcarousel_box" },
                    React.createElement(OwlContents, { imagedatas: this.state.resultImages, infoDatas: this.state.resultInfos, lines: this.state.lineList })),
                React.createElement("div", { className: "page_box" },
                    React.createElement("div", { className: "pre_page_box" },
                        React.createElement("i", { className: "fas fa-angle-double-left", onClick: this.firstPageOnclick }),
                        React.createElement("i", { className: "fas fa-angle-left", onClick: this.prePageOnclick })),
                    React.createElement("div", { className: "page_input_box" },
                        React.createElement("input", { type: "text", className: "current_page_input", id: "current_page", onKeyPress: this.currentPageOnKeyPress }),
                        React.createElement("div", { className: "slash" }),
                        React.createElement("input", { type: "text", className: "max_page_input", readOnly: "true", id: "max_page" })),
                    React.createElement("div", { className: "post_page_box" },
                        React.createElement("i", { className: "fas fa-angle-right", onClick: this.nextPageOnclick }),
                        React.createElement("i", { className: "fas fa-angle-double-right", onClick: this.lastPageOnclick })))));
        }
        else {
            return (React.createElement("div", { className: "main_box" },
                React.createElement("div", { className: "current_page_info" },
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_file_root_name"),
                    React.createElement("i", { className: "fas fa-angle-right" }),
                    inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("menu_filelabel_name")),
                React.createElement("div", { className: "searchBox" },
                    React.createElement("div", { className: "filename_input_box" },
                        React.createElement("label", null,
                            inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_filename_label"),
                            React.createElement("input", { type: "text", className: "filename_input", id: "fileName", onChange: this.fileNameOnChange }))),
                    React.createElement("div", { className: "date_input_box" },
                        React.createElement("div", { className: "start_date_input_box" },
                            React.createElement("label", null,
                                inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_startdate_label"),
                                React.createElement("input", { type: "date", className: "start_date_input", onChange: this.startDateOnChange }))),
                        React.createElement("div", { className: "end_date_input_box" },
                            React.createElement("label", null,
                                inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_enddate_label"),
                                React.createElement("input", { type: "date", className: "end_date_input", onChange: this.endDateOnChange })))),
                    React.createElement("div", { className: "line_input_box" },
                        React.createElement("label", null,
                            inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_line_label"),
                            React.createElement("select", { className: "line_input", id: "lineSelector", onChange: this.lineSelectOnChange },
                                React.createElement("option", { value: "all" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_line_selector_option_all_name"))))),
                    React.createElement("div", { className: "perpage_input_box" },
                        React.createElement("label", null,
                            inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagelabeling_perpage_name"),
                            React.createElement("select", { className: "perpage", id: "perpage", onChange: this.perpageOnChange },
                                React.createElement("option", { value: "3" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagelabeling_three_perpage_name")),
                                React.createElement("option", { value: "5" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagelabeling_five_perpage_name")),
                                React.createElement("option", { value: "10" }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagelabeling_ten_perpage_name"))))),
                    React.createElement("div", { className: "button_box" },
                        React.createElement("div", { className: "set_specific_filter_button_box" },
                            React.createElement("button", { className: "set_specific_filter_button", onClick: this.filterOnclick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("imagefiletable_set_specific_filter_button_box_name"))),
                        React.createElement("div", { className: "search_button_box" },
                            React.createElement("button", { className: "searchButton", onClick: this.searchOnclick }, inversify_config_1.myContainer.get(types_1.TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_search_button_name"))))),
                React.createElement("div", { className: "owlcarousel_box" },
                    React.createElement(OwlContents, { imagedatas: this.state.resultImages, infoDatas: this.state.resultInfos, lines: this.state.lineList })),
                React.createElement("div", { className: "page_box" },
                    React.createElement("div", { className: "pre_page_box" },
                        React.createElement("i", { className: "fas fa-angle-double-left", onClick: this.firstPageOnclick }),
                        React.createElement("i", { className: "fas fa-angle-left", onClick: this.prePageOnclick })),
                    React.createElement("div", { className: "page_input_box" },
                        React.createElement("input", { type: "text", className: "current_page_input", id: "current_page", onKeyPress: this.currentPageOnKeyPress }),
                        React.createElement("div", { className: "slash" }),
                        React.createElement("input", { type: "text", className: "max_page_input", readOnly: "true", id: "max_page" })),
                    React.createElement("div", { className: "post_page_box" },
                        React.createElement("i", { className: "fas fa-angle-right", onClick: this.nextPageOnclick }),
                        React.createElement("i", { className: "fas fa-angle-double-right", onClick: this.lastPageOnclick })))));
        }
    }
}
ReactDOM.render(React.createElement("div", null,
    React.createElement(SearchBoxAndTable, null)), document.getElementById("main"));
