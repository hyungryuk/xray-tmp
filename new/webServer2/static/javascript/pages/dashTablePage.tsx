import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as Chartjs from 'chart.js'

import { HeaderComponent } from "../modulesForPages/header";
import {LineSelectorInterface} from '../others/selector/selector'
import {StaticLanguageInterface} from '../others/staticLanguageData/staticLanguageData'
import {myContainer} from '../DIContainerConf/inversify.config'
import {TYPES} from '../DIContainerConf/types'

import { LineAjaxCall,StatisticDataAjaxCall } from '../others/restCall/dbRestcall'
import {encoder} from "../others/encoder/encoder";

var selectedList:string[] = [];
Date.prototype.yyyymmddhhmmss = function() {
	var mm = this.getMonth() + 1;
	var dd = this.getDate();
	var hh = this.getHours();
	var minutes = this.getMinutes();
	var ss = this.getSeconds();

	return [
		this.getFullYear(),
		(mm > 9 ? "" : "0") + mm,
		(dd > 9 ? "" : "0") + dd,
		(hh > 9 ? "" : "0") + hh,
		(minutes > 9 ? "" : "0") + minutes,
		(ss > 9 ? "" : "0") + ss
	].join("");
};

ReactDOM.render(
  <HeaderComponent />,
  document.getElementById("header")
);

var csvModule = {
	onclickCsvButton: function(linecode:string, startdate:string, enddate:string, unit:number) {
		var csvData = csvModule.makeCsvData(linecode, startdate, enddate, unit);
		csvModule.downloadCsvFile(csvData, startdate, enddate);
	},
	downloadCsvFile: function(data:any[][], startdate:string, enddate:string) {
		var encodedStartdate = encoder.dateToYYMMDDHHMMSS(startdate);
		var encodedEnddate = encoder.dateToYYMMDDHHMMSS(enddate);

		let csvContent = "data:text/csv;charset=euc-kr,";
		data.forEach(function(rowArray) {
			let row = rowArray.join(",");
			csvContent += row + "\r\n";
		});

		var encodedUri = encodeURI(csvContent);
		var link = document.createElement("a");
		link.setAttribute("href", encodedUri);
		link.setAttribute(
			"download",
			encodedStartdate + "_" + encodedEnddate + ".csv"
		);
		document.body.appendChild(link); // Required for FF

		link.click();
	},
	makeCsvData: function(linecode:string, startdate_orin:string, enddate_orin:string, unit:number) {
		var startdate = new Date(startdate_orin + " 00:00:00");
		var enddate = new Date(enddate_orin + " 23:59:59");

		var option1:{[key:string]:string} = {};
		if (linecode != "all") {
			option1["linecode"] = linecode;
		}

		var option2:{[key:string]:string} = {};
		option2["startdate"] = startdate.yyyymmddhhmmss();
		option2["enddate"] = enddate.yyyymmddhhmmss();

		let lineRestCall = new LineAjaxCall()
        let lineRestCallRes = lineRestCall.retrieve([],[option1]);

		option1["kind"] = unit;
        let StatisticDataRestCall = new StatisticDataAjaxCall();
		var statisticsResult;
		if(startdate_orin==""||enddate_orin==""){
			statisticsResult  = StatisticDataRestCall.retrieve([],[option1]);
		}else{
			statisticsResult  = StatisticDataRestCall.retrieve([],[option1, option2]);
		}

		var csvData = [];
		var firstRow = [];
		firstRow.push(myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_line_label_name"));
		firstRow.push(myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_table_term_name"));
		firstRow.push(myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_table_total_name"));
		firstRow.push(myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_table_ok_name"));
		firstRow.push(myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_table_ng_name"));
		firstRow.push(myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_table_nr_name"));
		csvData.push(firstRow);

		var statisticsDataList = statisticsResult["items"];
		for (var i = 0; i < statisticsDataList.length; i++) {
			var row = [];
			for (var j = 0; j < lineRestCallRes["items"].length; j++) {
				if (
					statisticsDataList[i].linecode ==
					lineRestCallRes["items"][j].linecode
				) {
					row.push(
						lineRestCallRes["items"][j].factory +
							" " +
							lineRestCallRes["items"][j].line
					);
					break;
				}
			}
			row.push(statisticsDataList[i].date);
			row.push(
				statisticsDataList[i].ngcount +
					statisticsDataList[i].okcount +
					statisticsDataList[i].nrcount
			);
			row.push(statisticsDataList[i].okcount);
			row.push(statisticsDataList[i].ngcount);
			row.push(statisticsDataList[i].nrcount);
			csvData.push(row);
		}
		return csvData;
	}
};

class CheckBoxInTable extends React.Component<{date:string},{}> {
    constructor(props:any){
        super(props);
        this.checkBoxOnChange = this.checkBoxOnChange.bind(this);
    }
    checkBoxOnChange(e:any){
        if(e.target.checked==true){
            selectedList.push(this.props.date);
        }else{
            var index = selectedList.indexOf(this.props.date);
            selectedList.splice(index,1);
        }
    }
    render(){
        return(
            <input type="checkbox" onChange={this.checkBoxOnChange} className="checkbox"/>
        );
    }

}

class LineNameInTable extends React.Component<{lineList:any[],linecode:number},{}> {
    render() {
        for (var i = 0; i < this.props.lineList.length; i++) {
            if (this.props.lineList[i].linecode == this.props.linecode) {
                return (
                    this.props.lineList[i].factory +
                    " " +
                    this.props.lineList[i].line
                );
            }
        }
    }
}

class TableBody extends React.Component<{data:any[],lineList:any[]},{}> {
    render() {
        console.log(this.props.data);
        const data = this.props.data.map(item => (
            <tr>
				<td><CheckBoxInTable date={item.date}/></td>
                <td>
                    <LineNameInTable
                        linecode={item.linecode}
                        lineList={this.props.lineList}
                    />
                </td>
                <td>{item.date}</td>
                <td className="number">{item.ngcount + item.okcount + item.nrcount}</td>
                <td className="number">{item.okcount}</td>
                <td className="number">{item.ngcount}</td>
                <td className="number">{item.nrcount}</td>
            </tr>
        ));
        return data;
    }
}
class TableHeaderAndBox extends React.Component<{data:any[],lineList:any[]},{isSelectallClicked:boolean}> {
    constructor(props:any){
        super(props);
        this.state = {
            isSelectallClicked:false
        }
        this.selectAllOnClick = this.selectAllOnClick.bind(this);

    }
    selectAllOnClick(){

        var checkboxs = document.getElementsByClassName("checkbox");
        if(this.state.isSelectallClicked==false){
            for(var i=0;i<this.props.data.length;i++){
                selectedList.push(this.props.data[i].algorithmcode);
            }
            for(var i=0;i<checkboxs.length;i++){
                checkboxs[i].checked=true;
            }
        }else{
            selectedList=[];
            for(var i=0;i<checkboxs.length;i++){
                checkboxs[i].checked=false;
            }
        }

        this.setState(prevState => ({
            isSelectallClicked: !prevState.isSelectallClicked
        }));

    }
    render() {
        return (
            <div className="table_box">
                <table className="table">
                    <thead className="thead">
                        <tr>
							<th onClick={this.selectAllOnClick}>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("table_select_name")}
                            </th>
                            <th>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_table_line_name")}
                            </th>
                            <th>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_table_term_name")}
                            </th>
                            <th>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_table_total_name")}
                            </th>
                            <th>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_table_ok_name")}
                            </th>
                            <th>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_table_ng_name")}
                            </th>
                            <th>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_table_nr_name")}
                            </th>
                        </tr>
                    </thead>
                    <tbody className="tbody">
                        <TableBody
                            lineList={this.props.lineList}
                            data={this.props.data}
                        />
                    </tbody>
                </table>
            </div>
        );
    }
}


class SearchBoxAndTable extends React.Component<{data:any[],lineList:any[]},{startDate:string,endDate:string,unit:number,line:string,resultData:any[],lineList:any[],currentPage:number,maxPage:number,perpage:number}> {
    constructor(props:any) {
        super(props);
        this.state = {
            startDate: "",
            endDate: "",
            unit: 0,
            line: "all",
            resultData: [],
            lineList: [],
            currentPage: 1,
            maxPage: -1,
            perpage: 2
        };

        this.getDataUsingRestCallFunc = this.getDataUsingRestCallFunc.bind(
            this
        );

        this.startDateOnChange = this.startDateOnChange.bind(this);
        this.endDateOnChange = this.endDateOnChange.bind(this);
        this.searchUnitOnChange = this.searchUnitOnChange.bind(this);
        this.lineSelectOnChange = this.lineSelectOnChange.bind(this);
        this.csvOnclick = this.csvOnclick.bind(this);
        this.searchOnclick = this.searchOnclick.bind(this);

        this.firstPageOnclick = this.firstPageOnclick.bind(this);
        this.lastPageOnclick = this.lastPageOnclick.bind(this);
        this.prePageOnclick = this.prePageOnclick.bind(this);
        this.nextPageOnclick = this.nextPageOnclick.bind(this);
        this.currentPageOnKeyPress = this.currentPageOnKeyPress.bind(this);
    }

    getDataUsingRestCallFunc(page:number) {
        var staticticsResult;
        let StatisticDataRestCall = new StatisticDataAjaxCall()
        if (this.state.line == "all") {
            staticticsResult = StatisticDataRestCall.retrieve(
                [],
                [
                    { kind: this.state.unit },
                    {
                        startdate: encoder.dateToYYMMDDHHMMSS(
                            this.state.startDate
                        ),
                        enddate: encoder.dateToYYMMDDHHMMSS(this.state.endDate)
                    },
                    {},
                    { page: page, perpage: this.state.perpage }
                ]
            );
        } else {
            staticticsResult = StatisticDataRestCall.retrieve(
                [],
                [
                    { linecode: this.state.line, kind: this.state.unit },
                    {
                        startdate: encoder.dateToYYMMDDHHMMSS(
                            this.state.startDate
                        ),
                        enddate: encoder.dateToYYMMDDHHMMSS(this.state.endDate)
                    },
                    {},
                    { page: page, perpage: this.state.perpage }
                ]
            );
        }

        return staticticsResult;
    }

    firstPageOnclick() {
        var staticticsResult;
        if (this.state.currentPage != 1) {
            staticticsResult = this.getDataUsingRestCallFunc(1);
            this.setState({
                resultData: staticticsResult["items"],
                currentPage: 1
            });

            $("#current_page").val(1);
        }
    }

    lastPageOnclick() {
        var staticticsResult;
        if (
            this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage
        ) {
            staticticsResult = this.getDataUsingRestCallFunc(
                this.state.maxPage
            );
            this.setState({
                resultData: staticticsResult["items"],
                currentPage: this.state.maxPage
            });

            $("#current_page").val(this.state.maxPage);
        }
    }
    prePageOnclick() {
        var staticticsResult;
        if (this.state.maxPage != -1 && this.state.currentPage != 1) {
            staticticsResult = this.getDataUsingRestCallFunc(
                this.state.currentPage - 1
            );
            this.setState({ resultData: staticticsResult["items"] });
            $("#current_page").val(this.state.currentPage - 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage - 1
            }));
        }
    }
    nextPageOnclick() {
        var staticticsResult;
        if (
            this.state.maxPage != -1 &&
            this.state.currentPage != this.state.maxPage
        ) {
            staticticsResult = this.getDataUsingRestCallFunc(
                this.state.currentPage + 1
            );
            this.setState({ resultData: staticticsResult["items"] });
            $("#current_page").val(this.state.currentPage + 1);
            this.setState(prevState => ({
                currentPage: prevState.currentPage + 1
            }));
        }
    }
    currentPageOnKeyPress(e:any) {
        if (e.which == 13) {
            var pageVal = $("#current_page").val() as string;
            var page = parseInt(pageVal);
            if (isNaN(page)) {
                alert(
                    myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_is_not_number_alert")
                );
                $("#current_page").val(this.state.currentPage);
            } else {
                if (page > this.state.maxPage) {
                    alert(
                        myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("input_page_num_greater_than_max_alert")
                    );
                    $("#current_page").val(this.state.currentPage);
                } else {
                    if (
                        this.state.maxPage != -1 &&
                        this.state.currentPage != page
                    ) {
                        var staticticsResult = this.getDataUsingRestCallFunc(
                            page
                        );
                        this.setState({
                            resultData: staticticsResult["items"],
                            currentPage: page
                        });
                        $("#current_page").val(page);
                    }
                }
            }
        }
    }
    startDateOnChange(e:any) {
        this.setState({
            startDate: e.target.value
        });
    }
    endDateOnChange(e:any) {
        this.setState({
            endDate: e.target.value
        });
    }
    searchUnitOnChange() {
        var value = $("#unitSelector option:selected").val()
        this.setState({
            unit:parseInt(value as string) as number
        });
    }
    lineSelectOnChange() {
        this.setState({
            line: $("#lineSelector option:selected").val() as string
        });
    }
    csvOnclick() {
        console.log(this.state.startDate);
        csvModule.onclickCsvButton(
            this.state.line,
            this.state.startDate,
            this.state.endDate,
            this.state.unit
        );
    }
	deleteOnClick(e:any){
        var staticticsResultObj = new StatisticDataAjaxCall();
        var optionList:any[] = [];
        for(var i=0;i<selectedList.length;i++){
            optionList.push({algorithmcode:selectedList[i]});
        }
        staticticsResultObj.delete(
            [],
            optionList
        );
    }
    searchOnclick() {
        var staticticsResult = this.getDataUsingRestCallFunc(1);

        this.setState({ resultData: staticticsResult["items"] });

        var maxpage = Math.ceil(staticticsResult["total"] / this.state.perpage);
        this.setState({ maxPage: maxpage });

        $("#current_page").val(this.state.currentPage);
        $("#max_page").val(maxpage);
    }

    componentDidMount() {
        let lineRestCall = new LineAjaxCall()
        let lineRestCallRes = lineRestCall.retrieve([],[]);
        var lineList = lineRestCallRes["items"];

        this.setState({ lineList: lineList });

        let selectorObj = myContainer.get<LineSelectorInterface>(TYPES.LineSelectorInterface);
        selectorObj.setDivId("lineSelector")
        selectorObj.setDatas(lineList as any[]);
        selectorObj.makeSelector();

		this.searchOnclick();
    }

    render() {
        return (
            <div className="main_box">
				<div className="current_page_info">
                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_dash_root_name")}
                    <i className="fas fa-angle-right"></i>
                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("menu_dashtable_name")}
                </div>
                <div className="searchBox">
                    <div className="date_input_box">
                        <div className="start_date_input_box">
                            <label>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "dashtable_startdate_label_name"
                                )}
                                <input
                                    type="date"
                                    className="start_date_input"
                                    onChange={this.startDateOnChange}
                                />
                            </label>
                        </div>
                        <div className="end_date_input_box">
                            <label>
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "dashtable_enddate_label_name"
                                )}
                                <input
                                    type="date"
                                    className="end_date_input"
                                    onChange={this.endDateOnChange}
                                />
                            </label>
                        </div>
                    </div>
                    <div className="unit_input_box">
                        <label>
                            {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_unit_name")}
                            <select
                                className="unit_input"
                                id="unitSelector"
                                onChange={this.searchUnitOnChange}
                            >
                                <option value="0">
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "dashtable_unit_hourly_option_name"
                                    )}
                                </option>
                                <option value="1">
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "dashtable_unit_daily_option_name"
                                    )}
                                </option>
                                <option value="2">
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "dashtable_unit_weekly_option_name"
                                    )}
                                </option>
                                <option value="3">
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "dashtable_unit_monthly_option_name"
                                    )}
                                </option>
                                <option value="4">
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "dashtable_unit_yearly_option_name"
                                    )}
                                </option>
                            </select>
                        </label>
                    </div>
                    <div className="line_input_box">
                        <label>
                            {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_line_name")}
                            <select
                                className="line_input"
                                id="lineSelector"
                                onChange={this.lineSelectOnChange}
                            >
                                <option value="all">
                                    {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                        "dashtable_line_selector_option_all_name"
                                    )}
                                </option>
                            </select>
                        </label>
                    </div>
                    <div className="button_box">
                        <div className="csv_button_box">
                            <button
                                className="csv_button"
                                onClick={this.csvOnclick}
                            >
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey("dashtable_csv_button_name")}
                            </button>
                        </div>
                        <div className="search_button_box">
                            <button
                                className="searchButton"
                                onClick={this.searchOnclick}
                            >
                                {myContainer.get<StaticLanguageInterface>(TYPES.StaticLanguageInterface).getTextUsingKey(
                                    "dashtable_search_button_name"
                                )}
                            </button>
                        </div>
                    </div>
                </div>

                <div>
                    <TableHeaderAndBox
                        lineList={this.state.lineList}
                        data={this.state.resultData}
                    />
                </div>
                <div className="page_box">
                    <div className="pre_page_box">
                        <i
                            className="fas fa-angle-double-left"
                            onClick={this.firstPageOnclick}
                        />
                        <i
                            className="fas fa-angle-left"
                            onClick={this.prePageOnclick}
                        />
                    </div>
                    <div className="page_input_box">
                        <input
                            type="text"
                            className="current_page_input"
                            id="current_page"
                            onKeyPress={this.currentPageOnKeyPress}
                        />
						<div className="slash">
						</div>
                        <input
                            type="text"
                            className="max_page_input"
                            readOnly="true"
                            id="max_page"
                        />
                    </div>
                    <div className="post_page_box">
                        <i
                            className="fas fa-angle-right"
                            onClick={this.nextPageOnclick}
                        />
                        <i
                            className="fas fa-angle-double-right"
                            onClick={this.lastPageOnclick}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

ReactDOM.render(
    <div>
        <SearchBoxAndTable />
    </div>,
    document.getElementById("main")
);
