# -*- coding: utf-8 -*-

import base64
from shutil import rmtree
import json
from datetime import date, datetime
from ..configs.config import *


def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, (datetime, date)):
        return obj.isoformat()
    raise TypeError("Type %s not serializable" % type(obj))


def converter(o):
    if isinstance(o, datetime):
        return o.__str__()


def get_now_date():
    now = datetime.now()

    year = now.year

    month = now.month

    day = now.day

    hour = now.hour

    minute = now.minute

    now_date = "{0}{1}{2}{3}{4}".format(str(year), str(month), str(day), str(hour), str(minute))

    return now_date


def get_file_tree(node_id):
    if node_id == "0":
        json_result = '[{"id":"' + FILE_TREE_ROOT_ID + '","parent":"#","text":"' + os.path.basename(
            FILE_TREE_ROOT_PATH) + '","children":true}]'
        return json.loads(json_result)
    else:

        path_from_node_id = node_id.replace(FILE_ID_SEPERATOR, FILE_TREE_SEPERATOR)
        print(path_from_node_id)
        dirs = os.listdir(path_from_node_id)
        json_result = "["
        for sub_file_name in dirs:
            if os.path.isfile(path_from_node_id + FILE_TREE_SEPERATOR + sub_file_name):
                json_result += '{"id":"' + node_id + FILE_ID_SEPERATOR + sub_file_name + '","parent":"' + node_id + '","text":"' + sub_file_name + '","icon":"jstree-file"}'
            else:
                json_result += '{"id":"' + node_id + FILE_ID_SEPERATOR + sub_file_name + '","parent":"' + node_id + '","text":"' + sub_file_name + '","children":true}'
        json_result += "]"
        json_result = json_result.replace("}{", "},{")
        return json.loads(json_result)


def open_image(image_path):
    try:
        with open(image_path, "rb") as imageFile:
            str1 = base64.b64encode(imageFile.read())
            return str1
    except Exception as e:
        print(str(e))


def delete_image(image_path):
    if os.path.isfile(image_path):
        os.remove(image_path)
    else:
        rmtree(image_path)

def train_model(model_name,csv_path,algorithm_code,filter_code):
    print(model_name + ", " + csv_path + ", " + algorithm_code + ", " + filter_code)
