# -*-coding: utf-8 -*-
from ..configs.config import *
from .messages import *

from flask import json
import requests as rq

def get_algorithm(modelcode,algorithmcode):
    try:
        status = 2

      

        algorithm_response = rq.post(REST_SERVER_URL+'/rest/db/algorithm',json={'items':[],
                           'method_type': 'view',
                           'options':[{'algorithmcode':algorithmcode}]
                           })
        algorithm = json.loads(algorithm_response.json())

        alg_dwn_response = rq.post(REST_SERVER_URL+'/rest/dwn',json={'path':algorithm.get('items')[0].get('fullpath'),
                           'method_type': 'download'
                           })

        with open(ALR_DOWNLOAD_DIR % (modelcode) + "algorithm.py", "wb") as algorithm_file :
            algorithm_file.write(alg_dwn_response.content)



        result = rq.post(REST_SERVER_URL+'/rest/trn',json={'items': [{"contents":"complete to download algorithm file"}],
                           'method_type': 'update',
                           'options': [{"modelcode":modelcode}]})

        message = GETFILE_ALGORITHM_SUCCESS
        return message

    except Exception as e:
        result = rq.post(REST_SERVER_URL+'/rest/trn',json={'items': [{"contents":"error occured in downloading algorithm file"}],
                           'method_type': 'update',
                           'option': [{"modelcode":modelcode}]})
        message = GETFILE_ALGORITHM_SUCCESS_FAIL
        message.update({'cause': str(e)})
        print(message)
        return message
