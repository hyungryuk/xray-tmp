import json

from .conftest import post_json
from ..helper import messages
from .. import ALG_CTR_URL

def test_count_algorithm_success_with_options(client):
    test_dict = {'method_type': 'count',
                  'items': [],
                  'options': [{'version': '1.1'}]
                  }

    rv = post_json(client, ALG_CTR_URL, test_dict)
    resp = json.loads(rv.data)
    resp_dic = json.loads(resp)
    assert rv.status_code == 200
    assert resp_dic.get('return_code') == messages.ALG_COUNT_SUCCESS.get('return_code')
    assert len(resp_dic.get('items')) == 1
    assert resp_dic.get('items')[0].get('count') == 1


def test_view_algorithm_success_with_options(client):
    test_dict = {'method_type': 'view',
                  'items': [],
                  'options': [{'version': '1.1'}]
                  }

    rv = post_json(client, ALG_CTR_URL, test_dict)
    resp = json.loads(rv.data)
    resp_dic = json.loads(resp)
    assert rv.status_code == 200
    assert resp_dic.get('return_code') == messages.ALG_VIEW_SUCCESS.get('return_code')
    assert len(resp_dic.get('items')) == 1


def test_view_algorithm_success_with_options_pagination(client):
    test_dict = {'method_type': 'view',
                  'items': [],
                  'options': [{'version': '1.1'},{},
                              {}, {'page':'1', 'perpage': '1'}]
                  }

    rv = post_json(client, ALG_CTR_URL, test_dict)
    resp = json.loads(rv.data)
    resp_dic = json.loads(resp)
    assert rv.status_code == 200
    assert resp_dic.get('return_code') == messages.ALG_VIEW_SUCCESS.get('return_code')
    assert resp_dic.get('items')[0].get('algorithmname') == 'Keras_Cnn_mnist_Save'



def test_view_algorithm_success_with_options_with_date(client):
    test_dict = {'method_type': 'view',
                  'items': [],
                  'options': [{'version': '1.1'},{'startdate':'20180326130000', 'enddate':'20180326141000'}]
                 }
    rv = post_json(client, ALG_CTR_URL, test_dict)
    resp = json.loads(rv.data)
    resp_dic = json.loads(resp)
    assert rv.status_code == 200
    assert resp_dic.get('return_code') == messages.ALG_VIEW_SUCCESS.get('return_code')
    assert len(resp_dic.get('items')) == 0


def test_view_algorithm_success_with_options_with_date_and_orderby(client):
    test_dict = {'method_type': 'view',
                  'items': [],
                  'options': [{'version': '1.1'},{},
                              {'orderby': 'regdate'}]
                  }

    rv = post_json(client, ALG_CTR_URL, test_dict)
    resp = json.loads(rv.data)
    resp_dic = json.loads(resp)
    assert rv.status_code == 200
    assert resp_dic.get('return_code') == messages.ALG_VIEW_SUCCESS.get('return_code')
    assert len(resp_dic.get('items')) == 1


def test_view_all(client):
    test_dict = {'method_type': 'view',
                  'items': [],
                  'options': []
                  }

    rv = post_json(client, ALG_CTR_URL, test_dict)
    resp = json.loads(rv.data)
    resp_dic = json.loads(resp)
    assert rv.status_code == 200
    assert resp_dic.get('return_code') == messages.ALG_VIEW_SUCCESS.get('return_code')
    assert len(resp_dic.get('items')) == 1